#!/bin/sh

as-create-launch-config reppioLC --image-id ami-06f8686f --instance-type m1.medium
as-create-auto-scaling-group reppioAutoScalingGroup --launch-configuration reppioLC --availability-zones us-east-1b us-east-1c --min-size 1 --max-size 10 --load-balancers reppioLB

as-put-scaling-policy scaleUp --auto-scaling-group reppioAutoScalingGroup --adjustment=1 --type ChangeInCapacity --cooldown 300
mon-put-metric-alarm highCPUAlarm --comparison-operator GreaterThanThreshold --evaluation-periods 1 --metric-name CPUUtilization --namespace "AWS/EC2" --period 600 --statistic Average --threshold 80 --alarm-actions arn:aws:autoscaling:us-east-1:476681121099:scalingPolicy:d72801ee-c5fb-4f71-a084-a9e0932268cb:autoScalingGroupName/reppioAutoScalingGroup:policyName/scaleUp --dimensions "AutoScalingGroupName=reppioAutoScalingGroup"


as-put-scaling-policy scaleDown --auto-scaling-group reppioAutoScalingGroup --adjustment=-1 --type ChangeInCapacity --cooldown 300
mon-put-metric-alarm lowCPUAlarm --comparison-operator LessThanThreshold --evaluation-periods 1 --metric-name CPUUtilization --namespace "AWS/EC2" --period 600 --statistic Average --threshold 40 --alarm-actions arn:aws:autoscaling:us-east-1:476681121099:scalingPolicy:a367ab98-5df3-416e-bcff-e58ee24a7372:autoScalingGroupName/reppioAutoScalingGroup:policyName/scaleDown --dimensions "AutoScalingGroupName=reppioAutoScalingGroup"


as-describe-auto-scaling-groups reppioAutoScalingGroup --headers