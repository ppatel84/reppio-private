def move_emails_to_email_table
  User.all.each do |user|
    user.email_addresses.create(:email => user.email, :email_confirmation_code => user.email_confirmation_code, :email_confirmed => user.email_confirmed,
                                :is_primary => true)
  end
end

def put_existing_postings_into_omnigroup
  # about omnigroup
  #  ID=1
  # no user can join it, but postings default to it, as does search.

  group=Group.create(:long_name => "Everyone on Reppio", :short_name => "everyoneonreppio",
                     :description => "List your item on Reppio TODAY!!!!", :external_url => "www.reppio.com",
                     :may_join_without_admin_approval => 0, :owner_id => 1, :zip_code => "60613")
  User.all.each do |user|
    user.postings.each do |posting|
      group.postings<<posting
    end
  end
  group.save
end

def update_first_picture_urls
  Posting.find_each do |posting|
    tmp = posting.pictures[posting.picture_display_order.split(",").first.to_i]
    if tmp.nil?
      next
    end
    posting.update_attribute(:first_picture_url, tmp.photo.url(:large_thumb))
  end
end

def update_first_user_urls
  Posting.find_each(:batch_size => 1000) do |posting|
    user = User.find(posting.user_id) rescue false
    if (user)
      tmp = user.pictures[user.picture_display_order.split(",").first.to_i]
      if tmp.nil?
        next
      end
      posting.update_attribute(:first_user_url, tmp.photo.url(:thumb))
    end
  end
end

def update_all_repscores

  # determine how long the repscore history should be in days
  Userdata.find_each(:batch_size => 1000) do |userdata|

    repScoreTrueLength = ((Time.now - userdata.created_at)/60/60/24).to_i + 1
    repScoreCurrentLength = userdata.repscore_history.split(",").length

    # add the current repscore repScoreTrueLength - repScoreCurrentLength times
    repScoreHistory = userdata.repscore_history
    repScore = userdata.repscore
    diff = repScoreTrueLength - repScoreCurrentLength
    (1..diff).each do
      repScoreHistory = repScoreHistory + "," + repScore.to_s
    end
    userdata.update_attribute(:repscore_history, repScoreHistory)
  end
end

def delistPostingOneOff(user_id, posting_id)
  posting= User.find(user_id).postings.find(posting_id)
  posting.deleted=true
  posting.delisted_reason = "dontWantToSell"
  posting.flag_for_deletion = false
  posting.delist_notify_action = false
  posting.save
end


# usage: changeUpdatedAtForPostings(7.days, 7.days)
# do this later
#
#def time_rand from = 0.0, to = Time.now
#  Time.at(from + rand * (to.to_f - from.to_f))
#end
#
#def changeUpdatedAtForPostings
#
#  postingsToChange = Posting.where("created_at < ?", Time.now - 4.days)
#
#  postingsToChange.each { |postingToChange|
#    randDate = time_rand Time.local(2012, 12, 4), Time.local(2012, 11, 29)
#    postingToChange.update_column(:updated_at, randDate)
#  }
#
#end

def migratePostingsToNewCategories
  oldToNew={
      "Tablet" => "Tablets",
      "Sports & Outdoor" => "Sports & Outdoor Gear",
      "Laptop" => "Laptops",
      "Automobile" => "Cars & Trucks",
      "Business" => "Office & Industrial",
      "Watches" => "Jewelry",
      "MP3 Players" => "Sound & Audio Devices",
      "Artwork" => "Arts & Crafts",
      "Computer Parts" => "Components & Parts",
      "Digital Camera & Video" => "Cameras & Videos",
      "Dvd, Blu-ray & Projector" => "CDs, DVD & VHS",
      "Events" => "Tickets & Events",
      "Home and Tools" => "Household",
      "Laptop" => "Laptops",
      "Luggage" => "Travel",
      "Necklaces, Earrings & Bracelets" => "Jewelry",
      "Speakers & Audio" => "Sound & Audio Devices",
      "Tablet" => "Tablets",
      "Wallets" => "Fashion Accessories",
      "Watches" => "Jewelry"
  }

  oldToNew.each_pair { |old_name, new_name|

    postingsToChange = Posting.where("category_name=?", old_name)

    postingsToChange.each { |postingToChange|
      postingToChange.update_column(:category_name, new_name)
    }

  }
end

def cacheFirstUserPicture
  User.find_each(:batch_size => 1000) do |user|
    tmp_url = user.pictures[user.picture_display_order.split(',').first.to_i].photo.url(:thumb) rescue nil
    user.update_attribute(:first_user_url, tmp_url) if tmp_url
  end
end

def updateEachPostingConversationCount
  Posting.find_each(:batch_size => 1000) do |posting|
    posting.update_attribute(:conversation_counts, posting.conversations.count)
  end
end

def updateUserRepscoreColumn
  User.find_each(:batch_size => 1000) do |user|
    rep = Userdata.find_by_user_id(user.id).repscore rescue nil
    if rep.nil?
      next
    end
    user.update_attribute(:repscore, Userdata.find_by_user_id(user.id).repscore)
  end
end


def updateSocialNetworksColumns
  Authorization.find_each(:batch_size => 1000) do |auth|
    case auth.provider
      when "facebook"
        User.find(auth.user_id).update_attribute(:facebook_id, auth.uid)
      when "twitter"
        Userdata.find_by_user_id(auth.user_id).update_attribute(:linked_twitter, true)
      when "linkedin"
        Userdata.find_by_user_id(auth.user_id).update_attribute(:linked_linkedin, true)
    end
  end
end

def groupMembersCount
  Group.all.each do |group|
    group.update_attribute(:members_count, group.users.count)
  end
end

def generateRepItTokens
  User.find_each(:batch_size => 1000) do |user|
    if user.rep_it_token.blank?
      begin
        token = SecureRandom.urlsafe_base64
      end while User.where(:rep_it_token => token).exists?
      user.update_attribute(:rep_it_token, token)
    end
  end

end


def markConversationsAsFulfilled
  Posting.find_each(:batch_size => 1000) do |posting|
    posting.conversations.each do |conversation|
      if posting.deleted
        conversation.update_attribute(:posting_was_delisted, true)
      end
    end
  end
end


def movePostingsInReppioGroupToOtherGroups
  Group.find(1).postings do |posting|
    user=User.find(posting.user_id)
    user.groups.each do |group|
      group.postings <<= posting
      group.save
    end
  end
end

def changeBooksAndTextbooks
  Posting.find_all_by_category_name("Textbooks").each do |posting|
    posting.update_column(:category_name, "Books & Textbooks")
  end
  Posting.find_all_by_category_name("Books").each do |posting|
    posting.update_column(:category_name, "Books & Textbooks")
  end
  UpdatedPosting.find_all_by_subcategory_name("Textbooks").each do |posting|
    posting.update_column(:subcategory_name, "Books & Textbooks")
  end
  UpdatedPosting.find_all_by_subcategory_name("Books").each do |posting|
    posting.update_column(:subcategory_name, "Books & Textbooks")
  end
end

def changeBooksAndTextbooksToBooks
  Posting.find_all_by_category_name("Books & Textbooks").each do |posting|
    posting.update_column(:category_name, "Books")
  end
  UpdatedPosting.find_all_by_subcategory_name("Books & Textbooks").each do |posting|
    posting.update_column(:subcategory_name, "Books")
  end
end
