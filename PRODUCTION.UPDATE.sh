# SIMPLE SHELL SCRIPT TO DEPLOY TO PRODUCTION
# TODO: ROBUSTIFY THIS SCRIPT
# 1) DEPLOY TO OFFLINE SERVERS
# 2) TURN OFFLINE SERVERS ON
# 3) ONCE TURNED ON, TURN OFF ONLINE SERVERS
# 4) DEPLOY TO OFFLINE SERVERS AGAIN

## R STUFF ()
## These scripts create the phrase frequency table in the
## production database
## Written for Ubuntu 12.04 Precise
#sudo apt-get install libdbd-mysql libmysqlclient-dev
#algo/production.install_packages.R
#algo/production.category_algorithm.R

# PLAY THE DEPLOY SONG!!!
source ~/.bash_profile
#fplay -g Saxoman

# ONLY NEED TO CALL RESETDB ONCE SINCE SAME DATABASE

# DEPLOY TO MEDIUM INSTANCE:
cap web_medium deploy:serverkill
cap web_medium deploy:update
cap web_medium deploy:bundleinstall
cap web_medium deploy:resetdb
cap web_medium deploy:cleanup
cap web_medium deploy:updatecron
cap web_medium deploy:serverstart
cap web_medium deploy:sphinxrebuild

### DEFUNCT SEARCH INSTANCE #################################
#
# 1) Pull newest commit from Git
# 2) Cleanup older deploys
# 3) Bundle install
# 4) Update cron tasks
# 5) Start farming Craigslist
#cap search deploy:update
#cap search deploy:cleanup
#cap search deploy:bundleinstall
#cap search deploy:updatecron
#cap search deploy:farmcraigslist

sudo killall -m Quicktime Player

