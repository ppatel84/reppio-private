require 'spec_helper'

describe "services/edit" do
  before(:each) do
    @service = assign(:service, stub_model(Service,
      :category => "MyString",
      :specialty => "MyString",
      :location => "MyString",
      :availability => "MyString",
      :posting_subject => "MyString",
      :about_me => "MyString",
      :qualifications => "MyString",
      :links => "MyString",
      :faq => "MyString"
    ))
  end

  it "renders the edit service form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => services_path(@service), :method => "post" do
      assert_select "input#service_category", :name => "service[category]"
      assert_select "input#service_specialty", :name => "service[specialty]"
      assert_select "input#service_location", :name => "service[location]"
      assert_select "input#service_availability", :name => "service[availability]"
      assert_select "input#service_posting_subject", :name => "service[posting_subject]"
      assert_select "input#service_about_me", :name => "service[about_me]"
      assert_select "input#service_qualifications", :name => "service[qualifications]"
      assert_select "input#service_links", :name => "service[links]"
      assert_select "input#service_faq", :name => "service[faq]"
    end
  end
end
