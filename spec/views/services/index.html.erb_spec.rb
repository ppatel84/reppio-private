require 'spec_helper'

describe "services/index" do
  before(:each) do
    assign(:services, [
      stub_model(Service,
        :category => "Category",
        :specialty => "Specialty",
        :location => "Location",
        :availability => "Availability",
        :posting_subject => "Posting Subject",
        :about_me => "About Me",
        :qualifications => "Qualifications",
        :links => "Links",
        :faq => "Faq"
      ),
      stub_model(Service,
        :category => "Category",
        :specialty => "Specialty",
        :location => "Location",
        :availability => "Availability",
        :posting_subject => "Posting Subject",
        :about_me => "About Me",
        :qualifications => "Qualifications",
        :links => "Links",
        :faq => "Faq"
      )
    ])
  end

  it "renders a list of services" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Category".to_s, :count => 2
    assert_select "tr>td", :text => "Specialty".to_s, :count => 2
    assert_select "tr>td", :text => "Location".to_s, :count => 2
    assert_select "tr>td", :text => "Availability".to_s, :count => 2
    assert_select "tr>td", :text => "Posting Subject".to_s, :count => 2
    assert_select "tr>td", :text => "About Me".to_s, :count => 2
    assert_select "tr>td", :text => "Qualifications".to_s, :count => 2
    assert_select "tr>td", :text => "Links".to_s, :count => 2
    assert_select "tr>td", :text => "Faq".to_s, :count => 2
  end
end
