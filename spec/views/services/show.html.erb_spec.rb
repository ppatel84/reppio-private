require 'spec_helper'

describe "services/show" do
  before(:each) do
    @service = assign(:service, stub_model(Service,
      :category => "Category",
      :specialty => "Specialty",
      :location => "Location",
      :availability => "Availability",
      :posting_subject => "Posting Subject",
      :about_me => "About Me",
      :qualifications => "Qualifications",
      :links => "Links",
      :faq => "Faq"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Category/)
    rendered.should match(/Specialty/)
    rendered.should match(/Location/)
    rendered.should match(/Availability/)
    rendered.should match(/Posting Subject/)
    rendered.should match(/About Me/)
    rendered.should match(/Qualifications/)
    rendered.should match(/Links/)
    rendered.should match(/Faq/)
  end
end
