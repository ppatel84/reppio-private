require 'spec_helper'

describe "users/show" do
  before(:each) do
    @user = assign(:user, stub_model(User,
      :password_hash => "Password Hash",
      :email => "Email",
      :number_transactions => 1,
      :number_reviews => 2,
      :number_services => 3,
      :first_name => "First Name",
      :last_name => "Last Name",
      :home_address => "Home Address",
      :phone_number => "Phone Number",
      :about_me => "About Me",
      :education => "Education",
      :employer => "Employer",
      :profile_picture_location => "Profile Picture Location"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Password Hash/)
    rendered.should match(/Email/)
    rendered.should match(/1/)
    rendered.should match(/2/)
    rendered.should match(/3/)
    rendered.should match(/First Name/)
    rendered.should match(/Last Name/)
    rendered.should match(/Home Address/)
    rendered.should match(/Phone Number/)
    rendered.should match(/About Me/)
    rendered.should match(/Education/)
    rendered.should match(/Employer/)
    rendered.should match(/Profile Picture Location/)
  end
end
