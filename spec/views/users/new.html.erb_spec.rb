require 'spec_helper'

describe "users/new" do
  before(:each) do
    assign(:user, stub_model(User,
      :password_hash => "MyString",
      :email => "MyString",
      :number_transcations => 1,
      :number_reviews => 1,
      :number_services => 1,
      :first_name => "MyString",
      :last_name => "MyString",
      :home_address => "MyString",
      :phone_number => "MyString",
      :about_me => "MyString",
      :education => "MyString",
      :employer => "MyString",
      :profile_picture_location => "MyString"
    ).as_new_record)
  end

  it "renders new user form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => users_path, :method => "post" do
      assert_select "input#user_password_hash", :name => "user[password_hash]"
      assert_select "input#user_email", :name => "user[email]"
      assert_select "input#user_number_transcations", :name => "user[number_transcations]"
      assert_select "input#user_number_reviews", :name => "user[number_reviews]"
      assert_select "input#user_number_services", :name => "user[number_services]"
      assert_select "input#user_first_name", :name => "user[first_name]"
      assert_select "input#user_last_name", :name => "user[last_name]"
      assert_select "input#user_home_address", :name => "user[home_address]"
      assert_select "input#user_phone_number", :name => "user[phone_number]"
      assert_select "input#user_about_me", :name => "user[about_me]"
      assert_select "input#user_education", :name => "user[education]"
      assert_select "input#user_employer", :name => "user[employer]"
      assert_select "input#user_profile_picture_location", :name => "user[profile_picture_location]"
    end
  end
end
