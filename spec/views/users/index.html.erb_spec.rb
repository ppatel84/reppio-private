require 'spec_helper'

describe "users/index" do
  before(:each) do
    assign(:users, [
      stub_model(User,
        :password_hash => "Password Hash",
        :email => "Email",
        :number_transcations => 1,
        :number_reviews => 2,
        :number_services => 3,
        :first_name => "First Name",
        :last_name => "Last Name",
        :home_address => "Home Address",
        :phone_number => "Phone Number",
        :about_me => "About Me",
        :education => "Education",
        :employer => "Employer",
        :profile_picture_location => "Profile Picture Location"
      ),
      stub_model(User,
        :password_hash => "Password Hash",
        :email => "Email",
        :number_transcations => 1,
        :number_reviews => 2,
        :number_services => 3,
        :first_name => "First Name",
        :last_name => "Last Name",
        :home_address => "Home Address",
        :phone_number => "Phone Number",
        :about_me => "About Me",
        :education => "Education",
        :employer => "Employer",
        :profile_picture_location => "Profile Picture Location"
      )
    ])
  end

  it "renders a list of users" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Password Hash".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "First Name".to_s, :count => 2
    assert_select "tr>td", :text => "Last Name".to_s, :count => 2
    assert_select "tr>td", :text => "Home Address".to_s, :count => 2
    assert_select "tr>td", :text => "Phone Number".to_s, :count => 2
    assert_select "tr>td", :text => "About Me".to_s, :count => 2
    assert_select "tr>td", :text => "Education".to_s, :count => 2
    assert_select "tr>td", :text => "Employer".to_s, :count => 2
    assert_select "tr>td", :text => "Profile Picture Location".to_s, :count => 2
  end
end
