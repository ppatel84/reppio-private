require 'spec_helper'

describe "postings/index" do
  before(:each) do
    assign(:postings, [
      stub_model(Posting,
        :post_id => 1,
        :seller_username => "Seller Username",
        :category => "Category",
        :fulf => false,
        :desc => "Desc",
        :price => 1.5,
        :zip_code => 2,
        :posting_picture_location => "Posting Picture Location"
      ),
      stub_model(Posting,
        :post_id => 1,
        :seller_username => "Seller Username",
        :category => "Category",
        :fulf => false,
        :desc => "Desc",
        :price => 1.5,
        :zip_code => 2,
        :posting_picture_location => "Posting Picture Location"
      )
    ])
  end

  it "renders a list of postings" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Seller Username".to_s, :count => 2
    assert_select "tr>td", :text => "Category".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "Desc".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Posting Picture Location".to_s, :count => 2
  end
end
