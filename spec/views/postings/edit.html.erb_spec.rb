require 'spec_helper'

describe "postings/edit" do
  before(:each) do
    @posting = assign(:posting, stub_model(Posting,
      :post_id => 1,
      :seller_username => "MyString",
      :category => "MyString",
      :fulf => false,
      :desc => "MyString",
      :price => 1.5,
      :zip_code => 1,
      :posting_picture_location => "MyString"
    ))
  end

  it "renders the edit posting form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => postings_path(@posting), :method => "post" do
      assert_select "input#posting_post_id", :name => "posting[post_id]"
      assert_select "input#posting_seller_username", :name => "posting[seller_username]"
      assert_select "input#posting_category", :name => "posting[category]"
      assert_select "input#posting_fulf", :name => "posting[fulf]"
      assert_select "input#posting_desc", :name => "posting[desc]"
      assert_select "input#posting_price", :name => "posting[price]"
      assert_select "input#posting_zip_code", :name => "posting[zip_code]"
      assert_select "input#posting_posting_picture_location", :name => "posting[posting_picture_location]"
    end
  end
end
