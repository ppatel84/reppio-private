require 'spec_helper'

describe "postings/show" do
  before(:each) do
    @posting = assign(:posting, stub_model(Posting,
      :post_id => 1,
      :seller_username => "Seller Username",
      :category => "Category",
      :fulf => false,
      :desc => "Desc",
      :price => 1.5,
      :zip_code => 2,
      :posting_picture_location => "Posting Picture Location"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1/)
    rendered.should match(/Seller Username/)
    rendered.should match(/Category/)
    rendered.should match(/false/)
    rendered.should match(/Desc/)
    rendered.should match(/1.5/)
    rendered.should match(/2/)
    rendered.should match(/Posting Picture Location/)
  end
end
