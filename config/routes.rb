Reppio::Application.routes.draw do
  resources :groups
  controller :groups do
    get "join" => :join_group, :as => :join_group
    get "request_join" => :request_join_group, :as => :request_join_group
    get "leave" => :leave_group, :as => :leave_group
    get "remove_from_group" => :remove_from_group, :as => :remove_from_group
    get "handle_request" => :handle_request_join_group, :as => :handle_request
    get "/groups/:id/invite_to_group" => :invite_to_group, :as => :invite_to_group
    post "/groups/send_email_to_invite_to_group" => :send_email_to_invite_to_group
    post "invite_facebook_connections_to_group" => :invite_facebook_connections_to_group
    get "notifications" => :notifications, :as => :groups_notifications
    get "join_autogroup_modal" => :join_autogroup_modal, :as => :join_autogroup_modal
  end

  controller :payments do
    match "submit_deposit" => :submit_deposit, :as => :submit_deposit
    match "buy" => :buy, :as => :buy
    match "do_deposit" => :do_express_checkout_payment_for_deposit, :as => :do_express_checkout_payment_for_deposit
    match "payments/ipn_notification/:id/" => :ipn_notification, :as => :ipn_notification
    get "payments/canceled_payment_request" => :canceled_payment_request, :as => :canceled_payment
    get "payments/completed_payment_request/:id/" => :completed_payment_request, :as => :completed_payment
    get "payments/failed_payment" => :failed_payment, :as => :failed_payment
    post "request_payment" => :request_payment, :as => :request_payment
    match "cancel_request_payment" => :cancel_request_payment, :as => :cancel_request_payment
  end

  controller :manage do
    get "manage/selling" => :selling, :as => :manage_selling
    get "manage/buying" => :buying, :as => :manage_buying
    get "manage/completed_buying" => :completed_buying, :as => :manage_completed_buying
    get "manage/completed_selling" => :completed_selling, :as => :manage_completed_selling
    get "manage/drafts_selling" => :drafts_selling, :as => :manage_drafts_selling

    get "manage/selling/:id" => :selling_posting_conversations, :as => :selling_posting_conversations
    get "manage/buying/:id" => :buying_posting_conversation, :as => :buying_posting_conversation
    get "manage/completed_selling/:id" => :completed_selling_posting_conversations, :as => :completed_selling_posting_conversations
    get "manage/completed_buying/:id" => :completed_buying_posting_conversation, :as => :completed_buying_posting_conversation
    get "manage/drafts_selling/:id" => :drafts_selling_redirect

    get "manage/selling/:id/:buyer_id" => :selling_posting_conversation, :as => :selling_posting_conversation
    get "manage/completed_selling/:id/:buyer_id" => :completed_selling_posting_conversation, :as => :completed_selling_posting_conversation

    match "manage" => redirect("/manage/selling"), :as => :manage

    get "repscore" => :repscore, :as => :repscore

    get "manage/reviews" => :reviews

    match "manage/add_student_email" => :add_student_email
    match "manage/rep_center_references" => :rep_center_references, :as => :rep_center_references
    match "manage/rep_center_referrals" => :rep_center_referrals, :as => :rep_center_referrals
    match "rep_center" => redirect("/manage/reviews"), :as => :rep_center
    match "verifications" => :verifications, :as => :verifications
    match "reputation_explained" => :reputation_explained, :as => :reputation_explained

    get "manage/interests" => :interests, :as => :interests
    match "toggle_interest" => :toggle_interest, :as => :toggle_interest

    get "conversations" => :redirect_to_conversation
    get "conversations/:id" => :redirect_to_conversation

  end

  controller :watchlist do
    match "watchlist/bookmarks" => :bookmarks
    match "watchlist/alert" => :alert, :as => :search_alert
    match "watchlist" => redirect("/watchlist/bookmarks"), :as => :watchlist
  end

  controller :bookmarks do
    post "bookmarks" => :toggle
    match "bookmarks/:posting_id" => :toggle
  end


  resources :alerts, :only => [:create, :edit, :destroy, :update]

  controller :static do
    match "about" => :about
    match "jobs" => :jobs
    match "faq" => :faq
    match "terms" => :terms
    match "privacy" => :privacy
    match "team" => :team
    match "press" => :press
    match "app" => :app
    get "contact" => :contact
    post "contact" => :contactSend
  end


  resources :references
  controller :references do
    match "contacts/:importer/callback" => :invite
    match "contacts/failure" => :failure
    post "references/send_invites_list" => :send_invites_list
    post "references/send_references_given_email_list" => :send_references_given_email_list
    post "accept_or_reject_references" => :accept_or_reject_references, :as => :accept_or_reject_references

  end

  post 'flag', :to => 'flags#create', :as => :flag

  controller :reviews do
    get "reviews" => :index, :as => :my_reviews
    post "reviews/vote" => :vote, :as => :review_vote
    post "reviews" => :create
    match "reviews/:id" => :respond, :as => :respond_to_review
  end

  controller :browse do
    match "search" => :show, :as => :search
    match "search_json" => :autocomplete
    match "load_mini_map", :load_mini_map, :as => :load_mini_map
  end

  controller :sessions do
    get "login" => :new
    post "login" => :create
    delete "login" => :destroy
    get "jackterminal" => :jack_signin
    post "jackterminal" => :jack_create
    get "interstitial" => :interstitial, :as => :interstitial
    match "interstitial/*redirect" => :interstitial, :as => :interstitial
    match "logout" => :destroy, :as => :logout
    match 'auth/facebook/callback' => :create_auth
    match 'auth/twitter/callback' => :twitter
    match 'auth/linkedin/callback' => :linkedin
    match 'auth/failure' => :failure
  end

  controller :mobile do
    match "mobile/posting/categorize2" => :mobile_posting_categorize2
    match "mobile/posting/create2" => :mobile_posting_create2
    match "mobile/toggle/push" => :mobile_toggle_push
    match "mobile/group/info" => :group_info

    get "mobile/app/version" => :mobile_app_version
    match "mobile/auth/:provider/callback" => :mobile_create_auth
    match "mobile/login" => :mobile_create
    match "mobile/profile/update" => :mobile_profile_update
    match "mobile/profile/get" => :mobile_profile_get
    match "mobile/posting/categorize" => :mobile_posting_categorize
    match "mobile/posting/create" => :mobile_posting_create
    match "mobile/posting/get" => :mobile_posting_get
    match "mobile/posting/update" => :mobile_posting_update
    match "mobile/fields/get" => :mobile_fields_get
    match "mobile/fields/get2" => :mobile_fields_get2
    match "mobile/manager/get" => :mobile_manager_get
    match "mobile/messages/get" => :mobile_messages_get
    match "mobile/conversation/create" => :mobile_conversation_create
    match "mobile/conversations/get" => :mobile_conversations_get
    #match "mobile/manager/buying"=>:mobile_manager_buying
    #match "mobile/manager/selling"=>:mobile_manager_selling
    match "mobile/conversation/update" => :mobile_conversation_update
    match "mobile/email/signup" => :mobile_email_signup
    match "mobile/categories/all" => :mobile_categories_all
    match "mobile/alert/create" => :mobile_alert_create
    match "mobile/alert/remove" => :mobile_alert_remove
	match "mobile/alert/edit" => :mobile_alert_edit
	
    match "mobile/transaction/complete" => :mobile_transaction_complete
    match "mobile/transaction/cc" => :mobile_transaction_cc

    match "mobile/push/setup" => :mobile_push_setup
    match "mobile/push/setup/android" => :mobile_push_setup_android
    #TODO remove after testing
    match "mobile/push/send/android" => :mobile_push_send_android
    match "mobile/push/conversation/get" => :mobile_push_conversation_get

    match "mobile/posting/agree/sell" => :mobile_posting_agree_sell
    match "mobile/posting/cancel/sell" => :mobile_posting_cancel_sell

    match "mobile/payments/transact/cc" => :mobile_payments_transact_cc
    match "mobile/payments/set/seller" => :mobile_payments_set_seller
    match "mobile/payments/set/buyer" => :mobile_payments_set_buyer
    match "mobile/payments/get/cards" => :mobile_payments_get_cards
    match "mobile/payments/get/card" => :mobile_payments_get_card
    match "mobile/payments/update/card" => :mobile_payments_update_card
    match "mobile/payments/invalidate/card" => :mobile_payments_invalidate_card

    match "mobile/payments/get/bankAccounts" => :mobile_payments_get_bankAccounts
    match "mobile/payments/get/bankAccount" => :mobile_payments_get_bankAccount
    match "mobile/payments/delete/bankAccount" => :mobile_payments_delete_bankAccount

    match "mobile/browse" => :browse
    match "mobile/search" => :search
    match "mobile/listing_details_view" => :listing_details_view

    match "mobile/return_groups" => :return_groups

    match "mobile/toggle_bookmark" => :toggle_bookmark
    match "mobile/return_bookmarks" => :return_bookmarks


    match "mobile/join_group" => :join_group
    match "mobile/leave_group" => :leave_group

    match "mobile/onboarding_add_interests" => :onboarding_add_interests
    match "mobile/onboarding_add_school_email" => :onboarding_add_school_email


    match "mobile/reset_password" => :reset_password
	
	match "mobile/get_alerts"=>:get_alert_labels
	match "mobile/get_alert_postings"=>:get_alert_postings
	
	match "mobile/queue_cg"=>:queue_craigslist_post
  end


  controller :upload do
    get ":user_name/upload" => :catchGet #Not sure why; simply setting a form :url => handle_upload_url (as in fileupload/_main) will cause a get call to said URL. Catch it and return nothing to avoid an annoying (but harmless?) routing error.
    post ":user_name/upload" => :create, :as => :handle_upload
    delete ":user_name/postings/upload_destroy/:id" => :destroy, :as => :upload_destroy
  end


  scope ":user_name", :as => "user" do
    match "postings/:id/conversations" => redirect("/conversations/postings/%{id}")
    post "postings/new" => "postings#create"
    get "postings" => redirect("/%{user_name}/")
    resources :postings
  end

  controller :postings do
    match ":user_name/postings/:id/delist_posting_modal" => :load_delist_modal, :as => :load_delist_modal
    match ":user_name/postings/:id/promote_posting_modal" => :load_promote_modal, :as => :load_promote_modal
    match ":user_name/postings/:id/load_email_share_modal" => :load_email_share_modal, :as => :load_email_share_modal
    match ":user_name/postings/:id/delist_posting" => :delist_posting, :as => :delist_posting
    match ":user_name/postings/:id/relist_posting" => :relistPosting, :as => :relist_posting
    match ":user_name/postings/:id/relist_posting_copy" => :relistPostingCopy, :as => :relist_posting_copy
    match ":user_name/postings/:id/postToCraigslist" => :postToCraigslist, :as => :post_to_craigslist
    match ":user_name/postings/:id/posting_to_craigslist" => :craigslist_loading, :as => :craigslist_loading
    post ":user_name/postings/:id/craigslist_ajax" => :craigslist_ajax, :as => :craigslist_ajax
    post ":user_name/postings/:id/promote_posting_by_email" => :promote_posting_by_email, :as => :promote_posting_by_email
    match "postings/:id/remove_completed_posting" => :remove_completed_posting, :as => :remove_completed_posting
    match "get_custom_fields" => :get_custom_fields
    post 'categorize_posting' => :categorize_posting
    post 'new_posting_form_for_category' => :new_posting_form_for_category
    match 'form_data_for_bookmark' => :form_data_for_bookmark, :as => :form_data_for_bookmark
    get "all" => :all_postings
    match "post_from_bookmark" => :post_from_bookmark, :as => :post_from_bookmark
    match "promote_to_facebook_from_modal" => :promote_to_facebook_from_modal, :as => :promote_to_facebook_from_modal
    get "reppio_picks" => :reppio_picks_index
    match "toggle_reppio_pick" => :toggle_reppio_pick, :as => :toggle_reppio_pick
    get "remove_listings" => :remove_listings
    match "remove_listing" => :remove_listing, :as => :remove_listing
    match 'data_feed.:format' => :google_feed
  end

  resources :conversations, :only => [:create, :update]
  controller :conversations do
    match "conversations/:id/remove" => :remove_conversation, :as => :remove_conversation
    match "message_from_new_user" => :message_from_new_user, :as => :message_from_new_user
    match "create_conversation_with_offer" => :create_with_offer, :as => :create_conversation_with_offer
    match "load_message_modal/:id" => :load_message_modal, :as => :load_message_modal
  end

  controller :users do
    put "add_autogroup_email" => :add_autogroup_email, :as => :add_autogroup_email
    get "vendors" => :rep_it, :as => :rep_it
    get "repit" => :rep_it, :as => :rep_it
    get "rep-it" => :rep_it, :as => :rep_it
    get "rep_it" => :rep_it, :as => :rep_it
    get "rep_it_bookmark" => :rep_it_bookmark, :as => :rep_it_bookmark
    get "settings" => :settings, :as => :settings
    put "settings" => :update
    get "signup" => :new
    get "email_sign_up" => :email_sign_up
    post "email_sign_up" => :create
    get "create_user_name" => :create_user_name, :as => :create_user_name
    post "verify_phone_text" => :send_cell_phone_verification_text
    post "verify_phone" => :verify_cell_phone_number
    put ":user_name" => :update
    post "deactivate" => :deactivate
    match 'confirm_secondary_email' => :confirm_secondary_email, :as => :confirm_secondary_email
    match 'send_confirmation_secondary_email' => :send_confirmation_secondary_email, :as => :send_confirmation_secondary_email
    match 'cancel_secondary_email_confirmation' => :cancel_secondary_email_confirmation, :as => :cancel_secondary_email_confirmation
    post 'reset_password' => :reset_password, :as => :reset_password
    match 'send_confirmation_email' => :send_confirmation_email, :as => :send_confirmation_email
    match 'confirm' => :confirm, :as => :confirm
    match ":user_name/edit" => :edit, :as => :edit_user
    match ":user_name/reviews" => :user_reviews, :as => :user_reviews
    get ":user_name" => :show, :as => :user
  end

  root :to => "static#landing"

end
