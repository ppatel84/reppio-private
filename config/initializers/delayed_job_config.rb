Delayed::Worker.destroy_failed_jobs = false
Delayed::Worker.sleep_delay = 180
Delayed::Worker.max_attempts = 3
Delayed::Worker.max_run_time = 3.minutes
