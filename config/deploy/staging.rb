set :user, "bitnami"
set :runner, "bitnami"
ssh_options[:keys] = [File.join(ENV["HOME"], ".ec2", "jack.pem")]
ssh_options[:forward_agent] = true
set :branch, "master"
set :use_sudo, true
set :domain, "ec2-50-19-97-43.compute-1.amazonaws.com"
#server '23.21.180.134', :app, :web, :primary => true