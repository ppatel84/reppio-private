# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# search instance runs on UTC which is 7 hours ahead of CST
# all cron jobs are supposed to run in the early morning

# ensure path ends up in crontab
env :PATH, ENV['PATH']

# declare log file and job types
set :output, "~/cron.log"
job_type :runner_production, "cd :path && rails runner -e production ':task' :output"
job_type :runner_search, "cd :path && rails runner -e search ':task' :output"
job_type :runner_development, "cd :path && rails runner -e development ':task' :output"

job_type :rake_development, "cd :path && rake :task :output"
job_type :rake_search, "cd :path && RAILS_ENV=search :task --silent :output"

job_type :rails_command, "cd :path && :task :output"

# define instances to deploy cron tasks on
SEARCH_INSTANCE = "ip-10-245-86-193"
MEDIUM_INSTANCE = "ip-10-40-215-161"
LOCAL_INSTANCE = "jack"
socket_name = `hostname`.strip

# RUN ON SEARCH INSTANCE:
if (socket_name == SEARCH_INSTANCE)

  ## delete expired rows in Craigslist to ensure fresh data (IN SEARCH DB)
  #every 1.day, :at => '10:30am' do
  #  runner_search "FeedEntry.delete_expired_rows"
  #end
  #
  ## flag postings for deletion (IN PRODUCTION DB) and email corresponding sellers
  #every 1.day, :at => '10:45am' do
  #  runner "Posting.flag_postings_for_deletion"
  #end
  #
  ## update phrase_frequencies table for classification (IN SEARCH DB)
  #every 1.day, :at => '11:00am' do
  #  command "algo/production.category_algorithm.R"
  #end

end

# RUN ON WEB INSTANCES:
if (socket_name != SEARCH_INSTANCE && socket_name != LOCAL_INSTANCE)

  # fully rebuild Sphinx indices every night
  every 1.day, :at => '11:00am' do
    # replace this with actual indexer task to allow cache rotation
    rake "ts:index"
  end

end

# RUN ON HOME INSTANCE
# SHOULD HAVE THIS ON AMAZON...BUT THIS IS MORE COST EFFECTIVE
if (socket_name == LOCAL_INSTANCE)

  # DELETE EXPIRED CL POSTINGS AND RP POSTINGS, AND UPDATE PHRASES
  #every 1.day, :at => '2:30am' do
  #  runner_search "FeedEntry.delete_expired_rows"
  #end

  # Generate updated_postings table on local machine
  # Append those results to the production server
  # Delete the data in the feed_entry table
  every 1.week, :at => '2:30am' do
    command "algo/categorize_craigslist.R"
  end

  #every 1.week, :at => '3:00am' do
  #  command "algo/production.category_algorithm.R"
  #end
  
  every 1.day, :at => '2:00am' do
    runner_production "Posting.reset_view_count"
  end

  # Flag postings for deletion on the production server
  # We flag all postings that are older than 12 days
  # THIS ALSO EMAILS CORRESPONDING SELLERS FOR POSTINGS FLAGGED
  every 1.day, :at => '4:00am' do
    runner_production "Posting.flag_postings_for_deletion"
  end

  # Delete flagged postings once a week
  every 1.day, :at => '4:15am' do
    runner_production "Posting.delete_postings"
  end

  # Keep track of repscore history by adding a new value to the database (used to be an S3 write)
  every 1.day, :at => '4:30am' do
    runner_production "Userdata.push_repscore_history"
  end

end

# TEST ON LOCAL INSTANCE
#if (socket_name == LOCAL_INSTANCE)

  # fully rebuild Sphinx index and send me an email
#  every 2.minutes do
#    rake_development "ts:rebuild"
#    runner_development "Notifier.send_email_flagged_posting('jack.hs.chua@gmail.com','Jack', 'jackchua', 1, 'THE FEEDENTRY RUNNER WORKS!').deliver"
#  end

#end