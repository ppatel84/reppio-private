require 'capistrano/ext/multistage'

# CAPISTRANO CONFIG ###########################################

set :stages, %w(web_medium staging)
set :default_stage, "web_medium"
#set :default_environment, {
#    'PATH' => "/opt/bitnami/memcached/bin:/opt/bitnami/perl/bin:/opt/bitnami/git/bin:/opt/bitnami/nginx/sbin:/opt/bitnami/sphinx/bin:/opt/bitnami/sqlite/bin:/opt/bitnami/php/bin:/opt/bitnami/mysql/bin:/opt/bitnami/apache2/bin:/opt/bitnami/subversion/bin:/opt/bitnami/ruby/bin:/opt/bitnami/common/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/opt/bitnami/rvm/bin:$PATH"
#}

set :application, "reppio"
#set :ruby, "/opt/bitnami/ruby/bin/ruby"
#set :rake, "/opt/bitnami/ruby/bin/rake"
set :rails_env, "production"
#set :stack_home, "/home/bitnami/stack"
set :migrate_target, :latest
#set :migrate_env, ""

set :scm, :git
set :repository, "git@github.com:reppio/reppio.git"
set :deploy_to, "/opt/bitnami/apps/reppio"
set :deploy_via, :remote_cache
set :deploy_env, 'production'

# since :domain is defined in another file we need to delay its
# assignment until they're loaded
set(:domain) { "#{domain}" }
role(:web) { domain }      # Your HTTP server, Apache/etc
role(:app) { domain }      # =>  This may be the same as your `Web` server
role(:db, :primary=>true) { domain } # db properties defined in database.yml
	 
# CUSTOM CAPISTRANO TASKS ###################################
#
# cap <instancename> deploy:update
# cap <instancename> deploy:bundleinstall
# cap <instancename> deploy:sphinxstart
# cap <instancename> deploy:serverstart
#
namespace :deploy do
  desc "Bundle install"
  task :bundleinstall do
    run "cd #{deploy_to}/current && bundle install --without development"
  end

  desc "Rebuild Sphinx index'"
  task :sphinxrebuild do
    # probably don't need killall since sphinx rotates cache

    #run "cd #{deploy_to}/current && rake ts:stop RAILS_ENV=production"
    #run "cd #{deploy_to}/current && rake ts:start RAILS_ENV=production"
    #run "sudo killall .searchd.bin"
    run "cd #{deploy_to}/current && rake ts:index RAILS_ENV=production"
    run "cd #{deploy_to}/current && rake ts:restart RAILS_ENV=production"
  end

  desc "Reindex and start thinking sphinx server"
  task :sphinxrestart do
    run "cd #{deploy_to}/current && rake ts:restart RAILS_ENV=production"
  end

  desc "Start apache and thin server daemons on port 80"
  task :serverstart do
#    run "sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 3000"
    run "cd #{stack_home} && sudo ./ctlscript.sh restart apache"
    run "cd #{deploy_to}/current && thin -s 12 -p 3000 -e production -C config/thin.yml config"
    run "cd #{deploy_to}/current && thin -C config/thin.yml start"
  end

  desc "Kill all ruby processes on an instance"
  task :serverkill do
    # THIS SUCKS, CHANGE IT EVENTUALLY
    run "sudo killall .ruby.bin"
    run "sudo killall .ruby.bin"
    run "cd #{deploy_to}/current/tmp/pids && rm thin.*"   #get rid of pid files
  end

  desc "Reset web database state and delete all information. Also delete S3 buckets"
  task :resetdb do
    #run "cd #{deploy_to}/current && rake db:drop RAILS_ENV=production"
    #run "cd #{deploy_to}/current && rake db:create RAILS_ENV=production"
    run "cd #{deploy_to}/current && rake db:migrate RAILS_ENV=production"
    #run "cd #{deploy_to}/current && rake db:delete_s3_buckets RAILS_ENV=production"
  end

  desc "Update crontab"
  task :updatecron do
    run "cd #{deploy_to}/current && whenever --update-crontab #{application}"
  end

  desc "Farm Craigslist"
  task :farmcraigslist do
    # detach rails runner from the console
    run "cd #{deploy_to}/current && rake db:farm RAILS_ENV=search"
  end
end

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do
#   task :start do ; end
#   task :stop do ; task
#   end :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end