cap staging deploy:serverkill
cap staging deploy:update
cap staging deploy:bundleinstall
cap staging deploy:resetdb
cap staging deploy:cleanup
cap staging deploy:updatecron
cap staging deploy:serverstart
cap staging deploy:sphinxrebuild