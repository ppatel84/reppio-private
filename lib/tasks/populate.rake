# TO USE, RUN RAKE DB:POPULATE

namespace :db do
	desc "Erase and fill database"
	task :populate => :environment do
		require 'populator'
		require 'faker'
    require 'bcrypt'

    rand_bool = [true, false]
		[User, Userdata, Posting, Conversation, Review, Bookmark].each(&:delete_all)

    id_count = 1
		User.populate 100 do |user|
			user.user_name = (Populator.words(1) + rand(1000).to_s)
			user.email = Faker::Internet.email
			user.first_name = Faker::Name.first_name
      user.password_salt = BCrypt::Engine.generate_salt
      user.password_hash = BCrypt::Engine.hash_secret("password", user.password_salt)
      user.last_name = Faker::Name.last_name
			user.street_address = "835 W Bradley Ave Apt 1F" #Faker::Address.street_address
			user.city = "Chicago" #Faker::Address.city
			user.state = "IL" #Faker::Address.us_state_abbr
			user.zip_code = "60613" #Faker::Address.zip_code
      user.picture_display_order = ""
      user.email_private = true
      user.street_address_private = true
      user.phone_number_private = true
      user.education_private = true
      user.employer_private = true
      user.receive_email_notifications = true

        tmp_userdata = Userdata.create
        tmp_userdata.user_id = id_count
      tmp_userdata.repscore = 0
      
      x = "0"
      rand(20).times do
        x = x << "," << rand(100).to_s
      end
      tmp_userdata.repscore_history = x
        tmp_userdata.save

      id_count += 1
    end

    Posting.populate 1000 do |posting|
      temp_user = User.first(:offset => rand(User.count))
      posting.user_id = temp_user
      posting.title = Faker::Company.catch_phrase
      posting.description = Faker::Company.bs
      posting.price = rand(500)+rand(100)/100
      posting.address = temp_user.street_address
      posting.condition = ["New","Used"].sample
      posting.availability = ""
      posting.category_name = Posting.category_fields.keys[rand(Posting.category_fields.keys.count)]
      posting.deleted = false
      posting.picture_display_order = ""
      posting.flag_for_deletion = false
      posting.reviewed_by_buyer = false
      posting.reviewed_by_seller = false
      posting.view_count = rand(50)
      posting.latitude=(560000.01+rand(30000))/1000000
      posting.longitude=(2044218.01+rand(30000))/1000000*(-1)
      posting.city="Hoboken"
      posting.state="NJ"    
      x = "0"
      rand(10).times do
        x = x << "," << rand(300).to_s
      end
      posting.view_history = x

    end

    Bookmark.populate 2000 do |bookmark|
      bookmark.user_id = User.first(:offset => rand(User.count))
      bookmark.posting_id = Posting.first(:offset => rand(Posting.count))
    end

    Conversation.populate 200 do |conversation|
      buyer_off = 0
      seller_off = 0
      while (buyer_off == seller_off)
        buyer_off = rand(User.count)
        seller_off = rand(User.count)
      end
      seller = User.first(:offset => seller_off)
      buyer = User.first(:offset => buyer_off)

      conversation.potential_buyer_id = buyer.id
      conversation.seller_id = seller.id
      conversation.product_category = ""
      conversation.posting_id = Posting.find_all_by_user_id(seller.id).sample.id
      conversation.unread_by_buyer = rand_bool.sample

    end
    
    Offer.populate 1000 do |offer|
      offer.posting_id = (convo = Conversation.find(rand(Conversation.count - 1)+1)).posting_id
      offer.user_id = convo.potential_buyer_id
      offer.tenderer_user_id =  [convo.potential_buyer_id, convo.seller_id].sample
      offer.offer = rand(1000)
      offer.has_been_accepted = false
    end
    
    
    Review.populate 200 do |review|
      
      buyer_off = 0
      seller_off = 0
      while (buyer_off == seller_off)
        buyer_off = rand(User.count)
        seller_off = rand(User.count)
      end
      seller = User.first(:offset => seller_off)
      buyer = User.first(:offset => buyer_off)
      
      
      review.posting_id = Posting.select("id").where("user_id = ?",seller.id).sample
      review.user_id = [seller.id, buyer.id].sample
      review.reviewer_user_id = (review.user_id == seller.id ? buyer.id : seller.id)
      review.item_as_described = (review.user_id == seller.id ? rand(5) + 1 : nil)
      review.overall_experience = (rand(5) + 1)
      review.communication = (rand(5) + 1)
      review.count_not_useful = rand(20)
      review.count_useful = rand(100)
      review.anonymous = false
      review.description = Faker::Lorem.sentence(rand(70))
      review.response = ["", Faker::Lorem.sentence(rand(30))].sample
      review.recommended = [true, false].sample
    end
    

      

    Message.populate 1000 do |message|
      message.conversation_id = Conversation.first(:offset => rand(Conversation.count))
      message.message_from_seller = rand_bool.sample
      message.message_body = Faker::Lorem.paragraph(rand(10))
    end

		#Category.populate 20 do |category|
		#	category.category_name = Populator.words(1..2)
		#end
		#Posting.populate 1000 do |posting|
		#	#get a random user and category by using offset from
		#	#first
		# 	t_user = User.first(:offset => rand(User.count))
		# 	t_cat = Category.first(:offset => rand(Category.count))
    #
		# 	posting.seller_user_name = t_user.user_name
		# 	posting.category_name = t_cat.category_name
		# 	posting.fulfilled = rand(2)==1    # random boolean
		# 	posting.price = 550
		# 	posting.zip_code = t_user.zip_code
		# 	posting.description = Populator.words(20..200)
		#end
    #
		#Review.populate 100 do |review|
		# 	t_user = User.first(:offset => rand(User.count))
		# 	u_user = User.first(:offset => rand(User.count))
		# 	t_cat = Category.first(:offset => rand(Category.count))
    #
		#	review.rating = 1+rand(5)
		#	review.subject = Populator.words(8)
		#	review.from_user = t_user.user_name
		#	review.to_user = u_user.user_name #check on: from_user and to_user might be same
		#	review.descr = Populator.words(20..200)
		#	review.category = t_cat.category_name
		#	review.count_yes = rand(100)
		#	review.count_no = rand(50)
		# end

	#Populate CPostings database
	#urls = ['/ela/index.rss','/sya/index.rss','/fua/index.rss', '/boo/index.rss']
	#categories = ['electronics', 'computer', 'furniture', 'boats']
	#n = urls.count
	#(1..n).each { |i|
  #parser = Craigslist::Parser.new(:search_path => urls[i], :category => categories[i])
  #parser.get_cpostings
  #}
  #end
  end
end