namespace :db do
  desc "Delete S3 buckets when doing a new migration"
  task :delete_s3_buckets => :environment do
    Userdata.delete_s3_buckets
  end
end