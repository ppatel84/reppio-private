# rake tasks to update sphinx cache of all instances
# this can only run on jack's local machine, due to preconfigured
# environment variables

namespace :ts do
	desc "Reindex cache of all sphinx engines"
	task :update_cache => :environment do

		ec2 = AWS::EC2::Client.new
		instanceData = ec2.describe_instances[:reservation_set]

		instanceDNS = []
		instanceData.each { |instance| 
			instanceDNS << instance[:instances_set][0][:dns_name]
		}
		instanceDNS = instanceDNS.reverse


		# think of how to run this asynchronously
		instanceDNS.each { |dns| 

			commandString = "ssh -i ~/.ec2/jack.pem ubuntu@" + dns + " \" cd reppio; rake ts:index RAILS_ENV=production\""
			`#{commandString}`

		}

	end
end
