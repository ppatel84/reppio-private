# run "rake db:cg"
namespace :db do
  desc "database fill with craigslist categories, zip code mappings"
  task :cg => :environment do
    require 'csv'
    puts "loading craigslist categories data..."
    CSV.open((Rails.root.join "craigslist_category_mapping.csv"), 'r').each do |row|
      CraigslistCategories.create(:rcategory => row[0], :ccategory => row[1])
    end

    puts "loading craigslist url data..."
    ActiveRecord::Base.connection.execute(load_data_infile(Rails.root.join "craigslist_urls.csv"))
  end

  private
  def load_data_infile(temp_path)
    <<-EOF
            LOAD DATA LOCAL INFILE "#{temp_path}"
                     INTO TABLE craigslist_urls
                     FIELDS TERMINATED BY ',' LINES TERMINATED BY '#{'\r'}'
                        (zip, url)
    EOF
  end

end