class AddCraigslistPostingsTable < ActiveRecord::Migration
	def self.up
		create_table :cpostings, :force => true do |t|
			t.string :title
			t.string :link
			t.text :description
			t.datetime :posted_at
			t.string :language
			t.string :rights
			t.string :phone
			t.string :email
			t.timestamps
		end
		add_index :cpostings, [:title, :posted_at], :unique=>true
		add_index :cpostings, :email
	end
	def self.down
		drop_table :cpostings
	end
end