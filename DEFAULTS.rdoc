PUT DEFAULTS AND ANY OTHER IMPORTANT INFORMATION HERE

Ruby: 1.9.3
Rails: 3.2.3
PostgreSQL: 9.1.3

Search Engine: sphinx
http://freelancing-god.github.com/ts/en/deployment.html
- Uses postgreSQL on dev, mysql on prod

------------------
Bitnami Ruby Stack
Compile Sphinx - do
	sudo apt-get -y install libexpat1-dev

------------------
Have to do a shell script patch to get OPENSSL to work on Bitnami

------------------
install.packages(file_name_and_path, repos = NULL, type="source")

------------------
To get category with highest phrase frequency:

	category_probs = PhraseFrequency.search "^family guy$", :match_mode => :extended
	#not totally sure if the match mode is necessary
	category_array = category_probs.to_a
	best_match = category_array.max_by { |x| x.freq }
	best_match.category

------------------
To install Sun Java 6 on Ubuntu 12.04 (step required for rJava)
    1. Download sun jdk 6 binary
    2. chmod +x jdk-6u32-linux-x64.bin
    3. ./jdk-6u32-linux-x64.bin
    4. sudo mv jdk1.6.0_32 /usr/lib/jvm
    5. sudo update-alternatives --install /usr/bin/javac javac /usr/lib/jvm/jdk1.6.0_32/bin/javac 1
       sudo update-alternatives --install /usr/bin/java java /usr/lib/jvm/jdk1.6.0_32/bin/java 1
       sudo update-alternatives --install /usr/bin/javaws javaws /usr/lib/jvm/jdk1.6.0_32/bin/javaws 1
       sudo update-alternatives --install /usr/bin/javah javah /usr/lib/jvm/jdk1.6.0_32/bin/javah 1
       sudo update-alternatives --install /usr/bin/jar jar /usr/lib/jvm/jdk1.6.0_32/bin/jar 1
    6. sudo update-alternatives --config javac
       sudo update-alternatives --config java
       sudo update-alternatives --config javaws
       sudo update-alternatives --config javah
       sudo update-alternatives --config jar
    7. java -version
    8. ls -la /etc/alternatives/ja*
