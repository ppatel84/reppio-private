module ConversationsActions
  def markUnreadForOtherParty(conversation)

    if (conversation.potential_buyer_id == current_user.id)
      unread_by_user_toggle = :unread_by_seller
    elsif (conversation.seller_id == current_user.id)
      unread_by_user_toggle = :unread_by_buyer
    else
      return #Shouldn't ever reach this because of before_filter, but if we do, just return. Frontend code doesn't bother checking return value, so stuff will still appear to happen on frontend, but that's okay because no backend changes are being made.
    end #Close if/elsif/end starting with potential_buyer_id == current_user.id
    conversation.update_attribute(unread_by_user_toggle, true)
  end
  
  def updateReadStatus(conversation,markAsRead)

    if (conversation.potential_buyer_id == current_user.id)
      unread_by_user_toggle = :unread_by_buyer
      toggleValue = (conversation.unread_by_buyer ? false : true)
    elsif (conversation.seller_id == current_user.id)
      unread_by_user_toggle = :unread_by_seller
      toggleValue = (conversation.unread_by_seller ? false : true)
    else
      return #Shouldn't ever reach this because of before_filter, but if we do, just return. Frontend code doesn't bother checking return value, so stuff will still appear to happen on frontend, but that's okay because no backend changes are being made.
    end #Close if/elsif/end starting with potential_buyer_id == current_user.id

    if (params[:unread_by_x] == "0") || (markAsRead == :markAsRead);
      toggleValue = false;
    end #unread_by_x is sent as 0 when we want to mark it as off. Otherwise, as you can see above, it acts as a toggle.

    conversation.update_attribute(unread_by_user_toggle, toggleValue)
  end
  
end