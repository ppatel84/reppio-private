module DistanceCalculations
  def self.calculateOffset(originalLatitudeDegrees, originalLongitudeDegrees, maxOffsetInAnyDirectionMeters)
    # Earth’s radius, sphere
    r=6378137

    # offsets in meters
    dn = rand(maxOffsetInAnyDirectionMeters)*(2*rand(2)-1)
    de = rand(maxOffsetInAnyDirectionMeters)*(2*rand(2)-1)

    # Coordinate offsets in radians
    dLat = dn.to_f/r
    dLon = de/(r*Math.cos(Math::PI*originalLatitudeDegrees/180))

    # offset position, decimal degrees
    latitude = originalLatitudeDegrees + dLat * 180/Math::PI
    longitude = originalLongitudeDegrees + dLon * 180/Math::PI
    return latitude.to_s + ", " +longitude.to_s
  end

  def self.distance_between(lat1,lon1,lat2,lon2) #everything in radians          
    earthRadius = 3958.7558657 #miles
    dLat = (lat2-lat1)
    dLon = (lon2-lon1)
    a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(lat1) * Math.cos(lat2) * Math.sin(dLon/2) * Math.sin(dLon/2)
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))
    return c * earthRadius
  end

  def self.compass_bearing(lat1, lon1, lat2, lon2)
    #everything is in degrees
    bearing=Geocoder::Calculations.bearing_between([lat1, lon1], [lat2,lon2])
    Geocoder::Calculations.compass_point(bearing, ["N", "NE", "E", "SE", "S", "SW", "W", "NW"])
  end
end