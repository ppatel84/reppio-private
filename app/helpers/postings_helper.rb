module PostingsHelper
  def google_feed_header
    "title\tlink\tdescription\tid\tcondition\tprice\tavailability\timage link\tgoogle product category\tproduct type\tadditional image link\ttax\tshipping\tgender\tcolor\tage group\tsize\n"
  end

  def google_feed_item(posting)
    seller=User.find(posting.user_id)

    sortedPicturesNoIndices = []

    if posting.picture_display_order
      (posting.picture_display_order.split(",").map! { |x| x.to_i }).each do |index|
        sortedPicturesNoIndices.push(posting.pictures.fetch(index))
      end
    end


    if sortedPicturesNoIndices.first
      first_photo=sortedPicturesNoIndices.first.photo.url(:original_cropped)
    end

    pictures_besides_first=[]
    if sortedPicturesNoIndices.length > 1
      sortedPicturesNoIndices.last(sortedPicturesNoIndices.length-1).each do |picture|
        pictures_besides_first.push(picture.photo.url(:original_cropped))
      end
    end

    condition=posting.condition.include?("New") ? "new" : "used"


    if (size_field=CustomField.find_by_customizable_id_and_label(posting.id, "No. Size"))
      size=size_field.value
    end

    if (gender_field=CustomField.find_by_customizable_id_and_label(posting.id, "Gender"))
      gender=gender_field.value
    end

    if (color_field=CustomField.find_by_customizable_id_and_label(posting.id, "Color"))
      color=color_field.value
    end

    if posting.category_name=="Babies & Kids"
      age_group="Kids"
    else
      age_group="Adult"
    end


    "#{posting.title}\t#{user_posting_url(:user_name => seller.user_name, :id => posting.id)}\t#{posting.description.squish}\t#{posting.id}\t#{condition}\t#{posting.price}\tin stock\t#{first_photo}\t#{google_product_categories[posting.category_name]}\t#{posting.category_name}\t#{pictures_besides_first.join(",")}\tUS::0:\tUS:::0 USD\t#{gender}\t#{color}\t#{age_group}\t#{size}\n"
  end

  def google_product_categories
    {"Accessories & Covers" => "Electronics > Electronics Accessories",
     "Antiques" => "Furniture",
     "Appliances" => "Home & Garden",
     "Arts & Crafts" => "Arts & Entertainment > Hobbies & Creative Arts",
     "Auto Parts" => "Vehicles & Parts > Vehicle Parts & Accessories",
     "Babies & Kids" => "Baby & Toddler",
     "Beds" => "Furniture > Beds & Accessories > Beds",
     "Bicycles" => "Sporting Goods > Outdoor Recreation > Cycling > Bicycles",
     "Boats" => "Sporting Goods > Water Sports > Boating",
     "Books" => "Media > Books",
     "CDs, DVD & Blu-ray" => "Media",
     "Cabinets & Bookcases" => "Furniture > Shelving",
     "Cameras & Videos" => "Cameras & Optics > Cameras",
     "Cars & Trucks" => "Vehicles & Parts",
     "Cell Phones" => "Electronics > Communications > Telephony > Mobile Phones",
     "Chairs & Stools" => "Furniture > Chairs",
     "Clothing" => "Apparel & Accessories > Clothing",
     "Collectibles" => "Arts & Entertainment > Hobbies & Creative Arts > Collectibles",
     "Components & Parts" => "Electronics > Computers > Computer Components",
     "Computer Peripherals" => "Electronics > Computers > Computer Accessories",
     "Deals & Giftcards" => "Arts & Entertainment > Party & Celebration > Gift Giving > Gift Cards & Certificates",
     "Desks" => "Furniture > Office Furniture > Desks",
     "Desktops" => "Electronics > Computers > Desktop Computers",
     "Dressers & Armoires" => "Furniture > Cabinets & Storage > Armoires & Wardrobes",
     "Drives & Storage" => "Electronics > Computers > Computer Components > Storage Devices",
     "Fashion Accessories" => "Apparel & Accessories",
     "Furniture Sets" => "Furniture > Furniture Sets",
     "Handbags" => "Apparel & Accessories > Handbags",
     "Health & Beauty" => "Health & Beauty",
     "Household" => "Home & Garden",
     "Jewelry" => "Apparel & Accessories > Jewelry",
     "Laptops" => "Electronics > Computers > Laptops",
     "Lawn & Garden" => "Home & Garden > Lawn & Garden",
     "Materials" => "Hardware > Building Materials",
     "Media Players & Projectors" => "Electronics > Video > Projectors",
     "Monitors" => "Electronics > Video > Computer Monitors",
     "Motorcycles" => "Vehicles & Parts",
     "Musical Instruments" => "Arts & Entertainment > Hobbies & Creative Arts > Musical Instruments",
     "Office & Industrial" => "Business & Industrial",
     "Printers & Ink" => "Electronics > Print, Copy, Scan & Fax",
     "RVs" => "Vehicles & Parts",
     "Rugs" => "Home & Garden > Decor > Rugs",
     "Shoes" => "Apparel & Accessories > Shoes",
     "Sofas" => "Furniture > Sofas",
     "Software" => "Software",
     "Sound & Audio Devices" => "Electronics > Audio",
     "Sports & Outdoor Gear" => "Sporting Goods",
     "Sunglasses" => "Apparel & Accessories > Clothing Accessories > Sunglasses",
     "TVs" => "Electronics > Video > Televisions",
     "Tables & Stands" => "Furniture > Tables",
     "Tablets" => "Electronics > Computers > Tablet Computers",
     "Tickets & Events" => "Arts & Entertainment > Party & Celebration > Gift Giving > Gift Cards & Certificates",
     "Tools" => "Hardware > Tools",
     "Toys & Games" => "Toys & Games",
     "Travel" => "Luggage & Bags",
     "Video Games" => "Electronics > Video Game Consoles"}
  end
end
