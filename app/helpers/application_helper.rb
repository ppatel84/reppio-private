module ApplicationHelper
  def title(page_title, options={})
    content_for(:title, page_title.to_s)
    return content_tag(:h1, page_title, options)
  end

  def rep_meter_white_back(score, className)
    if score > 90
      imgString = image_path("rep/highscore3.png")
    elsif score > 80
      imgString = image_path("rep/highscore2.png")
    elsif score > 60
      imgString = image_path("rep/highscore1.png")
    elsif score > 40
      imgString = image_path("rep/midscore2.png")
    elsif score > 20
      imgString = image_path("rep/midscore1.png")
    else
      imgString = image_path("rep/lowscore.png")
    end

    return "<div class='rep-score-white #{className}' style=\"background: url(#{imgString}) center center no-repeat;\"><div class=\"rep-meter-rep\"><div class=\"rep-meter-rep-text-white\">REP</div><div class=\"rep-meter-score-white\">#{score.to_s}</div></div></div>".html_safe

  end


  def rep_meter_small(score, className)
    if score > 80
      imgString = image_path("rep/rep1_small.png")
    elsif score > 60
      imgString = image_path("rep/rep2_small.png")
    elsif score > 40
      imgString = image_path("rep/rep3_small.png")
    elsif score > 20
      imgString = image_path("rep/rep4_small.png")
    else
      imgString = image_path("rep/rep5_small.png")
    end

    return "<div class='rep-score-small #{className}' style=\"background: url(#{imgString}) center center no-repeat;\"><div class=\"rep-meter-rep\"><div class=\"rep-meter-rep-text-small\">REP</div><div class=\"rep-meter-score-small\">#{score.to_s}</div></div></div>".html_safe

  end

  def rep_meter_large(score, className)
    if score > 80
      imgString = image_path("rep/rep1_big.png")
    elsif score > 60
      imgString = image_path("rep/rep2_big.png")
    elsif score > 40
      imgString = image_path("rep/rep3_big.png")
    elsif score > 20
      imgString = image_path("rep/rep4_big.png")
    else
      imgString = image_path("rep/rep5_big.png")
    end

    return "<div class='rep-score-large #{className}' style=\"background: url(#{imgString}) center center no-repeat;\"><div class=\"rep-meter-rep\"><div class=\"rep-meter-rep-text-large\">REP</div><div class=\"rep-meter-score-large\">#{score.to_s}</div></div></div>".html_safe

  end

  def profile_pic_if_exists(size="thumb", user=@user)

    tmp = user.first_user_url
    tmp = tmp.gsub("thumb", size)

    if !tmp.blank?
      return ("<img src='#{tmp}' style=\"max-width:230px;\" alt=\"#{user.user_name}'s picture\"/>").html_safe
    else
      default_profile_pic_url=asset_path('default_profile_picture2.png')
      return ("<img src=#{default_profile_pic_url} alt='Profile Picture' \>").html_safe
    end

  end

  def posting_pic_if_exists(size="medium", posting=@posting)

    begin
      return "<img src='#{posting.first_picture_url.gsub("large_thumb", size)}' alt='Picture of #{posting.title}' />".html_safe
    rescue Exception
      default_posting_pic_url="http://www.reppio.com/assets/default_posting_picture.png"
      return ("<img src='#{default_posting_pic_url}' alt='User did not upload a posting picture' />").html_safe
    end
  end

  def display_stars(count)
    starsArray = ["<span class=\"star-rating-control\">"]
    for i in 0 .. (count - 1) do
      starsArray.push("<div class=\"star-rating star-rating-yellow\"><a>*</a></div>")
    end
    for i in (count) .. 4 do
      starsArray.push("<div class=\"star-rating\"><a>-</a></div>")
    end
    (starsArray.join("\n") << "</span>").html_safe
  end

  def edit_stars_for(field)
    ("<label class=\"radio inline stars\" for=\"#{field}\">
	      <input name=\"review[#{field}]\" type=\"radio\" class=\"star\" value=\"1\" />
	      <input name=\"review[#{field}]\" type=\"radio\" class=\"star\" value=\"2\" />
	      <input name=\"review[#{field}]\" type=\"radio\" class=\"star\" value=\"3\" />
	      <input name=\"review[#{field}]\" type=\"radio\" class=\"star\" value=\"4\" />
	      <input name=\"review[#{field}]\" type=\"radio\" class=\"star\" value=\"5\" />
	      </label>").html_safe
  end

  def link_to_user_profile_thumb(user=@user)

    tmp = user.first_user_url

    if !tmp.blank?
      return tmp
    else
      return asset_path('default_profile_picture2.png')
    end

  end

  def watchlist_badge
    total=current_user.alerts_badge_count
    if total==0
      return ''
    else
      return "<div class=\"badge\">#{total}</div>".html_safe
    end
  end

  def google_maps_script
    return '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA5uvuOfvOix5eUwSZSEEDqjuvKslbxghA&sensor=false&libraries=places"
    type="text/javascript"></script>'.html_safe
  end

  def formatted_price(price)
    if price<1000
      return price
    elsif price<10000
      large_price=price/1000.to_i
      price_string=price.to_s
      hundreds_place_decimal=price_string[1]
      return "#{large_price}.#{hundreds_place_decimal}K"
    else
      large_price=price/1000.to_i
      return "#{large_price}K"
    end

  end


  def indent(number)
    indentation_string=''
    number.times { indentation_string+="&nbsp;&nbsp;&nbsp;" }
    indentation_string.html_safe
  end


  def current_user_alerts
    Alert.find_all_by_user_id(current_user.id).reverse
  end

  def redirect_non_us_traffic
    if Rails.env == "development"
      return
    else
      #blank country is okay
      if request.location.nil? || request.location.country.nil? || request.location.country.blank?
        return
      end


      blocked_countries="Nigeria China Russian Federation"
      valid_country= !blocked_countries.include?(request.location.country)


      if valid_country
        return
      end
    end
    raise ActionController::RoutingError.new('Not Found')
  end

  def facebook_app_id
    if Rails.env == "production"
      "275614615869605"
    else
       "285440531564427"
    end

  end
end
