module GroupsHelper
  def group_header_column(label, params_name, current_order)
    if current_order==params_name
      "<th>#{label} &blacktriangledown;</th>".html_safe
    else
      "<th>#{link_to(label, groups_path(:order=>params_name))}</th>".html_safe
    end
  end
end
