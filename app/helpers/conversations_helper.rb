module ConversationsHelper
  def icon_link_to(path, opts={}, link_opts={})
    classes = []
    classes << "icon-#{opts[:icon]}"
    opts[:class] ||= ""
    opts[:class] << " " << classes.join(" ")
    link_to content_tag(:i, "", opts), path, link_opts
  end

  def unread_selling_count
    Conversation.count(:conditions=>"seller_id= #{current_user.id} AND unread_by_seller=true")
  end

  def unread_buying_count
    Conversation.count(:conditions=>"potential_buyer_id = #{current_user.id} AND unread_by_buyer = true")
  end

  def unread_buying_in_progress_count
    unread_buying_count-unread_buying_completed_count
  end

  def unread_buying_completed_count
    Conversation.count(:conditions=>"potential_buyer_id = #{current_user.id} AND unread_by_buyer = true AND posting_was_delisted = true")
  end

  def unread_selling_in_progress_count
      unread_selling_count-unread_selling_completed_count
  end

  def unread_selling_completed_count
    Conversation.count(:conditions=>"seller_id= #{current_user.id} AND unread_by_seller=true AND posting_was_delisted = true")
  end

  def total_count
    unread_selling_count+ unread_buying_count + current_user.groups_badge_count
  end

  def badge(count)
    if count==0
      return ''
    else
      return "<div class=\"badge\">#{count}</div>".html_safe
    end
  end

  def manage_badge
    total=total_count
    if total==0
      return ''
    else
      return "<div class=\"badge manage-badge\">#{total}</div>".html_safe
    end
  end

  # unread message is an action, opportunity to buy now is another
  def buyer_actions_count(posting, user_id=current_user.id)
    count=0

    count +=1 if Conversation.find_by_potential_buyer_id_and_posting_id(user_id, posting.id).unread_by_buyer
    return count
  end


  def seller_actions_count(posting)
    Conversation.find_all_by_posting_id_and_unread_by_seller(posting.id, true).count
  end

  def payment_label(payment_situation)
    case payment_situation
      # SELLER ACTIONS
      when 1 #Request payment
        "Request Payment"
      when 2  # Payment was requested from this potential buyer
        "Payment Pending"
      when 3  # Payment was requested from a different potential buyer
        "Payment Requested"
      # BUYER ACTIONS
      when 4  # Buyer has already purchased
        "Purchased"
      when 5  # Buyer can purchase
        "Buy Now"
      when 6  # Seller has not requested this buyer to buy
        "Buy Now"
      when 7
        "Sold"
    end
  end

  def last_offer_from_buyer_message(posting, buyer, conversation_page)
    if offer=posting.offers.find_by_tenderer_user_id(buyer.id) rescue false
      return "<strong>#{link_to buyer.first_name, buyer}</strong> #{link_to "offered #{number_to_currency(offer.offer)}.", conversation_page, :class=>"messages-link"}".html_safe
    else
      return "<strong>#{link_to buyer.first_name, buyer}</strong> #{link_to "sent a message.", conversation_page, :class=>"messages-link"}".html_safe
    end
  end

  def posting_row_link(selling_or_buying, in_progress_or_completed, posting_id)
    if selling_or_buying=="selling"
      if in_progress_or_completed=="in-progress"
        return selling_posting_conversations_url(:id=>posting_id)
      else
        return completed_selling_posting_conversations_url(:id=>posting_id)
      end
    else
      if in_progress_or_completed=="in-progress"
        return buying_posting_conversation_url(:id=>posting_id)
      else
        return completed_buying_posting_conversation_url(:id=>posting_id)
      end
    end
  end

  def message_box_price_disable_state(user_will_accept_other_offers)
    user_will_accept_other_offers ? "" : "disabled = 'disabled'"
  end


  def message_box_price_disable_class(user_will_accept_other_offers)
    user_will_accept_other_offers ? "" : "disabled"
  end


  def message_box_price_disable_text(user_will_accept_other_offers)
    user_will_accept_other_offers ? "" : '<p style="color:red;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Price not negotiable.</p>'.html_safe
  end


end