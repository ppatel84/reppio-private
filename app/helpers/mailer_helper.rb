module MailerHelper
  def reppio_domain
    "http://www.reppio.com"
  end

  def reppio_email_address
    "robot@reppio.com"
  end

  def format_time_month_day_year_time(the_time)
    the_time.strftime("%-m/%-d/%y at %l:%M %p")
  end

  def named_from_email(email, name=nil)
    if name.nil?
      return email
    else
      return "\"#{name}\" <#{email}>"
    end
  end

end