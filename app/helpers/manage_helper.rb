module ManageHelper
  def print_posting_delisted_reason(posting)
    case posting.delisted_reason

      when "soldHere"

        return "You sold this item here on Reppio."

      when "soldElsewhere"

        return "You sold this item outside Reppio."

      when "couldntSell"

        return "You weren't able to sell this item."

      when "dontWantToSell"

        return "You didn't want to sell this item."

    end
  end
end