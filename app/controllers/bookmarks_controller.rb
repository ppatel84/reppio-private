class BookmarksController < ApplicationController
  def toggle
    bookmark = Bookmark.find_by_user_id_and_posting_id(current_user.id, params[:posting_id]) rescue nil
    if bookmark
      bookmark.destroy

      if params[:source]=="bookmarks_page"
        respond_to do |format|
          format.js { render :js => "$('##{params[:posting_id]}').fadeOut();" }
          format.html { redirect_to :back, :notice => 'The bookmark was removed.' }
        end
      elsif params[:source]=="detailed_listing_page"
        respond_to do |format|
          format.js { render :js => "$('#bookmark-button').html('#{view_context.image_tag "detailed_listing/watchlist-add.png" }&nbsp;&nbsp;Add to Watchlist');" }
          format.html { redirect_to :back, :notice => 'The bookmark was removed.' }
        end
      else
        render :text => "destroyed"
      end
    else
      bookmark = current_user.bookmarks.build(:user_id => current_user.id, :posting_id => params[:posting_id])
      if bookmark.save
        if params[:source]=="detailed_listing_page"
          respond_to do |format|
            format.js { render :js => "$('#bookmark-button').html('#{view_context.image_tag  "detailed_listing/watchlist-remove.png" }&nbsp;&nbsp;Remove from Watchlist');" }
            format.html { redirect_to :back, :notice => 'The bookmark was saved.' }
          end
        else
          render :text => "created"
        end
      end
    end
  end
end
