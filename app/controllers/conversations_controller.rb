class ConversationsController < ApplicationController
  include ActionView::Helpers::NumberHelper
  include ::ConversationsActions

  skip_before_filter :require_login, :only => [:message_from_new_user, :load_message_modal]

  def message_from_new_user

    user_name= params[:user][:first_name].to_s.gsub(/[^A-Za-z]/, '')
    while user_name.length<5 || User.find_by_user_name(user_name)
      user_name=user_name+rand(10).to_s
    end
    email_address=params[:user][:email_addresses_attributes]["0"]["email"]
    new_user= User.new(:first_name => params[:user][:first_name],
                       :email => email_address,
                       :email_private => true,
                       :street_address_private => true,
                       :phone_number_private => true,
                       :education_private => true,
                       :employer_private => true,
                       :receive_email_notifications => true,
                       :picture_display_order => '',
                       :email_confirmed => false,
                       :street_address => "Chicago, IL 60601, USA",
                       :latitude => 0.730900615296422,
                       :longitude => -1.5291044688552635,
                       :city => "Chicago",
                       :state => "IL",
                       :zip_code => "60601",
                       :email_confirmation_code => BCrypt::Engine.generate_salt,
                       :password => Array.new(15).map { (48 + rand(75)).chr }.join.gsub(/[^0-9A-Za-z]/, ''))

    new_user.user_name=user_name

    unless cookies['cid'].nil?
      new_user.cid=cookies['cid']
    end

    if new_user.save
      new_user.email_addresses.build(:email => email_address, :is_primary => true)
      new_user.save
      Userdata.create(:user_id => new_user.id,
                      :full_name_complete => true,
                      :repscore_history => '0')
      Userdata.compute_repscore(new_user.id)


      @conversation = Conversation.new(:potential_buyer_id => new_user.id,
                                       :unread_by_seller => false, :is_verified => false,
                                       :seller_id => params[:user][:seller_id], :posting_id => params[:user][:posting_id])

      posting=Posting.find(@conversation.posting_id)


      if params[:offer]
        @offer=posting.offers.build(params[:offer])
        @conversation.has_offer = true
        @offer.user_id = @conversation.potential_buyer_id
        @offer.tenderer_user_id = @conversation.potential_buyer_id
        if params[:offer][:offer].nil?
          @offer.offer = posting.price
        end
        @offer.save
      end
      @conversation.save


      @message = @conversation.messages.build(params[:message])
      if @offer
        @message.message_body = "(offers a price of #{number_to_currency(@offer.offer, :precision => 0)}) #{params[:message][:message_body]}"
      else
        if params[:message][:message_body].blank?
          redirect_to :back, :flash => {:alert => 'Your message cannot be blank.'} and return
        else
          @message.message_body= params[:message][:message_body]
        end
      end
      @message.save

      sender_userdata=Userdata.find_by_user_id(new_user.id)
      sender_userdata.update_attribute(:messages, sender_userdata.messages + 1)


      # deliver email for verification
      UserMailer.send_email_confirmation_with_password(new_user.email,
                                                       new_user.email_confirmation_code,
                                                       new_user.first_name,
                                                       conversation_url(:id => @conversation.id),
                                                       new_user.password,
                                                       posting.title).deliver

      redirect_to :back, :flash => {:alert => "Almost done! Please check your inbox to verify your email address."} and return

    end
    redirect_to :back, :flash => {:alert => 'Sorry, an error occurred. Please try again.'} and return

  end

  def remove_conversation
    convToRemove = Conversation.find(params[:id])

    if (params[:asRole] == "buying")
      convToRemove.update_column(:hide_from_buyer, 1)
      redirect_to manage_buying_url, :notice => "Conversation has been removed successfully."
    end

    if (params[:asRole] == "selling")
      convToRemove.update_column(:hide_from_seller, 1)
      redirect_to manage_selling_url, :notice => "Conversation has been removed successfully."
    end

  end

  def create
    if current_user.is_scammer
      redirect_to :back, :flash => {:notice => 'Message sent.'} and return
    end
    @conversation = Conversation.new(params[:conversation])
    @conversation.potential_buyer_id=current_user.id

    posting= Posting.find(@conversation.posting_id)
    seller=User.find(posting.user_id)
    @conversation.seller_id=seller.id


    @message= @conversation.messages.build(params[:message])

    if @message.message_body.blank?
      redirect_to :back, :flash => {:alert => 'Your message cannot be blank.'} and return
    end

    sender_userdata=Userdata.find_by_user_id(current_user.id)
    sender_userdata.update_attribute(:messages, sender_userdata.messages + 1)


    if @conversation.save && seller.receive_email_notifications
      ConversationMailer.email_message_between_users(seller.email, current_user.first_name, sender_userdata.repscore, posting.title, @message.message_body, conversation_url(:id => @conversation.id)).deliver
    end

    push_notification(seller, current_user, @message)

    redirect_to :back, :flash => {:notice => 'Message sent.'} and return
  end

  def create_with_offer
    if current_user.is_scammer
      redirect_to :back, :flash => {:notice => 'Message sent.'} and return
    end
    @conversation = Conversation.new(params[:conversation])
    @conversation.potential_buyer_id=current_user.id

    posting= Posting.find(@conversation.posting_id)
    seller=User.find(posting.user_id)
    @conversation.seller_id=seller.id

    @offer=posting.offers.build(params[:offer])
    @conversation.has_offer = true
    @offer.user_id = @conversation.potential_buyer_id
    @offer.tenderer_user_id = @conversation.potential_buyer_id
    if params[:offer][:offer].nil?
      @offer.offer = posting.price
    end
    @offer.save

    params[:message][:message_body]="(offers a price of #{number_to_currency(@offer.offer, :precision => 0)}) #{params[:message][:message_body]}"
    @message= @conversation.messages.build(params[:message])

    sender_userdata=Userdata.find_by_user_id(current_user.id)
    sender_userdata.update_attribute(:messages, sender_userdata.messages + 1)


    if @conversation.save && seller.receive_email_notifications
      ConversationMailer.offer_updated(seller.email, number_to_currency(@offer.offer),
                                       posting.title, current_user.first_name, sender_userdata.repscore,
                                       conversation_url(:id => @conversation.id),
                                       params[:message][:message_body]).deliver
    end

    push_notification(seller, current_user, @message)

    redirect_to :back, :flash => {:notice => 'Message sent.'} and return

  end


  def update
    if current_user.is_scammer
      redirect_to :back, :flash => {:notice => 'Message sent.'} and return
    end
    conversation=Conversation.find(params[:id])
    @potential_buyer=User.find(conversation.potential_buyer_id)
    @seller=User.find(conversation.seller_id)
    message_body=params[:conversation][:message][:message_body]
    offer_price=params[:offer]

    if message_body.blank? && offer_price.blank? # no message or offer
      render :js => "alert('Message cannot be blank.');" and return
    else

      is_buyer= conversation.potential_buyer_id==current_user.id
      if is_buyer
        conversation.unread_by_buyer= false
        conversation.unread_by_seller= true
        recipient=@seller
        sender=@potential_buyer
      else
        conversation.unread_by_seller=false
        conversation.unread_by_buyer= true
        recipient=@potential_buyer
        sender=@seller

      end
      posting = Posting.find(conversation.posting_id)
      sender_user_data=Userdata.find_by_user_id(current_user.id)


      if offer_price.blank? #message only
        @message=conversation.messages.create(:message_body=>message_body, :message_from_seller => !is_buyer)
        ConversationMailer.email_message_between_users(recipient.email, sender.first_name, sender_user_data.repscore,
                                                       posting.title, @message.message_body,
                                                       conversation_url(:id => conversation.id)).deliver
      elsif message_body.blank? && !valid_float?(offer_price)
        render :js => "alert('You didn\'t submit a valid offer.');" and return
      else # offer OR message and offer
        conversation.has_offer = true

        offer=posting.offers.create(:user_id => conversation.potential_buyer_id,
                                    :tenderer_user_id => current_user.id, :offer => offer_price.to_f)

        ConversationMailer.offer_updated(recipient.email, number_to_currency(offer.offer),
                                         posting.title,
                                         current_user.first_name, sender_user_data.repscore,
                                         conversation_url(:id => conversation.id), message_body).deliver

        @message=conversation.messages.build(:message_body => "(offers a price of #{number_to_currency(offer.offer, :precision => 2)}) #{message_body}",
                                    :message_from_seller => !is_buyer)

      end

      # finally, in the event the conversation is hidden, make it visible again to both parties.
      conversation.hide_from_buyer= 0
      conversation.hide_from_seller=0
      conversation.save
      push_notification(recipient, sender, @message)
      # update seller response time
      if !is_buyer && conversation.messages.where("message_from_seller=true").count==1
        sender_user_data.update_attributes(:total_response_time_in_minutes =>
                                               sender_user_data.total_response_time_in_minutes + (Time.now-@message.created_at).minutes.to_f,
                                           :number_of_responses => sender_user_data.number_of_responses + 1)
      end

      sender_user_data.update_attribute(:messages, sender_user_data.messages + 1)

      respond_to do |format|
        format.js and return
      end
    end
  end

  def load_message_modal
    @posting= Posting.find(params[:id])
    @seller= User.find(@posting.user_id)
    @conversation_already_exists= current_user && !(Conversation.find_by_posting_id_and_potential_buyer_id(@posting.id, current_user.id)).nil?
    respond_to do |format|
      format.js and return
    end
  end


  private
  def push_notification(receivingUser, sender, message)
    user_mobile_device = UserMobileDevice.find_by_user_id(receivingUser.id)

    if user_mobile_device

      #Send push notification to recipient
      #app_key = "v88QG8SnTgy4OAE10Cpwsg"             #development
      #app_secret = "dZc-R7QvSIa9dL2vd18rSQ"          #development
      #app_secret_master = "6ZZ-TEq9TGOpTB2LCHy6iA"   #development
      app_key = "Gp6xyenNQQOhzWEnMBNfmw" #production
      app_secret = "fsa7FfiQT5e59uiBvKUMfA" #production
      app_secret_master = "h8zfX6sjRvyAzkiBnk8i-g" #production

      push_host = "https://go.urbanairship.com"
      push_url = "/api/push/"
      device_tokens = [user_mobile_device.device_token]
      unread_messages = Conversation.where("(seller_id = ? AND unread_by_seller = true) OR (potential_buyer_id = ? AND unread_by_buyer = true)", receivingUser.id, receivingUser.id).count
      full_message = User.find(sender.id).first_name.titlecase + ": " + message.message_body
      push_message = (full_message.strip.length <= 157) ? full_message.strip : full_message.strip[0, 153] + "..."
      conversation_data = {:msgid => message.id}

      push_params = {:app_key => app_key, :app_secret => app_secret_master, :push_host => push_host,
                     :push_url => push_url, :device_tokens => device_tokens, :badges => unread_messages,
                     :message => push_message, :recipient => receivingUser.id, :add => conversation_data}

      push_send_message(push_params)

    end
  end

end