class AlertsController < ApplicationController
  def create

    params[:alert][:max_price]=10000000 if params[:alert][:max_price].blank?
  	params[:alert][:radius]=30 if params[:alert][:radius]

    render :js => "alert('You must have an alert word, e.g. iPad, car.');" and return if params[:alert][:search].blank?


    location_info_from_ip_address=Geocoder.search(params[:alert][:address])[0]
    if location_info_from_ip_address && location_info_from_ip_address.latitude #has lat and long at least
      params[:alert][:latitude]= location_info_from_ip_address.latitude
      params[:alert][:longitude]=location_info_from_ip_address.longitude
    else
      render :js => "alert('Invalid location.');" and return
    end

    alert = current_user.alerts.create(params[:alert])

    if alert.save
      respond_to do |format|
        format.js
      end
    end
  end


  def update
    @alert = Alert.find_by_id_and_user_id(params[:id], current_user.id)

    location_info_from_ip_address=Geocoder.search(params[:alert][:address])[0]
    if location_info_from_ip_address && location_info_from_ip_address.latitude #has lat and long at least
      params[:alert][:latitude]= location_info_from_ip_address.latitude
      params[:alert][:longitude]=location_info_from_ip_address.longitude
    else
      render :js => "alert('Invalid location.');" and return
    end


    if @alert.update_attributes(params[:alert])
      respond_to do |format|
        format.js
      end
    else
      render :js=>"alert('There was something wrong with values you entered.');"
    end
  end


  def destroy
    alert = Alert.find_by_id_and_user_id(params[:id], current_user.id)
    if alert
      current_user.update_attribute(:alerts_badge_count, current_user.alerts_badge_count - alert.num_new_listings)
      alert.destroy
    end

    respond_to do |format|
      format.js
      format.html { redirect_to watchlist_url, :flash => {:notice => 'Alert successfully deleted.'} }
    end
  end


  def edit
    @alert = Alert.find_by_id_and_user_id(params[:id], current_user.id)

    if @alert
      respond_to do |format|
        format.js
      end
    else
      render :js=>"alert('We could not retrieve the alert.');"
    end
  end
end