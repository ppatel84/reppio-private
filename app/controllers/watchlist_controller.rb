class WatchlistController < ApplicationController
  def show
  end

  def alert
    @alert = Alert.find(params[:alert_id])
    if @alert.num_new_listings > 0
      current_user.update_attribute(:alerts_badge_count, current_user.alerts_badge_count - @alert.num_new_listings)
      @alert.update_attribute(:num_new_listings, 0)
    end
    @alertedPostings = @alert.postings

  end

  def bookmarks
    bookmarks = Bookmark.find_all_by_user_id(current_user.id)
    @bookmarkedPostings = bookmarks.collect { |b| Posting.find(b.posting_id) }.reverse
  end
end