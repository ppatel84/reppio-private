class GroupsController < ApplicationController
  protect_from_forgery

  def notifications

  end

  def index
    case params[:order]
      when "name"
        order="long_name ASC"
      when "count"
        order="members_count DESC"
      when "distance"
        degToR = Math::PI / 180
        @groups = Group.where("id!=1").sort_by { |group| ::DistanceCalculations.distance_between(current_user.latitude*degToR, current_user.longitude*degToR, group.latitude*degToR, group.longitude*degToR).round(1)
        }
        return
      else
        order="id ASC"
    end

    #do not include omnigroup with id 1
    @groups = Group.where("id != 1").order(order)
  end

  def show
    @group = Group.find_by_short_name(params[:id])
    @members=@group.users

    params[:page]=1 if params[:page].nil?


    @group_postings=@group.postings.where("deleted=? AND is_updated=?", false, true).order('updated_at DESC').paginate(:page => params[:page], :per_page => 15)

  end

  def join_group
    if group = Group.find_by_short_name(params[:id])
      if group.may_join_without_admin_approval
        group.users<<=current_user

        current_user.postings.reject { |i| i.deleted || !i.is_updated }.each do |user_posting|
          group.postings<<= user_posting
        end
        group.members_count+=1
        group.save

        redirect_to group, :notice => "You joined #{group.long_name}." and return
      else
        redirect_to group, :notice => "You cannot join this group unless the admin approves it." and return
      end
    else
      redirect_to groups_path, :notice => "The group you were looking for was not found." and return
    end
  end

  def leave_group
    if group = Group.find_by_short_name(params[:id])
      if group.users.delete(current_user)
        current_user.postings.each { |posting| group.postings.delete(posting) }
        group.members_count-=1
        group.save
        redirect_to group, :notice => "You left #{group.long_name}." and return
      else
        redirect_to group and return
      end
    else
      redirect_to groups_path, :notice => "The group you were looking for was not found." and return
    end
  end

  def remove_from_group
    if (group = Group.find_by_short_name(params[:id])) && (current_user.id==group.owner_id)
      @user_to_be_removed=User.find(params[:user_id])
      if group.users.delete(@user_to_be_removed)
        @user_to_be_removed.postings.each { |posting| group.postings.delete(posting) }
        group.members_count-=1
        if group.save
          respond_to do |format|
            format.js
            format.html { redirect_to group, :notice => "You removed #{@user_to_be_removed.first_name} #{@user_to_be_removed.last_name}." and return }
          end
        end
      else
        redirect_to group and return
      end
    else
      redirect_to groups_path, :notice => "The group you were looking for was not found." and return
    end
  end

  def request_join_group
    if group = Group.find_by_short_name(params[:id])
      group_request=GroupRequest.create(:group_id => group.id, :owner_id => group.owner_id, :user_id => current_user.id)
      owner=User.find(group.owner_id)
      owner.update_attribute(:groups_badge_count, owner.groups_badge_count+1)
      GroupMailer.request_to_join_a_group(owner.first_name, owner.email,
                                          group.long_name, group_url(group),
                                          current_user.first_name,
                                          user_url(current_user),
                                          handle_request_url(:id => group_request, :status => "accept"),
                                          handle_request_url(:id => group_request, :status => "ignore")).deliver
      redirect_to group and return
    else
      redirect_to groups_path, :notice => "The group you were looking for was not found." and return
    end
  end

  def handle_request_join_group
    if (group_request=GroupRequest.find(params[:id]) rescue nil)
      if (group = Group.find(group_request.group_id)) && group.owner_id==current_user.id
        if params[:status]=="accept"
          group.users<<= User.find(group_request.user_id)
          User.find(group_request.user_id).postings.reject { |i| i.deleted || !i.is_updated }.each do |user_posting|
            group.postings<<= user_posting
          end
          notice="Person was added to #{group.long_name}."
          group.members_count= group.members_count+1

          group.save
        else
          notice="Person was not added to #{group.long_name}."
        end

        GroupRequest.delete(group_request)
        owner=User.find(group.owner_id)
        owner.update_attribute(:groups_badge_count, owner.groups_badge_count-1)
        redirect_to groups_path, :notice => notice and return
      else
        redirect_to groups_path, :notice => "Only the group owner can allow people to join the group." and return
      end
    else
      redirect_to groups_path, :notice => "You have already accepted or ignored this request."
    end
  end

  def new
    redirect_to login_url, :notice => "You must be logged in to create a group." and return if not signed_in?

    @group = Group.new(:may_join_without_admin_approval => true)
  end

  def edit
    if @group = Group.find_by_short_name(params[:id])
      if current_user.id!=@group.owner_id
        redirect_to @group, :notice => "Only the owner of this group can edit it." and return
      end
    else
      redirect_to groups_path, :notice => "The group you were looking for was not found." and return
    end
  end

  def create
    @group = Group.new(params[:group])
    @group.owner_id=current_user.id
    @group.users<<= current_user

    current_user.postings.reject { |i| i.deleted || !i.is_updated }.each do |user_posting|
      @group.postings<<= user_posting
    end

    if @group.save
      redirect_to @group, notice: 'Your group was successfully created.'
    else
      render action: "new"
    end
  end

  def update
    @group = Group.find_by_short_name(params[:id])

    if current_user.id==@group.owner_id
      if @group.update_attributes(params[:group])
        redirect_to @group, notice: 'Your group was successfully updated.'
      else
        render action: "edit"
      end
    end
  end


  def invite_to_group
    @group=Group.find_by_short_name(params[:id])
    @userdata= Userdata.find_by_user_id(current_user.id)
    @fb_ids = @userdata.facebook_friends_on_reppio_list.split(",") if not @userdata.facebook_friends_on_reppio_list.nil?

  end


  def send_email_to_invite_to_group
    group=Group.find(params[:group_id])
    params[:emails].gsub(" ", "").split(",").each do |email|
      if valid_email?(email)
        GroupMailer.send_invitation_to_join_group(email, current_user.first_name, current_user.last_name, current_user.email, group.short_name, group.long_name, params[:personal_message]).deliver
      end
    end
    redirect_to :back, :notice => "Your friends have been invited!"
  end

  def invite_facebook_connections_to_group
    group=Group.find(params[:group_id])
    (params[:user_ids] ||= []).each do |user_id|
      user=User.find(user_id)
      GroupMailer.send_invitation_to_join_group(user.email, current_user.first_name, current_user.last_name, current_user.email, group.short_name, group.long_name, params[:personal_message]).deliver
    end
    redirect_to :back, :notice => "Your friends have been invited!"
  end


  def destroy
    group = Group.find_by_short_name(params[:id])
    if current_user.id==group.owner_id
      group_requests=GroupRequest.find_all_by_group_id(group.id)
      current_user.update_attribute(:groups_badge_count, current_user.groups_badge_count - group_requests.count)
      group_requests.each do |group_request|
        group_request.destroy
      end

      group.destroy

      redirect_to groups_url
    end
  end

  def join_autogroup_modal
    @group=Group.find_by_short_name(params[:id])
    respond_to do |format|
      format.js
    end
  end
end
