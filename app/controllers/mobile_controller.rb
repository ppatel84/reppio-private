class MobileController < ApplicationController
  include ::ConversationsActions
  include ActionView::Helpers::DateHelper
  include ActionView::Helpers::NumberHelper

  skip_before_filter :require_login

  def mobile_app_version
    render json: {:latest_version => "1.1", :message => "A newer version of Reppio Mobile is available"}
  end

  def mobile_email_signup

    email_address=params[:user][:email_addresses_attributes]["0"]["email"]

    @user = User.new(params[:user])
    @user.email_private =true
    @user.street_address_private=true
    @user.phone_number_private =true
    @user.education_private =true
    @user.employer_private =true
    @user.receive_email_notifications=true
    @user.picture_display_order=''
    @user.email=email_address
    @user.email_confirmed = false
    @user.email_confirmation_code = BCrypt::Engine.generate_salt
    @user.cid = "app"

    password = @user.password
    if password.present?
      @user.password_salt = BCrypt::Engine.generate_salt
      @user.password_hash = BCrypt::Engine.hash_secret(password, @user.password_salt)
    end

    if @user.save

      # user saved, returned true. create initial repscore entry
      Userdata.create(:user_id => @user.id,
                      :full_name_complete => true,
                      :repscore_history => '0')
      Userdata.compute_repscore(@user.id)


      #session[:user_id] = @user.id
      UserMailer.send_email_confirmation(@user.email,
                                         @user.email_confirmation_code,
                                         @user.first_name,
                                         "app").deliver
      #redirect_to root_url, notice: "Hi #{@user.first_name}! An email has been sent to #{@user.email}. Click the link in the email to finish the sign-up process." and return

      #login newly created user to app without requiring email validation
      current_user = User.find(@user.id)
      self.mobile_render_json_auth(current_user)

    else
      #render action: "email_sign_up"
      render json: {"success" => false, "message" => "User could not be created."}
    end
  end


  def mobile_create_auth

    #auth=request.env['omniauth.auth']

    auth = {
        "provider" => params[:provider],
        "info" => {
            "first_name" => params[:first_name],
            "last_name" => params[:last_name],
            "email" => params[:email],
            "location" => params[:location],
            "image" => "http://graph.facebook.com/#{params[:uid]}/picture?type=large"
        },
        "extra" => {
            "raw_info" => {
                "gender" => params[:gender],
                "education" => params[:education],
                "work" => params[:work],
                "birthday" => params[:birthday],
            }
        },
        "credentials" => {
            "token" => params[:token],
            "secret" => params[:secret]
        }
    }

    #request.env['omniauth.auth']=auth

    #render json: { "success" => true, "message" => auth["extra"]["raw_info"]["work"][0]["employer"]["name"] }

    unless @auth = Authorization.find_from_hash(auth)

      # Create a new user or add an auth to existing user, depending on
      # whether there is already a user signed in.
      current_user = self.current_user rescue nil
      auth["from_app"]="yes"
      @auth = Authorization.create_from_hash(auth, current_user)
    end

    # Log the authorizing user in.
    self.current_user = User.find(@auth.user_id)
    current_user = self.current_user

    if current_user.is_deactivated
      current_user.postings.each { |posting| posting.update_attribute(:is_updated, true) }
      current_user.update_attribute(:is_deactivated, false)
    end

    current_userdata = Userdata.find_by_user_id(current_user.id)

    # save facebook friend data to userdata table
    friends=Array.new
    me = FbGraph::User.me auth['credentials']['token']
    me.friends.each do |friend|
      friends<< friend.identifier
    end

    facebook_people_on_reppio = Authorization.where("provider = ?", "facebook").collect(&:uid)

    # save to userdata

    current_userdata.update_attribute(:facebook_friends, friends.to_s.gsub("\"", ''))
    current_userdata.update_attribute(:fb_friends, friends.length)
    current_userdata.update_attribute(:facebook_friends_on_reppio_list, (facebook_people_on_reppio & friends).to_s.gsub('"', '').gsub('[', '').gsub(']', '').gsub(' ', ''))
    current_userdata.update_attribute(:facebook_friends_on_reppio, (facebook_people_on_reppio & friends).length)

    if current_userdata.facebook_friends_on_reppio >= 10
      current_userdata.update_attribute(:rp_complete, true)
    end

    # queue up delayed_job to remote processor
    #Userdata.send_later(:compute_fb_nodes, @auth.user_id)

    # update userdata table if criteria of 100+ friends is met
    if (friends.length >= 100);
      current_userdata.update_attribute(:fb_complete, true);
    end
    if (friends.length>=25);
      current_userdata.update_attribute(:fb_half_complete, true);
    end

    # compute repscore, which will change in the event the fb_complete field is updated
    Userdata.compute_repscore(@auth.user_id)


    # if current_user does not have a user name
    if current_user.user_name.nil?
      user_name= current_user.first_name.gsub(/[^A-Za-z]/, '')
      while user_name.length<5 || User.find_by_user_name(user_name)
        user_name=user_name+rand(10).to_s
      end

      current_user.update_attribute(:user_name, user_name)
    end


    mobile_session_key = Array.new(100).map { (48 + rand(75)).chr }.join.gsub(/[^0-9A-Za-z]/, '')
    current_user.update_attribute(:mobile_session_key, mobile_session_key)

    # check if address, lat, lng has been input. if not put in geolocated params
    if (current_user.latitude.nil? || current_user.latitude.blank?)
      current_user.update_attribute(:latitude, params[:latitude])
      current_user.update_attribute(:longitude, params[:longitude])
    end

    if (current_user.street_address.nil? || current_user.street_address.blank?)
      current_user.update_attribute(:street_address, params[:address])
    end

    if (current_user.latitude.nil? || current_user.latitude.blank?) && (current_user.street_address.nil? || current_user.street_address.blank?)
      current_user.update_attribute(:latitude, 41.9605)
      current_user.update_attribute(:longitude, -87.6298)
      current_user.update_attribute(:city, "Chicago")
      current_user.update_attribute(:state, "IL")
    end

    self.mobile_render_json_auth(current_user)

    return

  end


  def mobile_render_json_auth(current_user)

    mobile_session_key = Array.new(100).map { (48 + rand(75)).chr }.join.gsub(/[^0-9A-Za-z]/, '')
    current_user.update_attribute(:mobile_session_key, mobile_session_key)

    push_devices = current_user.user_mobile_devices

    unread_buying_count = Conversation.count(:conditions => "potential_buyer_id = #{current_user.id} AND unread_by_buyer = true")
    unread_selling_count = Conversation.count(:conditions => "seller_id= #{current_user.id} AND unread_by_seller=true")

    render json: {"success" => true, "session_id" => mobile_session_key, "user_id" => current_user.id, "first_name" => current_user.first_name,
                  "last_name" => current_user.last_name, "email" => current_user.email, "user_name" => current_user.user_name,
                  "phone_number" => current_user.phone_number, "about_me" => current_user.about_me, "education" => current_user.education,
                  "employer" => current_user.employer, "zip_code" => current_user.zip_code,
                  "email_private" => current_user.email_private, "phone_number_private" => current_user.phone_number_private,
                  "education_private" => current_user.education_private, "employer_private" => current_user.employer_private,
                  "email_confirmed" => current_user.email_confirmed, "push_devices" => push_devices,
                  "last_posting_street_address" => current_user.last_posting_street_address,
                  "last_posting_latitude" => current_user.last_posting_latitude,
                  "last_posting_longitude" => current_user.last_posting_longitude,
                  "last_search_street_address" => current_user.last_search_street_address,
                  "first_user_url" => current_user.first_user_url, "city" => current_user.city,
                  "unread_buying_count" => unread_buying_count, "unread_selling_count" => unread_selling_count

    }

  end


  def mobile_create
    #params = {email:"hello@flemingau.com", password:"testing"}

    if user = User.authenticate(params[:email], params[:password])

      if user.is_deactivated
        user.postings.each { |posting| posting.update_attribute(:is_updated, true) }
        user.update_attribute(:is_deactivated, false)
      end

      Userdata.find_by_user_id(user.id).update_attribute(:last_login, Time.now)

      self.mobile_render_json_auth(user)

    else
      #redirect_to :back, :alert => "Invalid email/password combination" #was login_url ... now, it's very difficult to get to login_url. Guess that's okay.
      render json: {"success" => false, "message" => "Invalid email/password combination"}
    end

  end


  def mobile_auth(user_id, session_id)
    User.find_by_id_and_mobile_session_key(user_id, session_id)
  end


  def mobile_profile_get
    if (viewer= mobile_auth(params[:user_id], params[:session_id]))

      profile = User.find(params[:profile_id])
      profile_data = Userdata.find_by_user_id(params[:profile_id])
      user_picture_indexes = profile.picture_display_order

      group_ids= ([1].concat(viewer.groups.collect(&:id)).concat(Group.find_all_by_may_join_without_admin_approval_and_email_extension(true, "").collect(&:id))).uniq
      all_profile_postings = profile.postings.where(:deleted => false, :fulfilled => nil).order("updated_at DESC")

      selling_data=[]

      all_profile_postings.each do |posting|

        if (posting.groups.collect(&:id) & group_ids).length>0
          selling_data.push(posting)
        end
      end

      profile_pictures=[]
      profile_pics = Picture.find_all_by_imageable_type_and_imageable_id("User", params[:profile_id])

      user_picture_indexes.split(",").each { |picture_index|
        profile_pictures.push(profile_pics.fetch(picture_index.to_i).photo.url)
      }


      if profile_pictures.empty?
        profile_pictures=["http://www.reppio.com/assets/default_profile_picture2.png"]
      end
      #for i in 0..profile_pics.count-1
      #  #profile_pictures.push(profile_pics.fetch(i).photo.url.gsub("original","medium"))
      #  profile_pictures.push(profile_pics.fetch(i).photo.url)
      #end

      #User.find(2).pictures.fetch(0).photo.url
      userdata=profile_data
      number_of_connections=userdata.facebook_friends_on_reppio
      number_of_connections=0 if !number_of_connections

      fb_ids=[]

      fb_ids= userdata.facebook_friends_on_reppio_list.split(",").shuffle if not userdata.facebook_friends_on_reppio_list.nil?
      friends_pictures_array=[]

      if fb_ids
        fb_ids.each do |fb_id|
          friend=User.find_by_facebook_id(fb_id)
          if friend
            friends_pictures_array.push([friend.id, friend.first_user_url])
          end
        end
      end

      profile_reviews = Review.find_all_by_user_id(params[:profile_id])
      reviews_hash_array=[]

      profile_reviews.each do |review|
        reviewer=User.find(review.reviewer_user_id)

        review_hash=review.as_json(:only => [:overall_experience, :description])

        review_hash["reviewer_pic"]= reviewer.first_user_url
        review_hash["reviewer_name"]=reviewer.first_name
        review_hash["reviewer_repscore"]=reviewer.repscore
        review_hash["date"]= review.created_at.strftime("%B %e, %Y")
        reviews_hash_array.push(review_hash)
      end

      render json: {"success" => true, "message" => "Success!",
                    "data" => {
                        "user_id" => profile.id,
                        "user_name" => profile.user_name,
                        "first_name" => profile.first_name,
                        "last_initial" => "",
                        "about_me" => profile.about_me,
                        "user_city" => profile.city,
                        "user_state" => profile.state,
                        "zip_code" => profile.zip_code,
                        "picture_display_order" => profile.picture_display_order,
                        "user_data" => profile_data,
                        "user_pictures" => profile_pictures,
                        "number_of_connections" => number_of_connections,
                        "user_reviews" => reviews_hash_array,
                        "selling_data" => selling_data,
                        "friends_pictures_array" => friends_pictures_array
                    }
      }
    else

      render json: {"success" => false, "message" => "Authentication failure"}

    end
  end


  def mobile_profile_update
    #params = {user:{first_name:"static3"}, user_id:"1", user_name:"flemingtest", email:"fleming@reppio.com", session_id:"j6vuLFRx9XvIRj8BvJY7E94NgV8RFPGqoRNQExpyVJ6BlRMLvW3kxeLrA0Oe8HYnpoPCVrhEZwe142dI4C6gMSr"}
    #params = {user:"{\"first_name\" = Fleminggg;}", user_id:"1", user_name:"flemingtest", email:"fleming@reppio.com", session_id:"j6vuLFRx9XvIRj8BvJY7E94NgV8RFPGqoRNQExpyVJ6BlRMLvW3kxeLrA0Oe8HYnpoPCVrhEZwe142dI4C6gMSr"}

    if mobile_auth(params[:user_id], params[:session_id])
      user = User.find_by_id(params[:user_id])
      if !params[:first_name].blank?
        user.update_attribute(:first_name, params[:first_name])

      end


      if !params[:last_name].blank?
        user.update_attribute(:last_name, params[:last_name])
      end

      if !params[:city].blank?
        user.update_attribute(:city, params[:city])
      end

      if !params[:about_me].blank?
        user.update_attribute(:about_me, params[:about_me])
      end
    end
    render json: {"success" => true, "title" => "Profile Updated", "message" => "cool"}
  end


  def get_alert_labels
    if user=mobile_auth(params[:user_id], params[:session_id])
      render json: {"success" => false, "alert_titles" => user.alerts}
    else
      render json: {"success" => false, "message" => "Authentication failure"}
    end
  end

  def get_alert_postings
    if user=mobile_auth(params[:user_id], params[:session_id])
      alert = Alert.find(params[:alert_id])
      if alert.num_new_listings > 0
        user.update_attribute(:alerts_badge_count, user.alerts_badge_count - user.num_new_listings)
        alert.update_attribute(:num_new_listings, 0)
      end
      alert_postings = alert.postings
      render json: {"success" => true, "alert" => alert, "alert_postings" => alert_postings}
    end
  end

  def mobile_alert_create
    if user=mobile_auth(params[:user_id], params[:session_id])

      location_info_from_ip_address=Geocoder.search(params[:address])[0]
      if location_info_from_ip_address && location_info_from_ip_address.latitude #has lat and long at least
        params[:latitude]= location_info_from_ip_address.latitude
        params[:longitude]=location_info_from_ip_address.longitude
      end


      alert = Alert.new(
          :user_id => user.id,
          :search => params[:search],
          :max_price => (params[:max_price]=="" ? 10000000 : params[:max_price].to_i),
          :radius => (params[:radius]=="" ? 30 : params[:radius].to_i),
          :latitude => (params[:latitude] ? params[:latitude] : user.latitude),
          :longitude => (params[:longitude] ? params[:longitude] : user.longitude),
          :address => params[:address]
      )

      if alert.save
        render json: {"success" => true, "message" => "Alert created", "alert_id" => alert.id}
      end
    else
      render json: {"success" => false, "message" => "Authentication failure"}
    end
  end


  def mobile_alert_remove
    if mobile_auth(params[:user_id], params[:session_id])
      current_user = User.find(params[:user_id])
      alert = Alert.find_by_id_and_user_id(params[:id], current_user.id)

      if alert
        alert.destroy
        render json: {"success" => true, "message" => "Alert removed", "alert_id" => nil}
      else
        render json: {"success" => false, "message" => "Alert not found", "alert_id" => nil}
      end

    else
      render json: {"success" => false, "message" => "Authentication failure"}
    end
  end


  def mobile_posting_categorize
    #params = {product:"samsung", user_id:"2", user_name:"hellofleming", email:"hello@flemingau.com", session_id:"OfpjotjPkXLwCjyTKo410Jlw7NDOGg7ZOPvuBqwtPemmw5aqldUqLcfwhOFzatALZY3cDRc9bLQ0rNewy8k"}

    if mobile_auth(params[:user_id], params[:session_id])
      title = params[:product]

      # strip posting of numbers and other stuff
      title = '' if title.nil?
      title = title.gsub(/[^a-z ]/i, '')

      hard_coded_categories = Posting.get_hard_coded_categories

      topCategory=""
      title.split(" ").each { |word_in_title|
        category = hard_coded_categories[word_in_title]
        categoryPlural = hard_coded_categories[word_in_title[0..-2]]

        # do we have a hardcoded category for this? check if it's a leaf. if not then we don't want to hardcode
        if hard_coded_categories.has_key?(word_in_title) || hard_coded_categories.has_key?(word_in_title[0..-2]) && Posting.is_leaf?(category)
          if categoryPlural.nil? || categoryPlural.blank?
            topCategory=category
          elsif category.nil? || category.blank?
            topCategory=categoryPlural
          end
          break
        end
      }
      topThreeCategories=[]
      topThreeCategories = [topCategory] if not topCategory.blank?

      # sphinx query to get the other three categories for a given user input
      topThreeCategories.concat(UpdatedPosting.search(title).facets[:subcategory_name].sort_by { |key, value| value }.last(3).collect { |x| x[0] }.reverse)
      topThreeCategories=topThreeCategories.uniq

      render json: {"success" => true, "topThreeCategories" => [topThreeCategories[0]]}

    else
      render json: {"success" => false, "message" => "Authentication failure"}
    end

  end

  def mobile_categories_all
    all_leaf_categories= Posting.category_fields.keys.sort
    categories_with_parents=[]
    all_leaf_categories.each { |category|
      #string_with_category_and_ancestors=""
      #ancestry_line=Posting.ancestry_line(category)
      #ancestry_line.each { |ancestor| string_with_category_and_ancestors<<=ancestor +" > " }
      #string_with_category_and_ancestors<<= category
      if Posting.category_parent(category)
        categories_with_parents.push(Posting.category_parent(category) + " > " + category)
      else
        categories_with_parents.push(category)
      end


    }
    render json: {"success" => true, "topThreeCategories" => all_leaf_categories, "categoriesWithParents" => categories_with_parents}

    #def menu_print_leaf(category_leaf)
    #  '{"name":"'+category_leaf + '", "children":[]},'
    #end

    #def menu_print_expanded(category_with_children)
    #  output_json = '{"name":"'+category_with_children + '", "children":['
    #
    #      Posting.immediate_children_of_category(category_with_children).each { |child|
    #        if Posting.is_leaf?(child)
    #          output_json+= menu_print_leaf(child)
    #        else
    #          output_json += menu_print_expanded(child)
    #        end
    #      }
    #      output_json += ']},'
    #      return output_json
    #    end
    #
    #    output_json='{"success":true,"topThreeCategories":['
    #
    #    top_level_categories=Posting.top_level_categories
    #    top_level_categories.each { |top_level_category|
    #      if Posting.is_leaf?(top_level_category)
    #        output_json += menu_print_leaf(top_level_category)
    #      else
    #        output_json += menu_print_expanded(top_level_category)
    #      end
    #    }
    #    output_json += "]}"
    #    render :text=> output_json.gsub("},]", "}]")
  end

  def mobile_fields_get
    category = params[:category]

    category_fields=Posting.category_fields
    fields_for_category=category_fields[category].to_json
    render json: fields_for_category.gsub('{', '[').gsub('}', ']').gsub(':', ',')
  end

  def mobile_fields_get2

    def create_group_hash(group, distance)
      group_hash=group.as_json(:only => [:id, :long_name])
      group_hash["distance"]= distance

      group_hash
    end

    if (user=mobile_auth(params[:user_id], params[:session_id]))
      category = params[:category]

      category_fields=Posting.category_fields
      fields_for_category=category_fields[category].to_json


      groups_user_is_member_of = user.groups

      degToR = Math::PI / 180
      groups_user_is_member_of_hash_array=[]
      groups_user_is_member_of.each do |group|
        distance=::DistanceCalculations.distance_between(user.latitude*degToR, user.longitude*degToR, group.latitude*degToR, group.longitude*degToR).round(1).to_s
        groups_user_is_member_of_hash_array.push(create_group_hash(group, distance))
      end


      render json: {:fields_for_category => fields_for_category.gsub('{', '[').gsub('}', ']').gsub(':', ','),
                    :user_groups => groups_user_is_member_of_hash_array}
    end
  end


  def reverse_geocode(latitude, longitude)
    results=Geocoder.search(latitude.to_s+","+longitude.to_s)
    if top_result = results.first
      return top_result.address, top_result.city, top_result.state, top_result.postal_code
    end
  end


  def mobile_posting_create
    if mobile_auth(params[:user_id], params[:session_id])
      @user = User.find(params[:user_id])

      params[:price] = params[:price].gsub(",", "")

      posting_data=JSON.parse(params[:custom_fields])

      custom_field_list=[]
      posting_data.each_pair { |key, value|
        custom_field_list.push({:label => key, :value => value})
      }

      @posting = @user.postings.build(:title => params[:title], :price => params[:price],
                                      :description => params[:description],
                                      :condition => params[:condition],
                                      :category_name => CGI::unescape(params[:category_name]), :city => params[:city],
                                      :will_accept_other_offers => params[:will_accept_other_offers],
                                      :street1 => params[:address], :street2 => nil)
      @posting.deleted=false
      @posting.view_count=0
      @posting.view_history=0


      lat=Geocoder.coordinates(params[:address])[0]
      lng=Geocoder.coordinates(params[:address])[1]

      coord = lat.to_s + "," + lng.to_s
      location = Geocoder.search(coord).first

      radLatitude = lat.to_f / 180 * Math::PI
      radLongitude = lng.to_f / 180 * Math::PI

      #location = reverse_geocode(params[:latitude], params[:longitude])

      #radLatitude = params[:latitude].to_f / 180 * Math::PI
      #radLongitude = params[:longitude].to_f / 180 * Math::PI

      #@posting.address = (@posting.address == "") ? @user.zip_code : @posting.address #Can't use @posting.address ||= because that checks against nil, not ""
      @posting.address = (params[:address] == "") ? @user.zip_code : params[:address] #Can't use @posting.address ||= because that checks against nil, not ""
      @posting.address = (@posting.address == "") ? location.address : location.postal_code
      @posting.city=location.city
      @posting.zip_code=location.postal_code
      @posting.state=location.state
      @posting.latitude=radLatitude
      @posting.longitude=radLongitude

      #@posting.address="Halsted & Waveland, Chicago, IL 60613"
      #@posting.zip_code="60613"
      #@posting.state="IL"
      #@posting.latitude=0.730979
      #@posting.longitude=-1.52945

      @user.update_attribute(:last_posting_street_address, location.address)
      @user.update_attribute(:last_posting_latitude, radLatitude)
      @user.update_attribute(:last_posting_longitude, radLongitude)


      custom_field_list.each { |custom_field|
		if custom_field.label=="Author" && custom_field.value==""
			 render json: {:success => false, :title => "Error", :message => "Please fill in the author field."} and return
		end
	    @posting.custom_fields.build(custom_field) }

      #build pictures
      index=0
      while params.has_key?("photo"+index.to_s) do
        @posting.pictures.build(:photo => params["photo"+index.to_s])
        index+=1
      end
      @posting.save


      if params[:group_id] && valid_integer?(params[:group_id]) && params[:group_id]!="1"
        @posting.groups<<=Group.find(params[:group_id].to_i)
        @posting.save
      else
        @posting.groups=@user.groups
        @posting.groups<<= Group.find(1)
        @posting.save
      end


      #update picture display order
      picture_display_order="0"
      if @posting.pictures.length>1
        (@posting.pictures.length-1).times { |index|
          picture_display_order<<=","+ (index+1).to_s
        }
      end
      @posting.update_attribute(:picture_display_order, picture_display_order)

      # cache first pictures for reuse on search page
      postingPictures = Picture.where(:imageable_type => "Posting", :imageable_id => @posting.id)
      userPictures = Picture.where(:imageable_type => "User", :imageable_id => @user.id)

      @posting.update_attribute(:first_picture_url, postingPictures[@posting.picture_display_order.split(",").first.to_i].photo.url(:large_thumb))
      begin
        user_picture=userPictures[@user.picture_display_order.split(",").first.to_i].photo.url(:thumb)
      rescue Exception
        user_picture="http://www.reppio.com/assets/default_profile_picture2.png"
      end
      @posting.update_attribute(:first_user_url, user_picture)


      if @posting.save
        Alert.trigger_alert_for(@posting, @user.user_name)
        UpdatedPosting.create!(:title => @posting.title, :subcategory_name => @posting.category_name)

        # cache first pictures for reuse on search page
        postingPictures = Picture.where(:imageable_type => "Posting", :imageable_id => @posting.id)
        userPictures = Picture.where(:imageable_type => "User", :imageable_id => @user.id)


        @posting.update_attribute(:first_picture_url, postingPictures[@posting.picture_display_order.split(",").first.to_i].photo.url(:large_thumb))

        begin
          user_picture=userPictures[@user.picture_display_order.split(",").first.to_i].photo.url(:thumb)
        rescue Exception
          user_picture="http://www.reppio.com/assets/default_profile_picture2.png"
        end
        @posting.update_attribute(:first_user_url, user_picture)

        if not @user.has_made_a_posting
          @user.update_attribute(:has_made_a_posting, true)
          @user.update_attribute(:should_show_manage_tour, true)
        end

        render json: {:success => true, :posting_id => @posting.id, :message => "Loaded"}

        #render :text => "SUCCESS!" and return
      else
        #render :text => "ERROR!" and return
        render json: {:success => false, :title => "Hmmm...", :message => "The posting was unable to save"}
      end
    else
      #render :text => "ERROR!" and return
      render json: {"success" => false, "title" => "Problem!", "message" => "Authentication failure"}
    end
  end
  
  def queue_craigslist_post
   if user=mobile_auth(params[:user_id], params[:session_id])
   posting=Posting.find_by_id_and_user_id(params[:id], user.id)
    Posting.delay.post_to_craigslist(posting.id)
	end
  end


  def mobile_posting_update
    if mobile_auth(params[:user_id], params[:session_id])
      #params[:posting][:group_ids]||=[]

      current_user = User.find(params[:user_id])
      @posting = Posting.find_by_id_and_user_id(params[:id], params[:user_id])

      posting_data=params[:posting]

      #custom_field_list=[]
      #index=1
      #while posting_data.has_key?("custom_field"+index.to_s) do
      #  custom_field_list.push(posting_data.delete("custom_field"+index.to_s))
      #  index+=1
      #end

      posting_data=JSON.parse(params[:custom_fields])

      custom_field_list=[]
      posting_data.each_pair { |key, value|
        custom_field_list.push({:label => key, :value => value})
      }


      if not custom_field_list.empty?
        @posting.custom_fields.destroy_all
        custom_field_list.each { |custom_field| @posting.custom_fields.build(custom_field) }
      end


      #uploadedPictures = params[:posting][:uploaded_pictures].split(",")
      #params[:posting].delete(:uploaded_pictures)
      #
      #if params[:posting] && !uploadedPictures.empty? #FLAG: Possible "security" hole right here if the user edits uploadedPictures manually (using some extension or whatever ... can't trust client data). Issues are wherever we do something relating to bad uploadedPictures data ... 2-3 lines below this, and 7-8 lines below. Really our user would probably only be shooting himself in the foot, but it's something to think about.
      #  i = 0
      #  uploadedPictures.each { |key, value|
      #    params[:posting][:picture_display_order] <<= ("," + (@posting.pictures.length + i).to_s)
      #    i += 1
      #  }
      #
      #  uploadedPictures.map! { |x| x.to_i }.each do |id|
      #    Picture.find_by_imageable_id(id).update_attributes(:imageable_id => @posting.id, :imageable_type => "Posting")
      #  end
      #
      #  # remove comma if comma is first character of picture_display_order, so that after split is called the blank char does not get mapped to 0
      #  if params[:posting][:picture_display_order][0]==','
      #    params[:posting][:picture_display_order][0]=''
      #  end
      #end

      #if @posting.pictures.length>1
      #  (@posting.pictures.length-1).times { |index|
      #    picture_display_order<<=","+ (index+1).to_s
      #  }
      #end

      if @posting.update_attributes(params[:posting])
        #Alert.trigger_alert_for(@posting, current_user.user_name)

        # set flag for removal to true
        @posting.flag_for_deletion=false
        if params[:draft]
          @posting.is_updated=false
        else
          @posting.is_updated=true # adjust if this was a relisted posting
        end
        @posting.save!

        # retrieve incorrectly formatted latitude and longitude and correct
        posting_location = Posting.find_by_id_and_user_id(params[:id], params[:user_id])
        radLatitude = posting_location.latitude.to_f / 180 * Math::PI
        radLongitude = posting_location.longitude.to_f / 180 * Math::PI
        @posting.update_attribute(:latitude, radLatitude)
        @posting.update_attribute(:longitude, radLongitude)


        # cache first picture URLs for reuse on search page
        #postingPictures = Picture.where(:imageable_type => "Posting", :imageable_id => @posting.id)
        #@posting.update_attribute(:first_picture_url, postingPictures[params[:posting][:picture_display_order].split(",").first.to_i].photo.url(:large_thumb))

        #promote
        promotion_methods=params[:promote]||=[]
        promotion_params={}
        promotion_params[:cg]="true" if promotion_methods.include?("Craigslist")
        promotion_params[:id]=@posting.id

        if promotion_methods.include?("Facebook")
          if session[:fb_token]
            begin
              me = FbGraph::User.me(session[:fb_token])
              me.feed!(
                  :message => "Take a look at my listing: #{@posting.title}",
                  :picture => @posting.first_picture_url,
                  :link => user_posting_url(:user_name => current_user.user_name, :id => @posting.id),
                  :name => @posting.title,
                  :description => @posting.description
              )
            rescue Exception
              promotion_params[:header_error] ="no_fb_p"
            end
          else
            promotion_params[:header_error]= "no_fb"
          end
        end

        if (params[:delisted_reason] != nil)

          @posting.update_attribute(:deleted, 1)
          @posting.update_attribute(:delisted_reason, params[:delisted_reason])
          render json: {:success => true, :message => "Your posting has been de-listed."}

        else

          #redirect_to user_posting_url(promotion_params), notice: 'Your posting has been updated.'
          render json: {:success => true, :title => "Applaud!", :message => "Your posting has been updated!"}

        end


      else
        #redirect_to :back, :flash => {:error => 'All fields must be filled in.'}
        render json: {:success => false, :message => "All fields must be filled in."}
      end

    else

      render json: {:success => false, :message => "Authentication Failed"}

    end

  end


  def mobile_posting_get

    #posting = Posting.find(params[:posting_id]).attributes
    posting = Posting.find(params[:posting_id])
    category = posting.category_name

    posting_pictures=[]
    posting_pics = Picture.where(:imageable_id => params[:posting_id], :imageable_type => "Posting")

    category_fields=Posting.category_fields

    fields_for_category=category_fields[category].to_a
    custom_fields = CustomField.where(:customizable_id => params[:posting_id], :customizable_type => "Posting")

    for i in 0..posting_pics.count-1
      #posting_pictures.push(posting_pics.fetch(i).photo.url.gsub("original","medium"))
      posting_pictures.push(posting_pics.fetch(i).photo.url)
    end

    posting["all_images"] = posting_pictures
    posting["fields_for_category"] = fields_for_category
    posting["custom_fields"] = fields_for_category

    pp posting

    if posting
      render json: {:success => true, :data => posting, :message => "Loaded"}
    else
      render json: {:success => false, :message => "Sorry, we were unable to retrieve that posting"}
    end

  end


  def mobile_manager_get

    pp params
    #params[:user_id] = 2

    if mobile_auth(params[:user_id], params[:session_id])

      buying_data = []
      selling_data = []
      completed_data = []

      current_user = User.find(params[:user_id])

      unread_buying_count = Conversation.count(:conditions => "potential_buyer_id = #{current_user.id} AND unread_by_buyer = true")
      unread_selling_count = Conversation.count(:conditions => "seller_id= #{current_user.id} AND unread_by_seller=true")

      if params[:section] == "buying"

        buying_postings = Conversation.find_by_sql("SELECT `conversations`.*, `conversations`.id AS conversation_id,
            `postings`.*, `users`.first_name AS seller_name
          FROM `conversations`, `postings`, `users`
          WHERE (conversations.potential_buyer_id = " + current_user.id.to_s +
                                                       " AND conversations.posting_id = postings.id AND postings.deleted = false
            AND conversations.posting_was_delisted = false
            AND conversations.has_been_completed = false AND conversations.seller_id = users.id)
          ORDER BY conversations.updated_at DESC, postings.created_at DESC")

        #reppio_convenience_percent and reppio_transaction_cost must be floats witha leading zero (0)
        render json: {:success => true, :data => buying_postings.to_json, :reppio_convenience_percent => 0.05, :reppio_transaction_cost => 0.55,
                      :unread_buying_count => unread_buying_count, :unread_selling_count => unread_selling_count}

      elsif params[:section] == "selling"

        #selling_postings =  Posting.where("user_id = ? AND Deleted = ?", current_user.id, false).order("updated_at DESC, id ASC")
        selling_postings = Posting.find_by_sql("SELECT `postings`.*, `postings`.id AS posting_id
          FROM `postings`
          LEFT OUTER JOIN `conversations` ON `conversations`.`posting_id` = `postings`.`id`
          WHERE (user_id= " + current_user.id.to_s + " AND `postings`.`deleted` = false)
          GROUP BY `postings`.`id`
          ORDER BY postings.created_at DESC")

        selling_postings_hash_array=[]
        selling_postings.each do |posting|
          seller=User.find(posting.user_id)
          posting_hash=posting.as_json(:only => [:id, :title, :price, :first_picture_url, :requested_price, :fulfilled])
          posting_hash["unread_by_seller"] =(Conversation.count(:conditions => "posting_id = #{posting.id} AND unread_by_seller = true") >0)

          selling_postings_hash_array.push(posting_hash)

        end

        render json: {:success => true, :data => selling_postings_hash_array.to_json,
                      :unread_buying_count => unread_buying_count, :unread_selling_count => unread_selling_count}

      else

        # doing it this way maintains order. first having postings
        completedPostingsBuying = Posting.where("actual_buyer_user_id = ? AND hide_from_buyer_completed = ?", current_user.id, false)
        completedPostingsSelling = Posting.where("user_id = ? AND deleted = ? AND hide_from_seller_completed = ?", current_user.id, true, false)
        completed_postings = completedPostingsBuying + completedPostingsSelling
        render json: {:success => true, :data => completed_postings.to_json,
                      :unread_buying_count => unread_buying_count, :unread_selling_count => unread_selling_count}

      end

    else

      render json: {:success => false, :message => "Authentication Failed"}

    end

  end


  def mobile_messages_get

    #params[:user_id] = 2

    if mobile_auth(params[:user_id], params[:session_id])

      current_user = User.find(params[:user_id])
      conversation = Conversation.find(params[:conversation_id])

      if current_user.id == conversation.seller_id
        conversation.update_attribute("unread_by_seller", false)
      end

      if current_user.id == conversation.potential_buyer_id
        conversation.update_attribute("unread_by_buyer", false)
      end

      seller_info = User.find(conversation.seller_id)
      seller_name = seller_info.first_name
      seller_paypal_email = seller_info.email
      seller_pic = seller_info.first_user_url

      posting_info = Posting.find(conversation.posting_id)
      posting_available = !posting_info.deleted && posting_info.fulfilled.nil?
      posting_payment_acceptable = posting_info.requested_price.to_i > 0 && !posting_info.requested_price.nil?
      display_buy_buttons = (posting_info.user_id_with_permission_to_buy === current_user.id && posting_available && posting_payment_acceptable)

      buyer_info = User.find(conversation.potential_buyer_id)
      buyer_name = buyer_info.first_name
      buyer_pic = buyer_info.first_user_url
      buyer_id = buyer_info.id

      conversation_data = {:seller_id => conversation.seller_id, :buyer_id => buyer_id,
                           :posting_id => conversation.posting_id, :buyer_name => buyer_name,
                           :buyer_pic => buyer_pic, :seller_name => seller_name,
                           :seller_pic => seller_pic, :seller_email => seller_paypal_email,
                           :posting_available => posting_available,
                           :posting_payment_acceptable => posting_payment_acceptable,
                           :display_buy_buttons => display_buy_buttons}
      message_data = Message.where("conversation_id=?", params[:conversation_id]).order("updated_at DESC").limit(15).reverse

      current_user_is_seller = (current_user.id == conversation.seller_id) ? true : false

      unread_buying_count = Conversation.count(:conditions => "potential_buyer_id = #{current_user.id} AND unread_by_buyer = true")
      unread_selling_count = Conversation.count(:conditions => "seller_id= #{current_user.id} AND unread_by_seller=true")


      data = {:conversation => conversation_data, :messages => message_data, :current_user_is_seller => current_user_is_seller,
              :unread_buying_count => unread_buying_count, :unread_selling_count => unread_selling_count}

      render json: {:success => true, :data => data}

    else

      render json: {:success => false, :message => "Authentication Failed"}

    end

  end


  def mobile_posting_agree_sell

    if mobile_auth(params[:user_id], params[:session_id])

      posting = Posting.find(params[:posting_id])

      if (posting.requested_price.to_i == 0 && posting.user_id_with_permission_to_buy == 0)
        conversation = Conversation.find(params[:conversation_id])
        potential_buyer_id = conversation.potential_buyer_id
        seller = User.find(params[:user_id])

        posting.update_column(:requested_price, params[:requested_price])
        posting.update_column(:fund_bank_account_id, params[:selected_bank_account_id])
        posting.update_column(:user_id_with_permission_to_buy, potential_buyer_id)

        #Send push notification to recipient
        user_mobile_device = UserMobileDevice.find_by_user_id(potential_buyer_id)

        if (!user_mobile_device.nil?)
          device_tokens = [user_mobile_device.device_token]
          #app_key = "v88QG8SnTgy4OAE10Cpwsg"             #development
          #app_secret = "dZc-R7QvSIa9dL2vd18rSQ"          #development
          #app_secret_master = "6ZZ-TEq9TGOpTB2LCHy6iA"   #development
          app_key = "Gp6xyenNQQOhzWEnMBNfmw" #production
          app_secret = "fsa7FfiQT5e59uiBvKUMfA" #production
          app_secret_master = "h8zfX6sjRvyAzkiBnk8i-g" #production

          push_host = "https://go.urbanairship.com"
          push_url = "/api/push/"
          push_message = seller.first_name + " agrees to sell for $" + posting.requested_price.to_s
          conversation_data = {:msgid => nil}

          unread_buying_count = Conversation.count(:conditions => "potential_buyer_id = #{params[:user_id]} AND unread_by_buyer = true")
          unread_selling_count = Conversation.count(:conditions => "seller_id= #{params[:user_id]} AND unread_by_seller=true")
          badges = unread_buying_count + unread_selling_count

          push_params = {:app_key => app_key, :app_secret => app_secret_master, :push_host => push_host,
                         :push_url => push_url, :device_tokens => device_tokens,
                         :message => push_message, :recipient => potential_buyer_id, :add => conversation_data,
                         :badges => badges}

          push_send_message(push_params)
        end

        buyer=User.find(potential_buyer_id)
        PaymentMailer.payment_has_been_requested(buyer.email, buyer.first_name, posting.title,
                                                 user_posting_url(:user_name => buyer.user_name, :id => posting.id),
                                                 buy_url(:posting_id => posting.id)).deliver


        render json: {:success => true, :status_code => "200", :title => "You made a deal!", :message => "Now we just have to wait for the buyer to send over the payment."}

      else

        render json: {:success => false, :status_code => "403", :title => "Sale Already Pending!", :message => "You already agreed to sell this to someone else"}

      end

    else

      render json: {:success => false, :status_code => "401", :title => "Authentication Fail", :message => "You may need to logout and log back in"}

    end

  end


  def mobile_posting_cancel_sell

    if mobile_auth(params[:user_id], params[:session_id])

      posting = Posting.find_by_id_and_user_id(params[:posting_id], params[:user_id])
      potential_buyer = User.find(posting.user_id_with_permission_to_buy)
      potential_buyer_name = potential_buyer.first_name
      posting.update_column(:requested_price, 0)
      posting.update_column(:fund_bank_account_id, nil)
      posting.update_column(:user_id_with_permission_to_buy, 0)

      render json: {:success => true, :status_code => "200", :title => "Cancelled Offer to Sell", :message => "You have just retracted your offer to sell to " + potential_buyer_name}

    else

      render json: {:success => false, :status_code => "401", :title => "Authentication Fail", :message => "You may need to logout and log back in"}

    end

  end


  def mobile_payments_transact_cc

    if mobile_auth(params[:user_id], params[:session_id])

      #params = { :user_id => 2, :selected_card_id => 3, :posting_id => 12,
      #          #:credit => 1, :seller_id => 1, #:appears_on_statement_as => "Test Appears As Value",
      #          #:description => "This is the transaction description",
      #}

      posting = Posting.find(params[:posting_id])
      approved_buyer = posting.user_id_with_permission_to_buy

      if (params[:user_id].to_i === approved_buyer.to_i)

        seller_id = posting.user_id
        requested_price = posting.requested_price

        if requested_price.nil? || requested_price === 0
          render json: {:success => false, :status_code => "500", :title => "Agreed selling price missing!", :message => "The seller has to enter a price you both agree with"}
          return
        end

        seller_info = User.find(seller_id)
        buyer_info = User.find(params[:user_id])

        transaction_description = "Transaction between " + buyer_info.first_name + " " + buyer_info.last_name[0, 1] + ". and " + seller_info.first_name + " " + seller_info.last_name[0, 1] + ". for " + posting.title
        appears_on_statement_as = "Purchase via Reppio" #22 character limit

        total_cost = (((requested_price * 1.05) + 0.55) * 100).round(0) #must be an integer
        agreed_price = ((total_cost - 55) / 1.05)
        processing_fee = ((agreed_price * 0.05) + 55)

        #puts "X: " + total_cost.to_s
        #puts "Y: " + requested_price.to_s
        #puts "Z: " + processing_fee.to_s

        require 'balanced'
        #Balanced.configure('514675a273d511e29df4026ba7c1aba6') #development
        Balanced.configure('2df3bd6a7adf11e2874c026ba7cac9da') #production

        #debit buyer's credit card
        buyer_credit_card_uri = BalancedCreditCard.find_by_id_and_user_id(params[:selected_card_id], buyer_info.id).uri
        buyer_credit_card = Balanced::Account.find(buyer_credit_card_uri)
        buyer_credit_card.debit(
            :appears_on_statement_as => appears_on_statement_as,
            :amount => total_cost,
            :description => transaction_description,
        )

        #credit fees to reppio balanced account
        reppio_bank_account_uri = Balanced::Marketplace.my_marketplace.owner_account_uri
        reppio_bank_account = Balanced::BankAccount.find(reppio_bank_account_uri)

        pp processing_fee
        reppio_bank_account.credit(processing_fee.to_i)

        #credit agreed amount to seller balanced account
        seller_bank_account_uri = BalancedAccount.find_by_user_id(seller_id).uri
        seller_bank_account = Balanced::BankAccount.find(seller_bank_account_uri)
        seller_bank_account.credit(agreed_price)

        #mark the posting as sold
        posting.update_column(:fulfilled, 1)
        posting.update_column(:actual_price, requested_price)
        posting.update_column(:delisted_reason, "soldHere")
        posting.update_column(:actual_buyer_user_id, buyer_info.id)

        #mark conversation as completed
        conversation = Conversation.find_by_potential_buyer_id_and_seller_id_and_posting_id(buyer_info.id, seller_id, posting.id)
        conversation.update_attribute(:has_been_completed, 1)

        PaymentMailer.payment_complete(seller_info.email, seller_info.first_name, posting.title,
                                       user_posting_url(:user_name => seller_info.user_name, :id => posting.id),
                                       requested_price, buyer_info.first_name, buyer_info.user_name).deliver

        #render json: { :seller => seller_bank_account, :reppio => reppio_bank_account, :buyer => buyer_credit_card }
        render json: {:success => true, :status_code => "200", :title => "Sweeeeeet!", :message => "Enjoy your purchase!"}


        user_mobile_device = UserMobileDevice.find_by_user_id(seller_id)

        if !user_mobile_device.nil?
          #Send push notification to recipient
          #app_key = "v88QG8SnTgy4OAE10Cpwsg"             #development
          #app_secret = "dZc-R7QvSIa9dL2vd18rSQ"          #development
          #app_secret_master = "6ZZ-TEq9TGOpTB2LCHy6iA"   #development
          app_key = "Gp6xyenNQQOhzWEnMBNfmw" #production
          app_secret = "fsa7FfiQT5e59uiBvKUMfA" #production
          app_secret_master = "h8zfX6sjRvyAzkiBnk8i-g" #production

          push_host = "https://go.urbanairship.com"
          push_url = "/api/push/"
          device_tokens = [user_mobile_device.device_token]
          push_message = "You have just been paid $" + requested_price.to_s + " by " + buyer_info.first_name + " " + buyer_info.last_name[0] + "."
          conversation_data = {:msgid => nil}

          unread_buying_count = Conversation.count(:conditions => "potential_buyer_id = #{params[:user_id]} AND unread_by_buyer = true")
          unread_selling_count = Conversation.count(:conditions => "seller_id= #{params[:user_id]} AND unread_by_seller=true")
          badges = unread_buying_count + unread_selling_count

          push_params = {:app_key => app_key, :app_secret => app_secret_master, :push_host => push_host,
                         :push_url => push_url, :device_tokens => device_tokens, :badges => badges,
                         :message => push_message, :recipient => seller_id, :add => conversation_data}

          push_send_message(push_params)
        end


      else

        render json: {:success => false, :status_code => "401.3", :title => "Seller doesn't agree to sell!", :message => "They may have realized they just can't live without it"}

      end

    else

      render json: {:success => false, :status_code => "401", :title => "Authentication Fail", :message => "You may need to logout and log back in"}

    end

  end

  def mobile_payments_create_account(params)

    #if mobile_auth(params[:user_id], params[:session_id])

    require 'balanced'
    #Balanced.configure('514675a273d511e29df4026ba7c1aba6') #development
    Balanced.configure('2df3bd6a7adf11e2874c026ba7cac9da') #production

    begin
      marketplace = Balanced::Marketplace.my_marketplace.create_account(
          :name => params[:user_full_name],
          :email_address => params[:email_address],
          :meta => params[:meta],
      #:email_address => "testing0051@dfsfjhsfkjshf.com",
      )

      marketplace_attributes = marketplace.attributes

      BalancedAccount.create(:user_id => params[:user_id],
                             :account_id => marketplace_attributes[:id],
                             :uri => marketplace_attributes[:uri]
      )

      return marketplace_attributes

        #rescue Balanced::Conflict => ex
    rescue Balanced::Error => error
      #marketplace = Balanced::Marketplace.mine

      #render json: { :success => false, :title => "Oops!", :message => ex.description, :status_code => ex.status_code }
      puts error.description
      response = {:success => false, :title => "Hmmm...?", :message => error.description, :status_code => error.status_code}
      return response

    end

    #else
    #
    #  render json: {:success => false, :status_code => "401", :title => "Authentication Fail", :message => "You may need to logout and log back in"}
    #
    #end

  end

  def mobile_payments_create_bank(params)

    #if mobile_auth(params[:user_id], params[:session_id])

    require 'balanced'
    #Balanced.configure('514675a273d511e29df4026ba7c1aba6') #development
    Balanced.configure('2df3bd6a7adf11e2874c026ba7cac9da') #production

    begin

      bank_account = Balanced::BankAccount.new(
          :routing_number => params[:routing_number],
          :type => params[:type],
          :name => params[:account_name],
          :account_number => params[:account_number],
          :meta => {:nickname => params[:nickname]},
      #:routing_number => "121000358",
      #:type => "checking",
      #:name => "Wilma Flinstone Six",
      #:account_number => " ",
      ).save

      bank_account_attributes = bank_account.attributes

      BalancedBankAccount.create(:user_id => params[:user_id],
                                 :account_id => params[:account_id],
                                 :account_number => bank_account_attributes[:account_number],
                                 :bank_name => bank_account_attributes[:bank_name],
                                 :credits_uri => bank_account_attributes[:credits_uri],
                                 :fingerprint => bank_account_attributes[:fingerprint],
                                 :bank_account_id => bank_account_attributes[:id],
                                 :meta => bank_account_attributes[:meta].to_s.gsub("--- ", "").gsub("=>", ":"),
                                 :name => bank_account_attributes[:name],
                                 #:routing_number => bank_account_attributes[:routing_number],
                                 :account_type => bank_account_attributes[:type],
                                 :uri => bank_account_attributes[:uri]
      )

      return bank_account_attributes

    rescue Balanced::Error => error
      response = {:success => false, :title => "Hmmm...?", :message => error.description, :status_code => error.status_code}
      return response
    end

    #else
    #
    #  render json: {:success => false, :status_code => "401", :title => "Authentication Fail", :message => "You may need to logout and log back in"}
    #
    #end

  end

  def mobile_payments_create_card(params)
    #if mobile_auth(params[:user_id], params[:session_id])
    require 'balanced'
    #Balanced.configure('514675a273d511e29df4026ba7c1aba6') #development
    Balanced.configure('2df3bd6a7adf11e2874c026ba7cac9da') #production

    begin

      credit_card = Balanced::Card.new(
          :name => params[:name_on_card],
          :card_number => params[:card_number],
          :expiration_month => params[:expiration_month],
          :expiration_year => params[:expiration_year],
          :security_code => params[:security_code],
          :meta => {
              :zip_code => params[:zip_code],
              :nickname => params[:nickname]
          },
      #:name => params[:name_on_card],
      #:security_code => '123',
      #:card_number => '5105105105105100',
      #:expiration_month => '12',
      #:expiration_year => '2020',
      ).save

      credit_card_attributes = credit_card.attributes

      #puts "fleming3d: " + credit_card_attributes.to_s

      BalancedCreditCard.create(:user_id => params[:user_id],
                                :account_id => params[:account_id],
                                :brand => credit_card_attributes[:brand],
                                :card_type => credit_card_attributes[:card_type],
                                :expiration_month => credit_card_attributes[:expiration_month],
                                :expiration_year => credit_card_attributes[:expiration_year],
                                :balanced_hash => credit_card_attributes[:hash],
                                :credit_card_id => credit_card_attributes[:id],
                                :is_valid => credit_card_attributes[:is_valid],
                                :last_four => credit_card_attributes[:last_four],
                                :meta => credit_card_attributes[:meta].to_s.gsub("--- ", "").gsub("=>", ":"),
                                :name => credit_card_attributes[:name],
                                :uri => credit_card_attributes[:uri]
      )

      return credit_card_attributes

    rescue Balanced::Error => error

      response = {:success => false, :title => "Hmmm...?", :message => error.description, :status_code => error.status_code}
      return response

    end

    #else
    #
    #  render json: {:success => false, :status_code => "401", :title => "Authentication Fail", :message => "You may need to logout and log back in"}
    #
    #end
  end


  def mobile_payments_update_card
    if mobile_auth(params[:user_id], params[:session_id])
      require 'balanced'
      #Balanced.configure('514675a273d511e29df4026ba7c1aba6') #development
      Balanced.configure('2df3bd6a7adf11e2874c026ba7cac9da') #production

      begin

        selected_card = BalancedCreditCard.find(params[:selected_card_id])
        selected_card_uri = selected_card.uri
        credit_card = Balanced::Card.find(selected_card_uri)

        credit_card.expiration_month = params[:expiration_month]
        credit_card.expiration_year = params[:expiration_year]
        credit_card.name = params[:name]
        credit_card.meta = {:zip_code => params[:zip_code]}
        credit_card.save

        credit_card_account_attributes = credit_card.attributes[:account].attributes
        credit_card_attributes = credit_card.attributes

        selected_card.update_attributes(:expiration_month => credit_card_attributes[:expiration_month])
        selected_card.update_attributes(:expiration_year => credit_card_attributes[:expiration_year])
        selected_card.update_attributes(:name => credit_card_attributes[:name])
        selected_card.update_attributes(:meta => credit_card_attributes[:meta].to_s.gsub("--- ", "").gsub("=>", ":"))

        render json: {:success => true, :status_code => "200", :title => "Credit Card Updated!", :message => "Yeah! Make sure that info is up to date!", :data => credit_card_attributes}

      rescue Balanced::Error => error

        render json: {:success => false, :status_code => error.status_code, :title => "Hmmm...?", :message => error.description}

      end

    else

      render json: {:success => false, :status_code => "401", :title => "Authentication Fail", :message => "You may need to logout and log back in"}

    end
  end


  def mobile_payments_set_seller
    if mobile_auth(params[:user_id], params[:session_id])

      require 'balanced'
      #Balanced.configure('514675a273d511e29df4026ba7c1aba6') #development
      Balanced.configure('2df3bd6a7adf11e2874c026ba7cac9da') #production

      current_user = User.find(params[:user_id])

      user_balanced_account = BalancedAccount.find_by_user_id(params[:user_id])

      if user_balanced_account.nil?
        params["user_full_name"] = current_user.first_name
        params["email"] = current_user.email
        balanced_account = self.mobile_payments_create_account(params)

        if !balanced_account.nil? && balanced_account[:success].nil?
          balanced_account_uri = balanced_account[:uri]
          balanced_account_id = balanced_account[:id]
        else
          render json: {:success => false, :status_code => "401", :title => "Hmmmm... weird", :message => balanced_account[:message]}
          return
        end

      else
        balanced_account_uri = user_balanced_account.uri
        balanced_account_id = user_balanced_account.account_id
      end

      params["account_id"] = balanced_account_id
      bank_account = self.mobile_payments_create_bank(params)

      if !bank_account.nil? && bank_account[:success].nil?
        bank_account_uri = bank_account[:uri]
      else
        render json: {:success => false, :status_code => "401", :title => "Hmmmm... weird", :message => bank_account[:message]}
        return
      end

      puts "merchant account: " + balanced_account_uri
      puts "merchant bank account: " + bank_account_uri

      account = Balanced::Account.find(balanced_account_uri)
      account.add_bank_account(bank_account_uri)

      render json: {:success => true, :title => "Bank Account Added", :message => "Now go fill it up! Sell, sell, sell!", :data => account.attributes}

    else

      render json: {:success => false, :status_code => "401", :title => "Authentication Fail", :message => "You may need to logout and log back in"}

    end

  end

  def mobile_payments_set_buyer
    if mobile_auth(params[:user_id], params[:session_id])

      require 'balanced'
      #Balanced.configure('514675a273d511e29df4026ba7c1aba6')
      #Balanced.configure('514675a273d511e29df4026ba7c1aba6', :connection_timeout => 30, :read_timeout => 30)

      Balanced.configure('2df3bd6a7adf11e2874c026ba7cac9da', :connection_timeout => 30, :read_timeout => 30) #production

      current_user = User.find(params[:user_id])

      user_balanced_account = BalancedAccount.find_by_user_id(params[:user_id])

      if user_balanced_account.nil?
        params["user_full_name"] = current_user.first_name
        params["email"] = current_user.email
        balanced_account = self.mobile_payments_create_account(params)

        #if !bank_account.nil? && bank_account[:success].nil?
        if !balanced_account.nil? && balanced_account[:success].nil?
          balanced_account_uri = balanced_account[:uri]
          balanced_account_id = balanced_account[:id]
        else
          render json: {:success => false, :status_code => "401", :title => "Hmmmm... weird", :message => bank_account[:message]}
          return
        end

      else
        balanced_account_uri = user_balanced_account.uri
        balanced_account_id = user_balanced_account.account_id
      end

      params["account_id"] = balanced_account_id
      credit_card = self.mobile_payments_create_card(params)

      if !credit_card.nil? && credit_card[:success].nil?
        credit_card_uri = credit_card[:uri]
      else
        render json: {:success => false, :status_code => "401", :title => "Hmmmm... weird", :message => credit_card[:message]}
        return
      end

      puts "buyer account: " + balanced_account_uri
      puts "buyer credit card: " + credit_card_uri

      account = Balanced::Account.find(balanced_account_uri)
      account.add_card(credit_card_uri)

      render json: {:success => true, :title => "Credit Card Added!", :message => "Go ahead and buy something - you deserve it.", :data => account.attributes}

    else

      render json: {:success => false, :status_code => "401", :title => "Authentication Fail", :message => "You may need to logout and log back in"}

    end

  end

  def mobile_payments_get_cards
    if mobile_auth(params[:user_id], params[:session_id])

      #params = {:user_id => 1}
      user_credit_cards = BalancedCreditCard.find_all_by_user_id_and_is_valid(params[:user_id], true)

      pp "yoooob: " + user_credit_cards.length.to_s

      if user_credit_cards.length > 0 && !user_credit_cards.nil?
        results = {:success => true, :status_code => "200", :data => user_credit_cards}
        render json: results, :only => [:success, :status_code, :data, :id, :brand, :card_type, :expiration_month, :expiration_year, :is_valid, :last_four, :name, :meta]
      else
        render json: {:success => false, :status_code => "404", :title => "No credit cards found", :message => "You must store a credit card to make a purchase"}
      end

    else

      render json: {:success => false, :status_code => "401", :title => "Authentication Fail", :message => "You may need to logout and log back in"}

    end
  end

  def mobile_payments_get_card
    if mobile_auth(params[:user_id], params[:session_id])

      #params = {:user_id => 1}
      user_credit_card = BalancedCreditCard.find_by_id_and_user_id(params[:selected_card_id], params[:user_id])

      if !user_credit_card.nil?
        results = {:success => true, :status_code => "200", :data => user_credit_card}
        render json: results, :only => [:success, :status_code, :data, :id, :brand, :card_type, :expiration_month, :expiration_year, :is_valid, :last_four, :name, :meta]
      else
        render json: {:success => false, :status_code => "404", :title => "Unable to find card", :message => "Please try again in a minute"}
      end

    else

      render json: {:success => false, :status_code => "401", :title => "Authentication Fail", :message => "You may need to logout and log back in"}

    end
  end

  def mobile_payments_invalidate_card
    if mobile_auth(params[:user_id], params[:session_id])
      require 'balanced'
      #Balanced.configure('514675a273d511e29df4026ba7c1aba6') #development
      Balanced.configure('2df3bd6a7adf11e2874c026ba7cac9da') #production

      begin

        selected_card = BalancedCreditCard.find(params[:selected_card_id])
        selected_card_uri = selected_card.uri
        credit_card = Balanced::Card.find(selected_card_uri)
        credit_card.invalidate

        credit_card_attributes = credit_card.attributes[:account].attributes

        selected_card.update_attributes(:is_valid => false)

        render json: {:success => true, :status_code => "200", :title => "Credit Card Removed!", :message => "Make sure you have at least one stored though", :data => credit_card_attributes}

      rescue Balanced::Error => error

        render json: {:success => false, :status_code => error.status_code, :title => "Hmmm...?", :message => error.description}

      end

    else

      render json: {:success => false, :status_code => "401", :title => "Authentication Fail", :message => "You may need to logout and log back in"}

    end
  end

  def mobile_payments_get_bankAccounts
    if mobile_auth(params[:user_id], params[:session_id])

      #params = {:user_id => 1}
      user_bank_accounts = BalancedBankAccount.find_all_by_user_id(params[:user_id])

      if !user_bank_accounts.nil? && !user_bank_accounts.empty?
        results = {:success => true, :status_code => "200", :data => user_bank_accounts}
        render json: results, :only => [:success, :status_code, :data, :id, :account_number, :bank_name, :meta, :name, :account_type]
      else
        render json: {:success => false, :status_code => "404", :title => "No bank accounts found", :message => "You must store a bank account to receive payment"}
      end

    else

      render json: {:success => false, :status_code => "401", :title => "Authentication Fail", :message => "You may need to logout and log back in"}

    end
  end

  def mobile_payments_get_bankAccount
    if mobile_auth(params[:user_id], params[:session_id])

      #params = {:user_id => 1}
      user_bank_account = BalancedBankAccount.find_by_id_and_user_id(params[:selected_bank_account_id], params[:user_id])

      if !user_bank_account.nil?
        results = {:success => true, :status_code => "200", :data => user_bank_account}
        render json: results, :only => [:success, :status_code, :data, :id, :account_number, :bank_name, :meta, :name, :account_type]
      else
        render json: {:success => false, :status_code => "404", :title => "Unable to find bank account", :message => "Please try again in a minute"}
      end

    else

      render json: {:success => false, :status_code => "401", :title => "Authentication Fail", :message => "You may need to logout and log back in"}

    end
  end

  def mobile_payments_delete_bankAccount
    if mobile_auth(params[:user_id], params[:session_id])
      require 'balanced'
      #Balanced.configure('514675a273d511e29df4026ba7c1aba6') #development
      Balanced.configure('2df3bd6a7adf11e2874c026ba7cac9da') #production

      begin

        selected_bank_account = BalancedBankAccount.find(params[:selected_bank_account_id])
        selected_bank_account_uri = selected_bank_account.uri
        bank_account = Balanced::Card.find(selected_bank_account_uri)
        bank_account.invalidate

        puts bank_account.to_s

        bank_account_attributes = bank_account.attributes[:account].attributes

        selected_bank_account.destroy

        render json: {:success => true, :status_code => "200", :title => "Bank Account Removed!", :message => "Make sure you have at least one stored though", :data => bank_account_attributes}

      rescue Balanced::Error => error

        render json: {:success => false, :status_code => error.status_code, :title => "Hmmm...?", :message => error.description}

      end

    else

      render json: {:success => false, :status_code => "401", :title => "Authentication Fail", :message => "You may need to logout and log back in"}

    end
  end


  def mobile_transaction_complete
    if mobile_auth(params[:user_id], params[:session_id])

      posting = Posting.find(params[:id])

      buyer_info = User.find(params[:user_id])
      seller_info = User.find(posting.user_id)

      #mark the posting as sold
      posting.update_attributes(:fulfilled => 1, :sold_on_paypal => 1, :delisted_reason => "soldHere", :actual_buyer_user_id => buyer_info.id, :actual_price => posting.requested_price)

      #mark conversation as completed
      conversation = Conversation.find_by_potential_buyer_id_and_seller_id_and_posting_id(buyer_info.id, seller_info.id, posting.id)
      conversation.update_attribute(:has_been_completed, 1)

      PaymentMailer.payment_complete(seller_info.email, seller_info.first_name, posting.title,
                                     user_posting_url(:user_name => seller_info.user_name, :id => posting.id),
                                     posting.requested_price, buyer_info.first_name, buyer_info.user_name).deliver


      user_mobile_device = UserMobileDevice.find_by_user_id(seller_info.id)

      if !user_mobile_device.nil?
        #Send push notification to recipient
        #app_key = "v88QG8SnTgy4OAE10Cpwsg"             #development
        #app_secret = "dZc-R7QvSIa9dL2vd18rSQ"          #development
        #app_secret_master = "6ZZ-TEq9TGOpTB2LCHy6iA"   #development
        app_key = "Gp6xyenNQQOhzWEnMBNfmw" #production
        app_secret = "fsa7FfiQT5e59uiBvKUMfA" #production
        app_secret_master = "h8zfX6sjRvyAzkiBnk8i-g" #production

        push_host = "https://go.urbanairship.com"
        push_url = "/api/push/"
        device_tokens = [user_mobile_device.device_token]
        push_message = "You have just been paid $" + posting.requested_price.to_s + " by " + buyer_info.first_name + " " + buyer_info.last_name[0] + "."
        conversation_data = {:msgid => nil}

        unread_buying_count = Conversation.count(:conditions => "potential_buyer_id = #{seller_info.id} AND unread_by_buyer = true")
        unread_selling_count = Conversation.count(:conditions => "seller_id= #{seller_info.id} AND unread_by_seller=true")
        badges = unread_buying_count + unread_selling_count

        push_params = {:app_key => app_key, :app_secret => app_secret_master, :push_host => push_host,
                       :push_url => push_url, :device_tokens => device_tokens, :badges => badges,
                       :message => push_message, :recipient => seller_id, :add => conversation_data}

        push_send_message(push_params)
      end


      render json: {:success => true, :message => "Hope you had a great transaction"}

    else
      render json: {:success => false, :status_code => "401", :message => "Authentication Failed"}
    end
  end


  #def mobile_manager_buying
  #  #params = {user_id:"2"}
  #
  #  if mobile_auth(params[:user_id], params[:session_id])
  #    current_user = User.find(params[:user_id])
  #
  #    data = []
  #    order = ""
  #
  #    conversations = Conversation.where("potential_buyer_id = ? AND has_been_completed = ?", current_user.id, false).order("unread_by_buyer DESC,updated_at DESC")
  #    conversations_count = conversations.count
  #
  #    conversations.each do |conversation|
  #
  #      if conversation.hide_from_buyer == 1
  #        conversations_count = conversations_count - 1
  #        next
  #      end
  #
  #      latest_update = 10.years.ago
  #      posting = Posting.find(conversation.posting_id)
  #
  #      if conversation.messages.reverse.first.updated_at > latest_update
  #        latest_update = conversation.messages.reverse.first.updated_at
  #      end
  #
  #      if conversation.updated_at > latest_update
  #        latest_update = conversation.updated_at
  #      end
  #
  #      if posting.updated_at > latest_update
  #        latest_update = posting.updated_at
  #      end
  #
  #      data.push({:posting => posting, :conversation => conversation, :messages => conversation.messages.reverse, :latestUpdate => latest_update})
  #
  #    end
  #
  #    if (conversations_count > 0)
  #      render json: {:success => true, :data => data, :conversationsCount => conversations_count}
  #    else
  #      render json: {:success => false, :message => "No conversations exist"}
  #    end
  #
  #
  #  else
  #    render json: {:success => false, :message => "Authentication Failed"}
  #
  #  end
  #
  #end
  #
  #
  #def mobile_manager_selling
  #
  #  if mobile_auth(params[:user_id], params[:session_id])
  #    current_user = User.find(params[:user_id])
  #
  #    data = []
  #    order = ""
  #
  #    postings = Posting.find_by_sql("SELECT `postings`.* FROM `postings` LEFT OUTER JOIN `conversations` ON `conversations`.`posting_id` = `postings`.`id` WHERE (user_id= " + current_user.id.to_s + " AND `postings`.`deleted` = false ) GROUP BY `postings`.`id` ORDER BY conversations.updated_at DESC, postings.created_at DESC")
  #    unreadSellingCount = Conversation.find_by_sql("SELECT * FROM `conversations`, `postings` WHERE ((conversations.seller_id = " + current_user.id.to_s + " AND conversations.unread_by_seller = true AND conversations.posting_id = postings.id AND postings.deleted != true))").count
  #    posting_count = postings.count
  #    latest_update = 10.years.ago
  #
  #    pp "Show postings:"
  #    pp postings
  #
  #    postings.each do |posting|
  #      conversation = Conversation.find_by_posting_id(posting.id)
  #      messages = Message.where("conversation_id = ?", conversation.id).reverse
  #
  #      if conversation.updated_at > latest_update
  #        latest_update = conversation.updated_at
  #      end
  #
  #      if posting.updated_at > latest_update
  #        latest_update = posting.updated_at
  #      end
  #
  #      data.push({:posting => posting, :conversation => conversation, :messages => conversation.messages.reverse, :latestUpdate => latest_update})
  #    end
  #
  #    conversations = Conversation.where("seller_id = ? AND has_been_completed = ?", current_user.id, false).order("unread_by_buyer DESC,updated_at DESC")
  #    conversations_count = conversations.count
  #
  #    conversations.each do |conversation|
  #
  #      if conversation.hide_from_buyer == 1
  #        conversations_count = conversations_count - 1
  #        next
  #      end
  #
  #      latest_update = 10.years.ago
  #      posting = Posting.find(conversation.posting_id)
  #
  #      if conversation.messages.reverse.first.updated_at > latest_update
  #        latest_update = conversation.messages.reverse.first.updated_at
  #      end
  #
  #      if conversation.updated_at > latest_update
  #        latest_update = conversation.updated_at
  #      end
  #
  #      if posting.updated_at > latest_update
  #        latest_update = posting.updated_at
  #      end
  #
  #      data.push({:posting => posting, :conversation => conversation, :messages => conversation.messages.reverse, :latestUpdate => latest_update})
  #
  #    end
  #
  #    if (conversations_count > 0)
  #      render json: {:success => true, :data => data, :sellingCount => posting_count}
  #    else
  #      render json: {:success => false, :message => "No conversations exist"}
  #    end
  #
  #
  #  else
  #    render json: {:success => false, :message => "Authentication Failed"}
  #
  #  end
  #
  #end


  def mobile_conversations_get
    if mobile_auth(params[:user_id], params[:session_id])

      #conversations = Conversation.find_by_sql("SELECT C.*, C.ID AS conversation_id, U.*, U.ID AS user_id
      #  FROM Conversations C, Users U
      #  WHERE C.posting_id = " + params[:posting_id] + " AND C.seller_id = " + params[:user_id] + " AND C.potential_buyer_id = U.ID
      #  ORDER BY C.ID DESC")

      data = []
      conversations = Conversation.where("posting_id = ? AND seller_id = ?", params[:posting_id], params[:user_id]).order("updated_at DESC")

      all_custom_fields = Posting.category_fields

      posting = Posting.find(params[:posting_id])
      posting["custom_fields"] = CustomField.find_all_by_customizable_id_and_customizable_type(posting.id, "posting")
      posting["all_images"] = Picture.find_all_by_imageable_id_and_imageable_type(posting.id, "posting")
      posting["fields_for_category"] = all_custom_fields[posting.category_name]

      conversations.each do |conversation|
        buyer_id = conversation.potential_buyer_id

        buyer = User.find(buyer_id)
        tmp = buyer.picture_display_order.split(",").first.to_i
        buyer_pics = (buyer.pictures.count > 0) ? buyer.pictures[tmp].photo.url : "http://www.reppio.com/assets/scoundrel.png"
        buyer_rep_score = Userdata.find_by_user_id(buyer_id).repscore

        #posting_id = conversation.posting_id

        display_sell_button = (posting.user_id_with_permission_to_buy != 0 && posting.requested_price.to_i > 0) ? false : true
        display_cancel_sell_button = (posting.user_id_with_permission_to_buy === buyer_id) ? true : false

        #conversation_updated_at = DateHelper.time_ago_in_words(conversation.updated_at)

        data.push({:buyer => buyer, :buyer_photo_url => buyer_pics, :buyer_rep_score => buyer_rep_score, :conversation => conversation, :display_sell_button => display_sell_button, :display_cancel_sell_button => display_cancel_sell_button, :posting => posting})
      end

      render json: {:success => true, :data => data, :posting => posting}
    else
      render json: {:success => false, :message => "Authentication Failed"}
    end
  end


  def mobile_push_conversation_get
    if mobile_auth(params[:user_id], params[:session_id])

      message = Message.find(params[:message_id])
      #all_messages = Message.find_all_by_conversation_id(message.conversation_id)
      conversation = Conversation.find(message.conversation_id)
      posting = Posting.find(conversation.posting_id)
      buyer = User.find(conversation.potential_buyer_id)
      seller = User.find(conversation.seller_id)

      receiver_is_seller = (buyer.id == params[:user_id]) ? false : true

      display_sell_button = (posting.user_id_with_permission_to_buy != 0 && posting.requested_price.to_i > 0) ? false : true
      display_cancel_sell_button = (posting.user_id_with_permission_to_buy === buyer.id) ? true : false

      render json: {:success => true, :conversation => conversation,
                    :posting => posting, :buyer => buyer, :seller => seller,
                    :receiver_is_seller => receiver_is_seller, :display_sell_button => display_sell_button,
                    :display_cancel_sell_button => display_cancel_sell_button,

                    :title => posting.title, :first_picture_url => posting.first_picture_url, :price => posting.price,
                    :requested_price => posting.requested_price, :seller_name => seller.first_name,
                    :id => posting.id, :conversation_id => conversation.id
      }

    else
      render json: {:success => false, :status_code => "400", :title => "Hmmmm...", :message => "Authentication Failed"}
    end
  end


  def mobile_conversation_create

    if mobile_auth(params[:user_id], params[:session_id])

      current_user = User.find(params[:user_id])
      posting = Posting.find(params[:posting_id])
      seller = User.find(posting.user_id)

      existing_conversation = Conversation.find_by_posting_id_and_potential_buyer_id(posting.id, current_user.id)


      if existing_conversation.nil?
        params["conversation"] = {
            :posting_id => posting.id,
            :seller_id => posting.user_id,
            :product_category => posting.category_name,
            :potential_buyer_id => params[:user_id],
        }

        conversation = Conversation.new(params[:conversation])
      else
        conversation = existing_conversation
      end

      message = conversation.messages.build(params[:message])

      if message.message_body.blank?
        render json: {:success => false, :status_code => "400", :title => "Insufficient Data", :message => "Messages cannot be blank"} and return
      end

      sender_userdata=Userdata.find_by_user_id(current_user.id)
      sender_userdata.update_attribute(:messages, sender_userdata.messages + 1)


      if (conversation.save && seller.receive_email_notifications) || (message.save && seller.receive_email_notifications)
        pp "gooh email sent"
        ConversationMailer.email_message_between_users(seller.email, current_user.first_name, sender_userdata.repscore, posting.title, message.message_body, conversation_url(:id => conversation.id)).deliver
      end


      receivingUser = seller

      user_mobile_device = UserMobileDevice.find_by_user_id(receivingUser.id)

      if !user_mobile_device.nil?

        #Send push notification to recipient
        #app_key = "v88QG8SnTgy4OAE10Cpwsg"             #development
        #app_secret = "dZc-R7QvSIa9dL2vd18rSQ"          #development
        #app_secret_master = "6ZZ-TEq9TGOpTB2LCHy6iA"   #development
        app_key = "Gp6xyenNQQOhzWEnMBNfmw" #production
        app_secret = "fsa7FfiQT5e59uiBvKUMfA" #production
        app_secret_master = "h8zfX6sjRvyAzkiBnk8i-g" #production

        push_host = "https://go.urbanairship.com"
        push_url = "/api/push/"
        device_tokens = [user_mobile_device.device_token]
        unread_messages = Conversation.where("(seller_id = ? AND unread_by_seller = true) OR (potential_buyer_id = ? AND unread_by_buyer = true)", receivingUser.id, receivingUser.id).count
                                           #message = "New message from " + User.find(params[:user_id]).first_name.titlecase
        full_message = User.find(params[:user_id]).first_name.titlecase + ": " + params[:message][:message_body]
        push_message = (full_message.strip.length <= 157) ? full_message.strip : full_message.strip[0, 153] + "..."
        conversation_data = {:msgid => message.id}
                                           #conversation_data = {:conversation_id => conversation.id, :message_id => message.id, :message_from_seller => message.message_from_seller,
                                           #                     :posting_id => @conversation.posting_id }


        pp "Device Tokens:"
        pp unread_messages
        pp "End"

        push_params = {:app_key => app_key, :app_secret => app_secret_master, :push_host => push_host,
                       :push_url => push_url, :device_tokens => device_tokens, :badges => unread_messages,
                       :message => push_message, :recipient => receivingUser.id, :add => conversation_data}


        logger.debug("before push")
        push_send_message(push_params)
        logger.debug("after push")
      end

      render json: {:success => true, :status_code => "200", :title => "Nice going!", :message => "Message sent"}

    else
      render json: {:success => false, :status_code => "400", :title => "Hmmmm...", :message => "Authentication Failed"}
    end
  end

  def mobile_conversation_update
    if mobile_auth(params[:user_id], params[:session_id])
      #params = {id:"2", unread_by_x:false, conversation:{message:{message_body:"mr pibb two",message_from_seller:true}}}

      message_from_seller = (params[:user_id].to_i == Posting.find(Conversation.find(params[:id]).posting_id).user_id.to_i) ? true : false

      #pp "Ugh: #{params[:user_id]} == " + Posting.find(Conversation.find(params[:id]).posting_id).user_id.to_s
      #pp "Look at me: " + message_from_seller.to_s

      parameters = {id: params[:id],
                    unread_by_x: false,
                    conversation: {
                        message: {
                            message_body: params[:message_body],
                            message_from_seller: message_from_seller
                        }
                    }
      }

      @conversation=Conversation.find(parameters[:id])

      #if (parameters[:unread_by_x]) #effectively a toggle for unread_by_seller || unread_by_buyer, depending on user role. Only gets sent when clicking the "Read/unread button"
      #  updateReadStatus(@conversation, :toggle)
      #  render :nothing => true and return
      #end


      if (parameters[:conversation][:message][:message_body] == "")
        #redirect_to :back, :flash => {:error => 'You cannot send blank messages'} and return
        render json: {:success => false, :message => "You cannot send blank messages"} and return

      else
        @message=@conversation.messages.build(parameters[:conversation][:message])
        @conversation.touch #All we're updating with .save is messages, which actually means that conversation.updated_at doesn't get changed. So, touch.
        @conversation.save
        if @message.message_from_seller
          @conversation.update_attribute(:unread_by_seller, false)
          @conversation.update_attribute(:unread_by_buyer, true)
          recipient=User.find_by_id(@conversation.potential_buyer_id)
          senderID=@conversation.seller_id
          if @conversation.messages.where("message_from_seller=true").count==1
            userdata=Userdata.find_by_user_id(senderID)
            userdata.update_attribute(:total_response_time_in_minutes,
                                      userdata.total_response_time_in_minutes + (Time.now-@message.created_at).minutes.to_f)
            userdata.update_attribute(:number_of_responses, userdata.number_of_responses + 1)
          end
        else
          @conversation.update_attribute(:unread_by_buyer, false)
          @conversation.update_attribute(:unread_by_seller, true)
          recipient=User.find_by_id(@conversation.seller_id)
          senderID=@conversation.potential_buyer_id
        end

        userdata=Userdata.find_by_user_id(senderID)
        userdata.update_attribute(:messages, userdata.messages + 1)

        if (recipient.receive_email_notifications)
          ConversationMailer.email_message_between_users(recipient.email, User.find(senderID).first_name, userdata.repscore, Posting.find(@conversation.posting_id).title, parameters[:conversation][:message][:message_body], conversation_url(:id => @conversation.id)).deliver
        end

        @posting = Posting.find(@conversation.posting_id)

        user_mobile_device = UserMobileDevice.find_by_user_id(recipient)

        if !user_mobile_device.nil?
          #Send push notification to recipient
          #app_key = "v88QG8SnTgy4OAE10Cpwsg"             #development
          #app_secret = "dZc-R7QvSIa9dL2vd18rSQ"          #development
          #app_secret_master = "6ZZ-TEq9TGOpTB2LCHy6iA"   #development
          app_key = "Gp6xyenNQQOhzWEnMBNfmw" #production
          app_secret = "fsa7FfiQT5e59uiBvKUMfA" #production
          app_secret_master = "h8zfX6sjRvyAzkiBnk8i-g" #production

          push_host = "https://go.urbanairship.com"
          push_url = "/api/push/"
          device_tokens = [user_mobile_device.device_token]
          unread_messages = Conversation.where("(seller_id = ? AND unread_by_seller = true) OR (potential_buyer_id = ? AND unread_by_buyer = true)", recipient, recipient).count
                                             #message = "New message from " + User.find(params[:user_id]).first_name.titlecase
          full_message = User.find(params[:user_id]).first_name.titlecase + ": " + parameters[:conversation][:message][:message_body]
          push_message = (full_message.strip.length <= 157) ? full_message.strip : full_message.strip[0, 153] + "..."
          conversation_data = {:msgid => @message.id}
                                             #conversation_data = {:conversation_id => @conversation.id, :message_id => @message.id, :message_from_seller => @message.message_from_seller,
                                             #                     :posting_id => @conversation.posting_id}

          unread_buying_count = Conversation.count(:conditions => "potential_buyer_id = #{params[:user_id]} AND unread_by_buyer = true")
          unread_selling_count = Conversation.count(:conditions => "seller_id= #{params[:user_id]} AND unread_by_seller=true")
          badges = unread_buying_count + unread_selling_count

          pp "Device Tokens:"
          pp unread_messages
          pp "End"

          push_params = {:app_key => app_key, :app_secret => app_secret_master, :push_host => push_host,
                         :push_url => push_url, :device_tokens => device_tokens, :badges => unread_messages,
                         :message => push_message, :recipient => recipient, :add => conversation_data,
                         :badges => badges}

          push_send_message(push_params)
        end

      end #if message body isn't blank etc.

      # finally, in the event the conversation is hidden, make it visible again to both parties.
      @conversation.update_attribute(:hide_from_buyer, 0)
      @conversation.update_attribute(:hide_from_seller, 0)

      #redirect_url = (request.env['HTTP_REFERER']) rescue conversation_url(:id => @conversation.id)
      #redirect_to(redirect_url, :flash => {:notice => 'Message successfully sent!'}) and return

      render json: {:success => true, :title => "Great!", :message => "Message Sent"}
    else
      render json: {:success => false, :title => "Oops!", :message => "Message Failed to Send"}
    end
  end


  def mobile_push_setup

    if mobile_auth(params[:user_id], params[:session_id])

      user = User.find_by_id(params[:user_id])
      push_host = "https://go.urbanairship.com"
      push_port = "80"
      #app_code = "4658B-5A619"

      #app_key = "v88QG8SnTgy4OAE10Cpwsg"             #development
      #app_secret = "dZc-R7QvSIa9dL2vd18rSQ"          #development
      #app_secret_master = "6ZZ-TEq9TGOpTB2LCHy6iA"   #development
      app_key = "Gp6xyenNQQOhzWEnMBNfmw" #production
      app_secret = "fsa7FfiQT5e59uiBvKUMfA" #production
      app_secret_master = "h8zfX6sjRvyAzkiBnk8i-g" #production


      device_token = params[:device_token]

      case params[:device_type]
        #1 – iphone, 2 – blackberry, 3 – android, 4 – nokia, 5 – WP7, 7 – mac
        when "iphone"
          device_type_id = 1
        when "android"
          device_type_id = 3
      end

      #if params[:push_notifications_active] == true

      push_url = "/api/device_tokens/#{device_token.upcase}/"

      json_params = {"alias" => user.id.to_s}.to_json
      uri = URI.parse(push_host)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Put.new(push_url)
      request.basic_auth(app_key, app_secret)
      request.add_field('Content-Type', 'application/json')
      request.body = json_params
      response = http.request(request)

      pp "Create device"
      pp "Response #{response.code} #{response.message}: #{response.body}"
      pp response

      if (response.code == "200" || response.code == "201")
      end

      if UserMobileDevice.find_by_device_token_and_user_id(params[:device_token], params[:user_id]).nil?
        user.user_mobile_devices.create(:device_token => device_token, :device_hwid => params[:device_hwid], :device_type => "iphone")
      end

      #else
      #
      #  #if device=user.user_mobile_devices.find_by_device_token_and_device_hwid_and_device_type(params[:device_token], params[:device_hwid], params[:device_type])
      #  if device=user.user_mobile_devices.find_by_device_token_and_user_id(params[:device_token],params[:user_id])
      #
      #    push_url = "/api/device_tokens/#{device_token.upcase}/"
      #    uri = URI.parse(push_host)
      #    http = Net::HTTP.new(uri.host, uri.port)
      #    http.use_ssl = true
      #    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      #
      #    request = Net::HTTP::Delete.new(push_url)
      #    request.basic_auth(app_key, app_secret)
      #
      #    response = http.request(request)
      #
      #    pp "Response #{response.code} #{response.message}: #{response.body}"
      #
      #    if (response.code == "204")
      #      device.destroy
      #    end
      #
      #  end
      #
      #end

    end

    render json: {:success => true}

  end

  def mobile_push_setup_android
    user = User.find_by_id(params[:user_id])
     if UserMobileDevice.find_by_device_token_and_user_id(params[:device_token], params[:user_id]).nil?
        user.user_mobile_devices.create(:device_token => params[:device_token], :device_hwid => params[:device_hwid], :device_type => "android")
      end
  end
  
  def mobile_push_send_android
    require 'gcm'
    logger.debug("User DEBUG");
    # TODO : get actual api key
    gcm = GCM.new("AIzaSyAqzEgZ54uThU-lx--yU6yAgC1YehYQyw0") 
    # TODO get reg ids from DB
    registration_ids= ["APA91bHcT5K_SwwB-l1loIP0ysQLPFo9cWULaiBOUvNY25bmf5n3eYh8fQ46_pWobRlUFRd8FuQfyqm9LHpzqPOkQcaI_0MLpzA-vAJnalxOc3mJoqdQ_EBdr67mJiQKA-cUp5iQHsW1onaAhTzR8zbdKtOQItFeQw"] # an array of one or more client registration IDs
    # TODO pass actual data in notification   
    options = {data: {score: "123"}, collapse_key: "updated_score"}
    response = gcm.send_notification(registration_ids, options)
    logger.debug(response);
  end

  def mobile_toggle_push

    if mobile_auth(params[:user_id], params[:session_id])

      user = User.find_by_id(params[:user_id])
      push_host = "https://go.urbanairship.com"
      push_port = "80"
      #app_code = "4658B-5A619"

      #app_key = "v88QG8SnTgy4OAE10Cpwsg"             #development
      #app_secret = "dZc-R7QvSIa9dL2vd18rSQ"          #development
      #app_secret_master = "6ZZ-TEq9TGOpTB2LCHy6iA"   #development
      app_key = "Gp6xyenNQQOhzWEnMBNfmw" #production
      app_secret = "fsa7FfiQT5e59uiBvKUMfA" #production
      app_secret_master = "h8zfX6sjRvyAzkiBnk8i-g" #production


      device_token = params[:device_token]

      case params[:device_type]
        #1 – iphone, 2 – blackberry, 3 – android, 4 – nokia, 5 – WP7, 7 – mac
        when "iphone"
          device_type_id = 1
        when "android"
          device_type_id = 3
      end

      if params[:email_notifications_active] == true
        user.update_attribute(:receive_email_notifications, 1);
      else
        user.update_attribute(:receive_email_notifications, 0);
      end

      if params[:push_notifications_active] == true

        if UserMobileDevice.find_by_device_token_and_user_id(params[:device_token], params[:user_id]).nil?

          push_url = "/api/device_tokens/#{device_token.upcase}/"

          json_params = {"alias" => user.id.to_s}.to_json
          uri = URI.parse(push_host)
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Put.new(push_url)
          request.basic_auth(app_key, app_secret)
          request.add_field('Content-Type', 'application/json')
          request.body = json_params
          response = http.request(request)

          #pp "Create device"
          #pp "Response #{response.code} #{response.message}: #{response.body}"
          #pp response

          if (response.code == "200" || response.code == "201")
            user.user_mobile_devices.create(:device_token => device_token, :device_hwid => params[:device_hwid], :device_type => "iphone")
          end

        end

      else

        #if device=user.user_mobile_devices.find_by_device_token_and_device_hwid_and_device_type(params[:device_token], params[:device_hwid], params[:device_type])
        if device=user.user_mobile_devices.find_by_device_token_and_user_id(params[:device_token], params[:user_id])

          push_url = "/api/device_tokens/#{device_token.upcase}/"

          uri = URI.parse(push_host)
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE

          request = Net::HTTP::Delete.new(push_url)
          request.basic_auth(app_key, app_secret)

          response = http.request(request)

          pp "Response #{response.code} #{response.message}: #{response.body}"

          if (response.code == "204")
            device.destroy
          end

        end

      end

    end

    render json: {:success => true}

  end


  def push_send_message(params)

    json_params = {
        "device_tokens" => params[:device_tokens],
        "aps" => {
            "badge" => params[:badges],
            "alert" => params[:message]
        },
        "add" => params[:add],
        #"rid" => params[:recipient],
        #"conversation" => "1234567890 1234567890 1234567890 1234567890 1234567890 1"
    }.to_json

    pp json_params

    uri = URI.parse(params[:push_host])
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Post.new(params[:push_url])
    request.basic_auth(params[:app_key], params[:app_secret])
    request.add_field('Content-Type', 'application/json')
    request.body = json_params

    response = http.request(request)

    #logger.debug("Sent Push 2013 test01")
    logger.debug(response.inspect)

    return response

  end


  def push_send_message_pushwoosh(params)

    json_params = {
        "request" => {
            "application" => params[:app_code],
            #"applications_group" => "GROUP_CODE", # Optional. Can be used instead of "application"
            "auth" => params[:api_access_token],
            "notifications" => [
                {
                    # Content settings
                    "send_date" => "now", # YYYY-MM-DD HH:mm  OR 'now'
                    "content" => {# Object( language1: 'content1', language2: 'content2' ) OR string
                                  "en" => "English",
                    },
                    #"page_id" => 39, # Optional. int
                    #"link" => "http://google.com", # Optional. string
                    "data" => {# JSON string or JSON object, will be passed as "u" parameter in the payload
                               "User" => {"username" => "reppio", "password" => "billion"},
                               "custom" => params[:message]
                    },
                    "platforms" => [1, 2, 3, 4, 5, 6, 7], # 1 - iOS; 2 - BB; 3 - Android; 4 - Nokia; 5 - Windows Phone; 7 - OS X WP7 related.
                    "wp_type" => "Tile", # WP7 notification type. 'Tile' or 'Toast'. Raw notifications are not supported. 'Tile' if default
                    "wp_background" => "/Resources/Red.jpg", # WP7 Tile image
                    "wp_backbackground" => "/Resources/Green.jpg", # WP7 Back tile image
                    "wp_backtitle" => "back title", # WP7 Back tile title
                    "wp_count" => params[:badges], # Optional. Integer. Badge for WP7
                    "ios_badges" => params[:badges], # Optional. Integer. This value will be sent to ALL devices given in "devices"
                    #"ios_sound" => "soundfile", # Optional. Sound file name in the main bundle of application
                    #"android_sound" => "soundfile", # Optional. Sound file name in the "res/raw" folder, do not include the extension
                    #"ios_root_params" => {"content-available" => 1}, #Optional - root level parameters to the aps dictionary
                    "mac_badges" => params[:badges],
                    "mac_sound" => "sound.caf",
                    "mac_root_params" => {"content-available" => 1},
                    "android_root_params" => {"key" => "value"}, # custom key-value object. root level parameters for the android payload Recipients
                    "android_icon" => "icon.png",
                    "android_custom_icon" => "http://example.com/image.png",
                    "android_banner" => "http://example.com/banner.png",
                    "devices" => params[:device_ids],
                    #"filter" => "FILTER_NAME", #Optional.
                    #"conditions" => [] #Optional. See remark
                }
            ]
        }
    }.to_json

    uri = URI.parse(params[:push_host])
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Post.new(params[:push_url])
    request.add_field('Content-Type', 'application/json')
    request.body = json_params
    response = http.request(request)

    return response

  end

  ### NEW APP CONTROLLERS
  def mobile_posting_categorize2
    #params = {product:"samsung", user_id:"2", user_name:"hellofleming", email:"hello@flemingau.com", session_id:"OfpjotjPkXLwCjyTKo410Jlw7NDOGg7ZOPvuBqwtPemmw5aqldUqLcfwhOFzatALZY3cDRc9bLQ0rNewy8k"}

    if mobile_auth(params[:user_id], params[:session_id])
      title = params[:product]

      # strip posting of numbers and other stuff
      title = '' if title.nil?
      title = title.gsub(/[^a-z ]/i, '')

      hard_coded_categories = Posting.get_hard_coded_categories

      topCategory=""
      title.split(" ").each { |word_in_title|
        category = hard_coded_categories[word_in_title]
        categoryPlural = hard_coded_categories[word_in_title[0..-2]]

        # do we have a hardcoded category for this? check if it's a leaf. if not then we don't want to hardcode
        if hard_coded_categories.has_key?(word_in_title) || hard_coded_categories.has_key?(word_in_title[0..-2]) && Posting.is_leaf?(category)
          if categoryPlural.nil? || categoryPlural.blank?
            topCategory=category
          elsif category.nil? || category.blank?
            topCategory=categoryPlural
          end
          break
        end
      }
      topThreeCategories=[]
      topThreeCategories = [topCategory] if not topCategory.blank?

      # sphinx query to get the other three categories for a given user input
      topThreeCategories.concat(UpdatedPosting.search(title).facets[:subcategory_name].sort_by { |key, value| value }.last(3).collect { |x| x[0] }.reverse)
      topThreeCategories=topThreeCategories.uniq


      all_leaf_categories= Posting.category_fields.keys.sort
      categories_with_parents=[]
      all_leaf_categories.each { |category|
        #string_with_category_and_ancestors=""
        #ancestry_line=Posting.ancestry_line(category)
        #ancestry_line.each { |ancestor| string_with_category_and_ancestors<<=ancestor +" > " }
        #string_with_category_and_ancestors<<= category
        if Posting.category_parent(category)
          categories_with_parents.push(Posting.category_parent(category) + " > " + category)
        else
          categories_with_parents.push(category)
        end


      }

      render json: {"success" => true, "allCategories" => all_leaf_categories, "topCategory" => topThreeCategories[0]}

    else
      render json: {"success" => false, "message" => "Authentication failure"}
    end

  end

  def mobile_posting_create2

    # parse out custom fields
    params[:custom_fields] = {}
    params.keys.each do |key|
      if key.include?("custom_field_")
        tmp = key.gsub("custom_field_", "")
        params[:custom_fields][tmp] = params[key]
      end
    end


    if  (@user =mobile_auth(params[:user_id], params[:session_id]))

      if params[:price]
        params[:price] = params[:price].gsub("$", "").gsub(",", "")
      else
        render json: {"success" => false, "message" => "Enter a price.", :new_listing => "true"} and return
      end

      posting_data=params[:custom_fields]

      custom_field_list=[]
      posting_data.each_pair { |key, value|
	  if key=="Author" && value==""
			 render json: {:success => false, :title => "Error", :message => "Please fill in the author field.", :new_listing => "true"} and return
		end

        custom_field_list.push({:label => key, :value => value})
      }

      @posting = @user.postings.build(:title => params[:title], :price => params[:price],
                                      :description => params[:description],
                                      :condition => params[:condition],
                                      :category_name => CGI::unescape(params[:category_name]), :city => params[:city],
                                      :will_accept_other_offers => params[:will_accept_other_offers],
                                      :street1 => params[:address], :street2 => nil)
      @posting.deleted=false
      @posting.view_count=0
      @posting.view_history=0

      # validate address using Geocoder
      begin
        lat=Geocoder.coordinates(params[:address])[0]
      rescue
        render json: {:success => false, :message => "Your address could not be found. Please try again.", :new_listing => "true"} and return
      end
      lng=Geocoder.coordinates(params[:address])[1]
      coord = lat.to_s + "," + lng.to_s
      location = Geocoder.search(coord).first
      radLatitude = lat.to_f / 180 * Math::PI
      radLongitude = lng.to_f / 180 * Math::PI

      #@posting.address = (@posting.address == "") ? @user.zip_code : @posting.address #Can't use @posting.address ||= because that checks against nil, not ""
      @posting.address = (params[:address] == "") ? @user.zip_code : params[:address] #Can't use @posting.address ||= because that checks against nil, not ""
      @posting.address = (@posting.address == "") ? location.address : location.postal_code
      @posting.city=location.city
      @posting.zip_code=location.postal_code
      @posting.state=location.state
      @posting.latitude=radLatitude
      @posting.longitude=radLongitude


      # update used location attributes
      @user.update_attribute(:last_posting_street_address, location.address)
      @user.update_attribute(:last_posting_latitude, radLatitude)
      @user.update_attribute(:last_posting_longitude, radLongitude)

      # add custom fields
      custom_field_list.each { |custom_field| @posting.custom_fields.build(custom_field) }

      # build pictures
      index=0
      while params.has_key?("photo"+index.to_s) do
        @posting.pictures.build(:photo => params["photo"+index.to_s])
        index+=1
      end

      # add supergroup

      if @posting.save
        if params[:group_id] && valid_integer?(params[:group_id]) && params[:group_id]!="1"
          @posting.groups<<=Group.find(params[:group_id].to_i)
          @posting.save
        else
          @posting.groups=@user.groups
          @posting.groups<<= Group.find(1)
          @posting.save
        end


        #update picture display order
        picture_display_order="0"
        if @posting.pictures.length>1
          (@posting.pictures.length-1).times { |index|
            picture_display_order<<=","+ (index+1).to_s
          }
        end
        @posting.update_attribute(:picture_display_order, picture_display_order)

        # cache first pictures for reuse on search page
        postingPictures = Picture.where(:imageable_type => "Posting", :imageable_id => @posting.id)
        userPictures = Picture.where(:imageable_type => "User", :imageable_id => @user.id)
        @posting.update_attribute(:first_picture_url, postingPictures[@posting.picture_display_order.split(",").first.to_i].photo.url(:large_thumb))
        begin
          user_picture=userPictures[@user.picture_display_order.split(",").first.to_i].photo.url(:thumb)
        rescue Exception
          user_picture="http://www.reppio.com/assets/default_profile_picture2.png"
        end
        @posting.update_attribute(:first_user_url, user_picture)

        # trigger alerts for others
        Alert.trigger_alert_for(@posting, @user.user_name)
        UpdatedPosting.create!(:title => @posting.title, :subcategory_name => @posting.category_name)

        # render json upon success
        render json: {"success" => true, "posting_id" => @posting.id, "posting_title" => @posting.title,  "posting_picture" => @posting.first_picture_url.gsub("large_thumb", "medium"), "posting_price" => @posting.price, "posting_description" => @posting.description, "message" => "Posting has been created!", :new_listing => "true"} and return
      else
        pp @posting.errors
        render json: {"success" => false, "message" => "Failed to save the posting. Make sure all fields are filled out correctly.", :new_listing => "true"} and return
      end
    end
  else
    render json: {"success" => false, "message" => "Some error occurred.", :new_listing => "true"} and return
  end


  def join_group
    if (user=mobile_auth(params[:user_id], params[:session_id]))
      if (group=Group.find(params[:group_id]))
        if !group.email_extension.blank?
          #autogroup email
          if params[:email].include?(group.email_extension)
            user.email_addresses.create(:email => params[:email])
            pending_email=user.email_addresses.where("is_primary=false AND email_confirmed =false")[0]

            if pending_email

              UserMailer.send_secondary_email_confirmation(pending_email.email,
                                                           pending_email.email_confirmation_code,
                                                           user.first_name).deliver
              render json: {"success" => true, "message" => "Thanks! Check your inbox to verify that you own the email address."} and return

            else
              render json: {"success" => false, "message" => "Invalid email / email already in use."} and return
            end
          else
            render json: {"success" => false, "message" => "Invalid email."} and return
          end

        elsif group.may_join_without_admin_approval
          group.users<<=user

          user.postings.reject { |i| i.deleted || !i.is_updated }.each do |user_posting|
            group.postings<<= user_posting
          end
          group.members_count+=1
          group.save

          render json: {"success" => true, "message" => "Group joined."} and return
        else # need admin approval
          group_request=GroupRequest.create(:group_id => group.id, :owner_id => group.owner_id, :user_id => user.id)
          owner=User.find(group.owner_id)
          owner.update_attribute(:groups_badge_count, owner.groups_badge_count+1)
          GroupMailer.request_to_join_a_group(owner.first_name, owner.email,
                                              group.long_name, group_url(group),
                                              user.first_name,
                                              user_url(user),
                                              handle_request_url(:id => group_request, :status => "accept"),
                                              handle_request_url(:id => group_request, :status => "ignore")).deliver
          render json: {"success" => true, "message" => "Requested to join group."} and return
        end
      else
        render json: {"success" => false, "message" => "The group does not exist."} and return
      end
    end
  end

  def onboarding_add_school_email
    if (user=mobile_auth(params[:user_id], params[:session_id]))

      unless params[:email].include?(".edu")
        render json: {"success" => false, "message" => "The email you entered is not a .edu address."} and return
      end

      if user.email_addresses.find_by_email(params[:email])
        render json: {"success" => false, "message" => "You've already verified this email. Press the skip button."} and return
      end

      user.email_addresses.create(:email => params[:email])
      pending_email=user.email_addresses.where("is_primary=false AND email_confirmed =false")[0]

      if pending_email

        UserMailer.send_secondary_email_confirmation(pending_email.email,
                                                     pending_email.email_confirmation_code,
                                                     user.first_name).deliver
        school=""
        email_extension=pending_email.email.split('@')[1].downcase
        Group.all.each do |group|
          if email_extension==group.email_extension
            school=group.long_name
          end
        end

        render json: {"success" => true, "school" => school, "message" => "Thanks! Check your inbox to verify that you own the email address."} and return

      else
        render json: {"success" => false, "message" => "Invalid email address."} and return
      end
    end
  end

  def onboarding_add_interests
    if (user=mobile_auth(params[:user_id], params[:session_id]))
      if (params[:interests].count("~") >= 2)
        user.update_attribute(:interests, params[:interests])
        render json: {"success" => true, "message" => "Saved your interests."} and return
      else
        render json: {"success" => false, "message" => "Please select at least 2 interests."} and return
      end
    end
  end

  def leave_group
    if (user=mobile_auth(params[:user_id], params[:session_id]))
      if (group=Group.find(params[:group_id]))
        if group.users.delete(user)
          user.postings.each { |posting| group.postings.delete(posting) }
          group.members_count-=1
          group.save
          render json: {"success" => true, "message" => "You left the group."} and return
        else
          render json: {"success" => false, "message" => "You were not in the group."} and return
        end
      else
        render json: {"success" => false, "message" => "The group does not exist."} and return
      end
    end
  end

  # params posting_id
  def toggle_bookmark
    if (user=mobile_auth(params[:user_id], params[:session_id]))
      bookmark = Bookmark.find_by_user_id_and_posting_id(user.id, params[:posting_id]) rescue nil
      if bookmark
        bookmark.destroy

        render json: {"success" => true, "message" => "bookmark_removed"} and return
      else
        bookmark = user.bookmarks.build(:user_id => user.id, :posting_id => params[:posting_id])
        if bookmark.save
          render json: {"success" => true, "message" => "bookmark_created"} and return
        end
      end
    end
  end

  def reset_password

    if (user = User.find_by_email(params[:email]))
      random_password = Array.new(15).map { (48 + rand(75)).chr }.join.gsub(/[^0-9A-Za-z]/, '')
      user.password = random_password
      user.save
      UserMailer.password_reset(user.email, user.first_name, random_password).deliver
      render json: {"success" => true, "message" => "Your password was reset. Check your email for your new password. Once logged in, you can change your password from the Settings page."} and return
    else
      render json: {"success" => false, "message" => "An account with that email address does not exist."} and return
    end
  end

  def return_bookmarks
    if (user=mobile_auth(params[:user_id], params[:session_id]))

      postings_hash_array=[]
      user.bookmarks.each do |bookmark|
        posting=Posting.find(bookmark.posting_id)

        seller=User.find(posting.user_id)
        posting_hash=posting.as_json(:only => [:id, :user_id, :category_name, :title, :condition, :description, :price,
                                               :created_at, :updated_at, :city, :state, :latitude, :longitude,
                                               :zip_code, :address, :first_picture_url, :first_user_url, :is_updated])

        compass_bearings=::DistanceCalculations.compass_bearing(user.last_search_latitude, user.last_search_longitude, posting.latitude*180/Math::PI, posting.longitude*180 / Math::PI)
        distance_miles=::DistanceCalculations.distance_between(user.last_search_latitude/180*Math::PI, user.last_search_longitude/180*Math::PI, posting.latitude, posting.longitude)
        distance="#{number_with_precision(distance_miles, :precision => 1)} miles #{compass_bearings}"

        posting_hash["distance"]= distance
        posting_hash["seller_name"]= "#{seller.first_name}"
        posting_hash["seller_rep_score"]= seller.repscore
        postings_hash_array.push(posting_hash)
      end

      postings_hash_array.to_json


      render json: postings_hash_array
    end
  end


  # only used by browse suggested
  def browse
    if (user=mobile_auth(params[:user_id], params[:session_id]))

      search_options, with_search_options_hash=baseline_search_and_discover_options(user)

      if user.interests.blank? || user.interests=="Reppio Picks~"
        search_options.merge!(:order => 'reppio_pick DESC, updated_at DESC')
      else # select categories person is interested in
        search_options.merge!(:conditions => {:category => user.interests.split("~").collect { |interest| get_category_search_options(map_interest_to_category(interest)) }.join(" | ")
        })
      end

      search_options.merge!(:with => with_search_options_hash)
      render :json => {:posting_data => posting_data_for_mobile(Posting.search('', search_options), user),
                       :last_page => search_options[:page],
                       :count => Posting.search_count('', search_options.except(:page, :per_page))}

    end
  end

#OPTIONAL PARAMETERS pmin, pmax, query, price, location
  def search
    if (user=mobile_auth(params[:user_id], params[:session_id]))
      search_options, with_search_options_hash=baseline_search_and_discover_options(user)


      params[:query]='' if params[:query].nil?
      params[:query].gsub(/[^0-9a-z ]/i, '').gsub(" ", "%20").downcase
      search_terms=params[:query].split(" ")

      stop_words=%w{a about above across after again against all almost alone along already also although always am among an and another any anybody anyone anything anywhere are area areas aren't around as ask asked asking asks at away b back backed backing backs be became because become becomes been before began behind being beings below best better between big both but by c came can cannot can't case cases certain certainly clear clearly come could couldn't d did didn't differ different differently do does doesn't doing done don't down downed downing downs during e each early either end ended ending ends enough even evenly ever every everybody everyone everything everywhere f face faces fact facts far felt few find finds first for four from full fully further furthered furthering furthers g gave general generally get gets give given gives go going good goods got great greater greatest group grouped grouping groups h had hadn't has hasn't have haven't having he he'd he'll her here here's hers herself he's high higher highest him himself his how however how's i i'd if i'll i'm important in interest interested interesting interests into is isn't it its it's itself i've j just k keep keeps kind knew know known knows l large largely last later latest least less let lets let's like likely long longer longest m made make making man many may me member members men might more most mostly mr mrs much must mustn't my myself n necessary need needed needing needs never new newer newest next no nobody non noone nor not nothing now nowhere number numbers o of off often old older oldest on once one only open opened opening opens or order ordered ordering orders other others ought our ours ourselves out over own p part parted parting parts per perhaps place places point pointed pointing points possible present presented presenting presents problem problems put puts q quite r rather really right room rooms s said same saw say says second seconds see seem seemed seeming seems sees several shall shan't she she'd she'll she's should shouldn't show showed showing shows side sides since small smaller smallest so some somebody someone something somewhere state states still such sure t take taken than that that's the their theirs them themselves then there therefore there's these they they'd they'll they're they've thing things think thinks this those though thought thoughts three through thus to today together too took toward turn turned turning turns two u under until up upon us use used uses v very w want wanted wanting wants was wasn't way ways we we'd well we'll wells went were we're weren't we've what what's when when's where where's whether which while who whole whom who's whose why why's will with within without won't work worked working works would wouldn't x y year years yes yet you you'd you'll young younger youngest your you're yours yourself yourselves you've z}
      # takes out all characters that are not letters
      stop_words_removed=(search_terms.select { |word| !stop_words.member?(word.downcase) }).collect { |valid_word| valid_word.downcase.gsub(/[^0-9a-z]/, '') }
      final_search=stop_words_removed.join(" ")


      valid_pmin= valid_integer?(params[:pmin]), valid_pmax= valid_integer?(params[:pmax])
      if valid_pmin && valid_pmax
        with_search_options_hash[:price] =params[:pmin].to_i..params[:pmax].to_i
      elsif valid_pmin
        with_search_options_hash[:price] = params[:pmin].to_i..10_000_000.0
      elsif valid_pmax
        with_search_options_hash[:price] = 0.0..params[:pmax].to_i
      end

      search_options.merge!(:with => with_search_options_hash)

      render :json => {:from => params[:from], :posting_data => posting_data_for_mobile(Posting.search(final_search, search_options), user),
                       :count => Posting.search_count(final_search, search_options.except(:page, :per_page))}

    end

  end

  def group_info
    if (user=mobile_auth(params[:user_id], params[:session_id]))
      group = Group.find(params[:group_id])
      isMember = group.users.collect(&:id).include? user.id
      render :json => {:group => group, :isMember => isMember}
    end
  end

  #params posting_id
  def listing_details_view
    if (user=mobile_auth(params[:user_id], params[:session_id]))
      render :json => listing_details(Posting.find(params[:posting_id]), user)
    end
  end


  def return_groups
    if (user=mobile_auth(params[:user_id], params[:session_id]))
      def create_group_hash(group, distance)
        group_hash=group.as_json(:only => [:id, :long_name])
        group_hash["distance"]= distance

        group_hash
      end

      #groups user is in plus groups that are public minus omnigroup
      groups_user_is_member_of = user.groups

      public_groups= Group.all - groups_user_is_member_of

      degToR = Math::PI / 180
      groups_user_is_member_of_hash_array=[]
      groups_user_is_member_of.each do |group|
        distance=::DistanceCalculations.distance_between(user.latitude*degToR, user.longitude*degToR, group.latitude*degToR, group.longitude*degToR).round(1).to_s
        groups_user_is_member_of_hash_array.push(create_group_hash(group, distance))
      end

      public_groups_hash_array=[]
      public_groups.each do |group|
        distance=::DistanceCalculations.distance_between(user.latitude*degToR, user.longitude*degToR, group.latitude*degToR, group.longitude*degToR).round(1).to_s
        if distance.to_f < 10
          public_groups_hash_array.push(create_group_hash(group, distance))
        end
      end

      render :json => {:groups_user_is_member_of => groups_user_is_member_of_hash_array, :public_groups => public_groups_hash_array}.to_json
    end
  end

  private

#returns search_options, with_search_options_hash
# optional params page, location
  def baseline_search_and_discover_options(user)
    with_search_options_hash={}
    with_search_options_hash[:deleted]=false
    with_search_options_hash[:is_updated]=true

    per_page=15
    params[:page]=1 if !params[:page]
    search_options = {:page => params[:page], :per_page => per_page}

    ######### LOCATION ######################
    if user.last_search_street_address.blank? #use current address
      latitude=user.latitude
      longitude=user.longitude
    else #use last search location
      latitude=user.last_search_latitude
      longitude=user.last_search_longitude
    end
    user.update_attributes(:last_search_latitude => latitude, :last_search_longitude => longitude)

    if params[:location]
      location_info=Geocoder.search(params[:location])[0]
      if location_info && location_info.latitude #has lat and long at least
        latitude= location_info.latitude
        longitude=location_info.longitude
        user.update_attributes(:last_search_street_address => params[:location],
                               :last_search_latitude => latitude, :last_search_longitude => longitude)
      end
    end


    if valid_float?(params[:distance])
      within_distance=params[:distance].to_f
    else
      within_distance=30
    end


    # convert miles to meters
    with_search_options_hash["@geodist"] = 0.0..1609.34*within_distance

    #convert degrees to radians
    search_options.merge!(:geo => [latitude.to_f* Math::PI / 180, longitude.to_f* Math::PI / 180])

    search_options.merge!(:order => 'updated_at DESC')

    if params[:category] && !params[:category].blank?
      if params[:category]=="Books & Textbooks"
        params[:category]="Books"
      end

      string_of_all_subcategories=Posting.all_subcategories_of_category(params[:category]).collect { |subcategory| "(#{subcategory})" }.join(" | ")
      search_options.merge!(:conditions => {:category => string_of_all_subcategories})
    end

    if params[:group_id] && !params[:group_id].blank?
      with_search_options_hash[:group_id] = [params[:group_id].to_i]
    else
      with_search_options_hash[:group_id] = ([1].concat(user.groups.collect(&:id)).concat(Group.find_all_by_may_join_without_admin_approval_and_email_extension(true, "").collect(&:id))).uniq
    end
    pp with_search_options_hash[:group_id]
    [search_options, with_search_options_hash]
  end


  def posting_data_for_mobile(postings_array, user_who_is_searching)
    def special_hash(posting, distance, is_bookmarked)

      seller=User.find(posting.user_id)
      posting_hash=posting.as_json(:only => [:id, :user_id, :category_name, :title, :condition, :description, :price,
                                             :created_at, :updated_at, :city, :state, :latitude, :longitude,
                                             :zip_code, :address, :first_picture_url, :first_user_url, :is_updated])
      posting_hash["is_bookmarked"] =is_bookmarked
      posting_hash["distance"]= distance
      posting_hash["seller_name"]= "#{seller.first_name}"
      posting_hash["seller_rep_score"]= seller.repscore
      posting_hash
    end

    user_bookmarks=user_who_is_searching.bookmarks
    postings_hash_array=[]
    postings_array.each_with_geodist do |posting, distance|
      distance_miles=number_with_precision(distance*0.000621371, :precision => 1)

      compass_bearings=::DistanceCalculations.compass_bearing(user_who_is_searching.last_search_latitude, user_who_is_searching.last_search_longitude, posting.latitude*180/Math::PI, posting.longitude*180 / Math::PI)
      distance_miles=::DistanceCalculations.distance_between(user_who_is_searching.last_search_latitude/180*Math::PI, user_who_is_searching.last_search_longitude/180*Math::PI, posting.latitude, posting.longitude)
      distance="#{number_with_precision(distance_miles, :precision => 1)} miles #{compass_bearings}"

      is_bookmarked=user_bookmarks.any? { |bookmark| bookmark.posting_id==posting.id }
      postings_hash_array.push(special_hash(posting, distance, is_bookmarked))
    end

    postings_hash_array.to_json
  end

  def listing_details(posting, user_who_is_searching)
    compass_bearings=::DistanceCalculations.compass_bearing(user_who_is_searching.last_search_latitude, user_who_is_searching.last_search_longitude, posting.latitude*180/Math::PI, posting.longitude*180 / Math::PI)
    distance_miles=::DistanceCalculations.distance_between(user_who_is_searching.last_search_latitude/180*Math::PI, user_who_is_searching.last_search_longitude/180*Math::PI, posting.latitude, posting.longitude)
    distance="#{number_with_precision(distance_miles, :precision => 1)} miles #{compass_bearings}"
    is_bookmarked=user_who_is_searching.bookmarks.any? { |bookmark| bookmark.posting_id==posting.id }
    seller=User.find(posting.user_id)


    posting_hash=posting.as_json(:only => [:id, :user_id, :category_name, :title, :condition, :description, :price,
                                           :created_at, :updated_at, :city, :state, :latitude, :longitude,
                                           :zip_code, :address, :first_picture_url, :first_user_url, :is_updated])
    posting_hash["is_bookmarked"] =is_bookmarked
    posting_hash["distance"]= distance
    posting_hash["seller_name"]= "#{seller.first_name}"
    posting_hash["seller_rep_score"]= seller.repscore
    posting_hash["seller_id"]= seller.id

    posting_pictures_urls = []
    posting.picture_display_order.split(",").map! { |x| x.to_i }.each do |index|
      posting_pictures_urls.push(posting.pictures.fetch(index).photo.url(:medium))
    end
    posting_hash["pictures"]= posting_pictures_urls
    posting_hash["custom_fields"]=posting.custom_fields


    posting_hash.to_json
  end
end
