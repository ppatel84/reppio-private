class FlagsController < ApplicationController
  def create
    flag = Flag.create(:url => params[:url], :is_resolved => false, :user_id => (current_user.id rescue nil),
                        :content_creator_user_id => params[:content_creator_user_id],
                        :reason_for_flagging=>params[:reason_for_flagging])
    flag.save
    respond_to do |format|
      format.js
      format.html { render :text => "flag created" }
    end
  end
end
