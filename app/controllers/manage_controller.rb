class ManageController < ApplicationController

  def selling
    @asSellerPostings= Posting.where("user_id= #{current_user.id} AND deleted=false AND is_updated = 1").order("updated_at DESC")
  end

  def buying
    @asBuyerPostings = Posting.find_by_sql("SELECT `postings`.* FROM `postings` LEFT OUTER JOIN `conversations` ON `conversations`.`posting_id` = `postings`.`id` WHERE potential_buyer_id = #{current_user.id} AND has_been_completed = 0 AND hide_from_buyer= 0 ORDER BY conversations.unread_by_buyer DESC, conversations.updated_at DESC")
  end

  def completed_buying
    @completedPostingsBuying = Posting.where("actual_buyer_user_id=? AND hide_from_buyer_completed=?",current_user.id, false).order('updated_at DESC')
  end

  def completed_selling
    @completedPostingsSelling = Posting.where("user_id = ? AND deleted = ? AND hide_from_seller_completed = ?", current_user.id, true, false).order("updated_at DESC")

  end

  def drafts_selling
    @draftsPostings = Posting.where("user_id = ? AND is_updated = ?", current_user.id, 0)
  end


  def buying_posting_conversation
    @posting=Posting.find(params[:id])
    @conversation = Conversation.find_by_potential_buyer_id_and_posting_id_and_hide_from_buyer(current_user.id, @posting.id, false)
    @conversation.update_attribute(:unread_by_buyer, false)
  end

  def completed_buying_posting_conversation
    @posting=Posting.find(params[:id])
    @conversation = Conversation.find_by_potential_buyer_id_and_posting_id(current_user.id, @posting.id)
    @conversation.update_attribute(:unread_by_buyer, false)
  end

  def completed_selling_posting_conversations
    @posting=Posting.find_by_id_and_user_id(params[:id], current_user.id) #find by user_id so that the conversation is not accessed by some random person
    @conversations = Conversation.find_all_by_posting_id(@posting.id, :order=>:updated_at).reverse
  end

  def completed_selling_posting_conversation
    @posting=Posting.find_by_id_and_user_id(params[:id], current_user.id)
    @conversation=Conversation.find_by_posting_id_and_potential_buyer_id(@posting.id, params[:buyer_id])
    @conversation.update_attribute(:unread_by_seller, false)

    respond_to do |format|
      format.js
      format.html
    end
  end

  def selling_posting_conversations
    @posting=Posting.find_by_id_and_user_id(params[:id], current_user.id) #find by user_id so that the conversation is not accessed by some random person
    @conversations = Conversation.find_all_by_posting_id_and_hide_from_seller(@posting.id, false, :order=>:updated_at).reverse
  end

  def selling_posting_conversation
    @posting=current_user.postings.find_by_id(params[:id])
    @conversation=@posting.conversations.find_by_potential_buyer_id_and_hide_from_seller(params[:buyer_id], false)
    @conversation.update_attribute(:unread_by_seller, false)

    respond_to do |format|
      format.js
      format.html
    end
  end

  def drafts_selling_redirect
    redirect_to edit_user_posting_url(:user_name => current_user.user_name, :id => params[:id])
  end


  def reviews
    @pendingReviews = Posting.where("fulfilled = ? AND ((user_id = ? AND reviewed_by_seller = ?) OR (actual_buyer_user_id = ? AND reviewed_by_buyer = ?))", true, current_user.id, false, current_user.id, false)
    @review = Review.new
  end

  def rep_center_references
    @pendingReferences = []
    @reference = Reference.new

    if (!current_user.reppio_references.nil?)
      reppioReferenceList = current_user.reppio_references.split(',').uniq.delete_if(&:empty?)
      reppioReferenceList.each { |r| @pendingReferences << r }
      current_user.update_attribute("reppio_references", reppioReferenceList.join(","))
    end

    @pendingApprovals = Reference.find_all_by_reference_for_user_id(current_user.id, :conditions => "is_approved IS NULL")
  end

  def interests
    @add_student_email = params[:add_student_email]
    @categories_pictures={
        "Art" => "interests/art1.png",
        "Babies & Kids" => "interests/babieskids1.png",
        "Books" => "interests/books1.png",
        "Electronics" => "interests/electronics1.png",
        "Entertainment" => "interests/entertainment1.png",
        "Fashion" => "interests/fashion1.png",
        "Home & Decor" => "interests/homedecor1.png",
        "Motors" => "interests/motors1.png",
        "Sports" => "interests/sports1.png",
        "Tickets & Events" => "interests/tickets1.png"
    }
  end

  def verifications
    @user_data=Userdata.find_by_user_id(current_user.id)
  end

  def reputation_explained

  end

  def add_student_email   

    if EmailAddress.find_by_email(params[:email])
      render js: 'alert("You have already used this email address.");' and return
    end

    current_user.email_addresses.create(:email => params[:email])
    pending_email=current_user.email_addresses.where("is_primary=false AND email_confirmed =false")[0]

      if pending_email

        UserMailer.send_secondary_email_confirmation(pending_email.email,
                                                     pending_email.email_confirmation_code,
                                                     current_user.first_name).deliver
        respond_to do |format|          
          format.js
        end

      else
        render js: 'alert("Invalid email format. Please try again.");' and return
      end

  end

  def toggle_interest
    divider="~"
    if current_user.interests.include? params[:category]
      current_user.update_attribute(:interests, current_user.interests.sub(params[:category]+divider, ''))
    else
      current_user.update_attribute(:interests, current_user.interests+params[:category]+divider)
    end

    render :nothing => true and return
  end


  def redirect_to_conversation
    conversation=Conversation.find(params[:id])
    is_seller= current_user.id == conversation.seller_id

    if conversation.posting_was_delisted
      if is_seller
        redirect_to completed_selling_posting_conversation_url(:id=>conversation.posting_id,
                                                               :buyer_id=>conversation.potential_buyer_id) and return
      else
        redirect_to completed_buying_posting_conversation_url(:id=>conversation.posting_id) and return
      end
    else
      if is_seller
        redirect_to selling_posting_conversation_url(:id=>conversation.posting_id, :buyer_id=>conversation.potential_buyer_id) and return
      else
        redirect_to buying_posting_conversation_url(:id=>conversation.posting_id) and return
      end
    end
  end

end