class UsersController < ApplicationController
  before_filter :check_same_user, :only => [:edit, :update]
  skip_before_filter :require_login, :only => [:new, :create, :email_sign_up, :reset_password, :show,
                                               :confirm, :confirm_secondary_email, :omniauth_failure,
                                               :rep_it_bookmark, :rep_it]
  before_filter :bypass_login_if_logged_in, :only => [:new, :email_sign_up, :create]
  before_filter :pictures_in_sort_order, :only => [:show, :edit]
  include ActionView::Helpers::NumberHelper

  def pictures_in_sort_order
    redirect_to root_url and return if not (@user = User.find_by_user_name(params[:user_name]) rescue nil)

    @sortedPicturesNoIndices = []
    (sortArray = @user.picture_display_order.split(",").map! { |x| x.to_i }).each do |index|
      @sortedPicturesNoIndices.push(@user.pictures.fetch(index))
    end
    @sortedPictures = @sortedPicturesNoIndices.zip(sortArray)
  end

  def omniauth_failure
  end

  def reset_password
    @user = User.find_by_email(params[:email])
    if (@user.nil?)
      redirect_to :back, :flash => {:error => 'An account with that email address does not exist.'} and return
    end
    random_password = Array.new(15).map { (48 + rand(75)).chr }.join.gsub(/[^0-9A-Za-z]/, '')
    @user.password = random_password
    @user.save
    UserMailer.password_reset(@user.email, @user.first_name, random_password).deliver
    redirect_to :back, :flash => {:success => 'Your password was reset. Check your email for your new password. Once logged in, you can change your password from the "Settings" page.'}
  end

  def show

    unless params[:cid].nil?
      cookies['cid']=params[:cid]
    end

    @user = User.find_by_user_name!(params[:user_name])

    redirect_to root_url, :flash => {:warning => 'This account was deactivated.'} and return if @user.is_deactivated


    @userdata = Userdata.find_by_user_id(@user.id)

    if !current_user.nil? && !current_user.user_name.nil? && current_user.id == @user.id
      @profilePercentCompletion = Userdata.profile_complete_percent(@user.id)
      profile_completion_links
    end

    @fb_ids = @userdata.facebook_friends_on_reppio_list.split(",").shuffle if not @userdata.facebook_friends_on_reppio_list.nil?

    if @user.nil?
      respond_to do |format|
        format.html
      end and return
    end

    if same_user? && (params[:preview].nil? || params[:preview]!="true")
      redirect_to manage_path and return
    end

    @reviews = Review.find_all_by_user_id(@user.id).reverse
    @conversation=Conversation.new
    @postings=@user.postings.reject { |i| i.deleted || !i.is_updated }

    @references = Reference.where("is_approved = '1' AND reference_for_user_id = ?", @user.id)

    @userdata=Userdata.find_by_user_id(@user.id)

  end

  def verify_cell_phone_number
    if !current_user.cell_phone_verified && current_user.cell_phone_verification_code!= ""
      if params[:c]==current_user.cell_phone_verification_code
        current_user.update_attribute(:cell_phone_verification_code, "")
        current_user.update_attribute(:cell_phone_verified, true)
        Userdata.find_by_user_id(current_user.id).update_attribute(:phone_verified, true)

        respond_to do |format|
          format.js
          format.html { redirect_to edit_user_url(:user_name => current_user.user_name), :notice => 'Congrats! Your cell phone has been verified.' }
        end
        return
      end
    end
    redirect_to verifications_url, :flash => {:alert => 'Sorry, that was an invalid confirmation code.'}

  end

  def send_cell_phone_verification_text
    verification_number=''
    4.times do
      verification_number << rand(10).to_s
    end

    require 'twilio-ruby'

    account_sid = 'AC178ae05dbf39bed18b1a8af4c71ab140'
    auth_token = '26f73fa1f0ca0e2c78cc369f94050a44'

    client = Twilio::REST::Client.new account_sid, auth_token


    #phone number is a string of 9 digits; it will be in this format by validation
    client.account.sms.messages.create(
        :from => '+1 425-967-8302',
        :to => current_user.phone_number,
        :body => "Reppio confirmation code: #{verification_number}"
    )
    current_user.update_attribute(:cell_phone_verification_code, verification_number)
    respond_to do |format|
      format.js
    end
    return
  end

  def user_reviews
    @user=User.find_by_user_name(params[:user_name])
    @reviews = @user.reviews.order("created_at DESC")
  end


  def new
  end

  def email_sign_up
    @user=User.new
    @user.email_addresses.build
  end

  def edit
    @user=current_user
    @userData=Userdata.find_by_user_id(@user.id)
    @upload = Picture.new

    # reset these fields since the google maps javascript will only replace values that it finds
    @user.city=""
    @user.state=""

    non_primary_emails=EmailAddress.where("user_id=? AND is_primary=false", current_user.id)
    @pending_email=non_primary_emails.where("email_confirmed =false")
    @user.email_addresses.build if @pending_email.blank?
    @non_primary_validated_emails=non_primary_emails - @pending_email
  end

  def settings
  end

  def create_user_name
    url=params[:origin]

    # if user name is not blank, we've assigned them one
    # this means we should bring them right to their buying manage and tell them their password
    if (!current_user.user_name.blank?) && (convo = Conversation.find_by_potential_buyer_id(current_user.id))

      # create temporary password
      password = Array.new(15).map { (48 + rand(75)).chr }.join.gsub(/[^0-9A-Za-z]/, '')
      current_user.password = password
      current_user.save

      # deliver email for temporary password
      UserMailer.send_password_after_confirmation(current_user.email,
                                                  current_user.first_name,
                                                  password).deliver

      UserMailer.welcome(current_user.email, current_user.user_name).deliver


      # verify the conversation now that the CL user has signed up, and make unread to seller
      # i'm assuming that they only have started one conversation at this point with this particular account

      convo.update_attribute(:is_verified, true)
      convo.update_attribute(:unread_by_seller, true)

      # check if seller and buyer have had a conversation
      receivingUser = User.find(convo.seller_id)
      potential_buyer = User.find(convo.potential_buyer_id)
      unless receivingUser.has_had_a_conversation
        receivingUser.update_attribute(:has_had_a_conversation, true)
        receivingUser.update_attribute(:should_show_manage_posting_tour, true)
      end
      unless potential_buyer.has_had_a_conversation
        potential_buyer.update_attribute(:has_had_a_conversation, true)
        potential_buyer.update_attribute(:should_show_manage_posting_tour, true)
      end

      # send out the emails now
      message = convo.messages.first
      offer = Offer.find_by_user_id(current_user.id)
      posting = Posting.find(convo.posting_id)
      if convo.save && receivingUser.receive_email_notifications
        if offer
          ConversationMailer.offer_updated(receivingUser.email, number_to_currency(offer.offer), posting.title, potential_buyer.first_name, potential_buyer.repscore, conversation_url(:id => convo.id), message.message_body).deliver
        else
          ConversationMailer.email_message_between_users(receivingUser.email, potential_buyer.first_name, potential_buyer.repscore, posting.title, message.message_body, conversation_url(:id => convo.id)).deliver
        end
      end

      redirect_to manage_buying_url and return
    end

    if (!current_user.email_confirmed)
      redirect_to send_confirmation_email_url and return
    end

    user_name= current_user.first_name.gsub(/[^A-Za-z]/, '')
    while user_name.length<5 || User.find_by_user_name(user_name)
      user_name=user_name+rand(10).to_s
    end
    current_user.user_name= user_name

    if url == "app"
      current_user.save
      UserMailer.welcome(current_user.email, current_user.user_name).deliver

      redirect_to "reppio://" and return
    end
    location_info_from_ip_address=Geocoder.search(request.location)[0]
    if location_info_from_ip_address && location_info_from_ip_address.latitude #has lat and long at least
      current_user.latitude= location_info_from_ip_address.latitude
      current_user.longitude=location_info_from_ip_address.longitude
      if location_info_from_ip_address.postal_code #if has zip code, then has city, state, address
        current_user.zip_code=location_info_from_ip_address.postal_code
        current_user.street_address=location_info_from_ip_address.address
        current_user.city= location_info_from_ip_address.city
        current_user.state=location_info_from_ip_address.state
      else #use google to reverse geocode
        lat_long_string= current_user.latitude.to_s+","+ current_user.longitude.to_s
        result=Geocoder.search(lat_long_string)[0]
        current_user.zip_code=result.postal_code if result.postal_code
        current_user.street_address=result.address if result.address
        current_user.city= result.city if result.city
        current_user.state=result.state if result.state
      end
    else
      current_user.street_address="Chicago, IL"
      current_user.latitude =41.8781136
      current_user.longitude= -87.6297982
      current_user.city="Chicago"
      current_user.state="IL"
      current_user.zip_code="60604"
    end

    current_user.save
    UserMailer.welcome(current_user.email, current_user.user_name).deliver

    if url.blank? || (!url.include?("postings") && !url.include?("conversations"))
      redirect_to interests_url(:add_student_email => 1) and return
    else
      redirect_to url and return
    end

  end

  def create

    email_address=params[:user][:email_addresses_attributes]["0"]["email"]

    @user = User.new(params[:user])
    @user.email_private =true
    @user.street_address_private=true
    @user.phone_number_private =true
    @user.education_private =true
    @user.employer_private =true
    @user.receive_email_notifications=true
    @user.picture_display_order=''
    @user.email=email_address
    @user.email_confirmed = false
    @user.email_confirmation_code = BCrypt::Engine.generate_salt

    unless cookies['cid'].nil?
      @user.cid=cookies['cid']
    end

    if @user.save

      # user saved, returned true. create initial repscore entry
      Userdata.create(:user_id => @user.id,
                      :full_name_complete => true,
                      :repscore_history => '0')
      Userdata.compute_repscore(@user.id)


      # if user was referred, get info of referrer from cookie and update Reference table
      unless (cookies['referrer-id'].nil? || cookies['referee-temporary-id'].nil?)

        (referral=Referral.find_by_referrer_id_and_referee_temporary_id(cookies['referrer-id'], cookies['referee-temporary-id'])) rescue nil
        unless referral.nil?

          referral.update_attribute(:referee_signed_up, true)
          referral.update_attribute(:referred_id, @user.id)
          userdata=Userdata.find_by_user_id(cookies['referrer-id'])
          userdata.update_attribute(:referred_users, userdata.referred_users + 1)
        end
      end


      session[:user_id] = @user.id
      UserMailer.send_email_confirmation(@user.email,
                                         @user.email_confirmation_code,
                                         @user.first_name,
                                         @user.origin_if_facebook_sign_up).deliver
      redirect_to root_url, notice: "Hi #{@user.first_name}! An email has been sent to #{@user.email}. Click the link in the email to finish the sign-up process." and return
    else
      render action: "email_sign_up"
    end
  end


  def send_confirmation_email
    @user=current_user
    UserMailer.send_email_confirmation(@user.email,
                                       @user.email_confirmation_code,
                                       @user.first_name, root_url).deliver
    redirect_to search_url, notice: "Hi #{@user.first_name}! An email has been sent to #{@user.email}. Click the link in the email to finish the sign-up process." and return
  end

  def send_confirmation_secondary_email
    send_email_verifying_nonprimary_email_address_of_current_user
    redirect_to edit_user_url(:user_name => current_user.user_name), :notice => "An email has been sent to #{pending_email.email} to make sure it is a valid address." and return
  end

  def cancel_secondary_email_confirmation
    pending_email=current_user.email_addresses.where("is_primary=false AND email_confirmed =false")[0]
    current_user.email_addresses.delete(pending_email)
    redirect_to edit_user_url(:user_name => current_user.user_name), :notice => "Email verification cancelled." and return

  end

  def confirm_secondary_email
    if (email=EmailAddress.find_by_email(params[:email]))&& (email.email_confirmation_code==params[:confirmation_code])
      email.email_confirmed=true
      email.save

      if request.env['HTTP_USER_AGENT'].downcase.index("iphone")
        redirect_to "reppio://" and return  
      else
        redirect_to root_url, :notice => "Your email address has been verified." and return
      end
    else
      if request.env['HTTP_USER_AGENT'].downcase.index("iphone")
        redirect_to "reppio://" and return
      else
        redirect_to root_url, :notice => "Sorry, your email address could not be verified." and return
      end
    end
  end

  def confirm
    if (@user=User.find_by_email(params[:email])) && (@user.email_confirmation_code==params[:confirmation_code])
      @user.email_confirmed=true
      @user.save
      EmailAddress.find_by_email(params[:email]).update_attribute(:email_confirmed, true)

      if @user!=current_user
        session[:user_id] = @user.id
      end

      redirect_to create_user_name_url(:origin => params[:origin]) and return

    else #wrong confirmation code
      @confirmation_success=false
    end
  end

  def update

    if (!params[:user][:uploaded_pictures].nil?)
      uploadedPictures = params[:user][:uploaded_pictures].split(",")
      params[:user].delete(:uploaded_pictures)

      if params[:user] && !uploadedPictures.empty?
        i = 0
        uploadedPictures.each { |key, value|
          params[:user][:picture_display_order] <<= ("," + (current_user.pictures.length + i).to_s)
          i += 1
        }

        uploadedPictures.map! { |x| x.to_i }.each do |id|
          Picture.find_by_imageable_id(id).update_attributes(:imageable_id => current_user.id, :imageable_type => "User")
        end

      end

      # remove comma if comma is first character of picture_display_order, so that after split is called the blank char does not get mapped to 0
      if params[:user][:picture_display_order][0]==','
        params[:user][:picture_display_order][0]=''
      end
    end


    params[:old_password]='' if params[:old_password].nil?
    params[:new_password]='' if params[:new_password].nil?
    params[:new_password_confirm]='' if params[:new_password_confirm].nil?

    alert_message = ("Changes successful!").html_safe

    if !(params[:new_password].empty?) && !(params[:new_password_confirm].empty?)
      if !params[:old_password].empty?
        if !User.authenticate(current_user.email, params[:old_password]).nil? &&
            (params[:new_password] == params[:new_password_confirm]) #If all three are defined, and the user provided the correct old password, and the two new passwords match
          params[:user][:password] = params[:new_password]
        elsif User.authenticate(current_user.email, params[:old_password]).nil?
          alert_message = ("The provided current (old) password was incorrect.").html_safe
        elsif params[:new_password] != params[:new_password_confirm]
          alert_message = ("New passwords did not match.").html_safe
        end
      elsif current_user.email_confirmation_code.nil?
        if (params[:new_password] == params[:new_password_confirm])
          params[:user][:password] = params[:new_password]
          params[:user][:email_confirmation_code]="blahblah"
        elsif params[:new_password] != params[:new_password_confirm]
          alert_message = ("New passwords did not match.").html_safe
        end

      end
    end


    unless params[:user][:phone_number].nil? || params[:user][:phone_number].blank? || params[:user][:phone_number]==current_user.phone_number
      params[:user][:cell_phone_verification_code]=""
      params[:user][:cell_phone_verified]=false

      #if cell phone being changed from verified to not verified, update userdata
      if current_user.cell_phone_verified
        Userdata.find_by_user_id(current_user.id).update_attribute(:phone_verified, false)
      end
    end

    if params[:user][:picture_display_order] #came from edit profile page, so could have changed profile photo; did not come from account settings page
                                             # find any postings that this user has made an update the user picture cache for those postings
      usersPostings = Posting.find_all_by_user_id(current_user.id)
      usersPictures = Picture.where(:imageable_type => "User", :imageable_id => current_user.id)
      usersPostings.each do |posting|
        # use update_column because we don't want to touch the updated_at value
        posting.update_column(:first_user_url, usersPictures[params[:user][:picture_display_order].split(",").first.to_i].photo.url(:thumb))
      end
    end


    if current_user.update_attributes(params[:user])
      if params[:user][:email_addresses_attributes]
        send_email_verifying_nonprimary_email_address_of_current_user
        alert_message="Please check your inbox to verify that you own the email address."
      end

      # update attributes returned successful => now write to userdata table,
      # and update repscore
      User.update_userdata(current_user.id)
      Userdata.compute_repscore(current_user.id)

      if params[:from_conversation_paypal_email]
        redirect_to :back, :notice => alert_message
      else
        redirect_to user_path(current_user, :preview => true), :notice => alert_message
      end

    else

      render :action => "edit", :alert => alert_message

    end
  end


  def add_autogroup_email
    if current_user.update_attributes(params[:user]) && params[:user][:email_addresses_attributes]
      send_email_verifying_nonprimary_email_address_of_current_user

      respond_to do |format|
        format.js
      end
    else
      render :js => "alert('Email has already been taken.');"
    end
  end


  def deactivate
    current_user.postings.each { |posting| posting.update_attribute(:is_updated, false) }
    current_user.update_attribute(:is_deactivated, true)
    redirect_to logout_url, :notice => 'Your account has been deactivated.' and return
  end


  def rep_it
  end

  def rep_it_bookmark
    if @user=User.find_by_rep_it_token(params[:u])
      respond_to do |format|
        format.js
      end
    else
      render :js => "alert('A new version of the Rep It bookmark has been made. Please go to #{rep_it_url} to re-install it.');"
    end

  end


####################### Private Methods ##################################################################


  private
  def profile_completion_links
    userdata = Userdata.find_by_user_id(current_user.id)
    user=current_user

    @profileCompletionLinks=''

    def add_to_links(message)
      @profileCompletionLinks<< '<br/><a href="http://www.reppio.com/'+current_user.user_name+'/edit">' << message << '</a>'
    end

    add_to_links("Add a profile picture.") if !userdata.picture_complete?
    add_to_links("Add a phone number (will be private).") if !userdata.phone_number_complete?
    add_to_links("Verify your phone number.") if !userdata.phone_verified?
    add_to_links('Fill out your "About Me" section.') if !userdata.about_me_complete?
    add_to_links("Add your education history.") if user.education.blank?
    add_to_links("Add your current employment.") if user.employer.blank?
    add_to_links("Link your Facebook account.") if userdata.fb_friends.nil?
    add_to_links("Link your Linkedin account.") if !userdata.linked_linkedin
    add_to_links("Link your Twitter account.") if !userdata.linked_twitter
  end

  def send_email_verifying_nonprimary_email_address_of_current_user
    pending_email=current_user.email_addresses.where("is_primary=false AND email_confirmed =false")[0]
    UserMailer.send_secondary_email_confirmation(pending_email.email,
                                                 pending_email.email_confirmation_code,
                                                 current_user.first_name).deliver

  end

end
