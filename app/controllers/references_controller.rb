class ReferencesController < ApplicationController
  def create
    @user=current_user
    @reference = Reference.new(params[:reference])

    # who wrote this reference, and for who
    r_for = @reference.reference_for_user_id
    r_by = @reference.reference_written_by_user_id

    @alreadyWrittenForThisUser=false
    @referenceWasBlank=false
    # check to see if a reference has already been made with this particular {r_for, r_by}
    # pair
    if (!Reference.find_by_reference_for_user_id_and_reference_written_by_user_id(r_for, r_by).nil?)
      if !@user.reppio_references.nil?
        @user.reppio_references = @user.reppio_references.gsub(r_for.to_s, '')
      end
      @user.save
      @alreadyWrittenForThisUser=true
    end

    if @reference.save

      # remove the user the review was written for, from the current user
      if !@user.reppio_references.nil?
        @user.reppio_references = @user.reppio_references.gsub(r_for.to_s, '')
      end
      @user.save
    else
      @referenceWasBlank=true
    end

    respond_to do |format|
      format.js
    end

  end

  def send_references_given_email_list
    params[:emails].gsub(" ", "").split(",").each do |email|
      if valid_email?(email)
        ReferenceMailer.send_reference_request(email, current_user.id, current_user.first_name, current_user.last_name, current_user.email, params[:personal_message]).deliver
      end
    end

    respond_to do |format|
      format.js
      format.html{ redirect_to :back, :notice => "Your reference request has been sent successfully!"}
    end

  end

  def send_invites_list
    params[:emails].gsub(" ", "").split(",").each do |email|
      if valid_email?(email)
        ReferenceMailer.send_invite_no_name(email, current_user.id, current_user.first_name, current_user.last_name, current_user.email, params[:personal_message]).deliver
      end
    end

    respond_to do |format|
      format.js
      format.html{ redirect_to :back, :notice => "Your friends have been invited!"}
    end
  end

  def accept_or_reject_references
    @reference = Reference.find(params[:id])
    if (params[:accept]=="yes")

      # the reference was saved. update corresponding userdata table
      @user = current_user
      userData = Userdata.find_by_user_id(@user.id)
      userReferences = userData.references_received
      userData.update_attribute("references_received",userReferences+1)

      @reference.update_attribute("is_approved",1)
      @message = "Reference accepted!"
    else
      @reference.update_attribute("is_approved",0)
      @message = "Reference not accepted."
    end
    respond_to { |format|
        format.js
    }
  end

  def failure
    redirect_to :back, :alert => "Your contacts could not be retrieved."
  end


end
