class MessagesController < ApplicationController
  def new
    @message=Message.new
  end

  def create
    @message = Message.new(params[:message])

    respond_to do |format|
      if @message.save

        format.html { redirect_to @posting }
        format.json { head :no_content }
      else
        format.html { render action: "create_message" }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end
end
