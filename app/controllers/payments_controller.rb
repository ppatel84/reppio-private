class PaymentsController < ApplicationController
  skip_before_filter :require_login, :only => [:ipn_notification, :buy]

  include ActionView::Helpers::NumberHelper


  def buy
    posting=Posting.find(params[:posting_id])

    price=posting.requested_price

    buyer=current_user
    seller=User.find(posting.user_id)


    pay_request = PaypalAdaptive::Request.new


    #if params[:c]=="y" #paying by credit card
    #  reppio_cut=calculateReppioCut(price)
    #  reppio_cut=0
    #
    #  payment=Payment.create(:seller_id => seller.id, :buyer_id => buyer.id, :price => price, :seller_cut => price,
    #                         :reppio_cut => reppio_cut,
    #                         :seller_email => seller.email, :posting_id => posting.id)
    #
    #
    #  data = {
    #      "returnUrl" => completed_payment_url(:id => payment.id),
    #      "requestEnvelope" => {"errorLanguage" => "en_US"},
    #      "currencyCode" => "USD",
    #      "memo" => "Reppio purchase: #{posting.title.gsub('"', '')}",
    #      "receiverList" => {"receiver" => [{"email" => "samir@reppio.com",
    #                                         "amount" => "#{number_to_currency(reppio_cut, :precision => 2, :unit => '', :delimiter => "")}",
    #                                         "primary" => "false"},
    #                                        {"email" => seller.email,
    #                                         "amount" => "#{number_to_currency(price, :precision => 2, :unit => '', :delimiter => "")}",
    #                                         "primary" => "true"}]},
    #      "feesPayer" => "SECONDARYONLY",
    #      "cancelUrl" => canceled_payment_url,
    #      "actionType" => "PAY",
    #      "ipnNotificationUrl" => ipn_notification_url(:id => payment.id)
    #  }
    #
    #else
    reppio_cut=0

    payment=Payment.create(:seller_id => seller.id, :buyer_id => buyer.id, :price => price, :seller_cut => price,
                           :reppio_cut => reppio_cut,
                           :seller_email => seller.paypal_email, :posting_id => posting.id)


    data = {
        "returnUrl" => completed_payment_url(:id => payment.id),
        "requestEnvelope" => {"errorLanguage" => "en_US"},
        "currencyCode" => "USD",
        "memo" => "Reppio purchase: #{posting.title.gsub('"', '')}",
        "receiverList" => {"receiver" => [{"email" => seller.paypal_email,
                                           "amount" => "#{number_to_currency(price, :precision => 2, :unit => '', :delimiter => "")}"}]},
        "feesPayer" => "SENDER",
        "cancelUrl" => canceled_payment_url,
        "actionType" => "PAY",
        "ipnNotificationUrl" => ipn_notification_url(:id => payment.id)
    }
    pp data

    #end
    logger.debug("PAYMENT INFO:")
    logger.debug(payment)

    pay_response = pay_request.pay(data)

    if pay_response.success?
      redirect_to pay_response.approve_paypal_payment_url
    else
      logger.debug("*******************************")
      logger.debug(pay_response.errors)
      logger.debug(pay_response.errors.first['message'])
      redirect_to failed_payment_url
    end

  end

  def submit_deposit
    posting=Posting.find(params[:posting_id])

    amount=posting.deposit

    buyer=current_user
    seller=User.find(posting.user_id)


    deposit=Deposit.create(:seller_id => seller.id, :buyer_id => buyer.id, :amount => amount,
                           :posting_id => posting.id, :posting_title => posting.title)

    paypal = Paypal.new("reppio_1352236409_biz_api1.reppio.com", "1352236430", "Ahnh00lddyckTPY8oWxWrlPfVXFSA29pc1B-jIkAD39esoXvSE5lo8sF")

    params = {
        'method' => 'SetExpressCheckout',
        'returnURL' => do_express_checkout_payment_for_deposit_url,
        'cancelURL' => canceled_payment_url,
        'paymentrequest_0_paymentaction' => "authorization",
        'paymentrequest_0_amt' => amount,
        'paymentrequest_0_currencycode' => 'USD',
        "l_paymentrequest_0_name0" => "deposit for "+posting.title.gsub(/[^0-9A-Za-z ]/, ''),
        "l_paymentrequest_0_amt0" => amount,
        "l_paymentrequest_0_qty0" => 1,
        'noshipping' => 1,
        'allownote' => 0,
        'VERSION' => 65.0,
        'hdrimg' => "http://www.reppio.com/assets/reppio_logo.png"
    }

    res=paypal.make_nvp_call(params)
    deposit.update_attributes(:set_express_checkout_dump => res, :token => res.token)

    redirect_to express_checkout_redirect_url(res.token) and return
  end

  def do_express_checkout_payment_for_deposit
    deposit=Deposit.find_by_token(params[:token])
    deposit.update_attributes(:payer_id => params[:PayerID], :do_express_checkout_payment_dump => params)

    paypal = Paypal.new("reppio_1352236409_biz_api1.reppio.com", "1352236430", "Ahnh00lddyckTPY8oWxWrlPfVXFSA29pc1B-jIkAD39esoXvSE5lo8sF")

    params = {
        'method' => 'DoExpressCheckoutPayment',
        'token' => deposit.token,
        'paymentaction' => 'authorization',
        'payerid' => deposit.payer_id,
        'amount' => deposit.amount,
        'currencycode' => 'USD',
        'paymentrequest_0_paymentaction' => "authorization",
        'paymentrequest_0_amt' => deposit.amount,
        "l_paymentrequest_0_name0" => "deposit for "+deposit.posting_title,
        "l_paymentrequest_0_amt0" => deposit.amount,
        "l_paymentrequest_0_qty0" => 1,
        'VERSION' => 65.0,
    }

    res=paypal.make_nvp_call(params)
    deposit.update_attributes(:do_express_checkout_payment_dump => res, :transaction_id => res.PAYMENTINFO_0_TRANSACTIONID)

    #DoAuthorization not needed

    #params={
    #  'method'=>'DoAuthorization',
    #  'TRANSACTIONID'=>deposit.transaction_id,
    #  'VERSION'=>65.0,
    #  'AMT'=>deposit.amount
    #}
    #res=paypal.make_nvp_call(params)
    #
    #deposit.update_attributes(:do_authorization_dump=>res, :paid=>true)


    redirect_to manage_buying_url, :notice => "You paid the deposit." and return

  end


  def canceled_payment_request
    redirect_to manage_buying_url, :notice => "You did not pay the seller. Your account was not charged."
  end

  def completed_payment_request
    payment=Payment.find(params[:id])
    payment.update_attribute(:paid, true)
    redirect_to manage_buying_url, :notice => "You paid the seller."
  end

  def failed_payment
    redirect_to manage_buying_url, :notice => "There was something wrong with the transaction. Your account was not charged."
  end


  def ipn_notification
    payment=Payment.find(params[:id])

    ipn = PaypalAdaptive::IpnNotification.new
    ipn.send_back(request.raw_post)

    payment.update_column(:ipn_dump, params)
    if ipn.verified?
      payment.update_column(:ipn_received, true)
      posting=Posting.find(payment.posting_id)
      posting.update_column(:fulfilled, 1)
      posting.update_column(:delisted_reason, "soldHere")
      posting.update_column(:actual_buyer_user_id, payment.buyer_id)
      posting.update_column(:sold_on_paypal, true)

      conversation = Conversation.find_by_potential_buyer_id_and_seller_id_and_posting_id(payment.buyer_id, posting.user_id, posting.id)
      conversation.update_column(:has_been_completed, 1)

      seller=User.find(posting.user_id)
      buyer=User.find(payment.buyer_id)
      PaymentMailer.payment_complete(seller.email, seller.first_name, posting.title,
                                     user_posting_url(:user_name => seller.user_name, :id => posting.id),
                                     payment.price, buyer.first_name, buyer.user_name).deliver


    end

    render nothing: true
  end

  def request_payment
    requested_price=params[:requested_price].to_i
    if requested_price > 0
      posting=Posting.find_by_id_and_user_id(params[:posting_id], current_user.id)
      if posting.user_id_with_permission_to_buy !=0
        buyer=User.find(posting.user_id_with_permission_to_buy)
        posting.update_column(:requested_price, requested_price)
      else
        buyer=User.find(params[:user_id])

        posting.update_column(:user_id_with_permission_to_buy, buyer.id)
        posting.update_column(:requested_price, requested_price)
      end

      PaymentMailer.payment_has_been_requested(buyer.email, buyer.first_name, posting.title,
                                               user_posting_url(:user_name => buyer.user_name, :id => posting.id),
                                               buy_url(:posting_id => posting.id)).deliver


      #create message for buyer
      conversation=Conversation.find_by_posting_id_and_seller_id(posting.id, current_user.id)
      message_body="[#{current_user.first_name} has requested payment of #{number_to_currency(requested_price)}. Click \"Buy Now\" to purchase using Paypal.]"
      conversation.messages.create(:message_body => message_body, :message_from_seller => true)
      conversation.unread_by_seller=false
      conversation.unread_by_buyer= true
      conversation.save

      redirect_to :back, :notice => "Your item can now be purchased on Reppio via PayPal. The buyer has been notified by email."
    else
      redirect_to :back, :notice => "The price you entered was invalid."
    end
  end

  def cancel_request_payment
    posting= current_user.postings.find(params[:id])
    posting.update_column(:requested_price, 0)
    posting.update_column(:user_id_with_permission_to_buy, 0)
    #send message to buyer
    #create message for buyer
    conversation=Conversation.find_by_posting_id_and_seller_id(posting.id, current_user.id)
    message_body="[#{current_user.first_name} has cancelled the payment request.]"
    conversation.messages.create(:message_body => message_body, :message_from_seller => true)
    conversation.unread_by_seller=false
    conversation.unread_by_buyer= true
    conversation.save

    redirect_to :back, :notice => "Your request for payment was cancelled."
  end


  private

  def calculateReppioCut(price)
    number_with_precision(0.3+price*0.05, :precision => 2)
  end

  def express_checkout_redirect_url(token, useraction = nil)
    live = ENV['RAILS_ENV'] == 'production'
    if live
      url = "https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=#{token}"
    else
      url = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=#{token}"
    end
    url << "&useraction=#{useraction}" if useraction
    return url
  end


end

=begin
sample params in ipn notification
{"transaction"=>
      {"0"=>
        {".is_primary_receiver"=>"true",
          ".id_for_sender_txn"=>"0D1051131N314315E",
          ".receiver"=>"seller_1299622357_biz@gmail.com",
          ".amount"=>"USD 50.00", ".status"=>"Completed",
          ".id"=>"4WF324015C547172T",
          ".status_for_sender_txn"=>"Completed",
          ".paymentType"=>"SERVICE",
          ".pending_reason"=>"NONE"},
      "1"=>
        {".paymentType"=>"SERVICE",
          ".id_for_sender_txn"=>"8RN84136258572825",
          ".is_primary_receiver"=>"false",
          ".status_for_sender_txn"=>"Completed",
          ".receiver"=>"rroman_1307473832_per@gmail.com",
          ".amount"=>"USD 5.00",
          ".pending_reason"=>"NONE",
          ".id"=>"50546824JL989701Y",
          ".status"=>"Completed"}},
    "log_default_shipping_address_in_transaction"=>"false",
    "action_type"=>"PAY",
    "ipn_notification_url"=>"http://blah.com/mycallback",
    "charset"=>"windows-1252",
    "transaction_type"=>"Adaptive Payment PAY",
    "notify_version"=>"UNVERSIONED",
    "cancel_url"=>"blahblahs",
    "verify_sign"=>"blahblah",
    "sender_email"=>"blahblah@gmail.com", "fees_payer"=>"EACHRECEIVER",
    "return_url"=>"blahblah",
    "reverse_all_parallel_payments_on_error"=>"false",
    "pay_key"=>"487234987234",
    "status"=>"COMPLETED",
    "test_ipn"=>"1",
    "payment_request_date"=>"Mon Dec 12 12:25:56 PST 2011",
    "id"=>"1"}


 {"test_ipn"=>"1", "payment_type"=>"instant", "payment_date"=>"09:42:10 Dec 12, 2012 PST", "payment_status"=>"Completed", "payer_status"=>"unverified", "first_name"=>"John", "last_name"=>"Smith", "payer_email"=>"buyer@paypalsandbox.com", "payer_id"=>"TESTBUYERID01", "business"=>"seller@paypalsandbox.com", "receiver_email"=>"seller@paypalsandbox.com", "receiver_id"=>"TESTSELLERID1", "residence_country"=>"US", "item_name1"=>"something", "item_number1"=>"AK-1234", "quantity1"=>"1", "tax"=>"2.02", "mc_currency"=>"USD", "mc_fee"=>"0.44", "mc_gross"=>"15.34", "mc_gross_1"=>"12.34", "mc_handling"=>"2.06", "mc_handling1"=>"1.67", "mc_shipping"=>"3.02", "mc_shipping1"=>"1.02", "txn_type"=>"cart", "txn_id"=>"1012121742", "notify_version"=>"2.4", "custom"=>"xyz123", "invoice"=>"abc1234", "charset"=>"windows-1252", "verify_sign"=>"Aitl97hE9s4uSAexd3t1c8Z.K35nAvE71ie6rhNqDm0TqxCTBdP8lIcV"}

test paypal accounts

buy003_1354045096_per@reppio.com
testing1


Test Account:reppio_1352236409_biz@reppio.com
API Username:reppio_1352236409_biz_api1.reppio.com
API Password:1352236430
Signature:Ahnh00lddyckTPY8oWxWrlPfVXFSA29pc1B-jIkAD39esoXvSE5lo8sF

Test Account:sel001_1352236029_biz@reppio.com
API Username:sel001_1352236029_biz_api1.reppio.com
API Password:1352236047
Signature:AbznAk0iJ7DeYf-iKbv6IzR-nwW8AIIzkpUJwz2XMSKFVO5hwQgjJKCd

=end

