class StaticController < ApplicationController
  skip_before_filter :require_login, :set_timezone

  def app
  end

  def about
  end

  def jobs

  end

  def faq

  end

  def terms

  end

  def privacy

  end

  def team

  end

  def press

  end

  def contact
    @name = ""
    @email = ""
    if signed_in?
      @name = current_user.first_name + " " + current_user.last_name + " (" + current_user.user_name + ")"
      @email = current_user.email
    end
  end

  def contactSend
    if params[:contactForm][:email].empty?
      email = nil
    else
      email = params[:contactForm][:email]
    end
    name = params[:contactForm][:name]
    message = params[:contactForm][:message]
    if UserMailer.contact_reppio(name,email,message).deliver
      redirect_to :back, :flash => { :notice => ('Message sent! Thanks for your feedback. If you requested a response, we\'ll get back to you within 36 hours.')} and return
    else
      redirect_to :back, :flash => { :error => ('There was a problem delivering your message. Please try again.')} and return
    end
  end
  
  def landing

      # put id of referrer into database (even if referred person does not create account), save cookie
      unless params[:r].nil?
        referee_temporary_id=Array.new(20).map { (48 + rand(75)).chr }.join.gsub(/[^0-9A-Za-z]/, '')
        Referral.create(:referrer_id => params[:r], :referee_temporary_id => referee_temporary_id)
        cookies['referrer-id'] = params[:r]
        cookies['referee-temporary-id']=referee_temporary_id
      end

      #save a cookie for origin of user
      unless params[:cid].nil?
        cookies['cid']=params[:cid]
      end

      # store id of fb referrer into cookie. when persons signs up as new user or logs in, we check
      # to see if the facebook-referrer-id param has been set and ask for a review correspondingly
      unless params[:fbr].nil?
        if !current_user.nil?
          # the user is signed in. all we need to do is change
          if (current_user.reppio_references.nil?)
            #replace
            current_user.update_attribute(:reppio_references, params[:fbr])
          else
            #append
            current_user.update_attribute(:reppio_references,current_user.reppio_references+","+params[:fbr])
          end
        else
          # need to store a cookie because no user is signed in.
          cookies['facebook-referrer-id'] = params[:fbr]
        end
        redirect_to manage_selling_url, :notice => "Thanks for writing a reference for #{User.find(params[:fbr]).first_name}! Please sign up or login to your Manage page to view your pending references." and return
      end

    if !current_user.nil? && !current_user.user_name.nil?
      redirect_to search_url and return
    end
      # flash[:notice] = params[:something]
      render :layout => false
  end
end
