class BrowseController < ApplicationController
  skip_before_filter :require_login, :only => [:autocomplete]

  def show
    if current_user.user_name.nil? || current_user.user_name.blank?
      redirect_to "http://www.reppio.com", :notice => "To continue using the website, please check your email to verify your account." and return
    end

    @recommended_view=false
    if request.fullpath== "/search"
      params[:recommended]="yes"
      @recommended_view=true
    elsif params[:recommended]
      if params[:page].to_i==1
        params.delete(:recommended)
      else
        @recommended_view=true
      end
    end

    conditions_search_options_hash={}

    params[:search_item]='' if params[:search_item].nil? || params[:search_item].include?("utf8=")
    params[:search_item].gsub(/[^0-9a-z ]/i, '').gsub(" ", "%20").downcase
    search_terms=params[:search_item].split(" ")

    stop_words=%w{a about above across after again against all almost alone along already also although always am among an and another any anybody anyone anything anywhere are area areas aren't around as ask asked asking asks at away b back backed backing backs be became because become becomes been before began behind being beings below best better between big both but by c came can cannot can't case cases certain certainly clear clearly come could couldn't d did didn't differ different differently do does doesn't doing done don't down downed downing downs during e each early either end ended ending ends enough even evenly ever every everybody everyone everything everywhere f face faces fact facts far felt few find finds first for four from full fully further furthered furthering furthers g gave general generally get gets give given gives go going good goods got great greater greatest group grouped grouping groups h had hadn't has hasn't have haven't having he he'd he'll her here here's hers herself he's high higher highest him himself his how however how's i i'd if i'll i'm important in interest interested interesting interests into is isn't it its it's itself i've j just k keep keeps kind knew know known knows l large largely last later latest least less let lets let's like likely long longer longest m made make making man many may me member members men might more most mostly mr mrs much must mustn't my myself n necessary need needed needing needs never new newer newest next no nobody non noone nor not nothing now nowhere number numbers o of off often old older oldest on once one only open opened opening opens or order ordered ordering orders other others ought our ours ourselves out over own p part parted parting parts per perhaps place places point pointed pointing points possible present presented presenting presents problem problems put puts q quite r rather really right room rooms s said same saw say says second seconds see seem seemed seeming seems sees several shall shan't she she'd she'll she's should shouldn't show showed showing shows side sides since small smaller smallest so some somebody someone something somewhere state states still such sure t take taken than that that's the their theirs them themselves then there therefore there's these they they'd they'll they're they've thing things think thinks this those though thought thoughts three through thus to today together too took toward turn turned turning turns two u under until up upon us use used uses v very w want wanted wanting wants was wasn't way ways we we'd well we'll wells went were we're weren't we've what what's when when's where where's whether which while who whole whom who's whose why why's will with within without won't work worked working works would wouldn't x y year years yes yet you you'd you'll young younger youngest your you're yours yourself yourselves you've z
}
    # takes out all characters that are not letters
    stop_words_removed=(search_terms.select { |word| !stop_words.member?(word.downcase) }).collect { |valid_word| valid_word.downcase.gsub(/[^0-9a-z]/, '') }
    final_search=stop_words_removed.join(" ")

    baselineSearchConditions # set price, search radius, ordering of results; call this method to reset
    #remove invalid category parameter
    only_white_space_or_empty_regex=/^\s*$/
    params.delete(:category) if !params[:category].nil? && !(params[:category]=~only_white_space_or_empty_regex).nil?
    @ancestry_line=[]


    if @recommended_view
      if current_user.interests.blank? || current_user.interests=="Reppio Picks~"
        @search_options.merge!(:order => 'reppio_pick DESC, updated_at DESC')
      else # select categories person is interested in
        conditions_search_options_hash[:category]=current_user.interests.split("~").collect { |interest| get_category_search_options(map_interest_to_category(interest)) }.join(" | ")
        @search_options.merge!(:conditions => conditions_search_options_hash) unless conditions_search_options_hash.empty?
      end
      @results=Posting.search('', @search_options)
    else
      if params[:category]
        category= params[:category]
        conditions_search_options_hash[:category]=get_category_search_options(category)

        if Posting.is_leaf?(category)
          params.delete(:cfv) if !params[:cfv].nil? && !(params[:cfv]=~only_white_space_or_empty_regex).nil?
          if params[:cfv]
            conditions_search_options_hash[:custom_field_values] = Riddle.escape(params[:cfv])
            @search_options.merge!(:conditions => conditions_search_options_hash) unless conditions_search_options_hash.empty?
          end

          category_fields=Posting.category_fields
          if category_fields.has_key?(category)
            (@drop_down_menus=category_fields[category][:drop_down_menus]) rescue nil
          end
        end

        @ancestry_line=Posting.ancestry_line(category) if !(category.nil?||category.empty?)
      end

      @search_options.merge!(:conditions => conditions_search_options_hash) unless conditions_search_options_hash.empty?


      # find all postings with custom field value, e.g. "black", note that this will
      # cause problems if category has an option name that is shared by
      # more than one filter (ex: field cloth color can be "black", but field button color "black" too)
      @results=Posting.search(final_search, @search_options)
    end


    if params[:ajaxload]=="true"
      render :action => '_load_postings', :layout => false
    elsif params[:mapscripts]=="true"
      render :action => "map_markers", :layout => false
    elsif params[:update_filters]
      params.delete(:update_filters)
      respond_to do |format|
        format.html { render :action => 'show' }
        format.js { render :action => "update_filters", :layout => false }
      end

    else
      render :action => 'show'
    end
  end

  def autocomplete

    hard_coded_categories = Posting.get_hard_coded_categories

    topCategory=""
    params[:user_input].split(" ").each { |word_in_title|
      category = hard_coded_categories[word_in_title]
      categoryPlural = hard_coded_categories[word_in_title[0..-2]]

      # do we have a hardcoded category for this? check if it's a leaf. if not then we don't want to hardcode
      if hard_coded_categories.has_key?(word_in_title) || hard_coded_categories.has_key?(word_in_title[0..-2]) && Posting.is_leaf?(category)
        if categoryPlural.nil? || categoryPlural.blank?
          topCategory=category
        elsif category.nil? || category.blank?
          topCategory=categoryPlural
        end
        break
      end
    }
    topThreeCategories=[]
    topThreeCategories = [topCategory] if not topCategory.blank?

    # sphinx query to get the other three categories for a given user input
    topThreeCategories.concat(UpdatedPosting.search(params[:user_input]).facets[:subcategory_name].sort_by { |key, value| value }.last(3).collect { |x| x[0] }.reverse)

    render json: topThreeCategories.uniq

  end

  def load_mini_map
    if @posting=Posting.find(params[:id])
      respond_to do |format|
        format.js
      end
    end
  end


  private
  def baselineSearchConditions
    with_search_options_hash={}
    with_search_options_hash[:deleted]=false
    with_search_options_hash[:is_updated]=true
    @per_page=15

    params[:page]=1 if !params[:page]
    @search_options = {:page => params[:page], :per_page => @per_page}
    ######### LOCATION ######################
    if params[:search_within_map] #find listings within box
      north_east_lat=params[:north_east].split(",")[0].to_f* Math::PI / 180
      north_east_long=params[:north_east].split(",")[1].to_f * Math::PI / 180

      south_west_lat=params[:south_west].split(",")[0].to_f * Math::PI / 180
      south_west_long=params[:south_west].split(",")[1].to_f* Math::PI / 180

      with_search_options_hash[:latitude] = south_west_lat..north_east_lat
      with_search_options_hash[:longitude] = south_west_long..north_east_long

      params[:latitude]=(north_east_lat+south_west_lat)/2
      params[:longitude]=(north_east_long+south_west_long)/2

    else #find stuff within radius around lat /lang
      if params[:latitude].nil? || params[:longitude].nil? || params[:latitude].empty? || params[:longitude].empty?
        if params[:location].nil? || params[:location].blank?
          if current_user.user_name.nil? #user has not specified zip code, default latitude and longitude to chicago
            params[:location] = "Chicago, IL"
            params[:latitude]="41.8781136"
            params[:longitude]="-87.6297982"
          else
            if current_user.last_search_street_address.blank? #use current address
              params[:location]=current_user.street_address
              params[:latitude]=current_user.latitude
              params[:longitude]=current_user.longitude
            else #use last search location
              params[:location]=current_user.last_search_street_address
              params[:latitude]=current_user.last_search_latitude
              params[:longitude]=current_user.last_search_longitude
            end
          end
        else
          result=Geocoder.search(params[:location])[0]
          if result
            params[:latitude]=result.latitude
            params[:longitude]=result.longitude
          end
        end
      else # lat/long params were not blank, update user's last search location
        if params[:location].nil? || params[:location].blank?
          lat_long_string= params[:latitude]+","+ params[:longitude]
          result=Geocoder.search(lat_long_string)[0]
          params[:location]=result.address if result
        end
        current_user.update_attributes(:last_search_street_address => params[:location],
                                       :last_search_latitude => params[:latitude], :last_search_longitude => params[:longitude])


      end

      #default search within 30 miles
      if valid_float?(params[:wd])
        within_distance=params[:wd].to_f
      else
        within_distance=30
      end

      # convert miles to meters
      with_search_options_hash["@geodist"] = 0.0..1609.34*within_distance

      #convert degrees to radians
      @search_options.merge!(:geo => [params[:latitude].to_f* Math::PI / 180, params[:longitude].to_f* Math::PI / 180])


    end


    #if change location do not show current location icon on map and remove cookie
    if cookies['cl']
      if (cookies['cl'].to_f - params[:latitude].to_f).abs >0.03 #give some leeway of about 10 miles

        cookies.delete('cl')
        params.delete(:cl)
      else
        params[:cl]=true
      end
    end
    # add cookie for current location
    if params[:cl]=="true" && cookies['cl'].nil?
      cookies['cl'] = params[:latitude]
    end


    ########## GROUPS ###########################
    @users_groups=current_user.groups
    only_white_space_or_empty_regex=/^\s*$/
    params.delete(:group) if !params[:group].nil? && !(params[:group]=~only_white_space_or_empty_regex).nil?
    if params[:group]
      group_ids=[]
      params[:group].split("  ").each { |substring|
        id=substring.to_i
        group_ids<<=id if id !=0
      }

      with_search_options_hash[:group_id] = group_ids
    else
      #omnigroup id is 1
      with_search_options_hash[:group_id] = [1]<< @users_groups.collect(&:id)
    end

    ########## ORDER ###########################
    @sort_options=["Newest", "Price: low to high", "Price: high to low", "Distance"]
    @sort_links=[search_url(params.merge(:sb => "ud")), search_url(params.merge(:sb => "pa")), search_url(params.merge(:sb => "pd")), search_url(params.merge(:sb => "da"))]
    case params[:sb]
      when 'pa' #price ascending
        @search_options.merge!(:order => 'price ASC')
        @current_sort_index=1
      when 'pd' #price descending
        @search_options.merge!(:order => 'price DESC')
        @current_sort_index=2
      when 'da' #distance ascending
        @search_options.merge!(:order => '@geodist ASC')
        @current_sort_index=3
      else
        params[:sb] = 'ud' #updated descending
        @search_options.merge!(:order => 'updated_at DESC')
        @current_sort_index=0
    end

    valid_pmin= valid_integer?(params[:pmin]), valid_pmax= valid_integer?(params[:pmax])
    if valid_pmin && valid_pmax
      with_search_options_hash[:price] =params[:pmin].to_i..params[:pmax].to_i
    elsif valid_pmin
      with_search_options_hash[:price] = params[:pmin].to_i..10_000_000.0
    elsif valid_pmax
      with_search_options_hash[:price] = 0.0..params[:pmax].to_i
    end


    @search_options.merge!(:with => with_search_options_hash)


    @alreadyHasAlert = !Alert.where("user_id = ? AND search = ? AND radius = ? AND address = ?", current_user.id, params[:search_item], params[:wd], params[:location]).empty? rescue false


  end

  def get_category_search_options(category)
    # Given category, find all subcategories to search through
    Posting.all_subcategories_of_category(category).collect { |subcategory| "(#{subcategory})" }.join(" | ")
  end


  def map_interest_to_category(interest)
    case interest
      when "Entertainment"
        "CDs, DVD & Blu-ray"
      when "Sports"
        "Sporting Goods"
      else
        interest
    end
  end


end
