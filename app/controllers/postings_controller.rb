class PostingsController < ApplicationController

  before_filter :check_same_user, :only => [:new, :removePosting, :create, :update, :edit, :seller_availability, :promote]
  before_filter :pictures_in_sort_order, :only => [:edit, :show, :craigslist_ajax, :promote_posting_by_email]
  before_filter :check_valid_request, :only => [:promote]
  skip_before_filter :require_login, :only => [:show, :all_postings, :promote_posting_by_email,
                                               :load_email_share_modal, :post_from_bookmark,
                                               :form_data_for_bookmark, :google_feed]
  before_filter :bypass_login_if_logged_in, :only => []
  include ActionView::Helpers::NumberHelper
  include ActionView::Helpers::TextHelper

  def categorize_posting

    title = params[:product]

    # strip posting of numbers and other stuff
    title = '' if title.nil?
    title = title.gsub(/[^a-z ]/i, '')

    hard_coded_categories = Posting.get_hard_coded_categories

    @topCategory=""
    title.split(" ").each { |word_in_title|
      category = hard_coded_categories[word_in_title]
      categoryPlural = hard_coded_categories[word_in_title[0..-2]]

      # do we have a hardcoded category for this? check if it's a leaf. if not then we don't want to hardcode
      if hard_coded_categories.has_key?(word_in_title) || hard_coded_categories.has_key?(word_in_title[0..-2]) && Posting.is_leaf?(category)
        if categoryPlural.nil? || categoryPlural.blank?
          @topCategory=category
        elsif category.nil? || category.blank?
          @topCategory=categoryPlural
        end
        break
      end
    }

    if @topCategory.blank?
      @topCategory=UpdatedPosting.search(title).facets[:subcategory_name].sort_by { |key, value| value }.last[0] rescue nil
    end

    respond_to do |format|
      format.js
    end
  end

  def all_postings
    render :action => "all_postings", :layout => false
  end

  def pictures_in_sort_order
    @posting = Posting.find(params[:id])
    @sortedPicturesNoIndices = []
    (sortArray = @posting.picture_display_order.split(",").map! { |x| x.to_i }).each do |index|
      @sortedPicturesNoIndices.push(@posting.pictures.fetch(index))
    end
    @sortedPictures = @sortedPicturesNoIndices.zip(sortArray)
  end

  def seller_availability
    @posting = Posting.find_by_id_and_user_id(params[:id], User.find_by_user_name(params[:user_name]).id)
  end

  def show #@posting comes from one of the before_filters
    @user=User.find(@posting.user_id)
    if @user.user_name != params[:user_name]
      redirect_to root_url
    end
    @userdata = Userdata.find_by_user_id(@user.id)
    @postings=@user.postings.reject { |i| i.deleted || !i.is_updated }.reverse
    if current_user.nil? || current_user.id!=@user.id
      @posting.view_count+=1
      @posting.update_record_without_timestamping #method in initializer

      #save campaign id of user, if available; e.x. craigslist will have cid = 'cl'
      unless params[:cid].nil?
        cookies['cid']=params[:cid]
      end
    end

    @latitudeLongitudeOffset=::DistanceCalculations.calculateOffset(@posting.latitude*180/Math::PI, @posting.longitude*180 / Math::PI, 200)

    if same_user?
      conversation = Conversation.where("posting_id = ? AND seller_id = ? AND is_verified != ? AND hide_from_seller=false", params[:id], current_user.id, false).order("updated_at DESC")
      @conversations = * conversation
      #if current_user.should_show_manage_posting_tour && !@conversations.empty?
      #  current_user.update_attribute(:should_show_manage_posting_tour, false)
      #  @showTourGuiders=true
      #end
    end
  end

  def edit #@posting comes from one of the before_filters
    @upload = Picture.new
    # reset these fields since the google maps javascript will only replace values that it finds
    @posting.state=""
    @posting.zip_code=""
  end

  def new
    @upload = Picture.new
  end

  def new_posting_form_for_category
    @posting = Posting.new

    if not current_user.last_posting_street_address.blank?
      @posting.address=current_user.last_posting_street_address
      @posting.latitude=current_user.last_posting_latitude
      @posting.longitude=current_user.last_posting_longitude
    end
    render :layout => false
  end


  def create
    params[:posting][:group_ids]||=[]

    #if omnigroup selected, add to all other groups
    if params[:posting][:group_ids]==["1"]
      params[:posting][:group_ids].concat(current_user.groups.collect(&:id))
    end

    uploadedPictures = params[:posting][:uploaded_pictures].split(",").map(&:to_i)
    params[:posting].delete(:uploaded_pictures)
    params[:posting][:price] = params[:posting][:price].gsub(",", "")

    posting_data=params[:posting]


    custom_field_list=[]
    index=1
    while posting_data.has_key?("custom_field"+index.to_s) do
      custom_field_list.push(posting_data.delete("custom_field"+index.to_s))
      index+=1
    end

    @posting = current_user.postings.build(posting_data)
    @posting.address = (@posting.address == "") ? current_user.zip_code : @posting.address #Can't use @posting.address ||= because that checks against nil, not ""
    custom_field_list.each { |custom_field| @posting.custom_fields.build(custom_field) }

    if @posting.save

      uploadedPictures.each do |id|
        Picture.find_by_imageable_id(id).update_attributes(:imageable_id => @posting.id, :imageable_type => "Posting")
      end

      Alert.trigger_alert_for(@posting, current_user.user_name)

      uploaded_picture_ids=params[:picture_ids].split(",").map(&:to_i)
      picture_display_order=uploaded_picture_ids.zip((1..uploaded_picture_ids.length).to_a).sort_by { |e|
        e.first }.map { |e| e[1]-1 }

      @posting.update_attribute(:picture_display_order, (picture_display_order.map! { |x| x.to_s }.join(',')))
      UpdatedPosting.create!(:title => @posting.title, :subcategory_name => @posting.category_name)

      # cache first pictures for reuse on search page
      postingPictures = Picture.where(:imageable_type => "Posting", :imageable_id => @posting.id)
      userPictures = Picture.where(:imageable_type => "User", :imageable_id => current_user.id)

      @posting.update_attribute(:first_picture_url, postingPictures[@posting.picture_display_order.split(",").first.to_i].photo.url(:large_thumb))
      begin
        user_picture=userPictures[current_user.picture_display_order.split(",").first.to_i].photo.url(:thumb)
      rescue Exception
        user_picture="http://www.reppio.com/assets/default_profile_picture2.png"
      end
      @posting.update_attribute(:first_user_url, user_picture)

      current_user.update_attributes(:last_posting_street_address => @posting.address,
                                     :last_posting_latitude => @posting.latitude, :last_posting_longitude => @posting.longitude)


      if params[:draft]
        @posting.update_attribute(:is_updated, false)
        redirect_to user_posting_url(:id => @posting.id), notice: 'This posting is a draft. To publish it, you must click the "Publish" button on the Edit page.' and return
      else
        promotion_params={}
        promotion_params[:cg]="true"
        promotion_params[:id]=@posting.id

        if current_user.promote_postings_to_facebook

          if session[:fb_token]
            begin
              me = FbGraph::User.me(session[:fb_token])
              me.feed!(
                  :message => "Take a look at my listing: #{@posting.title}",
                  :picture => @posting.first_picture_url,
                  :link => user_posting_url(:user_name => current_user.user_name, :id => @posting.id),
                  :name => @posting.title,
                  :description => @posting.description
              )
            rescue Exception
              facebook_message= "We don't have permission to post on your Facebook wall. <a href='auth/facebook' target='_blank'>Grant permission</a>"
            end
          else
            facebook_message= "We could not post to your Facebook wall.  <a href='auth/facebook' target='_blank'>Login to Reppio with Facebook</a>"
          end
        end

        if facebook_message
          redirect_to user_posting_url(promotion_params), alert: facebook_message.html_safe and return
        else
          redirect_to user_posting_url(promotion_params), notice: "Posting was successfully created." and return
        end
      end

    else
      redirect_to :back, :flash => {:error => 'All fields must be filled in.'}
    end
  end

  def update
    params[:posting][:group_ids]||=[]

    #if omnigroup selected, add to all other groups
    if params[:posting][:group_ids]==["1"]
      params[:posting][:group_ids].concat(current_user.groups.collect(&:id))
    end
    @posting = Posting.find_by_id_and_user_id(params[:id], current_user.id)

    posting_data=params[:posting]
    custom_field_list=[]
    index=1
    while posting_data.has_key?("custom_field"+index.to_s) do
      custom_field_list.push(posting_data.delete("custom_field"+index.to_s))
      index+=1
    end
    @posting.custom_fields.destroy_all unless posting_data.has_key? ("custom_fields_attributes")


    custom_field_list.each { |custom_field| @posting.custom_fields.build(custom_field) }


    uploadedPictures = params[:posting][:uploaded_pictures].split(",")
    params[:posting].delete(:uploaded_pictures)

    if params[:posting] && !uploadedPictures.empty? #FLAG: Possible "security" hole right here if the user edits uploadedPictures manually (using some extension or whatever ... can't trust client data). Issues are wherever we do something relating to bad uploadedPictures data ... 2-3 lines below this, and 7-8 lines below. Really our user would probably only be shooting himself in the foot, but it's something to think about.
      i = 0
      uploadedPictures.each { |key, value|
        params[:posting][:picture_display_order] <<= ("," + (@posting.pictures.length + i).to_s)
        i += 1
      }

      uploadedPictures.map! { |x| x.to_i }.each do |id|
        Picture.find_by_imageable_id(id).update_attributes(:imageable_id => @posting.id, :imageable_type => "Posting")
      end

      # remove comma if comma is first character of picture_display_order, so that after split is called the blank char does not get mapped to 0
      if params[:posting][:picture_display_order][0]==','
        params[:posting][:picture_display_order][0]=''
      end
    end

    if @posting.update_attributes(params[:posting])
      Alert.trigger_alert_for(@posting, current_user.user_name)

      # set flag for removal to true
      @posting.flag_for_deletion=false
      if params[:draft]
        @posting.is_updated=false
      else
        @posting.is_updated=true # adjust if this was a relisted posting
      end
      @posting.save!

      # cache first picture URLs for reuse on search page
      postingPictures = Picture.where(:imageable_type => "Posting", :imageable_id => @posting.id)
      @posting.update_attribute(:first_picture_url, postingPictures[params[:posting][:picture_display_order].split(",").first.to_i].photo.url(:large_thumb))

      redirect_to user_posting_url(:id => @posting.id), notice: 'Your posting has been updated.'
    else
      redirect_to :back, :flash => {:error => 'All fields must be filled in.'}
    end

  end


  def load_delist_modal
    @posting=current_user.postings.find(params[:id])
    respond_to do |format|
      format.js
    end
  end

  def delist_posting
    @posting= current_user.postings.find(params[:id])
    @posting.deleted=true
    delisted_reason = params[:posting][:delisted_reason]
    if delisted_reason == "soldHere"
      @posting.actual_buyer_user_id = params[:posting][:possible_buyers]

      if @posting.actual_buyer_user_id != 0 # check if they specified a buyer
        Conversation.find_by_seller_id_and_potential_buyer_id(@posting.user_id, @posting.actual_buyer_user_id).update_attribute(:has_been_completed, true)
      end

      @posting.actual_price = params[:posting][:actual_price]
      @posting.fulfilled = true
      @posting.reviewed_by_buyer = ((@posting.actual_buyer_user_id != 0) ? false : nil) #If actual_buyer_user_id is 0, then seller submitted form saying "no valid transaction records found," i.e. didn't actually specify a buyer. In that case, there's nothing to review ... so set the flag to nil instead of false (where false ==> needs to be reviewed).
      @posting.reviewed_by_seller = ((@posting.actual_buyer_user_id != 0) ? false : nil)
      sellerdata = Userdata.find_by_user_id(@posting.user_id)
      sellerdata.update_attribute(:transactions_complete, sellerdata.transactions_complete + 1)

      if @posting.actual_buyer_user_id != 0
        seller=User.find(@posting.user_id)
        buyer=User.find(@posting.actual_buyer_user_id)

        buyerdata = Userdata.find_by_user_id(@posting.actual_buyer_user_id)
        buyerdata.update_attribute(:transactions_complete, buyerdata.transactions_complete + 1)

        ReviewMailer.pending_review_email(seller.email, @posting.title, "Buyer", buyer.first_name, buyerdata.repscore).deliver
        ReviewMailer.pending_review_email(buyer.email, @posting.title, "Seller", seller.first_name, sellerdata.repscore).deliver
      end

    end
    @posting.delisted_reason = delisted_reason
    @posting.flag_for_deletion = false
    @posting.delist_notify_action = false
    @posting.save

    if params[:posting][:delisted_reason]=="soldHere" && params[:shareFacebook]=="on"
      if session[:fb_token]
        begin
          me = FbGraph::User.me(session[:fb_token])
          me.feed!(
              :message => "I just sold something on Reppio!",
              :picture => @posting.first_picture_url,
              :link => user_posting_url(:user_name => current_user.user_name, :id => @posting.id),
              :name => @posting.title,
              :description => @posting.description
          )
        rescue Exception
        end
      end
    end


    respond_to do |format|
      format.js
      format.html { redirect_to user_postings_url, :notice => "Your posting has delisted." and return }
    end

  end

  def load_promote_modal
    @posting=current_user.postings.find(params[:id])
    respond_to do |format|
      format.js
    end
  end

  def relistPosting
    @posting = current_user.postings.find(params[:id])
    if @posting.delisted_reason != "soldHere"
      @posting.deleted = false
      @posting.flag_for_deletion = false
      @posting.save
    end
    redirect_to :back and return
  end

  def relistPostingCopy

    require 'aws-sdk'

    original_posting= current_user.postings.find(params[:id])
    temp_posting = original_posting.dup #Doesn't appear to dup id, updated_at, or created_at ... which is fantastic
    temp_posting.view_count = 0
    temp_posting.availability = nil
    temp_posting.deleted = false
    temp_posting.fulfilled = nil
    temp_posting.delta = 1
    temp_posting.flag_for_deletion = false
    temp_posting.reviewed_by_buyer = false
    temp_posting.reviewed_by_seller = false
    temp_posting.actual_buyer_user_id = nil
    temp_posting.delisted_reason = ""
    temp_posting.actual_price = nil
    temp_posting.view_history = ""

    # we don't want this duplicate to be seen until the user completes the update
    # form. this effectively makes it a draft until the user calls the update method
    temp_posting.is_updated = false

    if temp_posting.save
      #put posting in omnigroup
      temp_posting.groups<<=Group.find(1)

      #copy custom fields
      CustomField.find_all_by_customizable_id(params[:id]).each {
          |custom_field| temp_posting.custom_fields.build(:label => custom_field.label, :value => custom_field.value)
      }
      temp_posting.save


      # get picture objects from old posting
      old_posting_pictures = Picture.find_all_by_imageable_id(params[:id])
      s3 = AWS::S3.new(
          :access_key_id => 'AKIAI3L36MVAHQVYLN2A',
          :secret_access_key => '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'
      )
      if Rails.env == "production"
        rep_bucket = s3.buckets["reppio-bucket"]
      else
        rep_bucket = s3.buckets["reppio-dev-bucket"]
      end

      old_posting_pictures.each { |old_picture|

        #First do the database work
        new_picture_row = old_picture.dup
        new_picture_row.imageable_id = temp_posting.id
        new_picture_row.save!

        #Then do the S3 work
        old_attachment = old_picture.photo
        old_hash = old_attachment.hash_key
        new_hash = Picture.find(new_picture_row.id).photo.hash_key

        # get all pictures corresponding to a picture id
        from_pics = rep_bucket.objects.with_prefix(old_attachment.path[0...old_attachment.path.rindex("/")])

        # for each pic in an id folder, copy it to another id folder
        from_pics.each { |from_pic|
          to_pic_key = from_pic.key.gsub(old_hash, new_hash)
          from_pic.copy_to(rep_bucket.objects[to_pic_key], options={:acl => :public_read})
        }

        # duplicate the corresponding row in the pictures table
        # by construction, it should already have all the correct information
        # and the auto-incremented id and updated imageable_id

      }

      # close connection?


      #copy groups
      temp_posting.groups=original_posting.groups
    end
    redirect_to edit_user_posting_url(:id => temp_posting.id) and return
  end

  def promote
    @posting= current_user.postings.find(params[:id])
    @loadedPostingID = params[:id]
    @picture=@posting.pictures.first.photo.url(:original) rescue nil
    @picture= view_context.image_path("default_posting_picture.png") if @picture.nil?

    @link= user_posting_url(:user_name => current_user.user_name, :id => @posting.id)
    @title= @posting.title.gsub('\"', '')
    @description=@posting.description.gsub('\"', '')
  end

  def postToCraigslist
    @posting = current_user.postings.find(params[:id])
    zip_code_regex= /^\d{5}$/
    no_zip_code=@posting.zip_code.blank? && params[:zip_code].nil?
    invalid_saved_zip_code= @posting.zip_code.blank? || (@posting.zip_code=~zip_code_regex).nil?
    @invalid_zip_code_parameter=params[:zip_code].nil? || params[:zip_code].blank? || (params[:zip_code]=~zip_code_regex).nil?
    if no_zip_code || (invalid_saved_zip_code && @invalid_zip_code_parameter)
      @no_zip_code=true
    else
      @posting.update_attribute(:zip_code, params[:zip_code]) unless @invalid_zip_code_parameter
      redirect_to craigslist_loading_url and return
    end
  end

  def craigslist_loading
    @posting = current_user.postings.find(params[:id])
  end

  def craigslist_ajax
    @posting = current_user.postings.find(params[:id])

    require 'rubygems'
    require 'mechanize'
    require 'open-uri'


    # PAGE = GET PAGE FROM ZIP CODE TABLE

    def use_proxy_to_post_to_craigslist(proxy_ip, proxy_port)

      city_page=(CraigslistUrls.search @posting.zip_code).first.url
      a = Mechanize.new { |agent|
        agent.user_agent_alias = 'Mac Safari'
        agent.set_proxy(proxy_ip, proxy_port)
      }

      page=a.get(city_page)

      a.agent.http.ca_file="/etc/ssl/certs/ca-certificates.pem"
      a.agent.ssl_version="SSLv3"
      # a.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE

      type_of_posting_page=a.click(page.link_with(:text => 'post to classifieds'))

      form=type_of_posting_page.forms.first

      (category_id=CraigslistCategories.find_by_rcategory(@posting.category_name).ccategory) rescue nil
      if category_id.nil?
        category_id = CraigslistCategories.find_by_rcategory("General").ccategory
      end

      case category_id
        when "apartments"
        else # "for sale" category
          form.radiobutton_with(:value => "fs").check #  for sale has value 'fs'
          category_page=a.submit(form, form.buttons.first)

          form=category_page.forms.first
          form.radiobutton_with(:value => category_id.to_s).check #select category radio button given id, ex. computers is 7

          area_page=a.submit(form, form.buttons.first)

          form=area_page.forms.first
          if !form.radiobutton_with(:value => "1").nil? #  picks first sub-area in list if asked to pick subarea
            form.radiobutton_with(:value => "1").check
            create_posting_page_or_sub_area_page=a.submit(form, form.buttons.first)
            form=create_posting_page_or_sub_area_page.forms.first
            if !form.radiobutton_with(:value => "0").nil? #  picks "bypass neighborhood" if asked to pick neighborhood
              form.radiobutton_with(:value => "0").check
              create_posting_page=a.submit(form, form.buttons.first)
              form=create_posting_page.forms.first
            end
          else
            form=area_page.forms.first
          end
          form[form.fields[0].name]=@posting.title
          form[form.fields[1].name]=number_with_precision(@posting.price, :precision => 0)
          form[form.fields[2].name]=@posting.city+" "+@posting.state+", "+@posting.zip_code

          seller=User.find(@posting.user_id)

          form['FromEMail']=seller.email
          form['ConfirmEMail']=seller.email

          posting_url=user_posting_url(:user_name => current_user, :id => @posting.id, :cid => 'cl').gsub('reppio.com', 'yolodex.com')
          description=posting_description_html(@posting, posting_url, @sortedPicturesNoIndices, seller)

          if form.fields[11].nil?
            form[form.fields[6].name]= description
          else
            form[form.fields[11].name]= description
          end

          form.checkbox_with(:name => "wantamap").check # enter zip code into map info
          form['postal']=@posting.zip_code

          form.radiobutton_with(:value => "A").check # hide email address has value A


          image_or_geo_page=a.submit(form, form.buttons.first)
          if image_or_geo_page.forms.second.nil? #on geocode page
            form=image_or_geo_page.forms.first

            form['postal']=@posting.zip_code

            geo_coordinates=Geocoder.search(@posting.zip_code)[0]

            form['lat']= number_with_precision(geo_coordinates.latitude, :precision => 4)
            form['lng']= number_with_precision(geo_coordinates.longitude, :precision => 4)
            form['AreaID']=11
            form['seenmap']=1
            form['draggedpin']=0
            form['clickedinclude']=1
            form['geocoder_latitude']= number_with_precision(geo_coordinates.latitude, :precision => 4)
            form['geocoder_longitude']= number_with_precision(geo_coordinates.longitude, :precision => 4)
            form['geocoder_accuracy']=22
            form['geocoder_version']="2.2.0"

            image_or_geo_page=a.submit(form, form.buttons.first)
          end


          #save first image in medium size to tmp folder
          url=@sortedPicturesNoIndices.first.photo.url(:medium)
          file_name=url.gsub(/^.*\//, '')

          begin
            open(url, 'rb') do |infile|
              File.open("tmp/#{file_name}", 'wb') do |outfile|
                outfile.write(infile.read)
              end
            end


            # now on image upload page
            upload_form=image_or_geo_page.forms.first
            upload_form.file_uploads.first.mime_type = 'image/jpeg'
            upload_form.file_uploads.first.file_name = "tmp/#{file_name}"
            image_or_geo_page=upload_form.submit

            form=image_or_geo_page.forms[2]
            a.submit(form, form.buttons.first)
            return a.current_page().uri.to_s

          rescue OpenURI::HTTPError
            form=image_or_geo_page.forms.second
            a.submit(form, form.buttons.first)

            return a.current_page().uri.to_s
          end
      end
    end

    times_tried=0
    until times_tried==2
      @proxy=Proxy.first
      begin
        page_url=use_proxy_to_post_to_craigslist(@proxy.ip_address, @proxy.port)
        render :text => page_url and return
      rescue Exception => error
        logger.debug("------------------------")
        logger.debug(error)
        if @proxy
          @proxy.delete
          times_tried+=1
        else
          render :text => "error" and return
        end
      end
    end


  end

  def load_email_share_modal
    @posting=Posting.find(params[:id])
    respond_to do |format|
      format.js
    end

  end

  def promote_posting_by_email
    if current_user || (verify_recaptcha :private_key => '6Lf4g9wSAAAAAIikmx5jeG_LhEtNckIrCIMCvZ5T')

      if current_user || valid_email?(params[:sender_email])
        params[:sender_email]= current_user.email if current_user
        posting = Posting.find(params[:id])
        seller=User.find(posting.user_id)
        posting_url=user_posting_url(:user_name => seller.user_name, :id => posting.id)
        description=posting_description_html(posting, posting_url, @sortedPicturesNoIndices, seller, false)
        sender_name=(current_user ? "#{current_user.first_name} #{current_user.last_name}" : params[:sender_email])
        params[:emails].gsub(" ", "").split(",").each do |email|
          if valid_email?(email)
            PostingMailer.promote_posting_by_email(email, posting.title, description, params[:message], params[:sender_email], !current_user.nil?, sender_name).deliver
          end
        end

        if params[:email_copy]
          PostingMailer.promote_posting_by_email(params[:sender_email], posting.title, description, params[:message], params[:sender_email], !current_user.nil?, sender_name).deliver
        end


        respond_to do |format|
          format.js and return
        end

      else
        render :js => "alert('Please provide an email address.');" and return
      end
    else
      render :js => "alert('Check the CAPTCHA again.');" and return
    end
  end


  def remove_completed_posting
    postingToRemove = Posting.find(params[:id])

    postingToRemove.update_column(:hide_from_buyer_completed, 1) if params[:asRole] == "buyer"
    postingToRemove.update_column(:hide_from_seller_completed, 1) if params[:asRole] == "seller"


    redirect_to manage_completed_url, :notice => "Posting has been removed from your Completed tab."

  end


  def get_custom_fields
    render :partial => 'custom_fields', :locals => {:category_name => params[:cat]}, :layout => false
  end

  def post_from_bookmark
    if user=User.find_by_rep_it_token(params[:u])
      params[:posting][:group_ids]=[1]

      #uploadedPictures = params[:posting][:uploaded_pictures].split(",").map(&:to_i)
      #params[:posting].delete(:uploaded_pictures)
      params[:posting][:price] = params[:posting][:price].gsub(",", "").to_i

      custom_field_list=[]
      index=1
      while params.has_key?("custom_field"+index.to_s) do
        custom_field_list.push(params.delete("custom_field"+index.to_s))
        index+=1
      end

      @posting = user.postings.build(params[:posting])
      @posting.address = (@posting.address == "") ? user.zip_code : @posting.address #Can't use @posting.address ||= because that checks against nil, not ""
      custom_field_list.each { |custom_field| @posting.custom_fields.build(custom_field) }

      if @posting.save

        pictures_list=params[:pictures].split("|")
        pictures_list.each { |picture_url|
          url = URI.parse(picture_url)

          listing_image = Tempfile.new(["picture", ".jpg"])
          listing_image.binmode

          open(url) do |data|
            listing_image.write data.read
          end

          listing_image.rewind

          @posting.pictures.build(:photo => listing_image)

        }

        Alert.trigger_alert_for(@posting, user.user_name)

        @posting.update_attribute(:picture_display_order, ([*0..pictures_list.length-1].map! { |x| x.to_s }.join(',')))
        UpdatedPosting.create!(:title => @posting.title, :subcategory_name => @posting.category_name)

        # cache first pictures for reuse on search page
        postingPictures = Picture.where(:imageable_type => "Posting", :imageable_id => @posting.id)

        @posting.update_attribute(:first_picture_url, postingPictures[@posting.picture_display_order.split(",").first.to_i].photo.url(:large_thumb))
        begin
          user_picture=user.first_user_url
        rescue Exception
          user_picture="http://www.reppio.com/assets/default_profile_picture2.png"
        end
        @posting.update_attribute(:first_user_url, user_picture)

        user.update_attributes(:last_posting_street_address => @posting.address,
                               :last_posting_latitude => @posting.latitude, :last_posting_longitude => @posting.longitude)


        if params[:draft]
          @posting.update_attribute(:is_updated, false)
        end
        render :text => 'formResponse({"status":"success"});' and return
      else
        render :text => 'formResponse({"status":"fields"});' and return
      end

    else
      render :text => 'formResponse({"status":"user"});' and return
    end
  end

  def form_data_for_bookmark
    if @user=User.find_by_rep_it_token(params[:u])
      @category=params[:c]
      respond_to do |format|
        format.json
      end
    end
  end

  def promote_to_facebook_from_modal
    @posting=Posting.find(params[:id])
    current_user.update_attribute(:promote_postings_to_facebook, true)
    if session[:fb_token]
      begin
        me = FbGraph::User.me(session[:fb_token])
        me.feed!(
            :message => "Take a look at my listing: #{@posting.title}",
            :picture => @posting.first_picture_url,
            :link => user_posting_url(:user_name => current_user.user_name, :id => @posting.id),
            :name => @posting.title,
            :description => @posting.description
        )
      rescue Exception
        render :js => "$(\"#promoteModal .modal-body\").html(\"We do not have permission to post on your Facebook wall. <a href='http://www.reppio.com/auth/facebook' target='_blank'>Grant permission</a>\");" and return
      end
    else
      render :js => "$(\"#promoteModal .modal-body\").html(\"We could not post to your Facebook wall.  <a href='http://www.reppio.com/auth/facebook' target='_blank'>Login to Reppio with Facebook</a>\");" and return
    end

    render :js => "$(\"#promoteModal .modal-body\").html(\"Your listing has been shared on Facebook.\");setTimeout(function(){ $(\"#promoteModal\").modal(\"hide\");}, 3000);" and return

  end


  def google_feed
    @postings=Posting.find_all_by_deleted_and_is_updated(false, true)
  end

  def remove_listings
    reppio_admin_array=reppio_admins
    unless reppio_admin_array.include?(current_user.id)
      redirect_to :back
    end
  end

  def remove_listing
    reppio_admin_array=reppio_admins
    if reppio_admin_array.include?(current_user.id)
      posting=Posting.find(params[:posting_id])

      posting.groups.each do |group|
        group.postings.delete(posting)
        group.save
      end

      posting.destroy

    end

    redirect_to :back
  end

  def reppio_picks_index
    reppio_admin_array=reppio_admins
    if reppio_admin_array.include?(current_user.id)
      @postings=Posting.find_all_by_reppio_pick(true)
    else
      redirect_to :back
    end
  end

  def toggle_reppio_pick
    reppio_admin_array=reppio_admins
    if reppio_admin_array.include?(current_user.id)
      posting=Posting.find(params[:posting_id])
      posting.update_column(:reppio_pick, !posting.reppio_pick)
    end

    redirect_to :back
  end


  private

  def reppio_admins
    [1, #jack
     2, #samir
     7 #connie
    ]
  end


  def posting_description_html(posting, posting_url, posting_pictures, seller, include_contact_me_link=true)
    if include_contact_me_link
      description="<h3>Interested? <a href='#{posting_url}'>Contact Me Here</a></h3><BR>"
    else
      description=""
    end

    description<<"<table width='750' border='2' cellpadding='20'><tr><td width='400' valign='top' style='padding-right:20px;'>"
    description<<"<img src='#{posting_pictures.first.photo.url(:original_cropped)}' style='width:400px; height:auto; padding:5px; border:1px solid #777;'></td><td width='300' style='vertical-align:top;'>"
    description<<"<p><b>#{posting.title}</b></p>"
    description<<"<p><b>Price:</b> #{number_to_currency(posting.price)} #{posting.will_accept_other_offers ? "" : "(Will sell only at this price)"}</p>"
    description<<"<p><b>Condition:</b> #{posting.condition}</p>"
    description<< "<p><b>Seller:</b> #{seller.first_name} (Rep Score #{seller.repscore})</p>"
    description<<"<p><b>Address</b>: #{posting.city} #{posting.state},  #{posting.zip_code}</p>"
    description<< "<p><b>Description:</b> #{ truncate(posting.description, :length => 300, :separator => "...")} <a href='#{posting_url}'>Learn More</a></p>"

    posting.custom_fields.each do |custom_field|
      description<<"<p><b>#{custom_field.label}:</b> #{custom_field.value}</p>"
    end

    description<<"</td></tr>"


    if posting_pictures.length >= 2
      description<<"<tr><td colspan='2'><p>"
      posting_pictures.last(posting_pictures.length-1).each do |picture|
        description<<"<img src='#{picture.photo.url(:large_thumb)}' style='margin:5px;'>"
      end
      description<<"</p></td></tr>"
    end
    description<<"</table>"

    return description
  end

end
