class UploadController < ApplicationController
  before_filter :check_same_user

  # def index
  #   @pictures = Picture.all
  #   render :json => @pictures.collect { |p| p.to_jq_upload }.to_json
  # end
  
  def catchGet
    render :nothing => true and return
  end

  def create
    @picture = Picture.new
    @picture.photo = params[:picture][:path].first
    @picture.imageable_id = 67108864 + rand(10000000)
    if @picture.save
      respond_to do |format|
        format.html {                                         #(html response is for browsers using iframe sollution)
          render :json => [@picture.to_jq_upload(current_user.user_name)].to_json,
          :content_type => 'text/html',
          :layout => false
        }
        format.json {
          render :json => [@picture.to_jq_upload(current_user.user_name)].to_json
        }
      end
    else
      render :json => [{:error => "custom_failure"}], :status => 304
    end
  end

  def destroy
    if params[:id].to_i > 67108864
      #This is also done by a cron task every night. Having an ability to delete photos in this manner would be problematic given our current (non-)method for allowing users to rearrange photos -- EXCEPT the only way a user can trigger this method is if he hasn't saved the picture into the sort order anyway, i.e. using the "Delete" button in the file upload form. That said, it's still subject to some monkey business, since anyone visiting ":user_name/postings/upload_destroy/:id" can trigger this action ... so confirm the ID we're deleting is in the "not-yet-saved" category as well.
      
      @picture = Picture.find_by_imageable_id(params[:id])
      @picture.destroy
      render :json => true
    end
  end
  
  
end
