class ReviewsController < ApplicationController
  def respond
    review = current_user.reviews.find(params[:id]) #current_user.reviews.find won't work because
    review.response = params[:review][:response]
    review.save if !review.response.blank?
    redirect_to :back and return
    #current_user.reviews.find(params[:id]).update_attribute(:response,params[:response])
  end
  
  def create
    @user=current_user
    params[:review][:ids_of_users_that_voted]="#{params[:review][:user_id]},#{params[:review][:reviewer_user_id]}"
    @review = Review.new(params[:review])
    @reviewWasBlank=false
    if @review.save
      reviewed_by_x = (params[:review_from_seller] == "true" ? :reviewed_by_seller : :reviewed_by_buyer)
      Posting.find(@review.posting_id).update_attribute(reviewed_by_x,true)
      userdata = Userdata.find_by_user_id(@review.user_id)
      reviewResponseType = (@review.recommended ? :positive_reviews : :negative_reviews)
      reviewResponseCurrent = (@review.recommended ? userdata.positive_reviews : userdata.negative_reviews)
      userdata.update_attribute(reviewResponseType, reviewResponseCurrent + 1)
    end

    respond_to do |format|
      format.js
    end

  end
  
  def vote
    review = Review.find(params[:id])
    if params[:vote].to_i == -1
      review.count_not_useful+= 1
    elsif params[:vote].to_i == 1
      review.count_useful+= 1
      review.ids_of_users_that_voted= review.ids_of_users_that_voted+",#{current_user.id}"
      # increments useful reviews count only when number of votes hits 5 for this individual review
      if review.count_useful==5
        userdata = Userdata.find_by_user_id(review.reviewer_user_id)
        userdata.update_attribute(:useful_reviews, userdata.useful_reviews + 1)
      end
    end
    review.save
    render :nothing=>true and return
  end


end