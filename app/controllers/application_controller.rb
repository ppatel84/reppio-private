class ApplicationController < ActionController::Base
  
  helper_method :current_user, :signed_in?, :same_user?,
                :check_same_user, :rep_bucket, :require_login, :check_valid_request,
                :get_unread_count,
                :valid_float?, :valid_integer?,:valid_email?,:get_category_search_options,:map_interest_to_category,
                :push_send_message
  before_filter :set_timezone, :require_login

    def set_timezone
      Time.zone = current_user.time_zone rescue "Central Time (US & Canada)"
    end

  protect_from_forgery

  protected

  def current_user
    @current_user ||= User.find_by_id(session[:user_id])
  end

  def rep_bucket
    # expose correct buckets to the views
    if (Rails.env == "production"); @rep_bucket = "reppio-repscore" end
    if (Rails.env == "development"); @rep_bucket = "reppio-dev-repscore" end
    @rep_bucket
  end

  def signed_in?
    !!current_user
  end

  def same_user?
    if !params[:user_name].nil? && !current_user.nil?
      return current_user.user_name == params[:user_name].to_s
    end
    return !current_user.nil?
  end

  def check_same_user
    if !same_user?
      if signed_in?
        redirect_to user_url(:user_name => current_user.user_name) and return #Get sent back to your own user page if you're logged in and you try doing anything naughty
      end
      redirect_to root_url and return
    end
  end
  
  def check_valid_request #currently only works to look up postings. Confirm that any AJAX requests are made by the proper party.
    if params[:id]
      if Posting.find_by_id_and_user_id(params[:id], current_user.id).nil? #Confirm that the posting belongs to the user
        respond_to do |format|
            format.html { render :text => "Invalid request" and return }
            format.json { render json: "Invalid request" and return}        
        end
      end
    else
      true
    end
  end
  
  def require_login
    unless signed_in?
      redirect_to interstitial_url(:redirect =>request.path[1..-1]) and return #Chop off the / that begins the request.path, just for the sake of appearance
    end
  end
  
  def bypass_login_if_logged_in

    if signed_in?
      @user = current_user

      # if user has received confirmation email, but has not set up
      # a username or location
      if current_user.user_name.nil?
        redirect_to create_user_name_url and return
      end

      redirect_to search_url
    end
  end

  def current_user=(user)
    @current_user = user
    session[:user_id] = user.id
  end

  def get_unread_count
    Conversation.where("(potential_buyer_id = ? AND unread_by_buyer = true AND hide_from_buyer=false) OR (seller_id = ? AND unread_by_seller = true AND hide_from_seller=false)", current_user.id, current_user.id).count
  end

  def valid_float?(float_string)
    Float(float_string) rescue false
  end

  def valid_integer?(integer_string)
    Integer(integer_string) rescue false
  end

  def valid_email?(email_string)
    email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    !(email_string =~ email_regex).nil?
  end

  def get_category_search_options(category)
    # Given category, find all subcategories to search through
    Posting.all_subcategories_of_category(category).collect { |subcategory| "(#{subcategory})" }.join(" | ")
  end

  def map_interest_to_category(interest)
    case interest
      when "Entertainment"
        "CDs, DVD & Blu-ray"
      when "Sports"
        "Sporting Goods"
      else
        interest
    end
  end


  def push_send_message(params)

    json_params = {
        "device_tokens" => params[:device_tokens],
        "aps" => {
            "badge" => params[:badges],
            "alert" => params[:message]
        },
        "add" => params[:add],
        #"rid" => params[:recipient],
        #"conversation" => "1234567890 1234567890 1234567890 1234567890 1234567890 1"
    }.to_json

    pp json_params

    uri = URI.parse(params[:push_host])
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Post.new(params[:push_url])
    request.basic_auth(params[:app_key], params[:app_secret])
    request.add_field('Content-Type', 'application/json')
    request.body = json_params

    response = http.request(request)

    logger.debug(response.inspect)

    response

  end

end
