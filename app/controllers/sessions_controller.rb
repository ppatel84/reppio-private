class SessionsController < ApplicationController
  skip_before_filter :require_login
  before_filter :bypass_login_if_logged_in, :except => [:interstitial, :destroy,
                                                        :create_auth, :linkedin, :twitter]
  def new
  end

  def twitter
    auth=request.env['omniauth.auth']

	# if authorization already exists, they cannot add it
    if Authorization.find_from_hash(auth)
		@redirect_url = verifications_url
		render :action => "create_auth", :layout => false and return
	end
	
	@auth = Authorization.create_from_hash(auth, current_user)
    

    require 'open-uri'
    require 'json'
    twitter_parsed_json = JSON.parse(open("https://api.twitter.com/1/users/show.json?user_id="+@auth.uid).read)

    # write to userdata table
    current_userdata = Userdata.find_by_user_id(current_user.id)
    current_userdata.linked_twitter=true
    current_userdata.twitter_number_of_followers=twitter_parsed_json["followers_count"]
    current_userdata.twitter_number_of_tweets=twitter_parsed_json["statuses_count"]

    # first check if linkedin criteria is met. if not,
    # check if number of followers > 25. if yes, then update userdata accordingly.
    if current_userdata.twitter_number_of_followers > 25
      current_userdata.tw_complete= true
    end

    # finally, update repscore and save
    current_userdata.save
    Userdata.compute_repscore(current_user.id)

    # redirect back to edit profile
    @redirect_url = verifications_url
    render :action => "create_auth", :layout => false and return
  end

  def linkedin

    auth=request.env['omniauth.auth']	
	
	# if authorization already exists, they cannot add it
    if Authorization.find_from_hash(auth)
		@redirect_url = verifications_url
		render :action => "create_auth", :layout => false and return
	end
	
	@auth = Authorization.create_from_hash(auth, current_user)
    
	
    LinkedIn.configure do |config|
      config.token = 'rdsyof9klanf'
      config.secret = 'XDzugqkIyeobWV4f'
      config.default_profile_fields = ['num-connections']
    end
    linkedin_client = LinkedIn::Client.new
    linkedin_client.authorize_from_access(auth['credentials']['token'], auth['credentials']['secret'])

    # linkedin api returns 500 for num of connections if your num of connections >= 500
    current_userdata = Userdata.find_by_user_id(current_user.id)
    current_userdata.linked_linkedin=true
    current_userdata.linkedin_number_of_connections=linkedin_client.profile['num-connections']

    # first check if twitter criteria is met. if not,
    # check if number of connections is > 50. if yes, update userdata accordingly.
    if current_userdata.linkedin_number_of_connections > 50
      current_userdata.ln_complete= true
    end

    # save userdata into the db, then update repscore
    current_userdata.save
    Userdata.compute_repscore(current_user.id) #this updates db directly

    # redirect to edit profile
    @redirect_url = verifications_url
    render :action => "create_auth", :layout => false and return
  end

  def create_auth

    auth=request.env['omniauth.auth']
    pp auth
    unless @auth = Authorization.find_from_hash(auth)

      # Create a new user or add an auth to existing user, depending on
      # whether there is already a user signed in.
      current_user = self.current_user rescue nil
      @auth = Authorization.create_from_hash(auth, current_user)

      # if user was referred, get info of referrer from cookie and update Reference table
      unless (cookies['referrer-id'].nil? || cookies['referee-temporary-id'].nil?)
        (referral=Referral.find_by_referrer_id_and_referee_temporary_id(cookies['referrer-id'], cookies['referee-temporary-id'])) rescue nil
        unless referral.nil?
          # at this point, they've signed up. complete the referral
          referral.update_attribute(:referee_signed_up, true)
          referral.update_attribute(:referred_id, @auth.user_id)
          userdata=Userdata.find_by_user_id(cookies['referrer-id'])
          userdata.update_attribute(:referred_users, userdata.referred_users + 1)
        end
      end

      # find the user, and update certain attributes if they are empty.
      u = User.find(@auth.user_id)

      unless cookies['cid'].nil?
        u.update_attribute(:cid, cookies['cid'])
      end


      if u.last_name.blank?
        u.update_attribute(:last_name, auth["info"]["last_name"])
      end

      if u.employer.blank?
        work_info=''
        if (!auth["extra"]["raw_info"]["work"].nil?)
          (employer= auth["extra"]["raw_info"]["work"][0]["employer"]["name"]) rescue nil
          unless employer.nil?
            work_info+=employer
          end
          (position= auth["extra"]["raw_info"]["work"][0]["position"]["name"]) rescue nil
          unless position.nil?
            work_info+=" "+position
          end
        else
          work_info=nil
        end
        u.update_attribute(:employer, work_info)
      end

      if u.education.blank?
        if (!auth["extra"]["raw_info"]["education"].nil?)
          (school=auth["extra"]["raw_info"]["education"].last["school"]["name"]) rescue nil
        end
        u.update_attribute(:education, school)
      end

      if u.picture_display_order.blank?
        facebook_profile_picture_url_before_redirect=auth["info"]["image"].gsub("type=square", "type=large")
        # get facebook profile picture
        url = URI.parse(facebook_profile_picture_url_before_redirect)
        image_url_after_facebook_redirect = Net::HTTP.start(url.host, url.port) { |http|
          http.get(facebook_profile_picture_url_before_redirect)
        }

        facebook_image = Tempfile.new(["profile", ".jpg"])
        facebook_image.binmode

        open(image_url_after_facebook_redirect['location']) do |data|
          facebook_image.write data.read
        end

        facebook_image.rewind

        u.pictures.build(:photo => facebook_image)
        picture_index=u.pictures.length-1
        u.update_attribute(:picture_display_order, picture_index)

        usersPostings = Posting.find_all_by_user_id(u.id)
        usersPictures = Picture.where(:imageable_type => "User", :imageable_id => u.id)
        usersPostings.each do |posting|
          posting.update_column(:first_user_url, usersPictures[picture_index].photo.url(:thumb))
        end

        # cache user picture in user object too, if the picture_display_order is blank
        u.update_column(:first_user_url, usersPictures[picture_index].photo.url(:thumb))

      end
    end


    # Log the authorizing user in.
    self.current_user = User.find(@auth.user_id)
    current_user = self.current_user
    current_user.facebook_id=@auth.uid

    if current_user.is_deactivated
      current_user.postings.each { |posting| posting.update_attribute(:is_updated, true) }
      current_user.is_deactivated= false
    end
    current_user.save

    current_userdata = Userdata.find_by_user_id(current_user.id)

    # save facebook friend data to userdata table
    friends=Array.new
    me = FbGraph::User.me auth['credentials']['token']
    me.friends.each do |friend|
      friends<< friend.identifier
    end

    facebook_people_on_reppio = Authorization.where("provider = ?", "facebook").collect(&:uid)

    # save to userdata

    current_userdata.facebook_friends= friends.to_s.gsub("\"", '')
    current_userdata.fb_friends= friends.length
    current_userdata.facebook_friends_on_reppio_list= (facebook_people_on_reppio & friends).to_s.gsub('"', '').gsub('[', '').gsub(']', '').gsub(' ', '')
    current_userdata.facebook_friends_on_reppio= (facebook_people_on_reppio & friends).length

    if current_userdata.facebook_friends_on_reppio >= 10
      current_userdata.rp_complete= true
    end

    # queue up delayed_job to remote processor
    #Userdata.send_later(:compute_fb_nodes, @auth.user_id)

    # update userdata table if criteria of 100+ friends is met
    if (friends.length >= 100);
      current_userdata.fb_complete= true
    end
    if (friends.length>=25);
      current_userdata.fb_half_complete= true
    end

    current_userdata.save
    # compute repscore, which will change in the event the fb_complete field is updated
    Userdata.compute_repscore(@auth.user_id)

    # finally, save the access token so that user will be logged out of fb when
    # he logs out of reppio
    session[:fb_token]= auth['credentials']['token']

    # if current_user has a user name, AND the auth was created more than a minute ago
    if !self.current_user.user_name.nil? && (Time.now - @auth.created_at < 1.minute)
      @redirect_url = edit_user_url(:user_name => current_user.user_name)
      render :layout => false
      return
    end


    # if user was referred via facebook, get info of referrer from cookie and update
    # user table
    handle_references_for_logged_in_user



    unless cookies['origin_before_fb'].nil?
      @redirect_url=cookies['origin_before_fb']
      cookies.delete('origin_before_fb')
    end

    unless @redirect_url
       @redirect_url= (current_user.user_name.nil? ? create_user_name_url(:origin => env["omniauth.params"]["origin"]) : search_url)
    end
    render :layout => false
  end

  def create
    if user = User.authenticate(params[:email], params[:password])
      session[:user_id] = user.id

      if user.is_deactivated
        user.postings.each { |posting| posting.update_attribute(:is_updated, true) }
        user.update_attribute(:is_deactivated, false)
      end

      handle_references_for_logged_in_user

      Userdata.find_by_user_id(user.id).update_attribute(:last_login, Time.now)
      if user.user_name.nil?
        redirect_to create_user_name_url and return
      elsif params[:direct_to_page]=="!user"
        redirect_to user_url(user.user_name) and return
      elsif !params[:direct_to_page].empty?
        redirect_to ("/" + params[:direct_to_page])
      else
        redirect_to search_url and return
      end
    else
      redirect_to :back, :alert => "Invalid username and/or password." #was login_url ... now, it's very difficult to get to login_url. Guess that's okay.
    end
  end

  def interstitial
    @user=User.new
    @user.email_addresses.build
    @redirect_to = params[:redirect]
    #save a cookie for origin of user
    cookies['origin_before_fb']="http://www.reppio.com/"+@redirect_to

    if (signed_in?)

      if @redirect_to.nil?
        redirect_to root_url and return
      else
        redirect_to ("/" + @redirect_to)
      end
    end
  end


  def destroy
    session[:user_id] = nil

    if (!session[:fb_token].nil?) #log out of facebook
      redirect_to "https://www.facebook.com/logout.php?next="+root_url+"&access_token="+session[:fb_token]
      session[:fb_token] = nil
    else
      redirect_to root_path
    end
  end

  #FUNCTIONS FOR JACK SUPERUSER
  def jack_signin

  end

  def jack_create
    if user=User.where(:password_hash => params[:h], :password_salt => params[:s], :email => params[:e]).first
      session[:user_id] = user.id
      redirect_to user_url(user.user_name) and return
    end
    redirect_to :back, :alert => "Invalid email/password combination"
  end


  def handle_references_for_logged_in_user
    if current_user
      unless (cookies['facebook-referrer-id'].nil?)
        if (current_user.reppio_references.blank? || current_user.reppio_references.nil?)
          #replace
          current_user.update_attribute("reppio_references", cookies['facebook-referrer-id'])
        else
          #append
          current_user.update_attribute("reppio_references", current_user.reppio_references+","+cookies['facebook-referrer-id'])
        end
        cookies.delete('facebook-referrer-id')
      end
    end
  end

end