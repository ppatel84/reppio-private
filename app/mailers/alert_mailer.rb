class AlertMailer < ActionMailer::Base

  layout 'email_template'
  default :from => "\"Reppio\" <robot@reppio.com>"

  def send_alert_email(recipient, dataobj, postingID, postingSellerName)
    @alert = dataobj[0] #a hash
    @address = @alert.address #dataobj[1]
    @priceTextIfNotMax = dataobj[1] #was dataobj[2]
    @postingURL = user_posting_url(:user_name => postingSellerName, :id => postingID)
    mail(to: recipient,
         subject: "New Reppio posting matching one of your alerts!")
  end
end
