class ReferenceMailer < ActionMailer::Base

  layout 'email_template'
  default :from => "\"Reppio\" <robot@reppio.com>"

  def send_reference_request(recipient_email, referrer_user_id, referrer_first_name, referrer_last_name, referrer_email, personal_message)
    @invite_link=reppio_domain+"/?fbr=#{referrer_user_id}"
    @referrer_first_name=referrer_first_name
    @referrer_last_name=referrer_last_name
    @personal_message=(personal_message.blank? ? "Join me on Reppio!" :
        personal_message)

    mail(to: recipient_email, subject: "Write #{referrer_first_name} #{referrer_last_name} a reference on Reppio",
         from: named_from_email(reppio_email_address, "#{referrer_first_name} #{referrer_last_name}"),
         reply_to: named_from_email(referrer_email, "#{referrer_first_name} #{referrer_last_name}"))
  end

  def send_invite_no_name(recipient_email, referrer_user_id, referrer_first_name, referrer_last_name, referrer_email, personal_message)
    @invite_link=reppio_domain+"/?r=#{referrer_user_id}"
    @referrer_first_name=referrer_first_name
    @referrer_last_name=referrer_last_name
    @personal_message=personal_message

    mail(to: recipient_email, subject: "Join Me on Reppio",
         from: named_from_email(reppio_email_address, "#{referrer_first_name} #{referrer_last_name}"),
         reply_to: named_from_email(referrer_email, "#{referrer_first_name} #{referrer_last_name}"))
  end

end
