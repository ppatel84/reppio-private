class PaymentMailer < ActionMailer::Base
  layout 'email_template'
  default :from => "\"Reppio\" <robot@reppio.com>"

  def payment_has_been_requested(buyer_email, buyer_first_name, posting_title, posting_link, buy_link)
    @buyer_email=buyer_email
    @buyer_first_name=buyer_first_name
    @posting_title=posting_title
    @posting_link=posting_link
    @buy_link=buy_link

    mail(to: buyer_email, subject: "Payment Requested for \"#{posting_title}\"")
  end


  def payment_complete(seller_email, seller_first_name, posting_title, posting_link, amount, buyer_first_name, buyer_user_name)
    @seller_first_name=seller_first_name
    @posting_title=posting_title
    @posting_link=posting_link
    @amount=amount
    @buyer_first_name=buyer_first_name
    @buyer_user_name=buyer_user_name

    mail(to: seller_email, subject: "Payment Received for \"#{posting_title}\"")
  end
end
