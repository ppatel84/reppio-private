class ReviewMailer < ActionMailer::Base

  layout 'email_template'
  default :from => "\"Reppio\" <robot@reppio.com>"

  def pending_review_email(receiver_email, postingTitle, other_party_is_buyer_or_seller, other_party_first_name, other_party_repscore)
    @postingTitle=postingTitle
    @other_party_is_buyer_or_seller=other_party_is_buyer_or_seller
    @other_party_first_name=other_party_first_name
    @other_party_repscore=other_party_repscore
    mail(to: receiver_email, subject: "Review #{other_party_first_name} re: \"#{postingTitle}\"")
  end
end
