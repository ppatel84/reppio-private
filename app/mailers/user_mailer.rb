class UserMailer < ActionMailer::Base
  layout 'email_template'
  default :from => "\"Reppio\" <robot@reppio.com>"

  def password_reset(recipient, name, new_password)
    @name=name
    @new_password=new_password
    @set_password_link = settings_url
    mail(to: recipient,
         subject: 'Your Reppio Password was Reset')
  end

  def send_email_confirmation(recipient, confirmation_code, first_name, origin)
    @first_name=first_name
    @email= recipient
    @confirmation_code=confirmation_code
    @link=reppio_domain+confirm_path(:email => @email, :confirmation_code => @confirmation_code, :origin => origin)
    mail(to: recipient,
         subject: "Please confirm your email address.")
  end

  def send_email_confirmation_with_password(recipient, confirmation_code, first_name, origin, password, posting_title)
    @first_name=first_name
    @email= recipient
    @confirmation_code=confirmation_code
    @postingTitle = posting_title
    @password=password
    @link=reppio_domain+confirm_path(:email => @email, :confirmation_code => @confirmation_code, :origin => origin)
    mail(to: recipient,
         subject: "Re: " + posting_title)
  end

  def send_password_after_confirmation(recipient, first_name, password)
    @first_name=first_name
    @email=recipient
    @password=password
    mail(to: recipient,
         subject: "Your Temporary Password for Reppio")
  end

  def send_secondary_email_confirmation(email, confirmation_code, first_name)
    @first_name=first_name
    @email= email
    @confirmation_code=confirmation_code
    @link=reppio_domain+confirm_secondary_email_path(:email => email, :confirmation_code => confirmation_code)
    mail(to: email,
         subject: "Validate your email address on Reppio.")
  end

  def contact_reppio(sender_name, sender_email, sender_message)
    @sender_name=sender_name
    @sender_email=sender_email rescue "robot@reppio.com"
    @sender_message = sender_message

    mail(to: "jack@reppio.com", subject: "Message from Reppio contact form",
         from: named_from_email("#{@sender_email}", "#{@sender_name}"),
         reply_to: named_from_email("#{@sender_email}", "#{@sender_name}"))

  end

  def welcome(recipient_email, user_name)
    @user_name=user_name
    mail(to: recipient_email,
         subject: "Welcome to Reppio!") do |format|
      format.html { render :layout => false }
    end
  end
end
