class GroupMailer < ActionMailer::Base

  layout 'email_template'
  default :from => "\"Reppio\" <robot@reppio.com>"

  def send_invitation_to_join_group(recipient_email, referrer_first_name, referrer_last_name, referrer_email,
      group_short_name, group_long_name, personal_message)
    @group_link=reppio_domain+"/groups/"+group_short_name
    @referrer_first_name=referrer_first_name
    @referrer_last_name=referrer_last_name
    @group_long_name=group_long_name
    @personal_message=(personal_message.blank? ? "Join me on \"#{group_long_name}\" group on Reppio, so that we can give each other first dibs or share our stuff." :
        personal_message)

    mail(to: recipient_email, subject: "Join #{@referrer_first_name} #{@referrer_last_name} in #{@group_long_name} group",
         from: named_from_email(reppio_email_address, "#{referrer_first_name} #{referrer_last_name}"),
         reply_to: named_from_email(referrer_email, "#{referrer_first_name} #{referrer_last_name}"))
  end

  def request_to_join_a_group(group_owner_first_name, group_owner_email, group_name, group_url,
      requester_first_name, requester_url, accept_link_url, ignore_link_url)
    @group_owner_first_name=group_owner_first_name
    @group_name=group_name
    @group_url=group_url
    @requester_first_name=requester_first_name
    @requester_url=requester_url
    @accept_link_url=accept_link_url
    @ignore_link_url=ignore_link_url
    mail(to: group_owner_email, subject: "#{requester_first_name} would like to join \"#{group_name}\"")
  end
end
