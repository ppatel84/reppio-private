class PostingMailer < ActionMailer::Base

  layout 'email_template'
  default :from => "\"Reppio\" <robot@reppio.com>"


  def send_email_flagged_posting(recipient, first_name, user_name, posting_id, posting_title)
    @first_name=first_name
    @email=recipient
    @user_name=user_name
    @posting_id=posting_id
    @posting_title=posting_title
    @link = reppio_domain + edit_user_posting_path(:user_name => @user_name, :id => @posting_id)
    mail(to: recipient,
         subject: "Your Reppio Posting is expiring!")
  end


  def promote_posting_by_email(recipient_email, posting_title,  html_description, message, sender_email, is_verified, sender_name)
    @html_description=html_description.html_safe
    @posting_title=posting_title
    @message=message
    @verified_message=is_verified ? '' : "<span style='color:#ccc; text-align:center; font-size:10px;'>* Please note, the sender's email address has not been verified.</span>".html_safe
    mail(to: recipient_email, subject: "Reppio Listing: #{posting_title}",
         from: named_from_email(reppio_email_address, sender_email),
         reply_to: named_from_email(sender_email, sender_name))
  end

end
