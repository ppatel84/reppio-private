class ConversationMailer < ActionMailer::Base
  layout 'email_template'
  default :from => "\"Reppio\" <robot@reppio.com>"

  def email_message_between_users(recipient, sender_first_name, sender_repscore, listing_title, body_text, link_to_conversation)
    @body_text=body_text
    @link_to_conversation=link_to_conversation
    @sender_first_name=sender_first_name
    @sender_repscore=sender_repscore
    @listing_title=listing_title
    mail(to: recipient, subject: "Message from #{sender_first_name} re: \"#{listing_title}\"")
  end

  def offer_accepted(recipient, offerPrice, postingTitle, sender_first_name, sender_repscore, conversationLink)
    @offerPrice = offerPrice
    @postingTitle = postingTitle
    @sender_first_name = sender_first_name
    @sender_repscore=sender_repscore
    @link_to_conversation = conversationLink
    mail(to: recipient, subject: "Offer accepted by #{sender_first_name} re: \"#{postingTitle}\"")
  end

  def offer_updated(recipient, offerPrice, postingTitle, sender_first_name, sender_repscore, conversationLink, message='')
    @offerPrice = offerPrice
    @postingTitle = postingTitle
    @sender_first_name = sender_first_name
    @sender_repscore=sender_repscore
    @link_to_conversation = conversationLink
    @message=message
    mail(to: recipient, subject: "Offer received from #{sender_first_name} re: \"#{postingTitle}\"")
  end

end
