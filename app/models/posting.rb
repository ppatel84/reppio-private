class Posting < ActiveRecord::Base

  def self.notification_for_delisting_posting
    # this function gets called every night via a cron job. it updates a field in the postings table
    # that indicates whether or not the seller of a given posting should be notified for its removal.

    #check appointments table to see which confirmed appointments have past.
    #   for each confirmed appointment that hasn't been checked, check completion.
    #   if not, then we want to notify the seller to take an action to delist
    Appointment.find_all_by_appointment_confirmed(1).each do |appointment|
      relevantPosting = Posting.find(Conversation.find(appointment.conversation_id).posting_id)

      if (appointment.end_time < Time.now)
        relevantPosting.update_column(:delist_notify_action, 1) #0 no need to notify, since no appointments have passed for this posting. OR appointments have passed and action has been taken
                                                                #1 notify, an appointment has passed and no action has been taken
      end

      appointment.update_column(:appointment_checked, 1)
    end

  end

  ####### HOW TO CHANGE / ADD CATEGORIES #####################################
  # 1. if category is a top-level category (has no parent category), add to top-level categories method
  # 2. if category has parents or children, set proper relations in immediate_children_of_category method
  # 3. if category has no children, add to category_fields




   #run rake jobs:work RAILS_ENV=staging or rake jobs:work RAILS_ENV=production to start a process that will
  # clear the queue. this should be done ONCE only.
  def self.post_to_craigslist(posting_id)
    posting=Posting.find(posting_id)

    sorted_pictures = []
    (sort_array = posting.picture_display_order.split(",").map! { |x| x.to_i }).each do |index|
      sorted_pictures.push(posting.pictures.fetch(index))
    end

    require 'rubygems'
    require 'mechanize'
    require 'open-uri'


    # PAGE = GET PAGE FROM ZIP CODE TABLE


    until false
      proxy=Proxy.first
      proxy_ip=proxy.ip_address
      proxy_port=proxy.port
      begin
        city_page=(CraigslistUrls.search posting.zip_code).first.url
        a = Mechanize.new { |agent|
          agent.user_agent_alias = 'Mac Safari'
          agent.set_proxy(proxy_ip, proxy_port)
        }

        page=a.get(city_page)

        a.agent.http.ca_file="/etc/ssl/certs/ca-certificates.pem"
        a.agent.ssl_version="SSLv3"
        # a.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE

        type_of_posting_page=a.click(page.link_with(:text => 'post to classifieds'))

        form=type_of_posting_page.forms.first

        (category_id=CraigslistCategories.find_by_rcategory(posting.category_name).ccategory) rescue nil
        if category_id.nil?
          category_id = CraigslistCategories.find_by_rcategory("General").ccategory
        end

        form.radiobutton_with(:value => "fs").check #  for sale has value 'fs'
        category_page=a.submit(form, form.buttons.first)

        form=category_page.forms.first
        form.radiobutton_with(:value => category_id.to_s).check #select category radio button given id, ex. computers is 7

        area_page=a.submit(form, form.buttons.first)

        form=area_page.forms.first
        if !form.radiobutton_with(:value => "1").nil? #  picks first sub-area in list if asked to pick subarea
          form.radiobutton_with(:value => "1").check
          create_posting_page_or_sub_area_page=a.submit(form, form.buttons.first)
          form=create_posting_page_or_sub_area_page.forms.first
          if !form.radiobutton_with(:value => "0").nil? #  picks "bypass neighborhood" if asked to pick neighborhood
            form.radiobutton_with(:value => "0").check
            create_posting_page=a.submit(form, form.buttons.first)
            form=create_posting_page.forms.first
          end
        else
          form=area_page.forms.first
        end
        form[form.fields[0].name]=posting.title
        form[form.fields[1].name]=posting.price
        form[form.fields[2].name]=posting.city+" "+posting.state+", "+posting.zip_code

        seller=User.find(posting.user_id)

        form['FromEMail']=seller.email
        form['ConfirmEMail']=seller.email

        posting_url="http://www.reppio.com/#{seller.user_name}/postings/#{posting.id}?cid=cl".gsub('reppio.com', 'yolodex.com')

        description="<h3>Interested? <a href='#{posting_url}'>Contact Me Here</a></h3><BR>"
        description<<"<table width='750' border='2' cellpadding='20'><tr><td width='400' valign='top' style='padding-right:20px;'>"
        description<<"<img src='#{sorted_pictures.first.photo.url(:original_cropped)}' style='width:400px; height:auto; padding:5px; border:1px solid #777;'></td><td width='300' style='vertical-align:top;'>"

        description<<"<p><b>Price:</b> $#{posting.price} #{posting.will_accept_other_offers ? "" : "(Will sell only at this price)"}</p>"
        description<<"<p><b>Condition:</b> #{posting.condition}</p>"
        description<< "<p><b>Seller:</b> #{seller.first_name} (Rep Score #{seller.repscore})</p>"
        description<<"<p><b>Address</b>: #{posting.city} #{posting.state},  #{posting.zip_code}</p>"
        description<< "<p><b>Description:</b> #{posting.description} <a href='#{posting_url}'>Learn More</a></p>"

        posting.custom_fields.each do |custom_field|
          description<<"<p><b>#{custom_field.label}:</b> #{custom_field.value}</p>"
        end

        description<<"</td></tr>"


        if sorted_pictures.length >= 2
          description<<"<tr><td colspan='2'><p>"
          sorted_pictures.last(sorted_pictures.length-1).each do |picture|
            description<<"<img src='#{picture.photo.url(:large_thumb)}' style='margin:5px;'>"
          end
          description<<"</p></td></tr>"
        end
        description<<"</table>"
        
        
        
        if form.fields[11].nil?
          form[form.fields[6].name]= description
        else
          form[form.fields[11].name]= description
        end

        form.checkbox_with(:name => "wantamap").check # enter zip code into map info
        form['postal']=posting.zip_code

        form.radiobutton_with(:value => "A").check # hide email address has value A


        image_or_geo_page=a.submit(form, form.buttons.first)
        if image_or_geo_page.forms.second.nil? #on geocode page
          form=image_or_geo_page.forms.first

          form['postal']=posting.zip_code

          geo_coordinates=Geocoder.search(posting.zip_code)[0]

          form['lat']= geo_coordinates.latitude
          form['lng']= geo_coordinates.longitude
          form['AreaID']=11
          form['seenmap']=1
          form['draggedpin']=0
          form['clickedinclude']=1
          form['geocoder_latitude']= geo_coordinates.latitude
          form['geocoder_longitude']= geo_coordinates.longitude
          form['geocoder_accuracy']=22
          form['geocoder_version']="2.2.0"

          image_or_geo_page=a.submit(form, form.buttons.first)
        end


        #save first image in medium size to tmp folder
        url=sorted_pictures.first.photo.url(:medium)
        file_name=url.gsub(/^.*\//, '')

        begin
          open(url, 'rb') do |infile|
            File.open("tmp/#{file_name}", 'wb') do |outfile|
              outfile.write(infile.read)
            end
          end


          # now on image upload page
          upload_form=image_or_geo_page.forms.first
          upload_form.file_uploads.first.mime_type = 'image/jpeg'
          upload_form.file_uploads.first.file_name = "tmp/#{file_name}"
          image_or_geo_page=upload_form.submit

          form=image_or_geo_page.forms[2]

          preview_page= a.submit(form, form.buttons.first)
          form=preview_page.forms.first
          a.submit(form, form.buttons.first)

          return

        rescue OpenURI::HTTPError
          form=image_or_geo_page.forms.second
          preview_page= a.submit(form, form.buttons.first)
          form=preview_page.forms.first
          a.submit(form, form.buttons.first)
          return
        end
      rescue Exception => error
        logger.debug("------------------------")
        logger.debug(error)
        proxy.delete if proxy
      end
    end
  end




  def self.get_hard_coded_categories
    # NO PLURALS - THEY GET ADDED IN THE CHECK    
    temphash = {
        "ipod" => "Sound & Audio Devices",
        "oven" => "Appliances",
        "iphone" => "Cell Phones",
        "chevy" => "Cars & Trucks",
        "xbox" => "Video Games",
        "mp3 player" => "Sound & Audio Devices",
        "luggage" => "Travel",
        "helmet" => "Sports & Outdoor Gear",
        "bed" => "Beds",
        "scooter" => "Sports & Outdoor Gear",
        "lamp" => "Household",
        "tie" => "Fashion Accessories",
        "purse" => "Handbags",
        "headphone" => "Sound & Audio Devices",
        "ticket" => "Tickets & Events",
        "tix" => "Tickets & Events",
        "shoe" => "Shoes",
        "wallet" => "Fashion Accessories",
        "handbag" => "Handbags",
        "sunglass" => "Sunglasses",
        "sunglasses" => "Sunglasses",
        "refrigerator" => "Appliances",
        "iphone case" => "Accessories & Covers",
        "iphone cover" => "Accessories & Covers",
        "car" => "Cars & Trucks",
        "bicycle" => "Bicycles",
        "computer" => "Desktops",
        "bike" => "Bicycles",
        "gift" => "Deals & Giftcards",
        "giftcard" => "Deals & Giftcards"
    }
    temphash
  end

  def self.flag_postings_for_deletion
    cutoff_time = Time.now - 12.days
    flagged_postings = Posting.where("updated_at < '#{cutoff_time.to_s}'")
    flagged_postings.each { |posting|
      posting.update_attribute(:flag_for_deletion, true)
      user = User.find(posting.user_id)
      PostingMailer.send_email_flagged_posting(user.email,
                                               user.first_name,
                                               user.user_name,
                                               posting.id,
                                               posting.title).deliver
    }
  end


  def self.delete_postings

    # the flags don't matter. we're just deleting everything older than 14 days
    Posting.where(:flag_for_deletion => true).update_all(:deleted => true)

  end

  def self.reset_view_count
    Posting.find_each do |posting| #posting.view_count is 0 by default. This is good.
      temp_string = posting.view_history + "," + posting.view_count.to_s
      posting.view_history = temp_string
      posting.view_count = 0
      posting.update_record_without_timestamping
    end
  end


  def self.top_level_categories
    return Posting.immediate_children_of_category("All")
  end

  def self.broadest_category_of_subcategory(subcategory)
    return 'All' if subcategory=="All"
    top_level_categories=self.top_level_categories
    top_level_categories.each { |top_level_category|
      children= self.immediate_children_of_category(top_level_category)
      if subcategory==top_level_category || children.include?(subcategory)
        return top_level_category
      else
        children.each {
            |child|
          if self.check_if_subcategory_is_in_category(subcategory, child)
            return top_level_category
          end
        }
      end
    }
    return 'All'
  end

  def self.check_if_subcategory_is_in_category(subcategory, category)
    children= self.immediate_children_of_category(category)
    if children.include?(subcategory)
      return true
    else
      children.each {
          |child|
        if self.check_if_subcategory_is_in_category(subcategory, child)
          return true
        end
      }
    end
    return false
  end


  def self.is_leaf?(category)
    return self.immediate_children_of_category(category).empty?
  end

  def self.ancestry_line(category, broader_category=self.broadest_category_of_subcategory(category))
    ancestry_line=[]

    if category != broader_category
      ancestry_line << broader_category
      immediate_children = self.immediate_children_of_category(broader_category)
      if !immediate_children.include?(category)
        immediate_children.each { |child|
          if (Posting.all_subcategories_of_category(category)-Posting.all_subcategories_of_category(child)).empty?
            ancestry_line.concat(self.ancestry_line(category, child))
          end }
      end
    end

    return ancestry_line
  end

  def self.category_parent(category)
    return self.ancestry_line(category)[-1]
  end


  def self.immediate_children_of_category(category)
    case category
      when "All" #top-level categories
        return ["Deals & Giftcards", "Office & Industrial", "Musical Instruments", "CDs, DVD & Blu-ray", "Toys & Games", "Travel", "Art & Collectibles", "Babies & Kids", "Books", "Electronics", "Fashion", "Home & Decor", "Motors", "Sporting Goods", "Tickets & Events"]

      when "Art & Collectibles"
        return ["Arts & Crafts", "Collectibles", "Antiques"]

      when "Electronics"
        return ["Cameras & Videos", "Cell Phones", "Computers", "TVs", "Accessories & Covers", "Video Games", "Media Players & Projectors", "Sound & Audio Devices"]

      when "Computers"
        return ["Tablets", "Laptops", "Desktops", "Monitors", "Software", "Components & Parts", "Computer Peripherals", "Drives & Storage", "Printers & Ink"]

      when "Fashion"
        return ["Clothing", "Sunglasses", "Handbags", "Shoes", "Fashion Accessories", "Jewelry", "Health & Beauty"]

      when "Home & Decor"
        return ["Appliances", "Furniture", "Household", "Materials", "Lawn & Garden", "Tools"]

      when "Furniture"
        return ["Dressers & Armoires", "Beds", "Desks", "Tables & Stands", "Chairs & Stools", "Furniture Sets", "Cabinets & Bookcases", "Sofas", "Rugs"]

      when "Motors"
        return ["Cars & Trucks", "Auto Parts", "Motorcycles", "Boats", "RVs"]

      when "Sporting Goods"
        return ["Bicycles", "Sports & Outdoor Gear"]

      else
        return []
    end
  end

  def self.all_subcategories_of_category(category, list_of_subcategories=[])
    if self.immediate_children_of_category(category).empty?
      return list_of_subcategories<<category
    else

      self.immediate_children_of_category(category).each {
          |child| list_of_subcategories.concat(self.all_subcategories_of_category(child))
      }
      return list_of_subcategories
    end
  end


  def self.category_fields
    furniture_colors=%w{Black Grey White Brown Beige Red Ivory Patterned Other}
    bed_sizes=["Twin", "Full", "Queen", "King", "California King"]
    gender =%w{Male Female Unisex}
    material_types=["Diamond", "Pearl", "Gemstones", "Cubic Zirconia", "Crystal", "Other"]
    metal_types=%w{Gold Sterling Platinum Titanium Stainless Brass Copper Palladium Tungsten Other}
    vehicle_colors=%w{Beige Black Blue Brown Gold Gray Green Orange Purple Red Silver White Yellow Other}
    #Sliders need field name, default value, min, max, step
    return {

        # MISCELLANEOUS ########################################
        "Deals & Giftcards" => {},
        "Office & Industrial" => {},
        "Musical Instruments" => {
            :drop_down_menus => {
                "Type" => ["Guitar", "Woodwind", "Keyboard", "Percussion", "Brass", "Amps & Speakers", "Other"]
            }
        },
        "CDs, DVD & Blu-ray" => {
            :drop_down_menus => {
                "Type" => ["Movies", "TV Shows", "Documentary", "Other"]
            }
        },
        "Toys & Games" => {
            :drop_down_menus => {
                "Type" => ["Board Games", "Dolls & Action Figures", "Models & Kits", "Card Games", "Costumes", "Other"]
            }
        },
        "Travel" => {
            :drop_down_menus => {
                "Type" => ["Luggage", "Travel Accessories", "Other"]
            }
        },

        # ART &  COLLECTIBLES ###############################
        "Arts & Crafts" => {},
        "Collectibles" => {},
        "Antiques" => {},

        # BABIES & KIDS #################################
        "Babies & Kids" => {
            :drop_down_menus => {
                "Type" => ["Baby Toys & Games", "Baby Clothing & Accessories", "Other Baby & Kids"]
            }
        },

        # BOOKS ########################################
        "Books" => {
            :text_fields => ["Author"]
            # :drop_down_menus => {
            # # "Subject" => ["Business", "Biology", "Chemistry", "Computer Science", "Engineering", "English & Creative Writing", "History", "Humanities", "Law", "Mathematics & Statistics", "Medicine", "Other", "N/A"],
            #     # "School" => ["University of Chicago", "Northwestern University", "Other School","N/A"]
            # }
        },

        # ELECTRONICS ###############################
        "Cameras & Videos" => {
            :drop_down_menus => {
                "Megapixels" => ["2MP and under", "3-5MP", "6-10MP", "11-15MP", "16MP-20MP", "20MP+"]
            }
        },
        "Cell Phones" => {
            :drop_down_menus => {
                "Carrier" => ["Unlocked/Open", "AT&T", "Verizon", "Sprint", "T-Mobile", "US Cellular", "Metro PCS", "Cricket", "Boost Mobile"],
                "Technology" => ["Android", "Apple", "Blackberry", "Windows", "Classic (non-smartphone)"]
            }
        },
        "Tablets" => {},
        "Laptops" => {
            :drop_down_menus => {
                "Operating System" => ["Windows 8", "Windows 7", "Windows Vista", "Windows XP", "Mac", "Linux"],
                "RAM(GB)" => ["1GB and under", "2GB", "3GB", "4GB", "6GB and up"],
                "Hard Drive" => ["120GB and under", "121 to 320GB", "321 to 500GB", "501 to 999GB", "1TB", "2TB", "8TB and up"],
                "Display Size" => ["9.9 Inches and under", "10 to 12.9 Inches", "13 to 15.9 Inches", "16 to 18.9 Inches", "19 Inches and up"]
            }
        },
        "Desktops" => {
            :drop_down_menus => {
                "Operating System" => ["Windows 8", "Windows 7", "Windows Vista", "Windows XP", "Mac", "Linux"],
                "RAM(GB)" => ["1GB and under", "2GB", "3GB", "4GB", "6GB and up"],
                "Hard Drive" => ["120GB and under", "121 to 320GB", "321 to 500GB", "501 to 999GB", "1TB", "2TB", "8TB and up"]
            }
        },
        "Monitors" => {
            :drop_down_menus => {
                "Display Size" => ["50 Inches and up", "49.9 to 40 Inches", "39.9 to 35 Inches", "34.9 to 30 Inches", "29.9 to 25 Inches", "24.9 to 20 Inches", "19.9 to 16 Inches", "Under 16 Inches"]
            }
        },
        "Software" => {},
        "Components & Parts" => {
            :drop_down_menus => {
                "Type" => ["Processors", "Motherboards", "Memory", "Hard Drives", "Video Cards", "Power Supplies", "Optical Storage", "Sound Card", "Computer Case", "Cooling Gear", "Other"]
            }
        },
        "Computer Peripherals" => {
            :drop_down_menus => {
                "Type" => ["Mouse", "Keyboard", "Scanners", "CD, DVD & Blu-ray Drives", "Other Peripherals"]
            }
        },
        "Drives & Storage" => {
            :drop_down_menus => {
                "Type" => ["Hard Drive", "USB & Portable Media", "RAM", "Other"],
            }
        },
        #"Dvd, Blu-ray & Projector" => {
        #  :drop_down_menus => {
        #    "Type" => ["DVD Players", "Blu-ray Players", "DVD Recorders", "Portable DVD Players", "Projectors"]
        #  }
        #},
        "Printers & Ink" => {
            :drop_down_menus => {
                "Type" => ["Printer", "All-In-One Machine", "Ink", "Other"]
            }
        },
        "TVs" => {
            :drop_down_menus => {
                "Technology" => ["3D", "LCD HDTVs", "Plasma HDTVs", "LED HDTVs", "Projection", "Classic"]
            },
            :sliders => {
                "Display (inches)" => [10, 5, 100, 1]
            }
        },
        "Accessories & Covers" => {
            :drop_down_menus => {
                "Type" => ["Phone Covers", "Tablet Covers", "Laptop Covers", "Keyboards", "Mice and Pointing Devices", "Networking", "Laptop Accessories", "Car Electronics"]
            }
        },
        "Video Games" => {
            :drop_down_menus => {
                "Type" => ["PS3", "XBox", "Wii", "Playstation Vita", "Nintendo DS", "Nintendo 3DS", "PC", "Mac", "Other"]
            }
        },
        "Media Players & Projectors" => {
            :drop_down_menus => {
                "Type" => ["DVD", "Blu-ray", "CD", "VHS", "Projector"]
            }
        },
        "Sound & Audio Devices" => {
            :drop_down_menus => {
                "Type" => ["Speakers", "Headphones", "Ipods & Portable Audio", "Audio Accessories"]
            }
        },

        # FASHION #########################################
        "Clothing" => {
            :drop_down_menus => {
                "Standard" => ["US", "UK", "Continental"],
                "Size" => ['XS', 'S', 'M', 'L', 'XL', 'XXL'],
                "Type" => ["Shirts", "Pants", "Dresses", "Suits & Formal", "Coats & Jackets"],
                "Gender" => gender
            },
            :sliders => {
                "No. Size" => [0, 0, 100, 1]
            }
        },
        "Sunglasses" => {},
        "Handbags" => {},
        "Shoes" => {
            :drop_down_menus => {
                "Department" => %w{Women Men Boys Girls},
                "Size" => ["Size 4 and under", "Size 5", "Size 6", "Size 7", "Size 8", "Size 9", "Size 10", "Size 11", "Size 12", "Size 13", "Size 14", "Size 15", "Size 16 and up"]
            }
        },
        "Fashion Accessories" => {},
        "Jewelry" => {
            :drop_down_menus => {
                "Type" => ["Rings", "Watches", "Necklaces", "Earrings", "Bracelets", "Jewelry Sets", "Other"],
                "Gender" => gender
            }
        },
        "Health & Beauty" => {},

        # HOME AND DECOR ##############################
        "Appliances" => {
            :drop_down_menus => {
                "Type" => ["Refridgeration", "Washer & Dryer", "Cooking", "Cleaning"]
            }
        },
        "Dressers & Armoires" => {
            :drop_down_menus => {
                "Type" => %w{Dresser Armoire},
                "Finish" => %w{Black Cherry Oak Walnut White Other}
            }
        },
        "Beds" => {
            :drop_down_menus => {
                "Type" => %w{Frame Headboard Mattress Futon Boxspring Set},
                "Size" => bed_sizes, "Color" => furniture_colors
            }
        },
        "Desks" => {
            :drop_down_menus => {
                "Material" => %w{Metal Glass Plastic Wood Other},
                "Type" => ["Office Desk", "Computer Desk", "Other"],
                "Color" => furniture_colors
            }
        },
        "Tables & Stands" => {
            :drop_down_menus => {
                "Material" => %w{Metal Glass Plastic Wood Other},
                "Type" => ["Dining Table", "Office Table", "Coffee Table", "End Table", "Other Table"],
                "Color" => furniture_colors
            }
        },
        "Chairs & Stools" => {
            :drop_down_menus => {
                "Type" => ["Barstool", "Dining Chair", "Office Chair", "Living Room Chair"],
                "Color" => furniture_colors
            }
        },
        "Cabinets & Bookcases" => {
            :drop_down_menus => {
                "Type" => ["Living Room Cabinet, Storage Cabinet, File Cabinet, Bookcase"],
                "Finish" => %w{Black Cherry Oak Walnut White Other}
            }
        },
        "Furniture Sets" => {
            :drop_down_menus => {
                "# of chairs" => ["0 chairs", "1 chair", "2 chairs", "3 chairs", "4 chairs", "5+ chairs"],
                "Type" => ["Dining Room Set", "Living Room Set", "Other Set"], "Color" => furniture_colors
            }
        },
        #"Other Furniture" => {
        #  :drop_down_menus => {
        #    "Type" => ["Hutches", "Buffets", "Other"],
        #    "Color"=>furniture_colors
        #  }
        #},
        "Sofas" => {
            :drop_down_menus => {
                "Type" => ["Regular", "Love Seat", "Sectional", "Sleeper", "Ottomans"]
            }
        },
        "Rugs" => {
            :drop_down_menus => {
                "Color" => furniture_colors
            }
        },
        "Household" => {},
        "Materials" => {},
        "Lawn & Garden" => {
            :drop_down_menus => {
                "Type" => ["Gardening", "Generators", "Outdoor Cooking", "Heaters & Fire", "Outdoor Decor", "Patio & Outdoor Furniture", "Pools & Spas", "Snow Removal", "Other Lawn and Garden"]
            }
        },
        "Tools" => {
            :drop_down_menus => {
                "Type" => ["Power Tools", "Electrical", "Construction"]
            }
        },


        # MOTORS #################################################
        "Cars & Trucks" => {
            :text_fields => ["Make", "Model"],
            :drop_down_menus => {
                "Body Type" => %w{Sedan Coupe Luxury Sport Convertible SUV Hatchback Crossover Mini-van Van Truck},
                "Transmission" => %w{Manual Automatic},
                "Fuel Type" => %w{Gasoline Diesel Ethanol Hybrid/Electric},
                "Color" => vehicle_colors
            },
            :sliders => {
                "Mileage (miles)" => [0, 0, 300000, 1000]
            }
        },
        "Auto Parts" => {},
        "Motorcycles" => {
            :text_fields => ["Make", "Model"],
            :drop_down_menus => {
                "Body Type" => %w{Cruiser Touring Sport Off-Road},
                "Fuel Type" => %w{Gasoline Diesel Ethanol Hybrid/Electric},
                "Color" => vehicle_colors
            },
            :sliders => {
                "Mileage (miles)" => [0, 0, 300000, 1000],
                "Engine Size (cc)" => [500, 100, 2000, 50]
            }
        },
        "Boats" => {},
        "RVs" => {},

        # SPORTING GOODS ############################################
        "Bicycles" => {},
        "Sports & Outdoor Gear" => {
            :drop_down_menus => {
                "Type" => ["Exercise & Fitness", "Outdoor Gear", "Athletic Clothing", "Team Sports", "Bikes & Scooters", "Golf", "Racket Sports", "Boating & Water Sports", "Other Sports & Outdoor"]
            }
        },

        # TICKETS & EVENTS ##########################################
        "Tickets & Events" => {
            :text_fields => ["Event Date", "Venue", "Seat Info"],
            :drop_down_menus => {
                "Type" => ["Sports Events", "Concert", "Theater"]
            }
        }
    }
  end


  belongs_to :user
  has_many :conversations, :dependent => :destroy
  has_many :reviews, :dependent => :destroy
  has_many :offers, :dependent => :destroy

  has_many :pictures, :as => :imageable, :dependent => :destroy
  accepts_nested_attributes_for :pictures, :allow_destroy => true, :reject_if => :all_blank

  has_many :custom_fields, :as => :customizable, :dependent => :destroy
  accepts_nested_attributes_for :custom_fields, :allow_destroy => true

  has_and_belongs_to_many :groups
  has_and_belongs_to_many :alerts


  after_save { |posting|
    if posting.deleted_changed?
      posting.conversations.each do |conversation|
        conversation.update_attribute(:posting_was_delisted, true)
      end
    end
  }


  ##################################

  validates_presence_of :title, :price, :category_name, :address, :message => " "
  validates_length_of :title, :maximum => 150, :minimum => 3
  validates_numericality_of :price, :greater_than_or_equal_to => 0, :less_than => 10000000, :only_integer => true


  ################################

  default_scope :order => 'updated_at DESC'
  scope :active, :conditions => {:deleted => false}


  ####### SPHINX #####################################
  # INDEX CACHE THE TITLE AND DESCRIPTION FIELDS
  # OF A POST TO MAKE IT SEARCHABLE
  define_index do
    indexes title
    #indexes description

    indexes category_name, :as => :category, :facet => true

    indexes custom_fields.value, :as => :custom_field_values

    has groups.id, :as => :group_id, :sortable => true

    has price, updated_at, address, deleted, latitude, longitude, is_updated, reppio_pick

    set_property :delta => true
  end

end