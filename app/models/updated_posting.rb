class UpdatedPosting < ActiveRecord::Base
  define_index do
    indexes title
    has category_name, :facet => true
    has subcategory_name, :facet => true
    set_property :delta => true
  end
end