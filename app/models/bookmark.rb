class Bookmark < ActiveRecord::Base
  belongs_to :user
  has_one :posting

  validates_presence_of :user_id, :posting_id
end
