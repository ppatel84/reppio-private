class Userdata < ActiveRecord::Base
  belongs_to :user
  # When called, this method updates the corresponding user's rep score,
  # based on changes to that user's Userdata
  def self.profile_complete_percent(user_id)
    userdata = Userdata.find_by_user_id(user_id)
    user=User.find(user_id)
    # add up the complete variables
    numberComplete = Utils::addBooleans(userdata.picture_complete?, userdata.address_complete?, userdata.phone_number_complete?, userdata.about_me_complete?, !user.education.blank?, !user.employer.blank?, !userdata.fb_friends.nil?, (userdata.linkedin_number_of_connections >0), (userdata.twitter_number_of_followers>0), userdata.phone_verified?).to_f
    percentComplete = numberComplete / 10.0
    percentComplete*100
  end

  def self.compute_repscore(user_id)
    #INCLUDE ALL REPSCORE COMPUTATION HERE
    finalscore = 0
    data = Userdata.find_by_user_id(user_id)

    #1. PROFILE COMPLETENESS

    if data.picture_complete; finalscore += 5; end
    if data.full_name_complete; finalscore += 3; end
    if data.address_complete; finalscore += 3; end
    if data.phone_number_complete; finalscore += 3; end
    if data.about_me_complete; finalscore += 3; end
    if (data.education_complete || data.employer_complete); finalscore +=3; end


    # BACKGROUND VERIFICATION
    #if data.background_complete; finalscore +=2; end

    # PHONE VERIFICATION
    if data.phone_verified; finalscore+=2; end

    # SOCIAL MEDIA VERIFICATION
    if data.fb_complete
      finalscore +=25
    elsif data.fb_half_complete
      finalscore +=12
    end
    if data.rp_complete; finalscore += 10; end
    if (data.ln_complete || data.tw_complete); finalscore += 5; end

    #2. REFERRING USERS
    referrals_counted_in_rep_score= data.referred_users > 5 ? 5 : data.referred_users
    finalscore += referrals_counted_in_rep_score*2

    #3. REFERENCES / REVIEWS

    if data.references_received>=2
      finalscore+=3
    elsif data.references_received==1
      finalscore+=2
    end


    # positive/negative reviews: total 10
    # first positive +5 points, subsequent +1 point; negative review -5 points each, maximum 30 points lost
    #         from bad reviews
    reviews_score = 0
    if data.positive_reviews==1
      reviews_score += 5
    elsif data.positive_reviews>1
      reviews_score += 4 + data.positive_reviews
    end
    reviews_score -= data.negative_reviews*5
    if (reviews_score < -30);  reviews_score=-30; end       # can't be less than -30
    if (reviews_score > 10);  reviews_score=10; end     # can't be larger than 10
    finalscore += reviews_score

    # REVIEWS WRITTEN
    if (data.useful_reviews == 1)
      finalscore += 1
    elsif (data.useful_reviews == 2)
      finalscore += 2
    elsif (data.useful_reviews > 2)
      finalscore += 3
    end


    # RECENT LOG IN
    if not (data.last_login).nil?
    if (7.days.ago..Time.now).cover?(data.last_login)
      finalscore += 3
    elsif (30.days.ago..Time.now).cover?(data.last_login)
      finalscore += 2
    elsif (3.months.ago..Time.now).cover?(data.last_login)
      finalscore += 1
    end
    end

      # MESSAGING AND RESPONSE TIME
    if (data.messages >=10)
      finalscore += 2
    elsif (data.messages > 0)
      finalscore += 1
    end

    if (data.number_of_responses>0) #make sure the person has responded before, prevents division by 0
     responses_average_time=data.total_response_time_in_minutes / data.number_of_responses
      if (responses_average_time < 12*60) #12 hours
        finalscore += 2
     end
    end

    # USED BOOKING FEATURE
    if (data.bookings_scheduled>4)
      finalscore += 3
    elsif (data.bookings_scheduled>1)
      finalscore += 2
    elsif data.bookings_scheduled==1
      finalscore += 1
    end

    # TRANSACTION COMPLETED
    if (data.transactions_complete == 1)
      finalscore += 3
    elsif (data.transactions_complete == 2)
      finalscore += 4
    elsif (data.transactions_complete > 4)
      finalscore += 5
    end

    if finalscore<0; finalscore=0; end

    # UPDATE REPSCORE
    Userdata.find_by_user_id(user_id).update_attribute(:repscore, finalscore)
    return finalscore
  end

  # delete data in S3 depending on the environment. called in the deploy script
  # when a db:drop occurs
  def self.delete_s3_buckets
    # open connection to s3
    s3 = AWS::S3.new(
        :access_key_id => 'AKIAI3L36MVAHQVYLN2A',
        :secret_access_key => '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'
    )

    if (Rails.env == "development")
      rep_bucket = s3.buckets["reppio-dev-repscore"]
      pic_bucket = s3.buckets["reppio-dev-bucket"]
    end
    if (Rails.env == "production")
      rep_bucket = s3.buckets["reppio-repscore"]
      pic_bucket = s3.buckets["reppio-bucket"]
    end
    rep_bucket.objects.delete_all
    pic_bucket.objects.delete_all

  end

  def self.push_repscore_history
    Userdata.find_each(:batch_size=>5000) do |userdata| #posting.view_count is 0 by default. This is good.
      temp_string = userdata.repscore_history + "," + userdata.repscore.to_s
      userdata.repscore_history = temp_string
      userdata.save!
    end
  end

  def self.avg_repscore_history
    #compute average repscore

  end

  #record each user's daily data history to S3. called via a
  # cron job from a local server
  def self.userdata_to_s3(local_data_dir)
    # open connection to s3
    s3 = AWS::S3.new(
        :access_key_id => 'AKIAI3L36MVAHQVYLN2A',
        :secret_access_key => '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'
    )
    if (Rails.env == "development"); rep_bucket = s3.buckets["reppio-dev-repscore"] end
    if (Rails.env == "production"); rep_bucket = s3.buckets["reppio-repscore"] end
    # this function is called from our local server to store user information daily into S3
    # using the production environment

    # why is a local copy stored? get requests from S3 cost money. so if we are always getting
    # a user's JSON data to update it, it will be expensive. but put requests are free! ergo
    # we are only getting the data from S3 when the user requests it from the website.
    Userdata.find_each do |userdata|

      file_name = userdata.user_id.to_s + "_data.json"
      file_dir = local_data_dir + "/" + file_name

      # update the relevant files on our local system in JSON format
      File.new(file_dir, "a+")   # create if new, otherwise open
      raw_data = File.read(file_dir)        # read the file

      if (raw_data != "")     # if read is not empty string
        raw_data = raw_data.gsub("jsonCallback(", "")    # remove excess js
        raw_data = raw_data.gsub(");", "")
        json_data = JSON.parse(raw_data)                     # parse JSON
      else
        json_data = {}
      end

      # initialize dates and scores. set to a blank array if nil
      dates = json_data["dates"]
      scores = json_data["scores"]
      if (dates == nil || scores == nil)
        dates = []
        scores = []
      end

      # push new data onto old array, and into new hash
      dates.push((Time.now).to_s[0,10])
      scores.push(userdata.repscore.to_s)
      new_data = {
          :dates => dates,
          :scores => scores
      }

      # resave as json. not an append operation, overwrite
      File.open(file_dir, "w") do |f|
        f.write("jsonCallback(")
        f.write(new_data.to_json)
        f.write(");")
      end

      # once updated, store the files on S3
      s3_file = rep_bucket.objects[file_name]
      s3_file.write(File.read(file_dir))
    end

  end

  # given a user_id:
  # (1) tokenize facebook_friends field
  # (2) for each token:
  #     - if DNE, create the node in neo graph database
  #     - create relationship with that node and user_id
  def compute_fb_nodes(user_id)
    current_user_fb_id = Authorization.find_by_user_id(user_id).uid
    current_user_fb_friends = Userdata.find_by_user_id(user_id).facebook_friends.split(", ")

    current_user_fb_friends.each { |fb_friend|

      # create array of arrays that contains

    }


  end

  after_save { |user_data|
    if user_data.repscore_changed?
      user = user_data.user
      user.update_attribute(:repscore, user_data.repscore)
    end
  }

end
