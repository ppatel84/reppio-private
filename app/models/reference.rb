class Reference < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :reference_text
  validates :relationship,
            :length => {:in => 0..20},
            :allow_nil => true
end
