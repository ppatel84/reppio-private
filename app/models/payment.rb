class Payment < ActiveRecord::Base
  validates_presence_of :seller_id, :buyer_id, :price, :seller_cut, :reppio_cut,
   :seller_email, :posting_id
end