class Picture < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  belongs_to :imageable, :polymorphic=>true

  if (Rails.env == "production"); pic_bucket = "reppio-bucket" end
  if (Rails.env == "staging"); pic_bucket = "reppio-dev-bucket" end
  if (Rails.env == "development"); pic_bucket = "reppio-dev-bucket" end

  has_attached_file :photo,
                    :styles => {
                        :thumb => {:geometry => "72x72#"},  #profile
                        :large_thumb => {:geometry => "200x150#"},
                        :medium => {:geometry => "400x400#"}, #profile
                        :original => {:geometry => "800x800^"},
                        :original_cropped => {:geometry => "500x500"}
                    },
                    :convert_options => { :thumb => '-quality 75 -strip',
                                          :large_thumb => '-quality 85 -strip',
                                          :medium => '-quality 85',
                                          :original => '-quality 75',
                                          :original_cropped => '-quality 75'},
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/s3.yml",
                    :path => "photos/:hash/:style.:extension",
                    :hash_data => ":id",
                    :hash_secret => "jr7M3yKhYQVX1CBQiFzyO2VNHd9GzJ818L+FUQgbkyEFTxt2KlHZgr+7bzAc/MNsoiqV38B7fYMe/uN+12+lE0Il/cPZZr3jlAcvzjSqrrHayRFXCFYC9ye0F4ZJW+ZzCKBzlh34RYXgI55wwwuZd
zG+xreGRivUH4cbN+pN+ks=",
                    :bucket => pic_bucket

  validates_attachment_size :photo, :less_than => 6.megabytes

  def resizeDynamic

    geo = Paperclip::Geometry.from_file(photo.queued_for_write[:original].path)
    ratio = geo.width / geo.height
    if ratio > 1       #landscape or square
        "500x400#"
    else                #portrait
        "400x500#"
    end

  end
  
  #one convenient method to pass jq_upload the necessary information
  def to_jq_upload(username)
    {
      "name" => photo_file_name,
      "size" => photo_file_size,
      "url" => photo.url,
      "id" => imageable_id,
      "picture_id"=>id,
      "thumbnail_url" => photo.url(:thumb),
      # "delete_url" => imageable_id #"upload_destroy", #picture_path(:id => id),
     "delete_url" => upload_destroy_path(:user_name => username, :id => imageable_id), #picture_path(:id => id),
     "delete_type" => "DELETE"
    }
  end
  
end
