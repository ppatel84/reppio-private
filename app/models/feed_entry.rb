class FeedEntry < ActiveRecord::Base

  # Sphinx ##########################################
  define_index do
   indexes title
   indexes summary
  end

  # Feedzirra #######################################
  def self.delete_expired_rows
    cutoff_time = Time.now - 30.days
    FeedEntry.delete_all("published_at < '#{cutoff_time}'")
  end

  private
  def self.add_entries(entries, url)

    price_regex=/\$[0-9]+/
    location_regex=/\([A-Za-z]+\)/


    entries.each do |entry|
      unless exists? :guid => entry.id
        create!(
            :title => entry.title,
            :summary => entry.summary,
            :url => entry.url,
			      :category_name => url,
            :published_at => entry.published,
            :guid => entry.id,
            :price => price_regex.match(entry.title).to_s.delete("$,"),
            :location => location_regex.match(entry.title).to_s.delete("()")
        )
      end
    end
  end

  def self.update_from_feed_continuously
    delay_interval = 30.minutes
    urls = ['ppa','ata', 'bar', 'boo', 'bka', 'bfa', 'sya', 'zip', 'fua',
            'jwa', 'maa', 'rva', 'sga', 'tia', 'tla', 'wan', 'apa', 'roo', 'sub','hsa','swp','vac','prk','off','rea',
   'ara','pta','baa','haa','cta','ema','moa','cla','cba','ela','gra','gms','hsa','mca','msa','pha','taa',
    'vga','bia']
    loop do
    urls.each do |url|
      feed = Feedzirra::Feed.fetch_and_parse("http://chicago.craigslist.org/"+url+"/index.rss")
      if (feed.class == Feedzirra::Parser::RSS) #nonzero feed
        add_entries(feed.entries, url)
      end
    end
    sleep delay_interval.to_i
    end

  end
end
