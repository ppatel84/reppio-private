class UserMobileDevice < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :user_id, :device_token
end