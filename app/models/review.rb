class Review < ActiveRecord::Base
  belongs_to :user
  validates :recommended, :inclusion => { :in => [true, false] }  
  validates_presence_of :description
  validates_length_of :item_as_described, :within => 1..5, :too_long => "must be rated between 1 and 5", :too_short => "must be rated between 1 and 5", :allow_nil => true
  validates_length_of :communication, :within => 1..5, :too_long => "must be rated between 1 and 5", :too_short => "must be rated between 1 and 5"
  validates_length_of :overall_experience, :within => 1..5, :too_long => "must be rated between 1 and 5", :too_short => "must be rated between 1 and 5"
end
