class Message < ActiveRecord::Base
  belongs_to :conversation
  validates_presence_of :message_body, :allow_blank => true
end
