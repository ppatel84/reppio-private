class Conversation < ActiveRecord::Base
  has_one :posting
  has_many :messages, :dependent => :destroy

  after_create { |conversation|
    tmp = Posting.find(conversation.posting_id)
    tmp.update_column(:conversation_counts, tmp.conversation_counts + 1)
  }

end
