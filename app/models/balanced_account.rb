class BalancedAccount < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :user_id, :account_id, :uri
end