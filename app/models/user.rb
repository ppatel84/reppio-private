require 'bcrypt'

class User < ActiveRecord::Base
  extend Geocoder::Model::ActiveRecord

  has_many :pictures, :as => :imageable, :dependent => :destroy
  has_many :user_mobile_devices, :dependent => :destroy
  has_one :balanced_account, :dependent => :destroy
  has_many :balanced_bank_accounts, :dependent => :destroy
  has_many :balanced_credit_cards, :dependent => :destroy
  accepts_nested_attributes_for :pictures, :allow_destroy => true, :reject_if => :all_blank

  attr_accessor :origin_if_facebook_sign_up
  attr_accessor :password
  before_save :encrypt_password

  has_one :userdata

  has_and_belongs_to_many :groups
  has_many :ownerships, :class_name => 'Group', :dependent => :destroy

  has_many :email_addresses, :dependent => :destroy
  accepts_nested_attributes_for :email_addresses

  geocoded_by :address
  after_validation :geocode

  def address
    [street_address, city, state, zip_code].compact.join(', ')
  end

  has_many :authorizations, :dependent => :destroy
  has_many :postings, :dependent => :destroy
  has_many :references, :dependent => :destroy
  has_many :reviews, :dependent => :destroy
  has_many :bookmarks, :dependent => :destroy
  has_many :alerts, :dependent => :destroy

  def self.update_userdata(id)

    # this is run when a user creates or updates their profile. it updates
    # the userdata table so repscore can be reprocessed
    # better way to do this? more activerecord queries, yay

    # assumes @user has been defined, i.e. we are in some well-defined controller action
    tmp_user = User.find(id)
    tmp_userdata = Userdata.find_by_user_id(id)

    tmp_userdata.address_complete = !(tmp_user.street_address.blank? &&
        tmp_user.city.blank? &&
        tmp_user.state.blank? &&
        tmp_user.zip_code.blank?)
    tmp_userdata.phone_number_complete = !tmp_user.phone_number.blank?
    tmp_userdata.about_me_complete = !tmp_user.about_me.blank?

    #removing employer and education from what was imported from facebook hurts your repscore
    # adding this info manually (not from facebook) does not improve rep score
    tmp_userdata.education_complete = false if tmp_user.education.blank?
    tmp_userdata.employer_complete = false if tmp_user.employer.blank?

    tmp_userdata.picture_complete=!tmp_user.picture_display_order.blank?
    ## INSERT BACKGROUND CHECK HERE
    tmp_userdata.save

  end

  def self.create_from_hash!(hash)
    require 'open-uri'
    facebook_profile_picture_url_before_redirect=hash["info"]["image"].gsub("type=square", "type=large")

    work_info=''
    if (!hash["extra"]["raw_info"]["work"].nil?)
      (employer= hash["extra"]["raw_info"]["work"][0]["employer"]["name"]) rescue nil
      unless employer.nil?
        work_info+=employer
      end
      (position= hash["extra"]["raw_info"]["work"][0]["position"]["name"]) rescue nil
      unless position.nil?
        work_info+=" "+position
      end
    else
      work_info=nil
    end
    if (!hash["info"]["location"].nil?)
      (city=hash["info"]["location"].split(",")[0]) rescue nil
      (state=hash["info"]["location"].split(",")[1]) rescue nil
    end

    if (!hash["extra"]["raw_info"]["education"].nil?)
      (school=hash["extra"]["raw_info"]["education"].last["school"]["name"]) rescue nil
    end
	if hash["from_app"]
		cid="app"
	else
		cid=""
	end
    @user=self.create(:first_name => hash["info"]["first_name"],
                      :last_name => hash["info"]["last_name"],
                      :password => BCrypt::Engine.generate_salt.to_s+"randompasswordofrainbowsandunicorns",
                      :email => hash["info"]["email"], :email_confirmed => true,
                      :gender => hash["extra"]["raw_info"]["gender"],
                      :education => school,
                      :employer => work_info,
                      :birthday => hash["extra"]["raw_info"]["birthday"],
                      :city => city,
                      :state => state,
                      :email_private => true,
                      :street_address_private => true,
                      :phone_number_private => true,
                      :education_private => true,
                      :employer_private => true,
                      :receive_email_notifications => true,
                      :picture_display_order => '0',
					  :cid=>cid)
    @user.email_addresses.build(:email => hash["info"]["email"], :email_confirmed => true, :is_primary => true)

    # initial repscore computation
    if not @user.save
      return User.find_by_email(hash["info"]["email"])
    end

    Userdata.create(:user_id => @user.id,
                    :fb_complete => true,
                    :full_name_complete => true,
                    :repscore_history => '0',
                    :education_complete => !school.nil?,
                    :employer_complete => !work_info.nil?)
    Userdata.compute_repscore(@user.id)


    begin

      # get facebook profile picture
      url = URI.parse(facebook_profile_picture_url_before_redirect)
      image_url_after_facebook_redirect = Net::HTTP.start(url.host, url.port) { |http|
        http.get(facebook_profile_picture_url_before_redirect)
      }

      facebook_image = Tempfile.new(["profile", ".jpg"])
      facebook_image.binmode

      open(image_url_after_facebook_redirect['location']) do |data|
        facebook_image.write data.read
      end

      facebook_image.rewind

      @user.pictures.build(:photo => facebook_image)
      save_first_user_pic
    rescue Exception
      #failed to save facebook picture, oh noes!
      @user.picture_display_order=''
    end


    :geocode
    @user.save

    logger.debug("---------------------")
    logger.debug(@user.errors)

    @user
  end

  def to_param
    user_name
  end

  def self.authenticate(email, password)
    email=email.downcase
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end

  include BCrypt

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  before_save { |user|
    user.email = email.downcase
    user.first_name=first_name.titlecase
    user.last_name=last_name.titlecase if user.last_name
    begin
      if user.picture_display_order_changed? && !user.picture_display_order.blank?
        user_pictures = Picture.where(:imageable_type => "User", :imageable_id => user.id)
        user.first_user_url = user_pictures[user.picture_display_order.split(',').first.to_i].photo.url(:thumb)
      end
    rescue Exception

    end
  }
  after_initialize {
      |user| user.interests='' if user.interests.nil?
  }

  before_create :generate_token

  VALID_NAME_REGEX = /^[A-Za-z ]*$/
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  PHONE_REGEX=/\d{10}/
  ALPHANUMERIC_REGEX=/^[a-zA-Z0-9]*$/

  validates_presence_of :first_name, :email
  validates_presence_of :password, :on => :create

  #validates :first_name,
  #          :format => {:with => VALID_NAME_REGEX, :message => "names must have no spaces and only consist of letters"}
  #
  #validates :last_name,
  #          :format => {:with => VALID_NAME_REGEX, :message => "names must have no spaces and only consist of letters"},
  #          :allow_nil => true

  validates :email,
            :format => {:with => VALID_EMAIL_REGEX, :message => "not a valid email"},
            :uniqueness => {:case_sensitive => false}

  validates :user_name,
            :format => {:with => ALPHANUMERIC_REGEX, :message => "username can only be letters and numbers"},
            :length => {:in => 5..20},
            :uniqueness => {:case_sensitive => false},
            :allow_nil => true #Different from 'allow ""'

  validates :phone_number,
            :format => {:with => PHONE_REGEX, :message => "must be 10 digits (with no dashes or parentheses)"},
            :allow_blank => true

  validates_exclusion_of :user_name, :in => ["about", "accept_or_reject_references", "add_verifications", "admin",
                                             "alerts", "all", "appointment", "appointments", "auth", "bookmark", "bookmarks",
                                             "categorize_posting", "confirm", "contact", "contacts", "conversation",
                                             "conversations", "create_user_name", "manage", "deactivate",
                                             "faq", "feedback", "flag", "get_custom_fields", "get_feedback",
                                             "group", "groups", "hitler",
                                             "interstitial", "jackterminal", "job", "jobs", "login", "logout", "manage_tour",
                                             "message_from_new_user", "mobile", "new", "postings", "press",
                                             "privacy", "reference", "references", "repscore", "reset_password", "reviews",
                                             "search", "search_json", "browse", "send_confirmation_email", "settings", "signup",
                                             "superuser", "team", "terms", "tos", "verify_phone", "verify_phone_text", "welcome"],
                         :message => "This username is invalid."


  protected

  def generate_token
    begin
      token = SecureRandom.urlsafe_base64
    end while User.where(:rep_it_token => token).exists?
    self.rep_it_token = token
  end

  def save_first_user_pic
    begin
      user_pictures = Picture.where(:imageable_type => "User", :imageable_id => self.id)
      self.first_user_url = user_pictures[user.picture_display_order.split(',').first.to_i].photo.url(:thumb)
    rescue Exception
    end
  end

end
