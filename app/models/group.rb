class Group < ActiveRecord::Base
  def to_param
    short_name
  end

  has_and_belongs_to_many :users
  has_one :owner, :class_name => 'User', :foreign_key => "owner_id"

  has_and_belongs_to_many :postings
  zip_code_regex=/\d{5}/
  validates :long_name, :length => {:in => 3..50}, :uniqueness => {:case_sensitive => false}
  validates :short_name, :length => {:in => 1..20}, :uniqueness => {:case_sensitive => false}
  validates :zip_code, :format => {:with => zip_code_regex, :message => "must be 5 digits"}

  geocoded_by :zip_code
  after_validation :geocode, :if=>:zip_code_changed?
end
