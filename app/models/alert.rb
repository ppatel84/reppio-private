class Alert < ActiveRecord::Base
  has_and_belongs_to_many :postings
  belongs_to :user

  validates_presence_of :search, :max_price, :radius, :latitude, :longitude, :address

  def self.trigger_alert_for(posting, sellerName)
    degToR = Math::PI / 180

    Alert.find_each do |alert| #posting.view_count is 0 by default. This is good.
      titleContainedMatch = (posting.title.downcase.split & (alert_terms = alert.search.downcase.split.sort)).sort == alert_terms #Should pick up permutations. Desirable behavior: every term in the alert search must be in the posting title, but not necessarily in the order given in the alert search. Consider the test case: Posting title is "new iPad - red" and alert search is "red iPad". This should match (speaking both normatively and positively, in the case of the above code).
      triggerAlert = titleContainedMatch && (posting.price < alert.max_price) && (::DistanceCalculations.distance_between(posting.latitude, posting.longitude, alert.latitude * degToR, alert.longitude * degToR) < alert.radius)
      if triggerAlert

        # alert has been triggered for the posting. save the association
        alert.postings <<= posting
        alert.num_new_listings += 1
        alert.save

        alert_owner=User.find(alert.user_id)
        alert_owner.update_attribute(:alerts_badge_count, alert_owner.alerts_badge_count+1)

        # check for vaild max price for email
        hasValidMax = (alert.max_price != 10_000_000.0)
        priceMax = (hasValidMax ? ", < $#{alert.max_price}" : "").html_safe

        AlertMailer.send_alert_email(User.find(alert.user_id).email, [alert, priceMax], posting.id, sellerName).deliver

        #push_notification_alert(alert, alert.user_id)

      end
    end

  end

  private
  def push_notification_alert(alert, user_id)
      user_mobile_device = UserMobileDevice.find_by_user_id(user_id)

      if user_mobile_device
        #Send push notification to recipient
        #app_key = "v88QG8SnTgy4OAE10Cpwsg"             #development
        #app_secret = "dZc-R7QvSIa9dL2vd18rSQ"          #development
        #app_secret_master = "6ZZ-TEq9TGOpTB2LCHy6iA"   #development
        app_key = "Gp6xyenNQQOhzWEnMBNfmw" #production
        app_secret = "fsa7FfiQT5e59uiBvKUMfA" #production
        app_secret_master = "h8zfX6sjRvyAzkiBnk8i-g" #production

        push_host = "https://go.urbanairship.com"
        push_url = "/api/push/"
        device_tokens = [user_mobile_device.device_token]
        full_message = "A new listing matches your alert for #{alert.search}."
        push_message = (full_message.strip.length <= 157) ? full_message.strip : full_message.strip[0, 153] + "..."
        conversation_data = {:msgid => message.id}

        push_params = {:app_key => app_key, :app_secret => app_secret_master, :push_host => push_host,
                       :push_url => push_url, :device_tokens => device_tokens,
                       :message => push_message, :recipient => user_id, :add => conversation_data}

        push_send_message(push_params)

    end
  end
end
