class EmailAddress < ActiveRecord::Base
  belongs_to :user

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email,
            :format => {:with => VALID_EMAIL_REGEX, :message => "not a valid email"},
            :uniqueness => {:case_sensitive => false}


  before_save { |email_address| email_address.email = email.downcase
  email_address.email_confirmation_code=BCrypt::Engine.generate_salt
  }

  after_save { |email_address|
    if email_address.email_confirmed_changed?
      email_extension=email_address.email.split('@')[1].downcase
      Group.all.each do |group|
        if email_extension==group.email_extension
          user=User.find(email_address.user_id)
          group.users<<=user

          user.postings.reject { |i| i.deleted || !i.is_updated }.each do |user_posting|
            group.postings<<= user_posting
          end

          group.members_count= group.members_count+1

          group.save
          return
        end
      end
    end
  }
end