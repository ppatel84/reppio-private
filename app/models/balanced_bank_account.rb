class BalancedBankAccount < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :user_id, :account_number, :bank_name, :credits_uri, :fingerprint, :name, :account_type, :uri
end