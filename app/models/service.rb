class Service < ActiveRecord::Base
  belongs_to :user
  CATEGORIES=["Personal Trainer", "Dogwalker", "Pole dance instructor"]
  has_attached_file :picture,
                    :styles => {
                        :thumb => "100x100",
                        :medium => "200x200",
                        :original => "600x400"
                    },
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/s3.yml",
                    :path => ":attachment/:id/:style.:extension",
                    :bucket => "reppio-dev-bucket"
  validates_attachment_size :picture, :less_than => 6.megabytes

  validates :category, :presence => true
  validates_length_of :posting_subject, :maximum => 100
  validates :location, :presence => true
  validates :availability, :presence => true

end
