class BalancedCreditCard < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :user_id, :credit_card_id, :last_four, :expiration_month, :expiration_year, :card_type, :uri
end