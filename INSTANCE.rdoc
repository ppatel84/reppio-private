IN CASE THE MACHINE IMAGE FAILS AND WE NEED TO MANUALLY GET A NEW INSTANCE
RUNNING (THIS TAKES ~2 HOURS)

1. Install git
	sudo apt-get -y install git-core
2. Install mysql
	sudo apt-get -y install mysql-server-5.5
	user: root, password: password
	there will be errors but it doesn't matter
3. Install other stuff
	sudo apt-get -y install openssl
	sudo apt-get -y install libxml2 libxml2-dev libxslt1-dev
	sudo apt-get -y install libexpat1-dev
	sudo apt-get -y install zlib1g-dev
4. Check the mysql connector install
	mysql -h production_host -u jack -p
5. Setup Capistrano to deploy to that instance
	- Create corresponding script file in config/deploy with same name
	as the instance
	- Still don't know why Capistrano fails for all computers except my Linux box
6. Change permissions on the instance "repp" folder so we can deploy to it
	- sudo chmod 777 apps/reppio
	- sudo chmod 777 apps/reppio/releases
	- sudo chmod 777 apps/reppio/shared
	- sudo chmod 777 apps/reppio/shared/*
7. Use wrapper for OpenSSL
	- wrapper script is misc/ssh
	- sudo mv old /usr/bin/ssh to ssh.bin, and copy wrapper into /usr/bin
	- do chmod 755 /usr/bin/ssh
8. Push latest release to the instance
	e.g. cap web_medium deploy:setup
	        cap web_medium deploy:update
9. Install all the gems in production mode
	- bundle install --without development
10. Install Sphinx
	- download latest source (2.0.4) from website
	- ./configure --with-mysql-includes=/opt/bitnami/mysql/include --with-mysql-libs=/opt/bitnami/mysql/lib
	- make
	- sudo make install
11. Index the phrase_frequency database
	- rake ts:index RAILS_ENV=production
	- rake ts:start RAILS_ENV=production