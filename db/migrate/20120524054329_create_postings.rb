class CreatePostings < ActiveRecord::Migration
  def self.up
    create_table :postings do |t|
      t.integer :user_id #id of seller
      t.integer :view_count
      t.string :category_name
      t.boolean :fulfilled
      t.string :title
      t.string :condition
      t.string :availability
      t.text :description
      t.decimal :price,
                :precision => 10, #mysql needs precision or it will save as int
                :scale => 2 #fixed decimal type - rounding errors with
                         #floats might be disastrous!
      t.string :address
      t.boolean :deleted
      t.string :picture_display_order

      t.timestamps
    end

  end

  def self.down
    drop_table :postings
  end
end


