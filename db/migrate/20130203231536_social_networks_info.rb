class SocialNetworksInfo < ActiveRecord::Migration
  def change
    add_column :userdata, :linked_linkedin, :boolean, :default => false
    add_column :userdata, :linked_twitter, :boolean, :default => false
    add_column :users, :facebook_id, :string, :default=>''
  end
end
