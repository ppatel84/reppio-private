class UserdataAddPhoneVerifiedAndFbHalfCompleteColumns < ActiveRecord::Migration
  def change
    add_column :userdata, :phone_verified, :boolean, :default => false
    add_column :userdata, :fb_half_complete, :boolean, :default => false
  end
end
