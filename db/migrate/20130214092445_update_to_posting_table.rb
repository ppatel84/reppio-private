class UpdateToPostingTable < ActiveRecord::Migration
  def change
    add_column :postings, :fund_bank_account_id, :integer
  end
end
