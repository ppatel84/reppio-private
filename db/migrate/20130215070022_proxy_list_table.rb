class ProxyListTable < ActiveRecord::Migration
  def change
    create_table :proxies do |t|
      t.string :ip_address
      t.integer :port

      t.timestamps
    end
  end
end
