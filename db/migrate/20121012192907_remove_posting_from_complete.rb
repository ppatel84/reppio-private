class RemovePostingFromComplete < ActiveRecord::Migration
  def change
    add_column :postings, :hide_from_seller_completed, :boolean, :default => false
    add_column :postings, :hide_from_buyer_completed, :boolean, :default => false
  end
end
