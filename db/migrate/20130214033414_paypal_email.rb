class PaypalEmail < ActiveRecord::Migration
  def change
    add_column :users, :paypal_email, :string, :default => ''
  end
end
