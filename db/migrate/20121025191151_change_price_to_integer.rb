class ChangePriceToInteger < ActiveRecord::Migration
  def change
    change_column(:postings, :price, :integer)
  end
end
