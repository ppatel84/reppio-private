class ViewedManageTour < ActiveRecord::Migration
  def change
    add_column :users, :viewed_manage_tour, :boolean, :default=>false
  end
end
