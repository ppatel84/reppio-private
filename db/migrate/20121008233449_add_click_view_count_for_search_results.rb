class AddClickViewCountForSearchResults < ActiveRecord::Migration
  def change
    add_column :postings, :clicks_on_search_thumbnail, :integer, :default => 0
  end
end
