class CreateUpdatedPostings < ActiveRecord::Migration
  def change
    create_table :updated_postings do |t|
      t.string :title
      t.string :category_name
      t.string :subcategory_name
    end

    # id field is primary key. this should automatically set that
    # field to autoincrement starting at 1
    # craigslist data is injected into this table, starting at 2000000
    # (if we have more than 2000000 organic postings, consider this venture
    #  a success and we can deal with this then!)
    execute("alter table updated_postings auto_increment=1")
  end
end
