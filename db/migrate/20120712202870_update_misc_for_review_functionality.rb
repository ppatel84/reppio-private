class UpdateMiscForReviewFunctionality < ActiveRecord::Migration
  def change
    drop_table :reviews
    create_table :reviews do |t|
      t.integer :posting_id
      t.integer :user_id #The user who is the subject of the review
      t.integer :reviewer_user_id #The user who is the author of the review
      t.boolean :recommended
      t.integer :item_as_described #"item as described" == -1 implies that User(user_id) is a buyer, and User(reviewer_id) is a seller
      t.integer :communication
      t.integer :overall_experience
      t.text :description
      t.text :response #If desired, authored by review subject and displayed immediately afterwards
      t.boolean :anonymous
      t.integer :count_useful
      t.integer :count_not_useful
    end
    if column_exists?(:postings, :reviewed_by_buyer); remove_column :postings, :reviewed_by_buyer; end
    if column_exists?(:postings, :reviewed_by_seller); remove_column :postings, :reviewed_by_seller; end
    if column_exists?(:postings, :actual_buyer_user_id); remove_column :postings, :actual_buyer_user_id; end
    add_column :postings, :reviewed_by_buyer, :boolean, :default => false, :null => true
    add_column :postings, :reviewed_by_seller, :boolean, :default => false, :null => true
    add_column :postings, :actual_buyer_user_id, :integer, :null => true #should only be set if fulfilled is true. Note semi-parallelism with "potential_buyer_id" used in conversations table.    
  end
end