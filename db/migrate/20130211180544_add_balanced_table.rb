class AddBalancedTable < ActiveRecord::Migration
  def change
    create_table :balanced_accounts do |t|
      t.integer :user_id
      t.string :account_id
      t.string :uri
      t.timestamps
    end

    add_index :balanced_accounts, :user_id
  end
end
