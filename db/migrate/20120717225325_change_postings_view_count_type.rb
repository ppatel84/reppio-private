class ChangePostingsViewCountType < ActiveRecord::Migration
  def change
    change_column :postings, :view_count, :integer, :default => 0, :null => false
  end
end
