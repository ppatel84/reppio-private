class IsScammer < ActiveRecord::Migration
  def change
    add_column :users, :is_scammer, :boolean, :default => false
  end
end
