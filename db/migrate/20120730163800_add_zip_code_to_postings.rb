class AddZipCodeToPostings < ActiveRecord::Migration
    def change
      add_column :postings, :zip_code, :string, :default => ''
    end
end
