class AddColumnAppointmentConfirmed < ActiveRecord::Migration
  def change
    add_column :appointments, :appointment_confirmed, :boolean, :default => false, :null => false
  end
end
