class AddRepHistory < ActiveRecord::Migration
  def change
    add_column :userdata, :repscore_history, :text
    change_column :postings, :view_history, :text
  end
end
