class ChangeDatabaseForTwitterNumberOfTweets < ActiveRecord::Migration
  def change
    remove_column :users, :twitter_number_of_followers if column_exists?(:users, :twitter_number_of_followers)
    remove_column :users, :twitter_number_of_tweets if column_exists?(:users, :twitter_number_of_tweets)
    remove_column :users, :linkedin_number_of_connections if column_exists?(:users, :linkedin_number_of_connections)
    add_column :userdata, :twitter_number_of_tweets, :integer, :default=>0
  end
end
