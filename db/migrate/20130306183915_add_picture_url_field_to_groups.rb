class AddPictureUrlFieldToGroups < ActiveRecord::Migration
  def change
  	add_column :groups, :picture_url, :string, :default=>""
  end
end
