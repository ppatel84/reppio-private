class AddShowConversationWhenVerified < ActiveRecord::Migration
  def change
  	add_column :conversations, :is_verified, :boolean, :default => true
  end
end
