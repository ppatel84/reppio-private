class PreventDuplicateJoinTableEntries < ActiveRecord::Migration
  def change
    add_index :groups_postings, [ :group_id, :posting_id ], unique: true
    add_index :groups_users, [ :group_id, :user_id ], unique: true
  end
end
