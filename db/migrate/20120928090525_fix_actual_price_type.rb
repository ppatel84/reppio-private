class FixActualPriceType < ActiveRecord::Migration
  def change
    change_table :postings do |t|
      t.change :actual_price, :string, :default=>""
    end
  end
end
