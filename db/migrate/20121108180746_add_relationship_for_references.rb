class AddRelationshipForReferences < ActiveRecord::Migration
  def change
    add_column :references, :relationship, :string, :limit => 20
  end
end
