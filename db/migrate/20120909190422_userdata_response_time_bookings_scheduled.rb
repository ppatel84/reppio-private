class UserdataResponseTimeBookingsScheduled < ActiveRecord::Migration
  def change
    remove_column :userdata, :responses_average_time
    add_column :userdata, :total_response_time_in_minutes, :float, :default => 0.0 # average delay to responding to a 1st msg
    add_column :userdata, :number_of_responses, :float, :default=> 0.0
    add_column :userdata, :bookings_scheduled, :integer, :default => 0
  end
end
