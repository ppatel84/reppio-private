class AddIsUpdatedFieldToPosting < ActiveRecord::Migration
  def change
    add_column :postings, :is_updated, :boolean, :default => 1
  end
end
