class GroupEmailExtension < ActiveRecord::Migration
  def change
    add_column :groups, :email_extension, :string, :default => ''
  end
end
