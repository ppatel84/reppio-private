class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.integer :user_id
      t.string :category
      t.string :specialty
      t.string :location
      t.string :availability
      t.string :posting_subject
      t.string :about_me
      t.string :qualifications
      t.string :links
      t.string :faq

      t.timestamps
    end
  end
end
