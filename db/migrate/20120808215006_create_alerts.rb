class CreateAlerts < ActiveRecord::Migration
  def change
    create_table :alerts do |t|
      t.integer :user_id
      t.string :search
      t.decimal :max_price, :scale => 2 #fixed decimal type - rounding errors with floats might be disastrous!
      t.float :radius
      t.float :latitude
      t.float :longitude
      t.string :address
      t.timestamps
    end
    
  end
end
