class CounterForGroupMembers< ActiveRecord::Migration
  def change
    add_column :groups, :members_count, :integer, :default => 1
  end
end
