class UserAlertBadgeCountGroupBadgeCount < ActiveRecord::Migration
  def change
    add_column :users, :alerts_badge_count, :integer, :default=>0
    add_column :users, :groups_badge_count, :integer, :default=>0
  end
end
