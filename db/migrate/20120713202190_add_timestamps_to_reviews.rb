class AddTimestampsToReviews < ActiveRecord::Migration
  def change
      if !(column_exists?(:reviews, :created_at)); add_column :reviews, :created_at, :timestamp, :null => false; end
      if !(column_exists?(:reviews, :updated_at)); add_column :reviews, :updated_at, :timestamp, :null => false; end    
  end
end