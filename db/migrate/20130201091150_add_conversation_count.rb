class AddConversationCount < ActiveRecord::Migration
  def change
    add_column :postings, :conversation_counts, :integer, :default => 0
    add_column :users, :conversations_with_unread_message_selling_count, :integer, :default => 0
  end
end
