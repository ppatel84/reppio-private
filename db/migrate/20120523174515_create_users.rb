class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :password_hash
      t.string :password_salt
      t.string :email
      t.string :email_confirmation_code
      t.boolean :email_confirmed
      t.string :gender
      t.string :user_name
      t.string :first_name
      t.string :last_name
      t.string :street_address
      t.string :city
      t.string :state
      t.string :birthday
      t.string :zip_code
      t.string :phone_number
      t.string :about_me
      t.string :education
      t.string :employer
      t.boolean :email_private
      t.boolean :street_address_private
      t.boolean :phone_number_private
      t.boolean :education_private
      t.boolean :employer_private
      t.boolean :receive_email_notifications
      t.integer :twitter_number_of_followers
      t.integer :twitter_number_of_tweets
      t.integer :linkedin_number_of_connections
      t.string :picture_display_order

      t.timestamps
    end
  end
end
