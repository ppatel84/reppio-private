class AddDeltaIndexingToPostings < ActiveRecord::Migration
  def change
    add_column :postings, :delta, :boolean, :default => true, :null => false
  end
end
