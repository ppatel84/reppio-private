class AddTimeStampToReferences < ActiveRecord::Migration
  def change
    change_table :references do |t|
      t.timestamps
    end
  end
end
