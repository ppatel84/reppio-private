class FlagPostingForDeletion < ActiveRecord::Migration
  def change
    add_column :postings, :flag_for_deletion, :boolean, :default => false, :null => false
  end
end
