class CreateReferences < ActiveRecord::Migration
  def change
    create_table :references do |t|
      t.integer :reference_for_user_id
      t.integer :reference_written_by_user_id
      t.text :reference_text
      t.boolean :reference_written, :default => false

    end
  end
end
