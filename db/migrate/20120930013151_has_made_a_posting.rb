class HasMadeAPosting < ActiveRecord::Migration
  def change
    rename_column :users, :viewed_manage_tour, :should_show_manage_tour
    add_column :users, :has_made_a_posting, :boolean, :default=>false
    add_column :users, :has_had_a_conversation, :boolean, :default=>false
    add_column :users, :should_show_manage_posting_tour, :boolean, :default=> false
  end
end
