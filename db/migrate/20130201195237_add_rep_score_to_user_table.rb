class AddRepScoreToUserTable < ActiveRecord::Migration
  def change
    add_column :users, :repscore, :integer, :default => 1
  end
end
