class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.integer :posting_id
      t.integer :user_id #Of the buyer who made the initial offer
      t.integer :tenderer_user_id #Of the user who's made the *latest* offer on the posting ... might be same as user_id, or might be the seller instead
      t.decimal :offer,
                :scale => 2 #fixed decimal type - rounding errors with floats might be disastrous!
      t.timestamps
    end
  end
end