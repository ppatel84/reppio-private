class CreateUserdata < ActiveRecord::Migration
  def change
    create_table :userdata do |t|
      t.integer :user_id
      t.integer :repscore

      # 1: picture, 2: full name, 2: home address, 2: phone #,
      # 2: about me, 2: education/employer history
      t.boolean :picture_complete, :default => false
      t.boolean :full_name_complete, :default => true
      t.boolean :address_complete, :default => false
      t.boolean :phone_number_complete, :default => false
      t.boolean :about_me_complete, :default => false
      t.boolean :education_complete, :default => false
      t.boolean :employer_complete, :default => false
      t.boolean :background_complete, :default => false

      # 10: fb complete, 5: rp complete, 5: ln or tw complete
      t.boolean :fb_complete, :default => false # >100 friends + permission
      t.boolean :rp_complete, :default => false # of these, >25 reppio connections
      t.boolean :ln_complete, :default => false # >50 connections
      t.boolean :tw_complete, :default => false # >25 followers

      # 10: number of referred users (up to 5)
      t.integer :referred_users, :default => 0        # number of referred users

      # x+y: x useful reviews, y references
      # up to 10 points for references, 20 points for reviews, no more than 20
      # total
      t.integer :references_received, :default => 0   # number of references received
      t.integer :positive_reviews, :default => 0      # number of positive reviews received
      t.integer :negative_reviews, :default => 0
      t.integer :useful_reviews, :default => 0        # number of useful reviews written

      # 1: login last 3 mo, 2: login last 30 days, 2: login last 7 days
      t.datetime :last_login           # time of last login

      # 1: msg 1, 1: msg 10, 2: delay < 12 hrs
      t.integer :messages, :default => 0               # number of msgs
      t.integer :responses, :default => 0              # number of responses to a 1st msg
      t.float :responses_average_time, :default => 0.0 # average delay to responding to a 1st msg

      # 3: tx 1, 1: tx 2, 1: tx 5
      t.integer :transactions_complete, :default => 0 # number of transactions completed

      # some data
      t.integer :twitter_number_of_followers, :default => 0
      t.integer :fb_friends, :default => 0  #number of friends
      t.integer :linkedin_number_of_connections, :default => 0
	  t.text    :facebook_friends           #ids of friends

      t.timestamps
    end
  end
end
