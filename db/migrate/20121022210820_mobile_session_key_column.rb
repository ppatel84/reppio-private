class MobileSessionKeyColumn < ActiveRecord::Migration
  def change
    add_column :users, :mobile_session_key, :string, :default => ""
  end
end
