class MobileDevices < ActiveRecord::Migration
  def change
    create_table :user_mobile_devices do |t|
      t.integer :user_id
      t.string :device_token
      t.string :device_hwid
      t.string :device_type

      t.timestamps
    end
  end
end
