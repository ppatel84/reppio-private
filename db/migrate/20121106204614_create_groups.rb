class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :long_name
      t.string :short_name
      t.string :description
      t.string :external_url
      t.integer :owner_id
      t.boolean :may_join_without_admin_approval
      t.timestamps
    end
  end
end
