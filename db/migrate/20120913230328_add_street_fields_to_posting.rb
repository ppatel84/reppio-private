class AddStreetFieldsToPosting < ActiveRecord::Migration
  def change
    add_column :postings, :street1, :string, :default => ""
    add_column :postings, :street2, :string, :default => ""
  end
end
