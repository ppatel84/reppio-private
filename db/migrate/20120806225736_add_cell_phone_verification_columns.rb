class AddCellPhoneVerificationColumns < ActiveRecord::Migration
  def change
    add_column :users, :cell_phone_verification_code, :string, :default=>""
    add_column :users, :cell_phone_verified, :boolean, :default=>false
  end
end
