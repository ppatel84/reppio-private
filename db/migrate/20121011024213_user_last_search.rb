class UserLastSearch < ActiveRecord::Migration
  def change
    add_column :users, :last_search_street_address, :string, :default => ''
    add_column :users, :last_search_latitude, :float, :default => 0
    add_column :users, :last_search_longitude, :float, :default => 0
  end
end
