class ConversationCompletedPostingTransaction < ActiveRecord::Migration
  def change
    add_column :conversations, :posting_was_delisted, :boolean, :default => false
  end
end
