class ReasonForFlagging < ActiveRecord::Migration
  def change
    add_column :flags, :reason_for_flagging, :string, :default => ''
  end
end
