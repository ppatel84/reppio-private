class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :conversation_id
      t.boolean :message_from_seller
      t.text :message_body

      t.timestamps
    end
  end
end
