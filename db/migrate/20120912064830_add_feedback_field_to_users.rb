class AddFeedbackFieldToUsers < ActiveRecord::Migration
  def change
    add_column :users, :feedback_search, :text, :default => ""
    add_column :users, :feedback_manage, :text, :default => ""
    add_column :users, :feedback_create, :text, :default => ""
    add_column :users, :feedback_general, :text, :default => ""
  end
end
