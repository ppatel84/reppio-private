class AddZipCodeToGroup < ActiveRecord::Migration
  def change
    add_column :groups, :zip_code, :string, :default => ''
    add_column :groups, :latitude, :float, :default=> 41.8781136
    add_column :groups, :longitude, :float, :default=> -87.6297982
  end
end
