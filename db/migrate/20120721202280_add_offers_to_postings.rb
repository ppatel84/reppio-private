class AddOffersToPostings < ActiveRecord::Migration
  def change
    if !(column_exists?(:postings, :will_accept_other_offers)); add_column :postings, :will_accept_other_offers, :boolean, :default => false; end
    remove_column :postings, :actual_price if column_exists?(:postings, :actual_price)
    add_column :postings, :actual_price, :decimal, :precision => 2, :scale => 2, :default => 0
    end
end