class AddFirstPictureToPostingAndUser < ActiveRecord::Migration
  def change
    add_column :postings, :first_picture_url, :string, :default=>""
    add_column :postings, :first_user_url, :string, :default=>""
  end
end
