class AddConversationLocation < ActiveRecord::Migration
  def change
    add_column :conversations, :availability_location, :string, :default => ''
  end
end
