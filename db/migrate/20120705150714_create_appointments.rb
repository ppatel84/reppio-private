class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.string :title
      t.datetime :start_time
      t.datetime :end_time
      t.string :location
      t.integer :posting_id
      t.timestamps
      t.integer :user_id #We can find seller ID by looking up posting_id. Use this to store buyer_id.
    end
  end
end
