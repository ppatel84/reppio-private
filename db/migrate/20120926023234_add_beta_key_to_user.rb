class AddBetaKeyToUser < ActiveRecord::Migration
  def change
    add_column :users, :beta_key, :string, :default=>""
  end
end
