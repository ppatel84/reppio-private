class Createalertspostings < ActiveRecord::Migration
  def change
    create_table :alerts_postings do |t|
      t.integer :alert_id
      t.integer :posting_id
    end
  end
end
