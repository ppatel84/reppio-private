class AddDelistingColumns < ActiveRecord::Migration
  def change
    add_column :postings, :delisted_reason, "ENUM('soldHere', 'soldElsewhere', 'couldntSell', 'dontWantToSell')"
  end
end
