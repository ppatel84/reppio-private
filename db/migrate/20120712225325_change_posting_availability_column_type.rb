class ChangePostingAvailabilityColumnType < ActiveRecord::Migration
  def change
    change_column :postings, :availability, :text
  end
end
