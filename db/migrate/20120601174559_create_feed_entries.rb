class CreateFeedEntries < ActiveRecord::Migration
  #this table should be as close a proxy to the real postings table
  def change
    create_table :feed_entries do |t|
      t.string :title
      t.text :summary
      t.string :url
      t.string :category_name
      t.string :location
      t.decimal :price,
                :scale => 2
      t.datetime :published_at
      t.string :guid
      t.timestamps
    end
  end
end
