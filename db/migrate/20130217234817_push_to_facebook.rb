class PushToFacebook < ActiveRecord::Migration
  def change
    add_column :users, :promote_postings_to_facebook, :boolean, :default=>false
  end
end
