class CreateFlags < ActiveRecord::Migration
  def change
    create_table :flags do |t|
      t.string :url
      t.boolean :is_resolved

      t.timestamps
    end
  end
end
