class PostingsCityStateLatLong < ActiveRecord::Migration
  def change
    add_column :postings, :city, :string, :default => ''
    add_column :postings, :state, :string, :default => ''
    add_column :postings, :latitude, :float, :null => false
    add_column :postings, :longitude, :float, :null => false
  end
end
