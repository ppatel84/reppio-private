class BunchOfColumnsThatWereNotInBranch < ActiveRecord::Migration
  def change
    add_column :postings, :user_id_with_permission_to_buy, :integer, :default=>0 unless column_exists?(:postings, :user_id_with_permission_to_buy, :integer)
    add_column :postings, :sold_on_paypal, :boolean, :default => false unless column_exists?(:postings, :sold_on_paypal, :boolean)
    add_column :postings, :for_sale, :boolean, :default => true unless column_exists?(:postings, :for_sale, :boolean)
    add_column :postings, :deposit, :integer, :default => 0  unless column_exists?(:postings, :deposit, :integer)
    add_column :postings, :requested_price, :integer,  :default=>0  unless column_exists?(:postings, :requested_price, :integer)

    unless table_exists?(:payments)
      create_table :payments do |t|
        t.integer :seller_id
        t.integer :buyer_id
        t.integer :posting_id
        t.boolean :paid, :default=>false
        t.boolean :ipn_received, :default=>false
        t.integer :price
        t.float :seller_cut
        t.float :reppio_cut
        t.string :seller_email
        t.text :ipn_dump, :default=>''


        t.timestamps

      end
    end
  end
end
