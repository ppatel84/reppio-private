class CreateReferrals < ActiveRecord::Migration
  def change
    create_table :referrals do |t|
      t.string :referrer_id
      t.boolean :referee_signed_up, :default => false
      t.string :referee_temporary_id
    end
  end
end
