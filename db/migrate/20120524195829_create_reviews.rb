class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.integer :rating
      t.string :subject
      t.string :from_user
      t.string :to_user
      t.text :descr
      t.string :category
      t.integer :count_yes
      t.integer :count_no

      t.timestamps
    end
  end
end
