class AddBalancedCreditCardsTable < ActiveRecord::Migration
  def change
    create_table :balanced_credit_cards do |t|
      t.integer :user_id
      t.string :account_id
      t.string :brand
      t.string :card_type
      t.integer :expiration_month
      t.integer :expiration_year
      t.string :balanced_hash
      t.string :credit_card_id
      t.boolean :is_valid
      t.string :last_four
      t.string :meta
      t.string :name
      t.string :uri
      t.timestamps
    end

    add_index :balanced_credit_cards, :user_id
    add_index :balanced_credit_cards, :uri
  end
end
