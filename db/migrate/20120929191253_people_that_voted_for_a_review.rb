class PeopleThatVotedForAReview < ActiveRecord::Migration
  def change
    add_column :reviews, :ids_of_users_that_voted, :text, :default=>''
  end
end
