class LastPostingAddress < ActiveRecord::Migration
  def change
    add_column :users, :last_posting_street_address, :string, :default => ""
    add_column :users, :last_posting_latitude, :float, :default => 0.0
    add_column :users, :last_posting_longitude, :float, :default => 0.0
  end
end
