class DelistNotifyActionTaken < ActiveRecord::Migration
  def change
    add_column :postings, :delist_notify_action, :integer, :default=>0
    add_column :appointments, :appointment_checked, :integer, :default=>0
  end
end
