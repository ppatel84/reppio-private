class ChangeReviewCountType < ActiveRecord::Migration
  def change
    change_column :reviews, :count_useful, :integer, :default => 0, :null => false
    change_column :reviews, :count_not_useful, :integer, :default => 0, :null => false
  end
end
