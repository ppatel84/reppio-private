class ReferenceApproved < ActiveRecord::Migration
  def change
    add_column :references, :is_approved, :boolean
  end
end
