class AddBalancedBankAccountsTable < ActiveRecord::Migration
  def change
    create_table :balanced_bank_accounts do |t|
      t.integer :user_id
      t.string :account_id
      t.string :account_number
      t.string :bank_name
      t.string :credits_uri
      t.string :fingerprint
      t.string :bank_account_id
      t.string :meta
      t.string :name
      t.string :routing_number
      t.string :account_type
      t.string :uri
      t.timestamps
    end

    add_index :balanced_bank_accounts, :user_id
    add_index :balanced_bank_accounts, :uri
  end
end
