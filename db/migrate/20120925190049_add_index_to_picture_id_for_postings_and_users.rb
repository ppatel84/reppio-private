class AddIndexToPictureIdForPostingsAndUsers < ActiveRecord::Migration
  def change
    add_index :pictures, [:imageable_id, :imageable_type]
  end
end
