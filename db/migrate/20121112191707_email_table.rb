class EmailTable < ActiveRecord::Migration
  def change
    create_table :email_addresses do |t|
      t.integer :user_id
      t.string :email
      t.string :email_confirmation_code
      t.boolean :email_confirmed, :default=> false
      t.boolean :is_primary, :default=> false
      t.timestamps
    end

    add_index :email_addresses, :user_id
  end
end
