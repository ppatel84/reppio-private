class CreateConversations < ActiveRecord::Migration
  def change
    create_table :conversations do |t|
      t.integer :potential_buyer_id
      t.integer :seller_id
      t.string :product_category
      t.integer :posting_id
      t.boolean :unread_by_buyer
      t.boolean :unread_by_seller #Yes, this could be logically inferred using unread_by_buyer. BUT: what if both the buyer and the seller want to mark a conversation as unread?

      t.timestamps
    end
  end
end
