class AddHideBuyerSellerToConversation < ActiveRecord::Migration
  def change
    add_column :conversations, :hide_from_buyer, :integer, :default=>"0"
    add_column :conversations, :hide_from_seller, :integer, :default=>"0"
  end
end
