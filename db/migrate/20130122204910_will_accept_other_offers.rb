class WillAcceptOtherOffers < ActiveRecord::Migration
  def change
    change_column_default(:postings, :will_accept_other_offers, true)
    change_column_default(:postings, :deleted, false)
    #change_column_default(:postings, :view_history, "0")
    change_column_default(:conversations, :unread_by_buyer, false)
    change_column_default(:conversations, :unread_by_seller, true)
    change_column_default(:conversations, :unread_by_seller, true)
    change_column_default(:messages, :message_from_seller, false)
  end
end
