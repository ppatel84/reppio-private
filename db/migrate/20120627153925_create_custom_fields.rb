class CreateCustomFields < ActiveRecord::Migration
  def self.up
    create_table :custom_fields do |t|
      t.integer :customizable_id
      t.string :customizable_type
      t.string :label
      t.string :value

      t.timestamps
    end
  end
  def self.down
    drop_table :pictures
  end
end
