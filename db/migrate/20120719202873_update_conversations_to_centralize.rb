class UpdateConversationsToCentralize < ActiveRecord::Migration
  def change
    
    add_column :conversations, :has_offer, :boolean, :default => false
    add_column :conversations, :has_appointment, :boolean, :default => false
    add_column :conversations, :has_been_rejected, :boolean, :default => false
    add_column :conversations, :availability, :text
    add_column :conversations, :availability_from_seller, :boolean, :default => false    
    add_column :offers, :has_been_accepted, :boolean, :default => false
    add_column :appointments, :appointment_from_seller, :boolean, :default => false    
    remove_column :appointments, :posting_id
    add_column :appointments, :conversation_id, :integer
    
    end
end