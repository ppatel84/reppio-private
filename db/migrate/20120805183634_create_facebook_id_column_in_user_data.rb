class CreateFacebookIdColumnInUserData < ActiveRecord::Migration
  def change
    add_column :userdata, :facebook_id, :integer
  end
end
