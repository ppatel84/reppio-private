class CreateGroupsProductsTable < ActiveRecord::Migration
  def change
    create_table :groups_postings do |t|
      t.integer :group_id
      t.integer :posting_id
    end


    add_index :groups_postings, :group_id
    add_index :groups_users, :group_id
    add_index :groups_users, :user_id
  end
end