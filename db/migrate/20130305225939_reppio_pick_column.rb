class ReppioPickColumn < ActiveRecord::Migration
  def change
    add_column :postings, :reppio_pick, :boolean, :default=>false
  end
end
