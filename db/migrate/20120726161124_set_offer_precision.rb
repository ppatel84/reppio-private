class SetOfferPrecision < ActiveRecord::Migration
  def change
    change_column :offers, :offer, :decimal, :precision => 10, :scale => 2
  end
end
