class AddDeltaIndexingToUpdatedPostings < ActiveRecord::Migration
  def self.up
    add_column :updated_postings, :delta, :boolean, :default => true, :null => false
  end
end
