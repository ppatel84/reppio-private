class AppointmentLocationAndConversationCompletion < ActiveRecord::Migration
  def change
    add_column :appointments, :natural_time, :string
    remove_column :conversations, :availability
    add_column :conversations, :has_been_completed, :boolean, :default => false
    add_column :appointments, :was_cancelled, :boolean
  end
end

