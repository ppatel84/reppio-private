class CreateBookmarks < ActiveRecord::Migration
  def change
    create_table :bookmarks do |t|
      t.integer :user_id #id of bookmark-er
      t.integer :posting_id #id of associated posting
      t.timestamps
    end
  end
end
