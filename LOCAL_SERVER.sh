# RUN THIS CODE ON THE LOCAL_SERVER AFTER A NEW DEPLOY

bundle install --without production
#rake db:drop RAILS_ENV=search
#rake db:create RAILS_ENV=search
#rake db:migrate RAILS_ENV=search
#rake db:populate RAILS_ENV=search
#rake db:delete_s3_buckets RAILS_ENV=search
whenever --update-crontab reppio
cd ~/reppio_data && rm *.json
crontab -l